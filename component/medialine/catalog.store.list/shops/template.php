<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Bitrix\Main\Localization\Loc;

use \Bitrix\Sale\Location\GeoIp;


$this->addExternalJS('//api-maps.yandex.ru/2.1/?lang=ru_RU');

if(strlen($arResult["ERROR_MESSAGE"])>0)
	ShowError($arResult["ERROR_MESSAGE"]);
$arPlacemarks = array();
$gpsN = '';
$gpsS = '';

$json1 = [];

//pr($arResult);
//$ipAddress = \Bitrix\Main\Service\GeoIp\Manager::getRealIp();
//$locCode = GeoIp::getLocationCode($ipAddress, LANGUAGE_ID);
//var_dump($ipAddress);
//var_dump($locCode);
?>
    <div class="mf-table-header">
        <div class="mf-table-h-cell mf-shoplist-cell">
            <ul class="mf-shoplist">
                <li class="mf-shoplist-map active">Карта</li>
                <li class="mf-shoplist-shops">Список</li>
            </ul>
        </div>
        <div class="mf-table-h-cell">
            <h1>Магазины</h1>
        </div>
        <div class="mf-table-h-cell mf-selectbox-cell shops-filters">
            <div class="mf-selectbox-cell__filter-item">
                <select name="store-country" id="store-country" class="custom_sel">
                    <option value="">Страна</option>
		            <? foreach ($arResult['COUNTRIES'] as $v) { ?>
                        <option value="<?= $v ?>"><?= $v ?></option>
		            <? } ?>
                </select>
            </div>

            <div class="mf-selectbox-cell__filter-item">
                <select name="store-city" id="store-city" class="custom_sel store-city">
                    <option value="">Город</option>
		            <? foreach ($arResult['CITIES'] as $v) { ?>
                        <option value="<?= $v ?>" data-country="<?= $arResult['COUNTRIES_CITIES'][$v] ?>"><?= $v ?></option>
		            <? } ?>
                </select>
            </div>
        </div>
    </div>
    <section class="map map-shop">
        <div class="mf-shoplist-container" >
            <div class="fix-block">
                <ul>
	                <?if(is_array($arResult["STORES"]) && !empty($arResult["STORES"])):
		                foreach($arResult["STORES"] as $pid=>$arProperty):?>
                        <?
                            $workTime = [];
                            if (!empty($arProperty['UF_WORKTIME15'])) $workTime[] = Loc::getMessage('S_WORKTIME15') . ': ' .$arProperty['UF_WORKTIME15'];
                            if (!empty($arProperty['UF_WORKTIME6'])) $workTime[] = Loc::getMessage('S_WORKTIME6') . ': ' .$arProperty['UF_WORKTIME6'];
                            if (!empty($arProperty['UF_WORKTIME7'])) $workTime[] = Loc::getMessage('S_WORKTIME7') . ': ' .$arProperty['UF_WORKTIME7'];

			                $key = $arProperty['UF_COUNTRY'] . '-' . $arProperty["UF_CITY"] . '-' . $arProperty["UF_CITY"] . '-' . $arProperty["ID"];
                            $balloonContent = '<div class="map-target-shop-info"><div class="h3 map-shop-title">'.$arProperty["STORE_TITLE"].'</div><p class="map-shop-address">'.$arProperty["ADDRESS"].'</p><p class="map-shop-address">'.$arProperty["PHONE"].'</p>'. (!empty($workTime)?'<p class="map-shop-address">'.implode('<br>', $workTime).'</p>':'') .'</div>';
			                if(!empty($arProperty['GPS_N']) && !empty($arProperty['GPS_S']))
                            {
	                            $json1[] = array(
		                            "type" => "Feature",
		                            "id" => $key,
//		            "id" => $arItem["ID"],
		                            "geometry" => array(
			                            "type" => "Point",
			                            "coordinates" => array(
				                            floatval(trim($arProperty['GPS_N'])),
				                            floatval(trim($arProperty['GPS_S'])),
			                            ),
		                            ),
		                            "properties" => array(
			                            "balloonContent" => $balloonContent,
			                            "clusterCaption" => $arProperty["STORE_TITLE"],
			                            "hintContent" => $arProperty["STORE_TITLE"],
		                            ),
		                            "options" => array(
			                            "iconLayout" => 'default#image',
//			                            "iconImageHref" => SITE_TEMPLATE_PATH .'/images/map-marker.png',
				                        "iconImageHref" => SITE_TEMPLATE_PATH . ($arProperty['UF_SHOP_TYPE']=='5' ? '/images/map-marker-purple.png' : '/images/map-marker-yellow.png'),
//				        "iconImageHref" => SITE_TEMPLATE_PATH .'/images/map-marker-point-red_.svg',

			                            "iconImageSize" => array(28, 37),
			                            "iconImageOffset" => array(-14, -37),
//			                            "iconImageSize" => array(48, 41),
//			                            "iconImageOffset" => array(-24, -41),
//				        "iconImageSize" => array(35, 49),
//				        "iconImageOffset" => array(-17, -45),
		                            )
	                            );
                            }
                            ?>
                            <li data-animate="shoplist" class="js-shop-item" data-country="<?= $arProperty['UF_COUNTRY'] ?>" data-city="<?= $arProperty['UF_CITY'] ?>" data-c1="<?= floatval(trim($arProperty['GPS_N'])) ?>" data-c2="<?= floatval(trim($arProperty['GPS_S'])) ?>">
                                <div class="shoplist-table">
                                    <div class="shoplist-cell sh-list-cell-address">
                                        <svg class="map-shop-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
											<g>
                                                <g>
                                                    <path d="M256,0C150.125,0,64,86.135,64,192c0,141.646,177.146,310,184.688,317.104C250.75,511.031,253.375,512,256,512
														s5.25-0.969,7.313-2.896C270.854,502,448,333.646,448,192C448,86.135,361.875,0,256,0z M256,298.667
														c-58.813,0-106.667-47.854-106.667-106.667S197.188,85.333,256,85.333S362.667,133.188,362.667,192S314.813,298.667,256,298.667z"></path>
                                                </g>
                                            </g>
                                            <circle fill="white" r="125" cy="180" cx="257"></circle>
										</svg>

                                        <p><b><?=$arProperty["STORE_TITLE"]?></b></p>
                                        <p class="sh-list-cell-desc"><?=$arProperty["ADDRESS"]?></p>
                                        <p><?=$arProperty["ID"]?></p>
                                        <a href="script:void(0)" class="js-locate-shop"<?= empty($arProperty['GPS_N'])||empty($arProperty['GPS_S'])?' style="opacity:0;visibility:hidden"':'' ?>><?= Loc::getMessage('S_SHOW_ON_MAP') ?></a>
                                    </div>
                                    <div class="shoplist-cell sh-list-cell-phones">
                                        <p><?= Loc::getMessage('S_WORKTIME15') ?>: <?= !empty($arProperty['UF_WORKTIME15'])?$arProperty['UF_WORKTIME15']:Loc::getMessage('S_WORKTIME_FREE') ?></p>
                                        <p><?= Loc::getMessage('S_WORKTIME6') ?>: <?= !empty($arProperty['UF_WORKTIME6'])?$arProperty['UF_WORKTIME6']:Loc::getMessage('S_WORKTIME_FREE') ?></p>
                                        <p><?= Loc::getMessage('S_WORKTIME7') ?>: <?= !empty($arProperty['UF_WORKTIME7'])?$arProperty['UF_WORKTIME7']:Loc::getMessage('S_WORKTIME_FREE') ?></p><br>
                                        <p><?=$arProperty["PHONE"]?></p>
	                                    <?/*<p><?=$arProperty["SCHEDULE"]?></p>*/?>
                                    </div>
                                    <!--<div class="shoplist-cell sh-list-cell-info">
                                        <p><?= nl2br($arProperty["DESCRIPTION"]); ?></p>
                                    </div>-->
                                    <!--<div class="shoplist-cell sh-list-cell-show-on-map">
                                        <a href="script:void(0)" class="js-locate-shop"<?= empty($arProperty['GPS_N'])||empty($arProperty['GPS_S'])?' style="opacity:0;visibility:hidden"':'' ?>><?= Loc::getMessage('S_SHOW_ON_MAP') ?></a>
                                    </div>-->
                                </div>
                                <div class="js-balloon-content shop-item__balloon-content"><?= $balloonContent ?></div>
                            </li>
                        <?/*
                            <div class="catalog-detail-property" style="display: none;">
                                <p class="catalog-detail-properties-title"><a href="<?=$arProperty["URL"]?>"><?=$arProperty["TITLE"]?></a></p><?
				                if (!empty($arProperty['DETAIL_IMG']))
				                {
					                ?><img class="catalog-detail-image" src="<?=$arProperty['DETAIL_IMG']['SRC']; ?>"><?
				                }
				                if ($arProperty["DESCRIPTION"] != '')
				                {
					                ?><p><?=$arProperty["DESCRIPTION"]; ?></p><?
				                }
				                if(isset($arProperty["PHONE"])):?>
                                    <span>&nbsp;&nbsp;<?=GetMessage('S_PHONE')?></span>
                                    <span><?=$arProperty["PHONE"]?></span>
				                <?endif;
				                if(isset($arProperty["SCHEDULE"])):?>
                                    <span>&nbsp;&nbsp;<?=GetMessage('S_SCHEDULE')?></span>
                                    <span><?=$arProperty["SCHEDULE"]?></span>
				                <?endif;
				                if($arProperty["GPS_S"]!=0 && $arProperty["GPS_N"]!=0)
				                {
					                $gpsN=substr(doubleval($arProperty["GPS_N"]),0,15);
					                $gpsS=substr(doubleval($arProperty["GPS_S"]),0,15);
					                $arPlacemarks[]=array("LON"=>$gpsS,"LAT"=>$gpsN,"TEXT"=>$arProperty["TITLE"]);
				                }
				                ?>
                            </div>
                            <br>
                        */?>
		                <?endforeach;
	                endif;?>
                </ul>
            </div>
        </div>
        <?/*
        <div class="map-target-shop-info">
            <h3 class="map-shop-title">Магазин 5</h3>
            <p class="map-shop-address">
                Минск, ул. Богдановича 54, к. 2
            </p>
        </div>
        <svg class="map-shop-target" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
						<g>
                            <g>
                                <path d="M256,0C150.125,0,64,86.135,64,192c0,141.646,177.146,310,184.688,317.104C250.75,511.031,253.375,512,256,512
									s5.25-0.969,7.313-2.896C270.854,502,448,333.646,448,192C448,86.135,361.875,0,256,0z M256,298.667
									c-58.813,0-106.667-47.854-106.667-106.667S197.188,85.333,256,85.333S362.667,133.188,362.667,192S314.813,298.667,256,298.667z"
                                />
                            </g>
                        </g>
            <circle fill="white" r="125" cy="180" cx="257"></circle>
        </svg>
        */?>

        <div class="stores-map" id="stores-map"></div>
	    <?
        /*
	    if ($arResult['VIEW_MAP'])
	    {
		    if($arResult["MAP"]==0)
		    {
			    $APPLICATION->IncludeComponent("bitrix:map.yandex.view", ".default", array(
				    "INIT_MAP_TYPE" => "MAP",
				    "MAP_DATA" => serialize(array("yandex_lat"=>$gpsN,"yandex_lon"=>$gpsS,"yandex_scale"=>10,"PLACEMARKS" => $arPlacemarks)),
				    "MAP_WIDTH" => "720",
				    "MAP_HEIGHT" => "500",
				    "CONTROLS" => array(
					    0 => "ZOOM",
				    ),
				    "OPTIONS" => array(
					    0 => "ENABLE_SCROLL_ZOOM",
					    1 => "ENABLE_DBLCLICK_ZOOM",
					    2 => "ENABLE_DRAGGING",
				    ),
				    "MAP_ID" => ""
			    ),
				    $component,
				    array("HIDE_ICONS" => "Y")
			    );
		    }
		    else
		    {
			    $APPLICATION->IncludeComponent("bitrix:map.google.view", ".default", array(
				    "INIT_MAP_TYPE" => "MAP",
				    "MAP_DATA" => serialize(array("google_lat"=>$gpsN,"google_lon"=>$gpsS,"google_scale"=>10,"PLACEMARKS" => $arPlacemarks)),
				    "MAP_WIDTH" => "720",
				    "MAP_HEIGHT" => "500",
				    "CONTROLS" => array(
					    0 => "ZOOM",
				    ),
				    "OPTIONS" => array(
					    0 => "ENABLE_SCROLL_ZOOM",
					    1 => "ENABLE_DBLCLICK_ZOOM",
					    2 => "ENABLE_DRAGGING",
				    ),
				    "MAP_ID" => ""
			    ),
				    $component,
				    array("HIDE_ICONS" => "Y")
			    );
		    }
	    }
	    /**/?>
    </section>

<?
$json = array(
	"type" => "FeatureCollection",
	"features" => $json1,
);

//pr($json1);
$jsonPoints = json_encode($json);
$js = '<script type="text/javascript">var jsonPoints = '.$jsonPoints.'</script>';
echo $js;
?>
