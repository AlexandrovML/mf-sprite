var myMap = null,
    objectManager = null,
    c1,c2;

(function($) {
    $(function() {
        var $shoplist = $('.mf-shoplist');
        var $shoplistMap = $shoplist.find('.mf-shoplist-map');

        $('select').styler();

        $('.js-locate-shop').on('click', function () {
            var $item = $(this).closest('.js-shop-item');

            c1 = parseFloat($item.data('c1'));
            c2 = parseFloat($item.data('c2'));

            myMap.setCenter([c1, c2], 12);
            myMap.balloon.open([c1, c2], $item.find('.js-balloon-content').html(), {});

            setTimeout(function () {
                myMap.panTo([c1, c2]);
                setTimeout(function () {
                    $('html, body').animate({ scrollTop: ($('#stores-map').offset().top - 150) }, 500);
                }, 200);
                $shoplist.find('li').removeClass('active');
                $shoplistMap.addClass('active');
                $('.mf-shoplist-container').removeClass('active');
                $('.mf-shoplist-container, .mf-shoplist-container li').removeAttr('style');


            }, 100)
        });

        $('#store-country').on('change', function () {
            var country = $(this).val(),
                pointsCount = jsonPoints.features.length;

            $('#store-city').val('');
            $('#store-city').find('option').attr('disabled', true);
            $('#store-city').find('option').each(function () {
                if($(this).data('country') == country) {
                    $(this).attr('disabled', false);
                }
            });

            $('#store-city').trigger('refresh');


            $('.js-shop-item').removeClass('shop-item-hidden');

            // фильтруем метки на карте
            myMap.geoObjects.removeAll();
            objectManager.removeAll();
            objectManager.add(jsonPoints);
            if(country !== '') {
                objectManager.objects.each(function (object) {
                    if(object.id.indexOf(country + '-') === -1) {
                        objectManager.objects.remove([object.id]);
                        pointsCount--;
                    }
                });
                $('.js-shop-item').addClass('shop-item-hidden');
                $('.js-shop-item[data-country="'+country+'"]').removeClass('shop-item-hidden')
            }

            myMap.geoObjects.add(objectManager);

            if(pointsCount > 1) {
                myMap.setBounds( objectManager.getBounds(),{zoomMargin: 13} );
            } else {
                objectManager.objects.each(function (object) {
                    myMap.setZoom(10);
                    myMap.panTo(object.geometry.coordinates);
                    // console.log();
                });
            }
        });

        $('#store-city').on('change', function () {
            var city = $(this).val(),
                pointsCount = jsonPoints.features.length;

            $('.js-shop-item').removeClass('shop-item-hidden');

            // фильтруем метки на карте
            myMap.geoObjects.removeAll();
            objectManager.removeAll();
            objectManager.add(jsonPoints);
            if(city !== '') {
                // console.log(city);
                objectManager.objects.each(function (object) {
                    // console.log(object.id);
                    if(object.id.indexOf(city + '-') === -1) {
                        objectManager.objects.remove([object.id]);
                        pointsCount--;
                    }
                });

                $('.js-shop-item').addClass('shop-item-hidden');
                $('.js-shop-item[data-city="'+city+'"]').removeClass('shop-item-hidden');
            }

            myMap.geoObjects.add(objectManager);

            if(pointsCount > 1) {
                myMap.setBounds( objectManager.getBounds(),{zoomMargin: 13} );
            } else {
                objectManager.objects.each(function (object) {
                    myMap.setZoom(10);
                    myMap.panTo(object.geometry.coordinates);
                });
            }
        });
    });
})(jQuery);

ymaps.ready(init);

function init () {
    var windowWidth = $(window).width();
    myMap = new ymaps.Map('stores-map', {
        center: [53.9071627244662,27.558078451171834],
        zoom: 6
    }, {
        searchControlProvider: 'yandex#search'
    }),
        objectManager = new ymaps.ObjectManager({
            clusterize: true,
            gridSize: 32
        });

    if(windowWidth < 800) {
        myMap.behaviors.disable(['drag']);
    } else {
        // myMap.behaviors.disable(['scrollZoom']);
    }


    //objectManager.clusters.options.set('preset', 'islands#blueClusterIcons');
    objectManager.clusters.options.set('clusterIconColor', '#ad1380');
    objectManager.add(jsonPoints);
    myMap.geoObjects.add(objectManager);
    myMap.setBounds( objectManager.getBounds(),{zoomMargin: 13} );
}