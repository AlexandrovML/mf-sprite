<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $previousLevel = 0; // переменная содержит значение DEPTH_LEVEL предыдущего пункта?>


<nav class="top-menu">
    <div class="fix-block">
        <a href="#" class="logo">
            <img src="<?= SITE_TEMPLATE_PATH ?>/images/main/logo.png"/>
        </a>
        <div class="menu-button-container">
						<span class="mb-icon"><svg class="menu-button" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                   xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                   viewBox="0 0 53 53" style="enable-background:new 0 0 53 53;"
                                                   xml:space="preserve">
							<path d="M2,14.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,14.5,2,14.5z"/><path
                                        d="M2,28.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,28.5,2,28.5z"/><path
                                        d="M2,44.5h49c1.104,0,2-0.896,2-2s-0.896-2-2-2H2c-1.104,0-2,0.896-2,2S0.896,44.5,2,44.5z"/>
						</svg></span>
            <span class="mb-icon-search"><svg class="search-icon" xmlns="http://www.w3.org/2000/svg"
                                              viewBox="-1 0 19 18" style="
							"><title>Oval 1</title><desc>Created with Sketch.</desc><path fill="rgb(94, 93, 93)"
                                                                                          d="M12.453 13.18c-1.318 1.056-3.005 1.69-4.844 1.69-4.213 0-7.627-3.328-7.627-7.435S3.398 0 7.61 0c4.21 0 7.624 3.33 7.624 7.436 0 1.643-.547 3.16-1.473 4.392l5.036 4.708c.25.236.25.62 0 .857l-.462.432c-.252.235-.664.235-.916 0l-4.964-4.646zm-4.844-.36c3.047 0 5.52-2.41 5.52-5.385S10.66 2.05 7.61 2.05 2.086 4.463 2.086 7.437 4.56 12.82 7.61 12.82z"></path></svg></span>
        </div>
        <form class="search">
            <div class="fix-block">
                <svg class="search-icon" xmlns="http://www.w3.org/2000/svg" viewBox="-1 0 19 18"><title>Oval
                        1</title>
                    <desc>Created with Sketch.</desc>
                    <path fill="rgb(94, 93, 93)"
                          d="M12.453 13.18c-1.318 1.056-3.005 1.69-4.844 1.69-4.213 0-7.627-3.328-7.627-7.435S3.398 0 7.61 0c4.21 0 7.624 3.33 7.624 7.436 0 1.643-.547 3.16-1.473 4.392l5.036 4.708c.25.236.25.62 0 .857l-.462.432c-.252.235-.664.235-.916 0l-4.964-4.646zm-4.844-.36c3.047 0 5.52-2.41 5.52-5.385S10.66 2.05 7.61 2.05 2.086 4.463 2.086 7.437 4.56 12.82 7.61 12.82z"/>
                </svg>
                <input type="search" name="search" placeholder="Поиск"/>
            </div>
        </form>
        <ul class="client-icon-list">
            <li class="icon-auth"><a href="#">
                    <svg fill="rgb(94, 93, 93)" enable-background="new 0 0 24 24" version="1.0" viewBox="0 0 24 24"
                         xml:space="preserve" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink"><circle cx="12" cy="8" r="4"/>
                        <path d="M12,14c-6.1,0-8,4-8,4v2h16v-2C20,18,18.1,14,12,14z"/></svg>
                </a></li>
            <li><a href="#">
                    <svg enable-background="new 0 0 128 128" version="1.1" viewBox="0 0 128 128" width="128px"
                         xml:space="preserve" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink"><path
                                d="M127,44.205c0-18.395-14.913-33.308-33.307-33.308c-12.979,0-24.199,7.441-29.692,18.276  c-5.497-10.835-16.714-18.274-29.694-18.274C15.912,10.898,1,25.81,1,44.205C1,79,56.879,117.104,64.001,117.104  C71.124,117.104,127,79.167,127,44.205z"
                                fill="rgb(94, 93, 93)"/></svg>
                </a></li>
            <li><a href="#">
                    <svg viewBox="0 0 18 18" version="1.1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink"
                         xmlns:sketch="http://www.bohemiancoding.com/sketch/ns">
                        <defs></defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"
                           sketch:type="MSPage">
                            <g id="Bag" sketch:type="MSLayerGroup" fill="rgb(94, 93, 93)">
                                <path d="M13.2,5.4 L13.2,3.6 C13.2,2.345 11.971,0 9.013,0 C6.107,0 4.8,2.105 4.8,3.6 L4.8,5.4 L0,5.4 L0,18 L18,18 L18,5.4 L13.2,5.4 L13.2,5.4 Z M6,3.6 C6,2.977 6.554,1.2 9.014,1.2 C11.48,1.2 12,3.205 12,3.6 L12,5.4 L6,5.4 L6,3.6 L6,3.6 Z"
                                      id="Shape" sketch:type="MSShapeGroup"></path>
                            </g>
                        </g>
                    </svg>
                </a></li>
        </ul>
        <ul class="mf-top-menu-list">
            <li class="mf-top-menu-el" data-id="novelty">
                <a href="#">Новинки</a>
            </li>
            <li class="mf-top-menu-el" data-id="woman">
                <a href="/catalog/">Женщинам</a>
            </li>
            <li class="mf-top-menu-el" data-id="man">
                <a href="#">Мужчинам</a>
            </li>

            <li class="mf-top-menu-el" data-id="children">
                <a href="#">Детям</a>
            </li>
            <li class="mf-top-menu-el" data-id="sale">
                <a href="#">Распродажа</a>
            </li>
            <li class="mf-top-menu-el">
                <a href="#">Лукбук</a>
            </li>
        </ul>
    </div>
    <div class="mf-social-l">
        <ul class="social-icon-list">
            <li><a href="#">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px" viewBox="-1 -1 50.652 50.652"
                         style="enable-background:new 0 0 49.652 49.652;" xml:space="preserve">
								<g>
                                    <g>
                                        <path d="M24.826,0C11.137,0,0,11.137,0,24.826c0,13.688,11.137,24.826,24.826,24.826c13.688,0,24.826-11.138,24.826-24.826C49.652,11.137,38.516,0,24.826,0z M31,25.7h-4.039c0,6.453,0,14.396,0,14.396h-5.985c0,0,0-7.866,0-14.396h-2.845v-5.088h2.845v-3.291c0-2.357,1.12-6.04,6.04-6.04l4.435,0.017v4.939c0,0-2.695,0-3.219,0c-0.524,0-1.269,0.262-1.269,1.386v2.99h4.56L31,25.7z"/>
                                    </g>
                                </g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
							</svg>
                </a></li>
            <li><a href="#">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px"
                         viewBox="-1 -1 50.652 50.652" style="enable-background:new 0 0 49.652 49.652;"
                         xml:space="preserve">
								<g>
                                    <g>
                                        <g>
                                            <path d="M24.825,29.796c2.739,0,4.972-2.229,4.972-4.97c0-1.082-0.354-2.081-0.94-2.897c-0.903-1.252-2.371-2.073-4.029-2.073
												c-1.659,0-3.126,0.82-4.031,2.072c-0.588,0.816-0.939,1.815-0.94,2.897C19.854,27.566,22.085,29.796,24.825,29.796z"/>
                                            <polygon
                                                    points="35.678,18.746 35.678,14.58 35.678,13.96 35.055,13.962 30.891,13.975 30.907,18.762 			"/>
                                            <path d="M24.826,0C11.137,0,0,11.137,0,24.826c0,13.688,11.137,24.826,24.826,24.826c13.688,0,24.826-11.138,24.826-24.826
												C49.652,11.137,38.516,0,24.826,0z M38.945,21.929v11.56c0,3.011-2.448,5.458-5.457,5.458H16.164
												c-3.01,0-5.457-2.447-5.457-5.458v-11.56v-5.764c0-3.01,2.447-5.457,5.457-5.457h17.323c3.01,0,5.458,2.447,5.458,5.457V21.929z"
                                            />
                                            <path d="M32.549,24.826c0,4.257-3.464,7.723-7.723,7.723c-4.259,0-7.722-3.466-7.722-7.723c0-1.024,0.204-2.003,0.568-2.897
												h-4.215v11.56c0,1.494,1.213,2.704,2.706,2.704h17.323c1.491,0,2.706-1.21,2.706-2.704v-11.56h-4.217
												C32.342,22.823,32.549,23.802,32.549,24.826z"/>
                                        </g>
                                    </g>
                                </g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g>
                        </g>
							</svg>
                </a></li>
            <li><a href="#">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px"
                         viewBox="0 0 52 52" style="enable-background:new 0 0 52 52;" xml:space="preserve">
							<style type="text/css">
                                .st0 {
                                    fill: none;
                                }
                            </style>
                        <g>
                            <rect x="-0.2" y="0.1" class="st0"/>
                            <path d="M49.7,16.5c1.3,3.1,2,6.3,2,9.7s-0.7,6.6-2,9.7c-1.3,3.1-3.1,5.7-5.3,8c-2.2,2.2-4.9,4-8,5.3c-3.1,1.3-6.3,2-9.7,2
									c-3.4,0-6.6-0.7-9.7-2s-5.7-3.1-8-5.3c-2.2-2.2-4-4.9-5.3-8c-1.3-3.1-2-6.3-2-9.7s0.7-6.6,2-9.7c1.3-3.1,3.1-5.7,5.3-8
									c2.2-2.2,4.9-4,8-5.3s6.3-2,9.7-2c3.4,0,6.6,0.7,9.7,2c3.1,1.3,5.7,3.1,8,5.3C46.6,10.8,48.3,13.5,49.7,16.5z M34.8,37.7l4.1-19.3
									c0.2-0.8,0.1-1.4-0.3-1.8c-0.4-0.4-0.8-0.4-1.4-0.2l-24.1,9.3c-0.5,0.2-0.9,0.4-1.1,0.7c-0.2,0.3-0.2,0.5-0.1,0.7
									c0.1,0.2,0.4,0.4,0.9,0.5l6.2,1.9l14.3-9c0.4-0.3,0.7-0.3,0.9-0.2c0.1,0.1,0.1,0.2-0.1,0.4L22.5,31.3L22,37.7
									c0.4,0,0.8-0.2,1.3-0.6l3-2.9l6.2,4.6C33.8,39.5,34.5,39.1,34.8,37.7z"/>
                        </g>
							</svg>
                </a></li>
            <li><a href="#">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px"
                         viewBox="-1 -1 98.75 98.75" style="enable-background:new 0 0 97.75 97.75;"
                         xml:space="preserve"
                    >
							<g>
                                <g>
                                    <path d="M39.969,59.587c7.334-3.803,14.604-7.571,21.941-11.376c-7.359-3.84-14.627-7.63-21.941-11.447
										C39.969,44.398,39.969,51.954,39.969,59.587z"/>
                                    <path d="M48.875,0C21.883,0,0,21.882,0,48.875S21.883,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.867,0,48.875,0z
										 M82.176,65.189c-0.846,3.67-3.848,6.377-7.461,6.78c-8.557,0.957-17.217,0.962-25.842,0.957c-8.625,0.005-17.287,0-25.846-0.957
										c-3.613-0.403-6.613-3.11-7.457-6.78c-1.203-5.228-1.203-10.933-1.203-16.314s0.014-11.088,1.217-16.314
										c0.844-3.67,3.844-6.378,7.457-6.782c8.559-0.956,17.221-0.961,25.846-0.956c8.623-0.005,17.285,0,25.841,0.956
										c3.615,0.404,6.617,3.111,7.461,6.782c1.203,5.227,1.193,10.933,1.193,16.314S83.379,59.962,82.176,65.189z"/>
                                </g>
                            </g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
							</svg>
                </a></li>
            <li><a href="#">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px" viewBox="-10 -10 496.392 496.392"
                         style="enable-background:new 0 0 486.392 486.392;" xml:space="preserve">
								<g>
                                    <g>
                                        <path d="M243.196,0C108.891,0,0,108.891,0,243.196s108.891,243.196,243.196,243.196
										s243.196-108.891,243.196-243.196C486.392,108.861,377.501,0,243.196,0z M364.186,188.598l0.182,7.752
										c0,79.16-60.221,170.359-170.359,170.359c-33.804,0-65.268-9.91-91.776-26.904c4.682,0.547,9.454,0.851,14.288,0.851
										c28.059,0,53.868-9.576,74.357-25.627c-26.204-0.486-48.305-17.814-55.935-41.586c3.678,0.699,7.387,1.034,11.278,1.034
										c5.472,0,10.761-0.699,15.777-2.067c-27.39-5.533-48.031-29.7-48.031-58.701v-0.76c8.086,4.499,17.297,7.174,27.116,7.509
										c-16.051-10.731-26.63-29.062-26.63-49.825c0-10.974,2.949-21.249,8.086-30.095c29.518,36.236,73.658,60.069,123.422,62.562
										c-1.034-4.378-1.55-8.968-1.55-13.649c0-33.044,26.812-59.857,59.887-59.857c17.206,0,32.771,7.265,43.714,18.908
										c13.619-2.706,26.448-7.691,38.03-14.531c-4.469,13.984-13.953,25.718-26.326,33.135c12.069-1.429,23.651-4.682,34.382-9.424
										C386.073,169.659,375.889,180.208,364.186,188.598z"/>
                                    </g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                    <g></g>
                                </g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
							</svg>
                </a></li>
            <li><a href="#">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px"
                         viewBox="-2 -2 100 100" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve"
                    >
								<g>
                                    <path d="M48.875,0C21.883,0,0,21.882,0,48.875S21.883,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.867,0,48.875,0z
									 M73.667,54.161c2.278,2.225,4.688,4.319,6.733,6.774c0.906,1.086,1.76,2.209,2.41,3.472c0.928,1.801,0.09,3.776-1.522,3.883
									l-10.013-0.002c-2.586,0.214-4.644-0.829-6.379-2.597c-1.385-1.409-2.67-2.914-4.004-4.371c-0.545-0.598-1.119-1.161-1.803-1.604
									c-1.365-0.888-2.551-0.616-3.333,0.81c-0.797,1.451-0.979,3.059-1.055,4.674c-0.109,2.361-0.821,2.978-3.19,3.089
									c-5.062,0.237-9.865-0.531-14.329-3.083c-3.938-2.251-6.986-5.428-9.642-9.025c-5.172-7.012-9.133-14.708-12.692-22.625
									c-0.801-1.783-0.215-2.737,1.752-2.774c3.268-0.063,6.536-0.055,9.804-0.003c1.33,0.021,2.21,0.782,2.721,2.037
									c1.766,4.345,3.931,8.479,6.644,12.313c0.723,1.021,1.461,2.039,2.512,2.76c1.16,0.796,2.044,0.533,2.591-0.762
									c0.35-0.823,0.501-1.703,0.577-2.585c0.26-3.021,0.291-6.041-0.159-9.05c-0.28-1.883-1.339-3.099-3.216-3.455
									c-0.956-0.181-0.816-0.535-0.351-1.081c0.807-0.944,1.563-1.528,3.074-1.528l11.313-0.002c1.783,0.35,2.183,1.15,2.425,2.946
									l0.01,12.572c-0.021,0.695,0.349,2.755,1.597,3.21c1,0.33,1.66-0.472,2.258-1.105c2.713-2.879,4.646-6.277,6.377-9.794
									c0.764-1.551,1.423-3.156,2.063-4.764c0.476-1.189,1.216-1.774,2.558-1.754l10.894,0.013c0.321,0,0.647,0.003,0.965,0.058
									c1.836,0.314,2.339,1.104,1.771,2.895c-0.894,2.814-2.631,5.158-4.329,7.508c-1.82,2.516-3.761,4.944-5.563,7.471
									C71.48,50.992,71.611,52.155,73.667,54.161z"/>
                                </g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g>
                        </g>
							</svg>
                </a></li>
            <li><a href="#">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                         x="0px" y="0px"
                         viewBox="-2 -2 100 100" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve"
                    >
								<g>
                                    <g>
                                        <path d="M48.921,40.507c4.667-0.017,8.384-3.766,8.367-8.443c-0.017-4.679-3.742-8.402-8.411-8.406
										c-4.708-0.005-8.468,3.787-8.432,8.508C40.48,36.826,44.239,40.524,48.921,40.507z"/>
                                        <path d="M48.875,0C21.882,0,0,21.883,0,48.875S21.882,97.75,48.875,97.75S97.75,75.867,97.75,48.875S75.868,0,48.875,0z
										 M48.945,14.863c9.52,0.026,17.161,7.813,17.112,17.438c-0.048,9.403-7.814,17.024-17.318,16.992
										c-9.407-0.032-17.122-7.831-17.066-17.253C31.726,22.515,39.445,14.837,48.945,14.863z M68.227,56.057
										c-2.105,2.161-4.639,3.725-7.453,4.816c-2.66,1.031-5.575,1.55-8.461,1.896c0.437,0.474,0.642,0.707,0.914,0.979
										c3.916,3.937,7.851,7.854,main-section11.754,11.802c1.33,1.346,1.607,3.014,0.875,4.577c-0.799,1.71-2.592,2.834-4.351,2.713
										c-1.114-0.077-1.983-0.63-2.754-1.407c-2.956-2.974-5.968-5.895-8.862-8.925c-0.845-0.882-1.249-0.714-1.994,0.052
										c-2.973,3.062-5.995,6.075-9.034,9.072c-1.365,1.346-2.989,1.59-4.573,0.82c-1.683-0.814-2.753-2.533-2.671-4.262
										c0.058-1.166,0.632-2.06,1.434-2.858c3.877-3.869,7.742-7.75,11.608-11.628c0.257-0.257,0.495-0.53,0.868-0.93
										c-5.273-0.551-10.028-1.849-14.099-5.032c-0.506-0.396-1.027-0.778-1.487-1.222c-1.783-1.711-1.962-3.672-0.553-5.69
										c1.207-1.728,3.231-2.19,5.336-1.197c0.408,0.191,0.796,0.433,1.168,0.689c7.586,5.213,18.008,5.356,25.624,0.233
										c0.754-0.576,1.561-1.05,2.496-1.289c1.816-0.468,3.512,0.201,4.486,1.791C69.613,52.874,69.6,54.646,68.227,56.057z"/>
                                    </g>
                                </g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g></g>
                        <g>
                        </g>
							</svg>
                </a></li>
        </ul>
    </div>
    <div class="mf-top-sublists-container" data-id="novelty">
        <div class="fix-block">
            <ul class="mf-top-sublist">
                <ul class="mf-top-sublist-submenu">
                    <h4><a href="#">Новинки женщинам</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Новинки одежда</a>
                        </li>
                        <li>
                            <a href="#">Новинки обувь</a>
                        </li>
                        <li>
                            <a href="#">Новинки аксессуары</a>
                        </li>
                    </div>
                </ul>
                <ul class="mf-top-sublist-submenu">
                    <h4><a href="#">Новинки мужчинам</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Новинки одежда</a>
                        </li>
                        <li>
                            <a href="#">Новинки аксессуары</a>
                        </li>
                    </div>
                </ul>
            </ul>
            <div class="mf-sl-background-container">
                <a class="mf-sl-background" href="#"
                   style="background-image:url(<?= SITE_TEMPLATE_PATH ?>/images/lookbook/15.jpg);"></a>
            </div>
        </div>
    </div>

    <? if (!empty($arResult)) {
        $main_cat_temp = array();
        foreach ($arResult as $arItem) { // пробегаем по пунктам, $arItem - массив с информацией о текущем пункте
            //pr($arItem);
            if ($arItem["PARAMS"]['ID'] == 16 && $arItem["PARAMS"]["DEPTH_LEVEL"] == 1) {
                if (in_array($arItem["PARAMS"]['ID'], $main_cat_temp) === false ) {
                    $main_cat_temp[] = $arItem["PARAMS"]['ID'];?>
                    <div class="mf-top-sublists-container" data-id="woman">
                        <div class="fix-block">
                            <ul class="mf-top-sublist">
                                <ul class="mf-top-sublist-submenu">
                                    <h4><a href="#">Спортивная одежда</a></h4>
                                    <div class="mf-top-sl-column">
                                        <li>
                                            <a href="#">Костюмы, комплекты</a>
                                        </li>
                                        <li>
                                            <a href="#">Бриджи, шорты</a>
                                        </li>
                                        <li>
                                            <a href="#">Брюки, легинсы</a>
                                        </li>
                                        <li>
                                            <a href="#">Аксессуары</a>
                                        </li>
                                        <li>
                                            <a href="#">Джемперы, футболки</a>
                                        </li>
                                    </div>
                                    <h4><a href="#">Нижнее белье</a></h4>
                                    <div class="mf-top-sl-column">
                                        <li>
                                            <a href="#">Бюстгальтеры</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Макси</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Мягкий+бюстье</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">T-shirt</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Push-up</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Корректирующее белье</a>
                                        </li>
                                        <li>
                                            <a href="#">Майки, боди, комплекты</a>
                                        </li>
                                        <li>
                                            <a href="#">Трусы</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Классика</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Макси</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Слип-бикини</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Стринги</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Шорты</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Корректирующее белье</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Бесшовные трусы</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Бразилиана</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Комплекты</a>
                                        </li>
                                    </div>
                                </ul>
                                <ul class="mf-top-sublist-submenu">
                                    <h4><a href="#">Домашняя одежда</a></h4>
                                    <div class="mf-top-sl-column">
                                        <li>
                                            <a href="#">Ночные сорочки</a>
                                        </li>
                                        <li>
                                            <a href="#">Халаты</a>
                                        </li>
                                        <li>
                                            <a href="#">Джемперы</a>
                                        </li>
                                        <li>
                                            <a href="#">Брюки, бриджи</a>
                                        </li>
                                        <li>
                                            <a href="#">Пижамы</a>
                                        </li>
                                        <li>
                                            <a href="#">Комбинезоны</a>
                                        </li>
                                        <li>
                                            <a href="#">Аксессуары, тапочки</a>
                                        </li>
                                    </div>
                                    <h4><a href="#">Верхняя одежда</a></h4>
                                    <div class="mf-top-sl-column">
                                        <li>
                                            <a href="#">Платья, туники, юбки</a>
                                        </li>
                                        <li>
                                            <a href="#">Футболки, топы</a>
                                        </li>
                                        <li>
                                            <a href="#">Кофточки, лонгсливы</a>
                                        </li>
                                        <li>
                                            <a href="#">Головные уборы</a>
                                        </li>
                                        <li>
                                            <a href="#">Брюки</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Шорты, бриджи</a>
                                        </li>
                                        <li class="sub-menu-el">
                                            <a href="#">Брюки, легинсы</a>
                                        </li>
                                        <li>
                                            <a href="#">Комбинезоны</a>
                                        </li>
                                    </div>
                                </ul>
                                <ul class="mf-top-sublist-submenu">
                                    <h4><a href="#">Термобелье</a></h4>
                                    <h4><a href="#">Пляжная одежда</a></h4>
                                    <div class="mf-top-sl-column">
                                        <li>
                                            <a href="#">Плавки</a>
                                        </li>
                                        <li>
                                            <a href="#">Лифы</a>
                                        </li>
                                        <li>
                                            <a href="#">Раздельные купальники</a>
                                        </li>
                                        <li>
                                            <a href="#">Слитные купальники</a>
                                        </li>
                                    </div>
                                    <h4><a href="#">Обувь</a></h4>
                                    <h4><a href="#">Носки и колготки</a></h4>
                                    <div class="mf-top-sl-column">
                                        <li>
                                            <a href="#">Классические</a>
                                        </li>
                                        <li>
                                            <a href="#">Укороченные</a>
                                        </li>
                                        <li>
                                            <a href="#">Подследники</a>
                                        </li>
                                        <li>
                                            <a href="#">Зимние</a>
                                        </li>
                                        <li>
                                            <a href="#">Колготки</a>
                                        </li>
                                    </div>
                                </ul>
                            </ul>
                            <div class="mf-sl-background-container">
                                <a class="mf-sl-background" href="#"
                                   style="background-image:url(<?= SITE_TEMPLATE_PATH ?>/images/lookbook/12.jpg);"></a>
                            </div>
                        </div>
                    </div>
                    <?//pr($arItem);
                    //echo ' -111-' . $arItem["TEXT"];
                }
            }
        }
    } ?>

    <div class="mf-top-sublists-container" data-id="man">
        <div class="fix-block">
            <ul class="mf-top-sublist">
                <ul class="mf-top-sublist-submenu">
                    <h4><a href="#">Спортивная одежда</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Костюмы, комплекты</a>
                        </li>
                        <li>
                            <a href="#">Брюки, шорты</a>
                        </li>
                        <li>
                            <a href="#">Джемперы, футболки</a>
                        </li>
                    </div>
                    <h4><a href="#">Нижнее белье</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Трусы</a>
                        </li>
                        <li class="sub-menu-el">
                            <a href="#">Плавки</a>
                        </li>
                        <li class="sub-menu-el">
                            <a href="#">Боксеры</a>
                        </li>
                        <li class="sub-menu-el">
                            <a href="#">Шорты</a>
                        </li>
                        <li>
                            <a href="#">Майки</a>
                        </li>
                    </div>
                    <h4><a href="#">Домашняя одежда</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Пижамы</a>
                        </li>
                        <li>
                            <a href="#">Халаты</a>
                        </li>
                        <li>
                            <a href="#">Брюки домашние</a>
                        </li>
                        <li>
                            <a href="#">Шорты домашние</a>
                        </li>
                    </div>
                </ul>
                <ul class="mf-top-sublist-submenu">
                    <h4><a href="#">Верхняя одежда</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Толстовки, лонгсливы</a>
                        </li>
                        <li>
                            <a href="#">Жакеты, куртки</a>
                        </li>
                        <li>
                            <a href="#">Поло</a>
                        </li>
                        <li>
                            <a href="#">Брюки</a>
                        </li>
                        <li>
                            <a href="#">Шорты</a>
                        </li>
                        <li>
                            <a href="#">Майки</a>
                        </li>
                        <li>
                            <a href="#">Футболки</a>
                        </li>
                    </div>
                    <h4><a href="#">Термобелье</a></h4>
                    <h4><a href="#">Пляжная одежда</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Плавки, трусы</a>
                        </li>
                        <li>
                            <a href="#">Шорты плавания</a>
                        </li>
                    </div>
                </ul>
                <ul class="mf-top-sublist-submenu">
                    <h4><a href="#">Носки</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Классические</a>
                        </li>
                        <li>
                            <a href="#">Укороченные</a>
                        </li>
                        <li>
                            <a href="#">Подследники</a>
                        </li>
                        <li>
                            <a href="#">Зимние</a>
                        </li>
                        <li>
                            <a href="#">Колготки</a>
                        </li>
                    </div>
                </ul>
            </ul>
            <div class="mf-sl-background-container"><a class="mf-sl-background" href="#"
                                                       style="background-image:url(<?= SITE_TEMPLATE_PATH ?>/images/lookbook/13.jpg);"></a>
            </div>
        </div>
    </div>
    <div class="mf-top-sublists-container" data-id="children">
        <div class="fix-block">
            <ul class="mf-top-sublist">
                <ul class="mf-top-sublist-submenu">
                    <h3><a href="#">Девочкам</a></h3>
                    <h4><a href="#">Нижнее белье</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Трусы</a>
                        </li>
                        <li>
                            <a href="#">Майки, бюстье, комплекты</a>
                        </li>
                    </div>
                    <h4><a href="#">Домашняя одежда</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Пижамы</a>
                        </li>
                        <li>
                            <a href="#">Сорочки ночные</a>
                        </li>
                        <li>
                            <a href="#">Халаты</a>
                        </li>
                    </div>
                    <h4><a href="#">Верхняя одежда</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Футболки, топы</a>
                        </li>
                        <li>
                            <a href="#">Кофточки, лонгсливы</a>
                        </li>
                        <li>
                            <a href="#">Костюмы, комплекты</a>
                        </li>
                        <li>
                            <a href="#">Шорты, бриджи</a>
                        </li>
                        <li>
                            <a href="#">Брюки, легинсы</a>
                        </li>
                        <li>
                            <a href="#">Жакеты, куртки</a>
                        </li>
                        <li>
                            <a href="#">Головные уборы</a>
                        </li>
                        <li>
                            <a href="#">Платья, туники, юбки</a>
                        </li>
                        <li>
                            <a href="#">Комбидрес</a>
                        </li>
                    </div>
                </ul>
                <ul class="mf-top-sublist-submenu without-title">
                    <h4><a href="#">Носки, колготки</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Классические</a>
                        </li>
                        <li>
                            <a href="#">Укороченные</a>
                        </li>
                        <li>
                            <a href="#">Колготки</a>
                        </li>
                    </div>
                    <h4><a href="#">Пляжная одежда</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Раздельные купальники</a>
                        </li>
                        <li>
                            <a href="#">Слитные купальники</a>
                        </li>
                    </div>
                    <h4><a href="#">Термобелье</a></h4>
                    <h4><a href="#">Школа</a></h4>
                    <h4><a href="#">По возрасту</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">от 3 до 6 лет</a>
                        </li>
                        <li>
                            <a href="#">от 6 до 12 лет</a>
                        </li>
                        <li>
                            <a href="#">от 12 до 18 лет</a>
                        </li>
                    </div>
                </ul>
                <ul class="mf-top-sublist-submenu">
                    <h3><a href="#">Мальчикам</a></h3>
                    <h4><a href="#">Нижнее белье</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Майки</a>
                        </li>
                        <li>
                            <a href="#">Трусы</a>
                        </li>
                        <li>
                            <a href="#" class="sub-menu-el">Боксеры</a>
                        </li>
                        <li>
                            <a href="#" class="sub-menu-el">Плавки</a>
                        </li>
                        <li>
                            <a href="#">Комплекты</a>
                        </li>
                    </div>
                    <h4><a href="#">Домашняя одежда</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Пижамы</a>
                        </li>
                        <li>
                            <a href="#">Халаты</a>
                        </li>
                    </div>
                    <h4><a href="#">Верхняя одежда</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Футболки, поло</a>
                        </li>
                        <li>
                            <a href="#">Джемперы, лонгсливы</a>
                        </li>
                        <li>
                            <a href="#">Костюмы, комплекты</a>
                        </li>
                        <li>
                            <a href="#">Брюки</a>
                        </li>
                        <li>
                            <a href="#">Шорты, бриджи</a>
                        </li>
                        <li>
                            <a href="#">Жакеты, куртки</a>
                        </li>
                    </div>
                </ul>
                <ul class="mf-top-sublist-submenu without-title">
                    <h4><a href="#">Носки, колготки</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Классические</a>
                        </li>
                        <li>
                            <a href="#">Укороченные</a>
                        </li>
                        <li>
                            <a href="#">Колготки</a>
                        </li>
                    </div>
                    <h4><a href="#">Пляжная одежда</a></h4>
                    <h4><a href="#">Термобелье</a></h4>
                    <h4><a href="#">Школа</a></h4>
                    <h4><a href="#">По возрасту</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">от 3 до 6 лет</a>
                        </li>
                        <li>
                            <a href="#">от 6 до 12 лет</a>
                        </li>
                        <li>
                            <a href="#">от 12 до 18 лет</a>
                        </li>
                    </div>
                </ul>
            </ul>
            <div class="mf-sl-background-container"><a class="mf-sl-background" href="#"
                                                       style="background-image:url(<?= SITE_TEMPLATE_PATH ?>/images/lookbook/15.jpg);"></a>
            </div>
        </div>
    </div>
    <div class="mf-top-sublists-container" data-id="sale">
        <div class="fix-block">
            <ul class="mf-top-sublist">
                <ul class="mf-top-sublist-submenu">
                    <h4><a href="#">Распродажа женщинам</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Распродажа Одежда</a>
                        </li>
                        <li>
                            <a href="#">Распродажа Обувь</a>
                        </li>
                        <li>
                            <a href="#">Распродажа Аксессуары</a>
                        </li>
                        <li>
                            <a href="#">Распродажа до -80%</a>
                        </li>
                    </div>
                </ul>
                <ul class="mf-top-sublist-submenu">
                    <h4><a href="#">Распродажа мужчинам</a></h4>
                    <div class="mf-top-sl-column">
                        <li>
                            <a href="#">Распродажа Одежда</a>
                        </li>
                        <li>
                            <a href="#">Распродажа Аксессуары</a>
                        </li>
                    </div>
                </ul>
            </ul>
            <div class="mf-sl-background-container"><a class="mf-sl-background" href="#"
                                                       style="background-image:url(<?= SITE_TEMPLATE_PATH ?>/images/lookbook/15.jpg);"></a>
            </div>
        </div>
    </div>
</nav>

<script>
    /*$('li.mf-top-menu-el, .mf-top-sublists-container').hover(function() {
        if ( $(this).hasClass('mf-top-sublists-container') ) {
            $(this).addClass('show');
            $('.mf-top-menu-el[data-id="'+$(this).attr('data-id')+'"]').addClass('mf-top-sl-menu-active')
        }
        if ( $(this).hasClass('mf-top-menu-el') ) {
            $('.mf-top-sublists-container[data-id="'+$(this).attr('data-id')+'"]').addClass('show');
            $(this).addClass('mf-top-sl-menu-active');
        }
    }, function() {
        $('.mf-top-sublists-container').removeClass('show');
        $('.mf-top-menu-el').removeClass('mf-top-sl-menu-active');
    });*/
</script>
