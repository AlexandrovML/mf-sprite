<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
function arraySort($a, $b)
{
	if ($a['SORT'] == $b['SORT']) {
		return 0;
	}
	return ($a['SORT'] < $b['SORT']) ? -1 : 1;
}

if (isset($arParams["TEMPLATE_THEME"]) && !empty($arParams["TEMPLATE_THEME"]))
{
	$arAvailableThemes = array();
	$dir = trim(preg_replace("'[\\\\/]+'", "/", dirname(__FILE__)."/themes/"));
	if (is_dir($dir) && $directory = opendir($dir))
	{
		while (($file = readdir($directory)) !== false)
		{
			if ($file != "." && $file != ".." && is_dir($dir.$file))
				$arAvailableThemes[] = $file;
		}
		closedir($directory);
	}

	if ($arParams["TEMPLATE_THEME"] == "site")
	{
		$solution = COption::GetOptionString("main", "wizard_solution", "", SITE_ID);
		if ($solution == "eshop")
		{
			$templateId = COption::GetOptionString("main", "wizard_template_id", "eshop_bootstrap", SITE_ID);
			$templateId = (preg_match("/^eshop_adapt/", $templateId)) ? "eshop_adapt" : $templateId;
			$theme = COption::GetOptionString("main", "wizard_".$templateId."_theme_id", "blue", SITE_ID);
			$arParams["TEMPLATE_THEME"] = (in_array($theme, $arAvailableThemes)) ? $theme : "blue";
		}
	}
	else
	{
		$arParams["TEMPLATE_THEME"] = (in_array($arParams["TEMPLATE_THEME"], $arAvailableThemes)) ? $arParams["TEMPLATE_THEME"] : "blue";
	}
}
else
{
	$arParams["TEMPLATE_THEME"] = "blue";
}

$arParams["FILTER_VIEW_MODE"] = (isset($arParams["FILTER_VIEW_MODE"]) && toUpper($arParams["FILTER_VIEW_MODE"]) == "HORIZONTAL") ? "HORIZONTAL" : "VERTICAL";
$arParams["POPUP_POSITION"] = (isset($arParams["POPUP_POSITION"]) && in_array($arParams["POPUP_POSITION"], array("left", "right"))) ? $arParams["POPUP_POSITION"] : "right";

// получаем доп. инфомацию из справочника цветов
//pr($arResult['ITEMS']);
$TABLE_NAME = '';
$arCodes = $arColorRefs = [];
foreach ($arResult['ITEMS'] as &$arItem)
{
	if($arItem['CODE'] == 'COLOR_FOR_FILTER')
	{
		$TABLE_NAME = $arItem['USER_TYPE_SETTINGS']['TABLE_NAME'];

		foreach ($arItem['VALUES'] as &$v)
		{
			$arCodes[] = $v['URL_ID'];
			if(!empty($v['FILE']))
			{
				$arFile = CFile::ResizeImageGet($v['FILE']['ID'], ["width" => 32, "height" => 32], BX_RESIZE_IMAGE_EXACT);
				$v['FILE']["SRC"] = $arFile['src'];
			}
		}
		unset($v);
	}
}
unset($arItem);

if(!empty($TABLE_NAME))
{
	foreach ($arResult['SKU_PROPS']['COLOR_FOR_FILTER']['VALUES'] as $v) $arCodes[] = $v['ID'];
	$arFilter = ['UF_XML_ID'=>$arCodes];
	$arColorRefs = getDataFromReference($TABLE_NAME, ['*'], 'UF_XML_ID', $arFilter, [], 10000, ["width" => 32, "height" => 32]);
//	pr($arColorRefs);
}

$arResult['COLORS_REFERENCE'] = $arColorRefs;

//pr($arParams['SECTION_ID']);
//pr($arResult["ITEMS"]);

// ищем в элементах фильра уровни разделов
$arResult['SECTIONS_INFO'] = $arResult['CURRENT_SECTION'] = $arSections = [];
foreach ($arResult["ITEMS"] as $arItem)
{
	if(strpos($arItem['CODE'], 'SECTION_L') !==false)
	{
//		pr($arItem);
		foreach ($arItem['VALUES'] as $arV)
		{
			$arSections[] = intval($arV['VALUE']);
		}
	}
}
//pr($arSections);

$currentSection = CIBlockSection::GetByID($arParams['SECTION_ID']);
if ($arCurrentSection = $currentSection->GetNext())
{
	$arResult['CURRENT_SECTION'] = $arCurrentSection;
//	pr($arCurrentSection);

	$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'), [
		'IBLOCK_ID' => $arParams["IBLOCK_ID"],
		"ID" => $arSections,
	], false, ['ID', 'NAME', 'DEPTH_LEVEL', 'IBLOCK_SECTION_ID']);

	while($arSect = $rsSect->Fetch())
	{
//		pr($arSect);
		$arResult['SECTIONS_INFO'][$arSect['ID']] = $arSect;
	}
}

//pr($arResult['SECTIONS_INFO']);
//pr($arResult['CURRENT_SECTION']['DEPTH_LEVEL']);
// убираем из списка текущий уровень раздела
$keyForDelete = 0;
foreach ($arResult["ITEMS"] as $key=>$arItem)
{
	if(strpos($arItem['CODE'], 'SECTION_L') !==false)
	{
//		pr($key);
//		pr($arItem);

		$itemDepth = intval(str_replace('SECTION_L', '', $arItem['CODE']));
		if($itemDepth == $arResult['CURRENT_SECTION']['DEPTH_LEVEL'])
		{
			foreach ($arItem['VALUES'] as $_k=>$_ar)
			{
				$keyForDelete = $_k;
			}
//			pr($arResult["ITEMS"][$key]);
			unset($arResult["ITEMS"][$key]);
		}
		elseif ($keyForDelete > 0 && isset($arItem['VALUES'][$keyForDelete]) && count($arItem['VALUES']) == 1)
		{
//			unset($arItem['VALUES'][$keyForDelete]);
			unset($arResult["ITEMS"][$key]);
		}
	}
}

$nextDepthLevel = $arResult['CURRENT_SECTION']['DEPTH_LEVEL'];
$nextDepthLevel++;
//pr($arResult['CURRENT_SECTION']['DEPTH_LEVEL']);
//pr($nextDepthLevel);

foreach ($arResult["ITEMS"] as $key=>&$arItem)
{
	if(strpos($arItem['CODE'], 'SECTION_L') !==false)
	{
		$itemDepth = intval(str_replace('SECTION_L', '', $arItem['CODE']));
		if($itemDepth == $nextDepthLevel)
		{
//			pr($arItem);
			foreach ($arItem['VALUES'] as &$arV)
			{
				$arV['CHILDREN'] = [];
				foreach ($arResult["ITEMS"] as $key1=>&$arItem1)
				{
					if(strpos($arItem1['CODE'], 'SECTION_L') !==false)
					{
						$itemDepth1 = intval(str_replace('SECTION_L', '', $arItem1['CODE']));
						if($itemDepth1 == $itemDepth+1)
						{
							foreach ($arItem1['VALUES'] as $kv1=>$arV1)
							{
								if($arResult['SECTIONS_INFO'][$arV1['VALUE']]['IBLOCK_SECTION_ID'] == $arV['VALUE'])
								{
									$arV['CHILDREN'][$kv1] = $arV1;
								}
							}
						}
					}
				}
			}
			unset($arV);
		}
	}
}
unset($arItem);

foreach ($arResult["ITEMS"] as $key=>$arItem)
{
//	pr($arItem);
	if(strpos($arItem['CODE'], 'SECTION_L') !==false)
	{
		$itemDepth = intval(str_replace('SECTION_L', '', $arItem['CODE']));
		if($itemDepth > $nextDepthLevel || $itemDepth < $arResult['CURRENT_SECTION']['DEPTH_LEVEL'])
		{
			unset($arResult["ITEMS"][$key]);
		}
	}

	$rsProperty = CIBlockProperty::GetByID($key);
	$arProperty = $rsProperty->Fetch();
	if($arProperty){
		$arResult["ITEMS"][$key]["SORT"] = $arProperty['SORT'];
	}
}

uasort($arResult["ITEMS"], 'arraySort');

//pr($arResult["ITEMS"][31]);
//pr($arResult['SECTIONS_INFO']);
//pr($arResult["JS_FILTER_PARAMS"]);