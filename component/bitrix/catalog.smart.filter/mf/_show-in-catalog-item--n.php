<?php
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var array $arItem */
?>
<div class="bx-filter-parameters-box bx-filter-parameters-box--<?= $arItem['CODE'] ?> <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>bx-active<?endif?><?= ($arItem['CODE']=='NEWPRODUCT'||$arItem['CODE']=='VOZRAST')?' hidden-block':'' ?> mf-filter-checkbox">
	<span class="bx-filter-container-modef"></span>
	<ul data-role="bx_filter_block">
        <?
        $arCur = current($arItem["VALUES"]);
        ?>

        <?foreach($arItem["VALUES"] as $val => $ar):?>
            <?
            //if($ar['VALUE'] != 'Y') continue;
            ?>
            <li>
	            <?/*foreach($arItem["VALUES"] as $val => $ar):?>
                    <div class="checkbox">
                        <label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
                                <span class="bx-filter-input-checkbox">
                                <input
                                        type="checkbox"
                                        value="<? echo $ar["HTML_VALUE"] ?>"
                                        name="<? echo $ar["CONTROL_NAME"] ?>"
                                        id="<? echo $ar["CONTROL_ID"] ?>"
                                    <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                                    onclick="smartFilter.click(this)"
                                />
                                <span class="bx-filter-param-text" title="<?=$ar["VALUE"];?>"><?=$ar["VALUE"];?><?
                                    if ($arParams["DISPLAY_ELEMENT_COUNT"] !== "N" && isset($ar["ELEMENT_COUNT"])):
                                        ?>&nbsp;(<span data-role="count_<?=$ar["CONTROL_ID"]?>"><? echo $ar["ELEMENT_COUNT"]; ?></span>)<?
                                    endif;?></span>
                            </span>
                        </label>
                    </div>
	            <?endforeach;*/?>

                <div class="checkbox checkbox--l1" <?=($ar['VALUE']=='Y')?'style="display:none;"':'' ?>>
                    <input
                            type="checkbox"
                            value="<? echo $ar["HTML_VALUE"] ?>"
                            name="<? echo $ar["CONTROL_NAME"] ?>"
                            id="<? echo $ar["CONTROL_ID"] ?>"
                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                            onclick="smartFilter.click(this)"
                    />
                    <label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
	                    <?=$ar['VALUE']=='N' ? 'Просмотреть все товары в магазинах' : $ar["VALUE"]?>
                    </label>
                </div>
                <?/**/?>
            </li>
        <?endforeach;?>
        <?/*
        <li>
            <div class="checkbox checkbox--l1">
                <input
                    type="checkbox"
                    value=""
                    name=""
                    id="show_all_products"
                    <? echo $ar["CHECKED"]? '': 'checked="checked"' ?>
                    onclick="smtest(document.getElementById('arrFilter_211_3233089245'))"
                />
                <label data-role="label_show_all_products" class="bx-filter-param-label" for="show_all_products">
                    !Показать все товары!
                </label>
            </div>
        </li>
        */?>
	</ul>
</div>