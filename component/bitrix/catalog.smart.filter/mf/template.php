<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
use \Bitrix\Main\Localization\Loc;
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//$templateData = array(
//	'TEMPLATE_THEME' => $this->GetFolder().'/themes/'.$arParams['TEMPLATE_THEME'].'/colors.css',
//	'TEMPLATE_CLASS' => 'bx-'.$arParams['TEMPLATE_THEME']
//);
//
//if (isset($templateData['TEMPLATE_THEME']))
//{
//	$this->addExternalCss($templateData['TEMPLATE_THEME']);
//}

//pr($arResult['CURRENT_SECTION']['NAME'] .' - ' . $arResult['CURRENT_SECTION']['ID'] .' - ' . $arResult['CURRENT_SECTION']['DEPTH_LEVEL']);

$itemsForPrice = $arResult["ITEMS"];

$arProps = [
    'SHOW_IN_CATALOG',
    'SECTION_L1',
    'SECTION_L2',
    'SECTION_L3',
    'SECTION_L4',
    'SECTION_L5',
    'TAGS',
    'COLLECTION',
    'COLOR_FOR_FILTER',
];
?>
<button data-show class="catalog-filter-button">
    Фильтр
    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
         viewBox="0 0 42 42" style="enable-background:new 0 0 42 42;" xml:space="preserve">
        <polygon points="42,20 22,20 22,0 20,0 20,20 0,20 0,22 20,22 20,42 22,42 22,22 42,22 "/>
    </svg>
</button>

<div class="catalog-filter-list bx-filter bx-filter-mf">
    <button data-hide class="catalog-filter-button">
        Фильтр
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 31.427 31.427" style="enable-background:new 0 0 31.427 31.427;" xml:space="preserve">
            <path d="M1.111,16.832C0.492,16.832,0,16.325,0,15.706c0-0.619,0.492-1.111,1.111-1.111H30.3
            c0.619,0,1.127,0.492,1.127,1.111c0,0.619-0.508,1.127-1.127,1.127H1.111z"/>
        </svg>
    </button>

    <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get" class="smartfilter">
		<?foreach($arResult["HIDDEN"] as $arItem):?>
            <input type="hidden" name="<?echo $arItem["CONTROL_NAME"]?>" id="<?echo $arItem["CONTROL_ID"]?>" value="<?echo $arItem["HTML_VALUE"]?>" />
		<?endforeach;?>
        <?

        foreach ($arProps as $_prop)
        {
	        //not prices
	        foreach($arResult["ITEMS"] as $key=>$arItem)
	        {
		        if(
			        empty($arItem["VALUES"])
			        || isset($arItem["PRICE"])
		        )
			        continue;

		        if ($arItem['CODE'] !== $_prop) continue;

		        if (
			        $arItem["DISPLAY_TYPE"] == "A"
			        && (
				        $arItem["VALUES"]["MAX"]["VALUE"] - $arItem["VALUES"]["MIN"]["VALUE"] <= 0
			        )
		        )
			        continue;

		        switch ($arItem['CODE'])
		        {
			        case 'SHOW_IN_CATALOG':
				        include '_show-in-catalog-item--n.php';
				        break;

			        case 'TAGS':
			        case 'COLLECTION':
				        include '_collection-item.php';
				        break;

			        case 'COLOR_FOR_FILTER':
				        include '_color-item.php';
				        break;

			        case 'PERIOD_PODPISKI':
			        case 'EVRO_RAZMER':
			        case 'SIZES_CLOTHES':
			        case 'SIZES_SHOES':
				        include '_size-item.php';
				        break;

			        case 'SECTION_L1':
			        case 'SECTION_L2':
			        case 'SECTION_L3':
			        case 'SECTION_L4':
			        case 'SECTION_L5':
				        $showItem = true;
				        include '_section-item.php';
				        break;

			        case 'NOT_SHOW_IN_RU':
				        include '_not-show-in-ru.php';
				        break;

			        default:
				        include '_default-item.php';
				        break;
		        }
		        ?>
		        <?
	        }
        }


        include '_prices-item.php';
        ?>

        <div class="bx-filter-parameters-box-container bx-filter-buttons-cnt">
            <input
                    class="btn btn-themes"
                    type="submit"
                    id="set_filter"
                    name="set_filter"
                    value="<?=GetMessage("CT_BCSF_SET_FILTER")?>"
            />
            <input
                    class="btn btn-link"
                    type="submit"
                    id="del_filter"
                    name="del_filter"
                    value="<?=GetMessage("CT_BCSF_DEL_FILTER")?>"
            />
            <div class="bx-filter-popup-result <?if ($arParams["FILTER_VIEW_MODE"] == "VERTICAL") echo $arParams["POPUP_POSITION"]?>" id="modef" <?if(!isset($arResult["ELEMENT_COUNT"])) echo 'style="display:none"';?> style="display: inline-block;">
                <a href="<?echo $arResult["FILTER_URL"]?>" target="">
                    <?echo GetMessage("CT_BCSF_FILTER_COUNT", array("#ELEMENT_COUNT#" => '<span id="modef_num">'.intval($arResult["ELEMENT_COUNT"]).'</span>'));?>
                    <span class="arrow"></span>
                    <br/>
                    <?echo GetMessage("CT_BCSF_FILTER_SHOW")?>
                </a>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">
	var smartFilter = new JCSmartFilter('<?echo CUtil::JSEscape($arResult["FORM_ACTION"])?>', '<?=CUtil::JSEscape($arParams["FILTER_VIEW_MODE"])?>', <?=CUtil::PhpToJSObject($arResult["JS_FILTER_PARAMS"])?>);
</script>