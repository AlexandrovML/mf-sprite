<?php
use \Bitrix\Main\Localization\Loc;

foreach($itemsForPrice as $keyP=>$arItemP)//prices
{
    $keyP = $arItemP["ENCODED_ID"];
    if(isset($arItemP["PRICE"])):
        if ($arItemP["VALUES"]["MAX"]["VALUE"] - $arItemP["VALUES"]["MIN"]["VALUE"] <= 0)
            continue;

        $step_num = 4;
        $step = ($arItemP["VALUES"]["MAX"]["VALUE"] - $arItemP["VALUES"]["MIN"]["VALUE"]) / $step_num;
        $prices = $pricesFloat = array();
        if (Bitrix\Main\Loader::includeModule("currency"))
        {
            for ($i = 0; $i < $step_num; $i++)
            {
                $prices[$i] = CCurrencyLang::CurrencyFormat($arItemP["VALUES"]["MIN"]["VALUE"] + $step*$i, $arItemP["VALUES"]["MIN"]["CURRENCY"], false);
                $pricesFloat[$i] = CCurrencyLang::CurrencyFormat($arItemP["VALUES"]["MIN"]["VALUE"] + $step*$i, $arItemP["VALUES"]["MIN"]["CURRENCY"], false);

            }
            $prices[$step_num] = CCurrencyLang::CurrencyFormat($arItemP["VALUES"]["MAX"]["VALUE"], $arItemP["VALUES"]["MAX"]["CURRENCY"], false);
            $pricesFloat[$step_num] = CCurrencyLang::CurrencyFormat($arItemP["VALUES"]["MAX"]["VALUE"], $arItemP["VALUES"]["MAX"]["CURRENCY"], false);
        }
        else
        {
            $precision = $arItemP["DECIMALS"]? $arItemP["DECIMALS"]: 0;
            for ($i = 0; $i < $step_num; $i++)
            {
                $prices[$i] = number_format($arItemP["VALUES"]["MIN"]["VALUE"] + $step*$i, $precision, ".", "");
                $pricesFloat[$i] = $arItemP["VALUES"]["MIN"]["VALUE"] + $step*$i;
            }
            $prices[$step_num] = number_format($arItemP["VALUES"]["MAX"]["VALUE"], $precision, ".", "");
            $pricesFloat[$step_num] = $arItemP["VALUES"]["MAX"]["VALUE"];
        }

//                if(empty($arItemP["VALUES"]["MIN"]["HTML_VALUE"]))
//                {
//	                $arItemP["VALUES"]["MIN"]["HTML_VALUE"] = $pricesFloat[0];
//                }
//                if(empty($arItemP["VALUES"]["MAX"]["HTML_VALUE"]))
//                {
//	                $arItemP["VALUES"]["MAX"]["HTML_VALUE"] = $pricesFloat[count($pricesFloat)-1];
//                }
        ?>
        <div class="mf-filter-price-range bx-filter-parameters-box bx-active">
            <span class="bx-filter-container-modef"></span>
            <div class="h3"><?= Loc::getMessage('CT_BCSF_FILTER_PRICE_TITLE') ?></div>

            <div class="bx-filter-block" data-role="bx_filter_block">
                <div class="bx-filter-parameters-box-container">
                    <div class="bx-ui-slider-track-container">
                        <div class="bx-ui-slider-track" id="drag_track_<?=$keyP?>">
                            <?for($i = 0; $i <= $step_num; $i++):?>
                                <div class="bx-ui-slider-part p<?=$i+1?>"><span><?=$prices[$i]?></span></div>
                            <?endfor;?>

                            <div class="bx-ui-slider-pricebar-vd" style="left: 0;right: 0;" id="colorUnavailableActive_<?=$keyP?>"></div>
                            <div class="bx-ui-slider-pricebar-vn" style="left: 0;right: 0;" id="colorAvailableInactive_<?=$keyP?>"></div>
                            <div class="bx-ui-slider-pricebar-v"  style="left: 0;right: 0;" id="colorAvailableActive_<?=$keyP?>"></div>
                            <div class="bx-ui-slider-range" id="drag_tracker_<?=$keyP?>"  style="left: 0%; right: 0%;">
                                <a class="bx-ui-slider-handle left"  style="left:0;" href="javascript:void(0)" id="left_slider_<?=$keyP?>"></a>
                                <a class="bx-ui-slider-handle right" style="right:0;" href="javascript:void(0)" id="right_slider_<?=$keyP?>"></a>
                            </div>
                        </div>
                    </div>

                    <div class="bx-filter-price-ranges">
                        <div class="bx-filter-parameters-box-container-block bx-left">
                            <?/*<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_FROM")?></i>*/?>
                            <div class="bx-filter-input-container">
                                <input
                                        class="min-price"
                                        type="text"
                                        name="<?echo $arItemP["VALUES"]["MIN"]["CONTROL_NAME"]?>"
                                        id="<?echo $arItemP["VALUES"]["MIN"]["CONTROL_ID"]?>"
                                        value="<?echo $arItemP["VALUES"]["MIN"]["HTML_VALUE"]?>"
                                        size="5"
                                        onkeyup="smartFilter.keyup(this)"
                                />
                            </div>
                        </div>
                        <div class="bx-filter-parameters-box-container-block bx-right">
                            <?/*<i class="bx-ft-sub"><?=GetMessage("CT_BCSF_FILTER_TO")?></i>*/?>
                            <div class="bx-filter-input-container">
                                <input
                                        class="max-price"
                                        type="text"
                                        name="<?echo $arItemP["VALUES"]["MAX"]["CONTROL_NAME"]?>"
                                        id="<?echo $arItemP["VALUES"]["MAX"]["CONTROL_ID"]?>"
                                        value="<?echo $arItemP["VALUES"]["MAX"]["HTML_VALUE"]?>"
                                        size="5"
                                        onkeyup="smartFilter.keyup(this)"
                                />
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    <?
    $arJsParams = array(
        "leftSlider" => 'left_slider_'.$keyP,
        "rightSlider" => 'right_slider_'.$keyP,
        "tracker" => "drag_tracker_".$keyP,
        "trackerWrap" => "drag_track_".$keyP,
        "minInputId" => $arItemP["VALUES"]["MIN"]["CONTROL_ID"],
        "maxInputId" => $arItemP["VALUES"]["MAX"]["CONTROL_ID"],
        "minPrice" => $arItemP["VALUES"]["MIN"]["VALUE"],
        "maxPrice" => $arItemP["VALUES"]["MAX"]["VALUE"],
        "curMinPrice" => $arItemP["VALUES"]["MIN"]["HTML_VALUE"],
        "curMaxPrice" => $arItemP["VALUES"]["MAX"]["HTML_VALUE"],
        "fltMinPrice" => intval($arItemP["VALUES"]["MIN"]["FILTERED_VALUE"]) ? $arItemP["VALUES"]["MIN"]["FILTERED_VALUE"] : $arItemP["VALUES"]["MIN"]["VALUE"] ,
        "fltMaxPrice" => intval($arItemP["VALUES"]["MAX"]["FILTERED_VALUE"]) ? $arItemP["VALUES"]["MAX"]["FILTERED_VALUE"] : $arItemP["VALUES"]["MAX"]["VALUE"],
        "precision" => $precision,
        "colorUnavailableActive" => 'colorUnavailableActive_'.$keyP,
        "colorAvailableActive" => 'colorAvailableActive_'.$keyP,
        "colorAvailableInactive" => 'colorAvailableInactive_'.$keyP,
    );
    ?>
        <script type="text/javascript">
            BX.ready(function(){
                window['trackBar<?=$keyP?>'] = new BX.Iblock.SmartFilter(<?=CUtil::PhpToJSObject($arJsParams)?>);
            });
        </script>
    <?endif;
}

