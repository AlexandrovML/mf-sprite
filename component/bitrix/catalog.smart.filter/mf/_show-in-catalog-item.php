<?php
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var array $arItem */
?>
<div class="bx-filter-parameters-box <?if ($arItem["DISPLAY_EXPANDED"]== "Y"):?>bx-active<?endif?><?= ($arItem['CODE']=='NEWPRODUCT'||$arItem['CODE']=='VOZRAST')?' hidden-block':'' ?> mf-filter-checkbox">
	<span class="bx-filter-container-modef"></span>
	<ul data-role="bx_filter_block">
        <?
        $arCur = current($arItem["VALUES"]);
        ?>

        <?foreach($arItem["VALUES"] as $val => $ar):?>
            <?
            if($ar['VALUE'] != 'Y') continue;
            ?>
            <li>
                <div class="checkbox checkbox--l1">
                    <input
                            type="checkbox"
                            value="<? echo $ar["HTML_VALUE"] ?>"
                            name="<? echo $ar["CONTROL_NAME"] ?>"
                            id="<? echo $ar["CONTROL_ID"] ?>"
                        <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                            onclick="smartFilter.click(this)"
                    />
                    <label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
	                    <?=$arItem["NAME"]?>
                    </label>
                </div>
                <?/*
                <div class="checkbox checkbox--l1">
                    <input
                        type="checkbox"
                        value=""
                        name=""
                        id="show_all_products"
                        <? echo $ar["CHECKED"]? '': 'checked="checked"' ?>
                        onclick="smtest(document.getElementById('arrFilter_211_3233089245'))"
                    />
                    <label data-role="label_show_all_products" class="bx-filter-param-label" for="show_all_products">
                        Показать все товары
                    </label>
                </div>*/?>
            </li>
        <?endforeach;?>
	</ul>
</div>