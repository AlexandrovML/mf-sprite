<?php
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
/** @var array $arItem */


//pr($arItem);
?>
<?/*
<div class="mf-filter-checkbox">
    <ul>
        <li>
            <span class="mf-filter-ch-point" data-checked="false"></span>
            Одежда
            <ul class="mf-filter-sublist">
                <li><span class="mf-filter-ch-point" data-checked="false"></span>Верхняя одежда</li>
                <li><span class="mf-filter-ch-point" data-checked="false"></span>Летняя одежда</li>
                <li><span class="mf-filter-ch-point" data-checked="false"></span>Одежда для парней</li>
                <li><span class="mf-filter-ch-point" data-checked="false"></span>Для малышей</li>
                <li class="mf-hide-value"><span class="mf-filter-ch-point" data-checked="false"></span>Зимняя одежда</li>
            </ul>
        </li>
    </ul>

    <span class="mf-filter-more dotted" data-show-items>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64"><g><path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path></g></svg>
        Показать все
    </span>
</div>
<?/**/?>

    <div class="h3">Категории</div>
<?foreach($arItem["VALUES"] as $val => $ar):?>
	<?
    $ar["VALUE"] = $arResult['SECTIONS_INFO'][$ar["VALUE"]]['NAME'];
    $showMore = false;
    ?>
    <div class="mf-filter-categories bx-filter-parameters-box bx-filter-parameters-box--<?= $arItem["CODE"] ?> bx-active mf-filter-checkbox <?= $ar["DISABLED"]?'hidden-block':''?>">
        <span class="bx-filter-container-modef"></span>
        <ul data-role="bx_filter_block">
            <li>
                <div class="checkbox checkbox--l1">
                    <input
                            type="checkbox"
                            value="<? echo $ar["HTML_VALUE"] ?>"
                            name="<? echo $ar["CONTROL_NAME"] ?>"
                            id="<? echo $ar["CONTROL_ID"] ?>"
                            <? echo $ar["CHECKED"]? 'checked="checked"': '' ?>
                            <? echo $ar["DISABLED"]? 'disabled="disabled"': '' ?>
                            onclick="smartFilter.click(this)"
                    />
                    <label data-role="label_<?=$ar["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar["CONTROL_ID"] ?>">
                        <?=$ar["VALUE"];?>
                    </label>
                </div>
                <? if(!empty($ar["CHILDREN"])) { ?>
                    <?
                    $showMore = count($ar["CHILDREN"]) > $arParams['MAX_SECTIONS_SHOW'];
                    $i = 1;
                    ?>
                    <? if($showMore) { ?>
<!--                        <span class="mf-filter-more" data-show-items>-->
<!--                            <svg class="icon-minus"><use xlink:href="/local/templates/markformelle/images/sprite.svg#minus-icon"></use></svg>-->
<!--                            <svg class="icon-plus"><use xlink:href="/local/templates/markformelle/images/sprite.svg#plus-icon"></use></svg>-->
<!--                        </span>-->
                    <? } ?>
                    <ul class="mf-filter-sublist">
                        <? foreach($ar["CHILDREN"] as $val1 => $ar1) { ?>
                            <? $ar1["VALUE"] = $arResult['SECTIONS_INFO'][$ar1["VALUE"]]['NAME']; ?>
                            <li<?= $i>$arParams['MAX_SECTIONS_SHOW']?' class="mf-hide-value"':'' ?>>
                                <div class="checkbox checkbox--l2">
                                    <input
                                        type="checkbox"
                                        value="<? echo $ar1["HTML_VALUE"] ?>"
                                        name="<? echo $ar1["CONTROL_NAME"] ?>"
                                        id="<? echo $ar1["CONTROL_ID"] ?>"
                                        <? echo $ar1["CHECKED"]? 'checked="checked"': '' ?>
                                        onclick="smartFilter.click(this)"
                                    />
                                    <label data-role="label_<?=$ar1["CONTROL_ID"]?>" class="bx-filter-param-label <? echo $ar1["DISABLED"] ? 'disabled': '' ?>" for="<? echo $ar1["CONTROL_ID"] ?>">
                                        <?=$ar1["VALUE"];?>
                                    </label>
                                </div>
                            </li>
                            <? $i++ ?>
                        <? } ?>
                    </ul>
                <? } ?>
            </li>
        </ul>
        <? if($showMore) { ?>
            <span class="mf-filter-more dotted" data-show-items>
                <svg class="mf-filter-more__icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64"><g><path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path></g></svg>
                Показать все
            </span>
        <? } ?>
    </div>
<?endforeach;?>