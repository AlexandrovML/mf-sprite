<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
//if (!$this->__component->__parent || empty($this->__component->__parent->__name)):
//	$GLOBALS['APPLICATION']->SetAdditionalCSS('/bitrix/components/bitrix/voting.current/templates/.default/style.css');
//endif;
?>
<div class="news-list news-page-container">
    <div class="fix-block">
        <section class="news-section">
            <ul class="news-list">
	            <?
	            $iCount = 0;
	            foreach ($arResult["VOTES"] as $arVote):
		            $iCount++;
		            ?>
                    <li data-animate="opacity-moveUp">
                        <a href="<?=$arVote["VOTE_FORM_URL"]?>" data-img class="news-list-image"
                           style="background-image: url(<?= $arVote["IMAGE"]["SRC"] ?>);"></a>
                        <a href="<?=$arVote["VOTE_FORM_URL"]?>">
                            <h3><?=$arVote["TITLE"];?></h3>
                        </a>
                        <p><? echo mb_strimwidth($arVote["DESCRIPTION"], 0, 140, "..."); ?></p>
                    </li>
	            <?endforeach; ?>
            </ul>
        </section>
    </div>

	<? if (strlen($arResult["NAV_STRING"]) > 0): ?>
        <br><?=$arResult["NAV_STRING"]?>
	<? endif; ?>
</div>
