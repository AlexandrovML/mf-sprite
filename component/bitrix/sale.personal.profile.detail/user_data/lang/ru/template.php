<?
$MESS ['SPPD_RECORDS_LIST'] = "В список профилей";
$MESS ['SALE_SAVE'] = "Сохранить";
$MESS ['SALE_APPLY'] = "Применить";
$MESS ['SALE_RESET'] = "Отмена";
$MESS ['SALE_PERS_TYPE'] = "Тип плательщика";
$MESS ['SALE_PNAME'] = "Название";
$MESS ["SPPD_PROFILE_NO"]="Профиль № #ID#";
$MESS ["SPPD_DOWNLOAD_FILE"]="Скачать файл #FILE_NAME#";
$MESS ["SPPD_DELETE_FILE"]="Удалить файл";
$MESS ["SPPD_SELECT"]="Выбрать";
$MESS ["SPPD_ADD"]="Добавить";
$MESS ["SPPD_FILE_NOT_SELECTED"]="Файл не выбран";
$MESS ["SPPD_FILE_COUNT"]="Выбрано файлов: ";

$MESS["ADD_PHOTO"] = "добавить фото";
$MESS["MY_DATA"] = "Мои данные";
$MESS["MY_DATA_SMALL"] = "управление личной информацией";
$MESS["MY_FAVORITE"] = "Мои избранные";
$MESS["MY_FAVORITE_SMALL"] = "просмотр отложенных товаров";
$MESS["MY_BUYES"] = "Мои покупки";
$MESS["HISTORY_BUY"] = "история заказов";
$MESS["EXIT"] = "Выйти";
$MESS["SECURITY"] = "Безопасность";

$MESS["ZIP"] = "Индекс";
$MESS["STREET"] = "Улица";
$MESS["CITY"] = "Город";
$MESS["HOUSE"] = "Дом";
$MESS["CORPUS"] = "Корпус";
$MESS["KV"] = "Квартира";
$MESS["NAME_PROFILE"] = "Название профиля";

$MESS["PROFILE_BUY"] = "Адреса доставки";
$MESS["PROFILE_BUY_SMALL"] = "Профили покупателя";
$MESS["MY_ADRESSES"] = "Мои адреса доставки";


$MESS["EDIT_ADRESS_TITLE"] = "Редактирование адреса доставки";
$MESS["LOCATION"] = "Местоположение";
?>