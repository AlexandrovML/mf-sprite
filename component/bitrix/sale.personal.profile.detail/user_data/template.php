<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

if (strlen($arResult["ID"]) > 0) {
    $rsUser = CUser::GetByID($USER->GetID());
    $arUser = $rsUser->Fetch();
    $user_photo = CFile::GetPath($arUser['PERSONAL_PHOTO']);
    ?>
    <section class="user-ac-section-background bx-auth-profile">
        <div class="fix-block">
            <form method="post" name="form1" action="<?= $arResult["FORM_TARGET"] ?>" enctype="multipart/form-data">
                <?= $arResult["BX_SESSION_CHECK"] ?>
                <input type="hidden" name="lang" value="<?= LANG ?>"/>
                <input type="hidden" name="ID" value=<?= $arResult["ID"] ?>/>
                <div class="user-ac-list">
                    <ul>
                        <li class="us-ac-inf">
                            <div class="user-ac-avatar">
                                <label for="photo_add_new">
                                    <? if (strlen($user_photo) > 0) { ?>
                                        <a onclick="ImgShw('<?= $user_photo ?>', 1024, 768, ''); return false;"
                                           href="<?= $user_photo ?>" target="_blank">
                                            <img src="<?= $user_photo ?>"
                                                 style="width: auto;height: 100%;" border="0"></a>
                                    <? } else { ?>
                                        <div class="no_photo">
                                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                                 x="0px" y="0px" viewBox="0 0 60 60"
                                                 style="enable-background:new 0 0 60 60;"
                                                 xml:space="preserve">
                                                <path d="M55.201,15.5h-8.524l-4-10H17.323l-4,10H12v-5H6v5H4.799C2.152,15.5,0,17.652,0,20.299v29.368
                                                    C0,52.332,2.168,54.5,4.833,54.5h50.334c2.665,0,4.833-2.168,4.833-4.833V20.299C60,17.652,57.848,15.5,55.201,15.5z M8,12.5h2v3H8
                                                    V12.5z M58,49.667c0,1.563-1.271,2.833-2.833,2.833H4.833C3.271,52.5,2,51.229,2,49.667V20.299C2,18.756,3.256,17.5,4.799,17.5H6h6
                                                    h2.677l4-10h22.646l4,10h9.878c1.543,0,2.799,1.256,2.799,2.799V49.667z"/>
                                                <path d="M30,14.5c-9.925,0-18,8.075-18,18s8.075,18,18,18s18-8.075,18-18S39.925,14.5,30,14.5z M30,48.5c-8.822,0-16-7.178-16-16
                                                    s7.178-16,16-16s16,7.178,16,16S38.822,48.5,30,48.5z"/>
                                                <path d="M30,20.5c-6.617,0-12,5.383-12,12s5.383,12,12,12s12-5.383,12-12S36.617,20.5,30,20.5z M30,42.5c-5.514,0-10-4.486-10-10
                                                    s4.486-10,10-10s10,4.486,10,10S35.514,42.5,30,42.5z"/>
                                                <path d="M52,19.5c-2.206,0-4,1.794-4,4s1.794,4,4,4s4-1.794,4-4S54.206,19.5,52,19.5z M52,25.5c-1.103,0-2-0.897-2-2s0.897-2,2-2
                                                    s2,0.897,2,2S53.103,25.5,52,25.5z"/>
                                            </svg>
                                        </div>
                                    <? } ?>
                                    <div class="user-ac-img"></div>
                                </label>
                            </div>
                            <p class="us-ac-name"><?= $arUser['NAME'] ?> <?= $arUser['LAST_NAME'] ?></p>
                            <p class="us-ac-datereg">с нами
                                с <?= mb_strtolower(FormatDate("j M. Y г", MakeTimeStamp($arUser["DATE_REGISTER"]))) ?></p>
                        </li>
                        <li class="us-ac-data">
                            <a href="<?= SITE_DIR ?>personal/">
                                <h3><?= GetMessage("MY_DATA"); ?></h3>
                                <p><?= GetMessage("MY_DATA_SMALL"); ?></p>
                            </a>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                 x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;"
                                 xml:space="preserve">
                                        <path d="M437.02,330.98c-27.883-27.882-61.071-48.523-97.281-61.018C378.521,243.251,404,198.548,404,148
                                        C404,66.393,337.607,0,256,0S108,66.393,108,148c0,50.548,25.479,95.251,64.262,121.962
                                        c-36.21,12.495-69.398,33.136-97.281,61.018C26.629,379.333,0,443.62,0,512h40c0-119.103,96.897-216,216-216s216,96.897,216,216
                                        h40C512,443.62,485.371,379.333,437.02,330.98z M256,256c-59.551,0-108-48.448-108-108S196.449,40,256,40
                                        c59.551,0,108,48.448,108,108S315.551,256,256,256z"/>
                                    </svg>
                        </li>
                        <li class="us-ac-favorites">
                            <a href="<?= SITE_DIR ?>personal/favorites/">
                                <h3><?= GetMessage("MY_FAVORITE"); ?></h3>
                                <p><?= GetMessage("MY_FAVORITE_SMALL"); ?></p>
                            </a>
                            <svg viewBox="-12 -28 512.001 512" xmlns="http://www.w3.org/2000/svg">
                                <path d="m256 455.515625c-7.289062 0-14.316406-2.640625-19.792969-7.4375-20.683593-18.085937-40.625-35.082031-58.21875-50.074219l-.089843-.078125c-51.582032-43.957031-96.125-81.917969-127.117188-119.3125-34.644531-41.804687-50.78125-81.441406-50.78125-124.742187 0-42.070313 14.425781-80.882813 40.617188-109.292969 26.503906-28.746094 62.871093-44.578125 102.414062-44.578125 29.554688 0 56.621094 9.34375 80.445312 27.769531 12.023438 9.300781 22.921876 20.683594 32.523438 33.960938 9.605469-13.277344 20.5-24.660157 32.527344-33.960938 23.824218-18.425781 50.890625-27.769531 80.445312-27.769531 39.539063 0 75.910156 15.832031 102.414063 44.578125 26.191406 28.410156 40.613281 67.222656 40.613281 109.292969 0 43.300781-16.132812 82.9375-50.777344 124.738281-30.992187 37.398437-75.53125 75.355469-127.105468 119.308594-17.625 15.015625-37.597657 32.039062-58.328126 50.167969-5.472656 4.789062-12.503906 7.429687-19.789062 7.429687zm-112.96875-425.523437c-31.066406 0-59.605469 12.398437-80.367188 34.914062-21.070312 22.855469-32.675781 54.449219-32.675781 88.964844 0 36.417968 13.535157 68.988281 43.882813 105.605468 29.332031 35.394532 72.960937 72.574219 123.476562 115.625l.09375.078126c17.660156 15.050781 37.679688 32.113281 58.515625 50.332031 20.960938-18.253907 41.011719-35.34375 58.707031-50.417969 50.511719-43.050781 94.136719-80.222656 123.46875-115.617188 30.34375-36.617187 43.878907-69.1875 43.878907-105.605468 0-34.515625-11.605469-66.109375-32.675781-88.964844-20.757813-22.515625-49.300782-34.914062-80.363282-34.914062-22.757812 0-43.652344 7.234374-62.101562 21.5-16.441406 12.71875-27.894532 28.796874-34.609375 40.046874-3.453125 5.785157-9.53125 9.238282-16.261719 9.238282s-12.808594-3.453125-16.261719-9.238282c-6.710937-11.25-18.164062-27.328124-34.609375-40.046874-18.449218-14.265626-39.34375-21.5-62.097656-21.5zm0 0"/>
                            </svg>
                        </li>
                        <li class="us-ac-hist-buy ">
                            <a href="<?= SITE_DIR ?>personal/order/">
                                <h3><?= GetMessage("MY_BUYES"); ?></h3>
                                <p><?= GetMessage("HISTORY_BUY"); ?></p>
                            </a>
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink"
                                 x="0px" y="0px" viewBox="-12 0 490.1 490.1"
                                 style="enable-background:new 0 0 490.1 490.1;"
                                 xml:space="preserve">
                                        <path d="M211.7,457.75c0-5.5-4.4-9.9-9.9-9.9H63.7c-24.2,0-43.8-19.7-43.8-43.8v-285.5h60v37.3
                                            c0,5.5,4.4,9.9,9.9,9.9s9.9-4.4,9.9-9.9v-37.3H219v37.3c0,5.5,4.4,9.9,9.9,9.9s9.9-4.4,9.9-9.9v-37.3h60v19.5
                                            c0,5.5,4.4,9.9,9.9,9.9s9.9-4.4,9.9-9.9v-29.4c0-5.5-4.4-9.9-9.9-9.9h-70c-1.7-42.3-36.6-76.3-79.4-76.3s-77.7,33.9-79.4,76.3h-70
                                            c-5.5,0-9.9,4.4-9.9,9.9v295.4c0,35.1,28.5,63.6,63.6,63.6h138.1C207.3,467.65,211.7,463.15,211.7,457.75z M159.3,42.25
                                            c31.8,0,57.9,25.1,59.6,56.5H99.7C101.4,67.35,127.5,42.25,159.3,42.25z"/>
                                <path d="M295.8,221.75v11.3h-54.5c-5.5,0-9.9,4.4-9.9,9.9v173.7c0,28.1,22.8,50.9,50.9,50.9h156.9
                                            c28.1,0,50.9-22.8,50.9-50.9v-173.7c0-5.5-4.4-9.9-9.9-9.9h-54.5v-11.3c0-35.8-29.1-65-65-65
                                            C324.9,156.85,295.8,185.95,295.8,221.75z M415.8,287.35c5.5,0,9.9-4.4,9.9-9.9v-24.6h44.6v163.8c0,17.2-14,31.1-31.1,31.1H282.3
                                            c-17.2,0-31.1-14-31.1-31.1v-163.8h44.6v24.6c0,5.5,4.4,9.9,9.9,9.9s9.9-4.4,9.9-9.9v-24.6h90.3v24.6
                                            C405.9,282.95,410.3,287.35,415.8,287.35z M405.9,221.75v11.3h-90.3v-11.3c0-24.9,20.3-45.2,45.2-45.2
                                            C385.6,176.65,405.9,196.85,405.9,221.75z"/>
                                </svg>
                        </li>
                        <li class="us-ac-profiles active">
                            <h3><?= GetMessage("PROFILE_BUY"); ?></h3>
                            <p><?= GetMessage("PROFILE_BUY_SMALL"); ?></p>
                        </li>

                        <li class="us-ac-exit">
                            <p>
                                <a href="<? echo $APPLICATION->GetCurPageParam("logout=yes", array(
                                    "login",
                                    "logout",
                                    "register",
                                    "forgot_password",
                                    "change_password")); ?>">
                                    <?= GetMessage("EXIT"); ?>
                                </a>
                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink"
                                     x="0px" y="0px" viewBox="0 0 55 55" style="enable-background:new 0 0 55 55;"
                                     xml:space="preserve">
                                        <path d="M53.924,24.382c0.101-0.244,0.101-0.519,0-0.764c-0.051-0.123-0.125-0.234-0.217-0.327L41.708,11.293
                                            c-0.391-0.391-1.023-0.391-1.414,0s-0.391,1.023,0,1.414L50.587,23H29.001c-0.553,0-1,0.447-1,1s0.447,1,1,1h21.586L40.294,35.293
                                            c-0.391,0.391-0.391,1.023,0,1.414C40.489,36.902,40.745,37,41.001,37s0.512-0.098,0.707-0.293l11.999-11.999
                                            C53.799,24.616,53.873,24.505,53.924,24.382z"/>
                                    <path d="M36.001,29c-0.553,0-1,0.447-1,1v16h-10V8c0-0.436-0.282-0.821-0.697-0.953L8.442,2h26.559v16c0,0.553,0.447,1,1,1
                                            s1-0.447,1-1V1c0-0.553-0.447-1-1-1h-34c-0.032,0-0.06,0.015-0.091,0.018C1.854,0.023,1.805,0.036,1.752,0.05
                                            C1.658,0.075,1.574,0.109,1.493,0.158C1.467,0.174,1.436,0.174,1.411,0.192C1.38,0.215,1.356,0.244,1.328,0.269
                                            c-0.017,0.016-0.035,0.03-0.051,0.047C1.201,0.398,1.139,0.489,1.093,0.589c-0.009,0.02-0.014,0.04-0.022,0.06
                                            C1.029,0.761,1.001,0.878,1.001,1v46c0,0.125,0.029,0.243,0.072,0.355c0.014,0.037,0.035,0.068,0.053,0.103
                                            c0.037,0.071,0.079,0.136,0.132,0.196c0.029,0.032,0.058,0.061,0.09,0.09c0.058,0.051,0.123,0.093,0.193,0.13
                                            c0.037,0.02,0.071,0.041,0.111,0.056c0.017,0.006,0.03,0.018,0.047,0.024l22,7C23.797,54.984,23.899,55,24.001,55
                                            c0.21,0,0.417-0.066,0.59-0.192c0.258-0.188,0.41-0.488,0.41-0.808v-6h11c0.553,0,1-0.447,1-1V30
                                            C37.001,29.447,36.553,29,36.001,29z M23.001,52.633l-20-6.364V2.367l20,6.364V52.633z"/>
                                    </svg>
                            </p>
                        </li>
                    </ul>
                </div>
                <div class="user-ac-content">
                    <?
                    ShowError($arResult["ERROR_MESSAGE"]);
                    ShowError($arResult["strProfileError"]); ?>
                    <div class="user-ac-form-userdelivery">
                        <div class="mf-form">
                            <div class="user-ac-form-header">
                                <?= GetMessage("EDIT_ADRESS_TITLE"); ?>
                            </div>
                            <div class="mf-form-body">
                                <div class="user-ac-form-body mf-form-tablelist">
                                    <form method="post" class="col-md-12 sale-profile-detail-form"
                                          action="<?= POST_FORM_ACTION_URI ?>" enctype="multipart/form-data">
                                        <?= bitrix_sessid_post() ?>
                                        <input type="hidden" name="ID" value="<?= $arResult["ID"] ?>">
                                        <input class="form-control" type="text" name="ORDER_PROP_20" maxlength="50"
                                               id="sppd-property-20"
                                               value="<?= $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_20"] ?>"
                                               style="display: none;">
                                        <input class="form-control" type="text" name="ORDER_PROP_1" maxlength="50"
                                               id="sppd-property-1"
                                               value="<?= $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_1"] ?>"
                                               style="display: none;">
                                        <input class="form-control" type="text" name="ORDER_PROP_2" maxlength="50"
                                               id="sppd-property-2"
                                               value="<?= $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_2"] ?>"
                                               style="display: none;">
                                        <input class="form-control" type="text" name="ORDER_PROP_3" maxlength="50"
                                               id="sppd-property-3"
                                               value="<?= $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_3"] ?>"
                                               style="display: none;">
                                        <input class="form-control" type="text" name="ORDER_PROP_4" maxlength="50"
                                               id="sppd-property-4"
                                               value="<?= $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_4"] ?>"
                                               style="display: none;">
                                        <div class="mf-f-t-row">
                                            <div class="mf-f-t-cell">
                                                <label class="mf-form-label">
                                                    <?= GetMessage("NAME_PROFILE"); ?>
                                                    <input class="form-control" type="text" name="NAME"
                                                           id="sale-personal-profile-detail-name"
                                                           value="<?= $arResult["NAME"] ?>"/>
                                                </label>
                                            </div>

                                            <div class="mf-f-t-cell location_profiles">
                                                <label class="mf-form-label">
                                                    <div class="profiles_text">
				                                        <?= GetMessage("LOCATION"); ?>
                                                    </div>
			                                        <? $currentValue = $arResult["ORDER_PROPS_VALUES"]['ORDER_PROP_6'];
			                                        $name = "ORDER_PROP_6";
			                                        $locationTemplate = ($arParams['USE_AJAX_LOCATIONS'] !== 'Y') ? "popup" : "";
			                                        $locationClassName = 'location-block-wrapper';
			                                        if ($arParams['USE_AJAX_LOCATIONS'] === 'Y') {
				                                        $locationClassName .= ' location-block-wrapper-delimeter';
			                                        }
			                                        if ($arResult["ORDER_PROPS"][2]['PROPS'][1]['MULTIPLE'] === 'Y') {
				                                        if (empty($currentValue) || !is_array($currentValue))
					                                        $currentValue = array($arResult["ORDER_PROPS"][2]['PROPS'][1]["DEFAULT_VALUE"]);
				                                        foreach ($currentValue as $code => $elementValue) {
					                                        $locationValue = intval($elementValue) ? $elementValue : $arResult["ORDER_PROPS"][2]['PROPS'][1]["DEFAULT_VALUE"];
					                                        CSaleLocation::proxySaleAjaxLocationsComponent(
						                                        array(
							                                        "ID" => "propertyLocation" . $name . "[$code]",
							                                        "AJAX_CALL" => "N",
							                                        'CITY_OUT_LOCATION' => 'Y',
							                                        'COUNTRY_INPUT_NAME' => $name . '_COUNTRY',
							                                        'CITY_INPUT_NAME' => $name . "[$code]",
							                                        'LOCATION_VALUE' => $locationValue,
						                                        ),
						                                        array(),
						                                        $locationTemplate,
						                                        true,
						                                        $locationClassName
					                                        );
				                                        }
				                                        ?>
                                                        <span class="btn-themes btn-default btn-md btn input-add-multiple"
                                                              data-add-type="LOCATION"
                                                              data-add-name="<?= $name ?>"
                                                              data-add-last-key="<?= $code ?>"
                                                              data-add-template="<?= $locationTemplate ?>"><?= Loc::getMessage('SPPD_ADD') ?></span>
				                                        <?
			                                        } else {
				                                        $locationValue = (int)($currentValue) ? (int)$currentValue : $arResult["ORDER_PROPS"][2]['PROPS'][1]["DEFAULT_VALUE"];
				                                        CSaleLocation::proxySaleAjaxLocationsComponent(
					                                        array(
						                                        "AJAX_CALL" => "N",
						                                        'CITY_OUT_LOCATION' => 'Y',
						                                        'COUNTRY_INPUT_NAME' => $name . '_COUNTRY',
						                                        'CITY_INPUT_NAME' => $name,
						                                        'LOCATION_VALUE' => $locationValue,
					                                        ),
					                                        array(),
					                                        $locationTemplate,
					                                        true,
					                                        'location-block-wrapper'
				                                        );
			                                        } ?>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="mf-f-t-row">
                                            <div class="mf-f-t-cell">
                                                <label class="mf-form-label">
			                                        <?= GetMessage("ZIP"); ?>
                                                    <input class="form-control" type="text" name="ORDER_PROP_4"
                                                           maxlength="50" id="sppd-property-4"
                                                           value="<?= $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_4"] ?>">
                                                </label>
                                            </div>

                                            <div class="mf-f-t-cell">
                                                <label class="mf-form-label">
			                                        <?= GetMessage("STREET"); ?>
                                                    <input class="form-control" type="text" name="ORDER_PROP_22"
                                                           maxlength="50" id="sppd-property-22"
                                                           value="<?= $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_22"] ?>">
                                                </label>
                                            </div>
                                            <div class="mf-f-t-cell">
                                                <label class="mf-form-label">
			                                        <?= GetMessage("HOUSE"); ?>
                                                    <input class="form-control" type="text" name="ORDER_PROP_23"
                                                           maxlength="50" id="sppd-property-23"
                                                           value="<?= $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_23"] ?>">
                                                </label>
                                            </div>
                                            <div class="mf-f-t-cell">
                                                <label class="mf-form-label">
			                                        <?= GetMessage("CORPUS"); ?>
                                                    <input class="form-control" type="text" name="ORDER_PROP_24"
                                                           maxlength="50" id="sppd-property-24"
                                                           value="<?= $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_24"] ?>">
                                                </label>
                                            </div>
                                            <div class="mf-f-t-cell">
                                                <label class="mf-form-label">
			                                        <?= GetMessage("KV"); ?>
                                                    <input class="form-control" type="text" name="ORDER_PROP_25"
                                                           maxlength="50" id="sppd-property-25"
                                                           value="<?= $arResult["ORDER_PROPS_VALUES"]["ORDER_PROP_25"] ?>">
                                                </label>
                                            </div>

                                            <div class="col-md-offset-3 col-sm-9 sale-personal-profile-btn-block">
                                                <input type="submit"
                                                       class="btn btn-themes btn-default btn-md button-link black-tr-b"
                                                       name="save" value="<? echo GetMessage("SALE_SAVE") ?>">
                                                <a class="button-link button-link--cancel black-tr-b" href="<?= SITE_DIR ?>personal/profiles/"><? echo GetMessage("SALE_RESET") ?></a>
                                            </div>

                                        </div>
                                    </form>
                                </div>
                            </div>
                            <?
                            //pr($arResult["ORDER_PROPS"][2]);
                            //pr($arResult["ORDER_PROPS_VALUES"]);
                            ?>
                            <!--<div class="user-ac-form-footer">
                                <input id="user-ac-delivery" class="button-link black-tr-b" value="Сохранить" type="submit">
                                <span class="user-ac-more-userdelivery">
                                                Добавить адрес
                                                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 42 42" style="enable-background:new 0 0 42 42;" xml:space="preserve">
                                                <polygon points="42,20 22,20 22,0 20,0 20,20 0,20 0,22 20,22 20,42 22,42 22,22 42,22 "></polygon></svg>
                                            </span>
                                <span class="user-ac-remove-userdelivery">
                                                Удалить адрес
                                            </span>
                            </div>-->
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <? $javascriptParams = array(
        "ajaxUrl" => CUtil::JSEscape($this->__component->GetPath() . '/ajax.php'),
    );
    $javascriptParams = CUtil::PhpToJSObject($javascriptParams);
    ?>
    <script>
        BX.message({
            SPPD_FILE_COUNT: '<?=Loc::getMessage('SPPD_FILE_COUNT')?>',
            SPPD_FILE_NOT_SELECTED: '<?=Loc::getMessage('SPPD_FILE_NOT_SELECTED')?>'
        });
        BX.Sale.PersonalProfileComponent.PersonalProfileDetail.init(<?=$javascriptParams?>);
    </script>
    <?
} else {
    header('Location: /personal/profiles/');
    ShowError($arResult["ERROR_MESSAGE"]);
}
?>

