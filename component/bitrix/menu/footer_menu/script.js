var wWindow = $(window).width();

if(wWindow <= 600){
    $('.footer-menu h4').click(function () {
        var foot = $(this).closest('.footer-menu');

        if(foot.hasClass('open')){
            foot.removeClass('open');
            foot.children('ul').slideUp(200);
        }
        else{
            foot.addClass('open');
            foot.siblings().children('ul').slideUp(200);
            foot.siblings().removeClass('open');
            foot.children('ul').slideDown(200);
        }
    });
    $('<li>', {
        text: 'Показать еще',
        class: 'btn--tag',
        on: {
            click: function(event){
                if(!$(this).hasClass('open')){
                    $(this).addClass('open')
                    $(this).closest('ul').addClass('open');
                    $(this).text('Скрыть');
                }
                else{
                    $(this).removeClass('open')
                    $(this).closest('ul').removeClass('open');
                    $(this).text('Показать еще');
                }
            },
        },
    }).appendTo('.cat-tags-list');
}