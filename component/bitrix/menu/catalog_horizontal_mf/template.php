<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);

if (empty($arResult["ALL_ITEMS"]))
	return;

CUtil::InitJSCore();

if (file_exists($_SERVER["DOCUMENT_ROOT"].$this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css'))
	$APPLICATION->SetAdditionalCSS($this->GetFolder().'/themes/'.$arParams["MENU_THEME"].'/colors.css');

$menuBlockId = "catalog_menu_".$this->randString();

//pr($arResult);
//pr($arResult["ALL_ITEMS"]);
//pr($arResult["MENU_STRUCTURE"]);

//foreach ($arResult as $k=>$v)
//{
//    pr($k);
//}

//pr(SITE_DIR);
$arColumnsMatrix = [
    SITE_DIR . 'catalog/zhenshchinam/' => [
        1 => [
            SITE_DIR . 'catalog/zhenshchinam/mf-life/',
        ],
        2 => [
            SITE_DIR . 'catalog/zhenshchinam/mf-home/',
        ],
        3 => [
            SITE_DIR . 'catalog/zhenshchinam/mf-sport/',
        ],
        4 => [
            SITE_DIR . 'catalog/zhenshchinam/nizhnee-bele/',
        ],
        5 => [
            SITE_DIR . 'catalog/zhenshchinam/plyazhnaya-kollektsiya/',
        ],
        6 => [
            SITE_DIR . 'catalog/zhenshchinam/mf-zima/',
            SITE_DIR . 'catalog/zhenshchinam/noski-i-kolgotki/',
            SITE_DIR . 'catalog/zhenshchinam/obuv/',
            SITE_DIR . 'catalog/zhenshchinam/rezinki-dlya-volos/',
            SITE_DIR . 'catalog/zhenshchinam/prochee/',
            '',
        ],
    ]
];

$arColumnsMatrixDetyam = [
        SITE_DIR . 'catalog/detyam/devochkam/' => [
                1 => [
                    SITE_DIR . 'catalog/detyam/devochkam/mf-life/',
                ],
                2 => [
	                SITE_DIR . 'catalog/detyam/devochkam/mf-home/',
                    SITE_DIR . 'catalog/detyam/devochkam/nizhnee-bele/',
                    ''
                ],
                3 => [
                    SITE_DIR . 'catalog/detyam/devochkam/plyazhnaya-kollektsiya/',
                    SITE_DIR . 'catalog/detyam/devochkam/mf-zima/',
                ],
                4 => [
                    SITE_DIR . 'catalog/detyam/noski-detskie/',
                    SITE_DIR . 'catalog/detyam/kolgotki-detskie/',
                ],
                5 => [
//                    SITE_DIR . 'catalog/detyam/devochkam/shkola/',
                    SITE_DIR . 'catalog/detyam/devochkam/po-vozrastu/',
	                SITE_DIR . 'catalog/detyam/leginsy-detskie/'
                ],
        ],
        SITE_DIR . 'catalog/detyam/malchikam/' => [
	        1 => [
		        SITE_DIR . 'catalog/detyam/malchikam/mf-life/',
	        ],
	        2 => [
		        SITE_DIR . 'catalog/detyam/malchikam/mf-home/',
		        SITE_DIR . 'catalog/detyam/malchikam/nizhnee-bele/',
	        ],
	        3 => [
		        SITE_DIR . 'catalog/detyam/malchikam/plyazhnaya-kollektsiya/',
		        SITE_DIR . 'catalog/detyam/malchikam/mf-zima/',
	        ],
	        4 => [
		        SITE_DIR . 'catalog/detyam/noski-detskie/',
		        SITE_DIR . 'catalog/detyam/kolgotki-detskie/',
	        ],
	        5 => [
//		        SITE_DIR . 'catalog/detyam/malchikam/shkola/',
		        SITE_DIR . 'catalog/detyam/malchikam/po-vozrastu/',
		        SITE_DIR . 'catalog/detyam/leginsy-detskie/'
	        ],
        ],
];

$arNoskiLegginsy = [
    SITE_DIR . 'catalog/detyam/kolgotki-detskie/',
    SITE_DIR . 'catalog/detyam/leginsy-detskie/',
    SITE_DIR . 'catalog/detyam/noski-detskie/',
];

$arMenuDetyamAppend = [];

//pr($arResult);
?><nav class="top-menu">
    <div class="fix-block">
        <ul class="mf-top-menu-list">
	        <?foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns) { ?><!--<?//pr($arResult["ALL_ITEMS"][$itemID]);?>-->
                <? //if($arResult["ALL_ITEMS"][$itemID]["LINK"] == SITE_DIR . 'catalog/podpiska-na-noski/') continue; ?>
                <?if(SITE_ID == 's1'):?>
                    <li class="mf-top-menu-el" <?= (is_array($arColumns) && count($arColumns) > 0)?'data-id="'. $itemID .'"':'' ?>>
                        <a href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemID]["SELECTED"]?' class="active"':'' ?>><?=($arResult["ALL_ITEMS"][$itemID]["TEXT"])?></a>
                    </li>
                <?else:?>
                    <?if($arResult["ALL_ITEMS"][$itemID]["ITEM_INDEX"] != "188"):?>
                        <li class="mf-top-menu-el" <?= (is_array($arColumns) && count($arColumns) > 0)?'data-id="'. $itemID .'"':'' ?>>
                            <a href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemID]["SELECTED"]?' class="active"':'' ?>><?=($arResult["ALL_ITEMS"][$itemID]["TEXT"])?></a>
                        </li>
                    <?endif;?>
                <?endif;?>
            <? } ?>
        </ul>
    </div>

	<? foreach($arResult["MENU_STRUCTURE"] as $itemID => $arColumns) { ?>
        <? $isChildren = $isNew = false;

		if(strpos($arResult["ALL_ITEMS"][$itemID]["LINK"], 'detyam'))
		{
			$isChildren = true;
		}

		if(strpos($arResult["ALL_ITEMS"][$itemID]["LINK"], 'novinki') || strpos($arResult["ALL_ITEMS"][$itemID]["LINK"], 'rasprodazha'))
		{
			$isNew = true;
		}


//		pr($arResult["ALL_ITEMS"][$itemID]);
        ?>
		<?if (is_array($arColumns) && count($arColumns) > 0 && $itemID !== 2010804290) { ?>
			<? //$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"] || $arResult["ALL_ITEMS"][$itemID]["PARAMS"]["description"]) ? true : false;?>
			<?$existPictureDescColomn = ($arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"]) ? true : false;?>
            <div class="mf-top-sublists-container<?= $isChildren?' mf-top-sublists-container--children':'' ?>" data-id="<?= $itemID ?>">
                <div class="fix-block">
                    <div class="scroll_menu<?=$isChildren||$isNew?' scroll_menu-big_razdel':'' ?><?=$isChildren?' scroll_menu-big_razdel--children':'' ?><?= $isNew?' scroll_menu-big_razdel--4col scroll_menu-big_razdel--new':'' ?><?= $existPictureDescColomn?' scroll_menu-big_razdel--4col-img':'' ?>">
                        <div class="razdel_menu razdel_menu--all">
                            <div>
                                <div class="razdel_menu-zag">
                                    <a href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemID]["SELECTED"]?' class="active"':'' ?>>Все</a>
                                </div>
                            </div>
                        </div>
	                    <? if(empty($arColumnsMatrix[$arResult["ALL_ITEMS"][$itemID]["LINK"]])) { ?>
                            <?
                            if($isChildren)
                            {
	                            foreach($arColumns as $key=>$arRow)
	                            {
		                            foreach($arRow as $itemIdLevel_2=>$arLevel_3)
		                            {
			                            if(in_array($arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"], $arNoskiLegginsy))
			                            {
				                            $arMenuDetyamAppend[$itemIdLevel_2] = $arLevel_3;
				                            $l3 = $l4 = [];

				                            if (is_array($arLevel_3) && count($arLevel_3) > 0)
				                            {
					                            foreach($arLevel_3 as $itemIdLevel_3_Key=>$itemIdLevel_3)
					                            {
						                            if (is_array($itemIdLevel_3) && count($itemIdLevel_3) > 0)
						                            {
							                            foreach ($itemIdLevel_3 as $itemIdLevel_4)
							                            {
								                            $l4[] = $itemIdLevel_4;
							                            }
						                            }

						                            if (!empty($l4)) $l3[$itemIdLevel_3_Key] = $l4;
					                            }
				                            }
			                            }
		                            }
                                }
                            }

                            ?>
		                    <? foreach($arColumns as $key=>$arRow) { ?>
			                    <? if(!$isChildren&&!$isNew) { ?>
                                    <? // стандартный раздел ?>
				                    <?foreach($arRow as $itemIdLevel_2=>$arLevel_3) { ?>
                                        <div class="razdel_menu">
                                            <div>
                                                <div class="razdel_menu-zag">
                                                    <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]?' class="active"':'' ?>><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a>
                                                </div>

							                    <?if (is_array($arLevel_3) && count($arLevel_3) > 0) { ?>
                                                    <ul>
									                    <?foreach($arLevel_3 as $itemIdLevel_3_Key=>$itemIdLevel_3) { ?>
										                    <?$k = $itemIdLevel_3_Key; ?>
                                                            <li>
                                                                <a href="<?=$arResult["ALL_ITEMS"][$k]["LINK"]?>" class="srtd a1 <?= $k ?><?= $arResult["ALL_ITEMS"][$k]["SELECTED"]?' active':'' ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></a>
											                    <? /*if (is_array($itemIdLevel_3) && count($itemIdLevel_3) > 0) {  ?>
                                                                    <ul>
													                    <? foreach ($itemIdLevel_3 as $itemIdLevel_4) { ?>
                                                                            <li>
                                                                                <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["TEXT"]?></a>
                                                                            </li>
													                    <? } ?>
                                                                    </ul>
											                    <? }*/ ?>
                                                            </li>
									                    <? } ?>
                                                    </ul>
							                    <? } ?>
                                            </div>
                                        </div>
				                    <? } ?>
			                    <? } elseif($isNew) { ?>
                                    <? //Новинки или Распродажа  ?>
				                    <?foreach($arRow as $itemIdLevel_2=>$arLevel_3) { ?>
                                        <div class="big_razdel">
                                            <div class="big_razdel-zag">
                                                <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]?' class="active"':'' ?>><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a>
                                            </div>

						                    <?if (is_array($arLevel_3) && count($arLevel_3) > 0) { ?>
							                    <?foreach($arLevel_3 as $itemIdLevel_3_Key=>$itemIdLevel_3) { ?>
								                    <?$k = $itemIdLevel_3_Key; ?>
                                                    <div class="razdel_menu">
                                                        <div>
                                                            <div class="razdel_menu-zag">
                                                                <a href="<?=$arResult["ALL_ITEMS"][$k]["LINK"]?>" class="a1 <?= $k ?><?= $arResult["ALL_ITEMS"][$k]["SELECTED"]?' active':'' ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></a>
                                                            </div>
										                    <?if (is_array($itemIdLevel_3) && count($itemIdLevel_3) > 0) {  ?>
                                                                <ul>
												                    <? foreach ($itemIdLevel_3 as $itemIdLevel_4) { ?>
                                                                        <li>
                                                                            <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemIdLevel_4]["SELECTED"]?' class="active"':'' ?>><?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["TEXT"]?></a>
                                                                        </li>
												                    <? } ?>
                                                                </ul>
										                    <? } ?>
                                                        </div>
                                                    </div>
							                    <? } ?>
						                    <? } ?>
                                        </div>
				                    <? } ?>
			                    <? } else { ?>
                                    <? // Детям ?>
				                    <? foreach($arRow as $itemIdLevel_2=>$arLevel_3) { ?>
                                        <?if(in_array($arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"], $arNoskiLegginsy)) continue; ?>
                                        <div class="big_razdel">
                                            <div class="big_razdel-zag">
                                                <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]?' class="active"':'' ?>><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a>
                                            </div>
						                    <?if (is_array($arLevel_3) && count($arLevel_3) > 0) { ?>
							                    <? foreach ($arColumnsMatrixDetyam[$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]] as $arCol_) { ?>
                                                    <div class="razdel_menu">
	                                                    <? foreach($arCol_ as $itemUrl) { ?>
		                                                    <?foreach($arLevel_3 as $itemIdLevel_3_Key=>$itemIdLevel_3) { ?>
			                                                    <?
			                                                    $k = $itemIdLevel_3_Key;
			                                                    if($itemUrl != $arResult["ALL_ITEMS"][$k]["LINK"]) continue;
			                                                    ?>
                                                                <div class="menu-<?= $k ?>">
                                                                    <div class="razdel_menu-zag">
					                                                    <? if($arResult["ALL_ITEMS"][$k]["LINK"] == SITE_DIR . 'catalog/detyam/devochkam/po-vozrastu/' || $arResult["ALL_ITEMS"][$k]["LINK"] == SITE_DIR . 'catalog/detyam/malchikam/po-vozrastu/') { ?>
                                                                            <span class="a1 <?= $k ?><?= $arResult["ALL_ITEMS"][$k]["SELECTED"]?' active':'' ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></span>
					                                                    <? } else { ?>
                                                                            <a href="<?=$arResult["ALL_ITEMS"][$k]["LINK"]?>" class="a1 <?= $k ?><?= $arResult["ALL_ITEMS"][$k]["SELECTED"]?' active':'' ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></a>
					                                                    <? } ?>
                                                                    </div>
				                                                    <?if (is_array($itemIdLevel_3) && count($itemIdLevel_3) > 0) {  ?>
                                                                        <ul class="menu-ul-<?= $k ?>">
						                                                    <? foreach ($itemIdLevel_3 as $itemIdLevel_4) { ?>
                                                                                <li>
                                                                                    <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemIdLevel_4]["SELECTED"]?' class="active"':'' ?>><?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["TEXT"]?></a>
                                                                                </li>
						                                                    <? } ?>
                                                                        </ul>
				                                                    <? } ?>
                                                                </div>
		                                                    <? } ?>

		                                                    <? if (count($arMenuDetyamAppend) > 0) { ?>
			                                                    <?foreach($arMenuDetyamAppend as $itemIdLevel_3_Key=>$itemIdLevel_3) { ?>
				                                                    <?
                                                                    $k = $itemIdLevel_3_Key;
				                                                    if($itemUrl != $arResult["ALL_ITEMS"][$k]["LINK"]) continue;
				                                                    ?>
                                                                    <div class="menu-<?= $k ?>">
                                                                        <div class="razdel_menu-zag">
                                                                            <? if($arResult["ALL_ITEMS"][$k]["LINK"] == SITE_DIR . 'catalog/detyam/devochkam/po-vozrastu/' || $arResult["ALL_ITEMS"][$k]["LINK"] == SITE_DIR . 'catalog/detyam/malchikam/po-vozrastu/') { ?>
                                                                                <span class="a1 <?= $k ?><?= $arResult["ALL_ITEMS"][$k]["SELECTED"]?' active':'' ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></span>
                                                                            <? } else { ?>
                                                                                <a href="<?=$arResult["ALL_ITEMS"][$k]["LINK"]?>" class="a1 <?= $k ?><?= $arResult["ALL_ITEMS"][$k]["SELECTED"]?' active':'' ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></a>
                                                                            <? } ?>
                                                                        </div>
                                                                        <?if (is_array($itemIdLevel_3) && count($itemIdLevel_3) > 0) {  ?>
                                                                            <? //pr($itemIdLevel_3) ?>
                                                                            <ul class="menu-ul-<?= $k ?>">
                                                                                <? foreach ($itemIdLevel_3 as $itemIdLevel_4_Key=>$itemIdLevel_4) { ?>
                                                                                    <li>
                                                                                        <? if(!is_array($itemIdLevel_4)) { ?>
                                                                                            <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemIdLevel_4]["SELECTED"]?' class="active"':'' ?>><?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["TEXT"]?></a>
                                                                                        <? } else { ?>
                                                                                            <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_4_Key]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemIdLevel_4]["SELECTED"]?' class="active"':'' ?>><?=$arResult["ALL_ITEMS"][$itemIdLevel_4_Key]["TEXT"]?></a>
                                                                                        <? } ?>
                                                                                    </li>
                                                                                <? } ?>
                                                                            </ul>
                                                                        <? } ?>
                                                                    </div>
			                                                    <? } ?>
		                                                    <? } ?>
	                                                    <? } ?>
                                                    </div>
							                    <? } ?>
						                    <? } ?>

                                        </div>
				                    <? } /**/ ?>

				                    <? /*foreach($arRow as $itemIdLevel_2=>$arLevel_3) { ?>
                                        <?if(in_array($arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"], $arNoskiLegginsy)) continue; ?>
                                        <div class="big_razdel">
                                            <div class="big_razdel-zag">
                                                <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a>
                                            </div>

						                    <?if (is_array($arLevel_3) && count($arLevel_3) > 0) { ?>
							                    <?foreach($arLevel_3 as $itemIdLevel_3_Key=>$itemIdLevel_3) { ?>
								                    <?$k = $itemIdLevel_3_Key; ?>
                                                    <div class="razdel_menu">
                                                        <div>
                                                            <div class="razdel_menu-zag">
											                    <? if($arResult["ALL_ITEMS"][$k]["LINK"] == SITE_DIR . 'catalog/detyam/devochkam/po-vozrastu/' || $arResult["ALL_ITEMS"][$k]["LINK"] == SITE_DIR . 'catalog/detyam/malchikam/po-vozrastu/') { ?>
                                                                    <span class="a1 <?= $k ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></span>
											                    <? } else { ?>
                                                                    <a href="<?=$arResult["ALL_ITEMS"][$k]["LINK"]?>" class="a1 <?= $k ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></a>
											                    <? } ?>
                                                            </div>
										                    <?if (is_array($itemIdLevel_3) && count($itemIdLevel_3) > 0) {  ?>
                                                                <ul>
												                    <? foreach ($itemIdLevel_3 as $itemIdLevel_4) { ?>
                                                                        <li>
                                                                            <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["TEXT"]?></a>
                                                                        </li>
												                    <? } ?>
                                                                </ul>
										                    <? } ?>
                                                        </div>
                                                    </div>
							                    <? } ?>
						                    <? } ?>

						                    <?if (count($arMenuDetyamAppend) > 0) { ?>
							                    <?foreach($arMenuDetyamAppend as $itemIdLevel_3_Key=>$itemIdLevel_3) { ?>
								                    <?$k = $itemIdLevel_3_Key; ?>
                                                    <div class="razdel_menu">
                                                        <div>
                                                            <div class="razdel_menu-zag">
											                    <? if($arResult["ALL_ITEMS"][$k]["LINK"] == SITE_DIR . 'catalog/detyam/devochkam/po-vozrastu/' || $arResult["ALL_ITEMS"][$k]["LINK"] == SITE_DIR . 'catalog/detyam/malchikam/po-vozrastu/') { ?>
                                                                    <span class="a1 <?= $k ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></span>
											                    <? } else { ?>
                                                                    <a href="<?=$arResult["ALL_ITEMS"][$k]["LINK"]?>" class="a1 <?= $k ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></a>
											                    <? } ?>
                                                            </div>
										                    <?if (is_array($itemIdLevel_3) && count($itemIdLevel_3) > 0) {  ?>
                                                                <? //pr($itemIdLevel_3) ?>
                                                                <ul>
												                    <? foreach ($itemIdLevel_3 as $itemIdLevel_4_Key=>$itemIdLevel_4) { ?>
                                                                        <li>
                                                                            <? if(!is_array($itemIdLevel_4)) { ?>
                                                                                <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["TEXT"]?></a>
                                                                            <? } else { ?>
                                                                                <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_4_Key]["LINK"]?>"><?=$arResult["ALL_ITEMS"][$itemIdLevel_4_Key]["TEXT"]?></a>
                                                                            <? } ?>
                                                                        </li>
												                    <? } ?>
                                                                </ul>
										                    <? } ?>
                                                        </div>
                                                    </div>
							                    <? } ?>
						                    <? } ?>


                                        </div>
				                    <? } /**/ ?>


			                    <? } ?>
		                    <? } ?>
	                    <? } else { ?>
		                    <? foreach ($arColumnsMatrix[$arResult["ALL_ITEMS"][$itemID]["LINK"]] as $arCol_) { ?>
                                <div class="razdel_menu">

					                    <? //pr($arCol_) ?>
					                    <? foreach($arCol_ as $itemUrl) { ?>
						                    <? //pr($itemUrl); ?>
						                    <? foreach($arColumns as $key=>$arRow) { ?>
						                    <? foreach($arRow as $itemIdLevel_2=>$arLevel_3) { ?>
							                    <?
							                    //pr($arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"] . ' | ' . $arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]);
							                    ?>
							                    <? if($itemUrl != $arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]) continue ?>
                                                    <div>
                                                <div class="razdel_menu-zag">
                                                    <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["LINK"]?>" <?= $arResult["ALL_ITEMS"][$itemIdLevel_2]["SELECTED"]?'class="active"':'' ?>><?=$arResult["ALL_ITEMS"][$itemIdLevel_2]["TEXT"]?></a>
                                                </div>

							                    <?if (is_array($arLevel_3) && count($arLevel_3) > 0) { ?>
                                                    <ul>
									                    <?foreach($arLevel_3 as $itemIdLevel_3_Key=>$itemIdLevel_3) { ?>
										                    <?
                                                            $k = $itemIdLevel_3_Key;
                                                            $hasChildL3 = (is_array($itemIdLevel_3) && count($itemIdLevel_3) > 0 && strpos($arResult["ALL_ITEMS"][$k]["LINK"],'noski-zhenskie')!== false);
                                                            ?>
                                                            <li <?=$hasChildL3?'class="hasChildL3"':''?>>
                                                                <a href="<?=$arResult["ALL_ITEMS"][$k]["LINK"]?>" class="a1 <?= $k ?><?= $hasChildL3?' js-parentL3 parentL3':''?><?= $arResult["ALL_ITEMS"][$k]["SELECTED"]?' active':'' ?>"><?=$arResult["ALL_ITEMS"][$k]["TEXT"]?></a>
											                    <? if ($hasChildL3) {  ?>
                                                                    <ul>
													                    <? foreach ($itemIdLevel_3 as $itemIdLevel_4) { ?>
                                                                            <li>
                                                                                <a href="<?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["LINK"]?>"<?= $arResult["ALL_ITEMS"][$itemIdLevel_4]["SELECTED"]?' class="active"':'' ?>><?=$arResult["ALL_ITEMS"][$itemIdLevel_4]["TEXT"]?></a>
                                                                            </li>
													                    <? } ?>
                                                                    </ul>
											                    <? } ?>
                                                            </li>
									                    <? } ?>
                                                    </ul>
							                    <? } ?>
                                                    </div>
						                    <? } ?>
						                    <? } ?>
					                    <? } ?>

                                </div>
		                    <? } ?>
	                    <? } ?>



                        <?if ($existPictureDescColomn) { ?>
                            <div class="mf-sl-background-container">
                                <a class="mf-sl-background" href="<?=$arResult["ALL_ITEMS"][$itemID]["LINK"]?>" style="background-image:url(<?=$arResult["ALL_ITEMS"][$itemID]["PARAMS"]["picture_src"]?>);"></a>
                            </div>
                        <? } ?>
                    </div>
                </div>
            </div>
	    <? } ?>
	<? } ?>
</nav>


