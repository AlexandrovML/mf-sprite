<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;
use Bitrix\Main\Grid\Declension;
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);
//$this->addExternalCss('/bitrix/css/main/bootstrap.css');

$this->addExternalJs('//yastatic.net/es5-shims/0.0.2/es5-shims.min.js');
$this->addExternalJs('//yastatic.net/share2/share.js');

$templateLibrary = array('popup', 'fx', 'ajax');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer)
	{
		if ($offer['MORE_PHOTO_COUNT'] > 1)
		{
			$showSliderControls = true;
			break;
		}
	}
}
else
{
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}

//pr($arResult['OFFERS'][0]);

$arActualOffersIds = [];
$arActualOffersIdsForStore = [];
foreach ($arResult['OFFERS'] as $arOffer)
{
    if($arOffer['CAN_BUY'])
    {
	    $arActualOffersIds[] = $arOffer['ID'];
    }
    $arActualOffersIdsForStore[]=$arOffer['ID'];
}

$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;
//pr($actualItem);
$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

//pr($arResult);
//pr($actualItem['MORE_PHOTO']);

$arTagsUrls = [];
//foreach ($arResult['PROPERTIES']['TAGS']['VALUE'] as $tag) {
//    $arTagsUrls[] = '<a href="/tags/'.$tag.'/">#'.$tag.'</a>';
//}
foreach ($arResult['PROPERTIES']['TAGS']['DISPLAY_VALUE'] as $tag) {
    $arTagsUrls[] = '<a href="'.$tag['DETAIL_PAGE_URL'].'">#'.$tag['NAME'].'</a>';
}


$arSizes = [];

$arResult['CURRENT_DECOR'] = [];

//pr($arParams['OFFER_TREE_PROPS']);
//pr($arResult['SKU_PROPS']);

$showLbl1P1 = isShow1P1Label($arResult['DISPLAY_PROPERTIES']['TAGS']);

unset($arResult['DISPLAY_PROPERTIES']['TAGS']);

//var_dump($arResult['SKU_PRICE_IS_CHANGING']);
//pr($arParams['SHOW_OLD_PRICE']);

$reviewsDeclension = new Declension('отзыв', 'отзыва', 'отзывов');
?>

<div id="<?=$itemIds['ID']?>" itemscope itemtype="http://schema.org/Product">

        <div class="mf-product-info-column-1">

            <div class="mf-product-gallery">
                <div id="mf-product-gallery-list" class="mf-product-gallery-list">
                    <? foreach ($actualItem['MORE_PHOTO'] as $photo) { ?>
                        <div class="mf-product-img selected" style="background-image: url(<?= $photo['SRC'] ?>);"></div>
                    <? } ?>
                </div>
                <div class="mf-product-gallery-image">
	                <? if ($showLbl1P1) { ?>
                        <img class="mf-cat-product__label mf-cat-product__label--1p1" src="<?= SITE_TEMPLATE_PATH ?>/images/labels/action-1plus1.svg" alt="action-1plus1">
	                <? } ?>

	                <? if (!empty($arResult['PROPERTIES']['TAGS']['VALUE']) && in_array(TAG_ID_HIT_PRODAZH, $arResult['PROPERTIES']['TAGS']['VALUE'])) { ?>
                        <span class="mf-cat-product__label-text mf-cat-product__label-text--hit"><?= Loc::getMessage('MESS_HIT_PRODAZH') ?></span>
	                <? } ?>

					<link itemprop="image" href="<?= $actualItem['MORE_PHOTO'][0]['SRC'] ?>" />
                    <div class="mf-gallery-current-img" style="background-image: url(<?= $actualItem['MORE_PHOTO'][0]['SRC'] ?>);"></div>
                </div>
            </div>

            <div class="mf-product-description">
                <div class="mf-pr-desc-tabs">
                    <div class="mf-pr-tab description-product selected">
                        Описание и состав
                    </div>
                    <div class="mf-pr-tab delivery-product">
                        Доставка и оплата
                    </div>
                    <div id="review-product" class="mf-pr-tab review-product not-visible">
                        <?= count($arResult['REVIEWS']) . ' ' .  $reviewsDeclension->get(count($arResult['REVIEWS'])); ?>
                    </div>
                </div>

                <div class="mf-description-container mf-product-data-container selected" itemprop="description">
	                <? if (!empty($arResult['DISPLAY_PROPERTIES']) || $arResult['SHOW_OFFERS_PROPS']) { ?>
		                <?
		                if (!empty($arResult['DISPLAY_PROPERTIES']))
		                {
			                ?>
			                <?
			                foreach ($arResult['DISPLAY_PROPERTIES'] as $property)
			                {
                                ?>
                                <p class="mf-desc-text-tr"><?=$property['NAME']?>:</p>
                                <p class="without-margin-top"><?=(is_array($property['DISPLAY_VALUE']) ? implode(' / ', $property['DISPLAY_VALUE']) : $property['DISPLAY_VALUE'])?></p>
                                <?
			                }
			                unset($property);
			                ?>
			                <?
		                }

		                if ($arResult['SHOW_OFFERS_PROPS'])
		                {
			                ?>
                            <div id="<?=$itemIds['DISPLAY_MAIN_PROP_DIV']?>"></div>
			                <?
		                }
		                ?>
	                <? } ?>

                    <? if ($showDescription) { ?>
                        <p class="mf-desc-text-tr more-margin-top">Описание:</p>
                        <?
	                    if (
		                    $arResult['PREVIEW_TEXT'] != ''
		                    && (
			                    $arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'S'
			                    || ($arParams['DISPLAY_PREVIEW_TEXT_MODE'] === 'E' && $arResult['DETAIL_TEXT'] == '')
		                    )
	                    )
	                    {
		                    echo $arResult['PREVIEW_TEXT_TYPE'] === 'html' ? $arResult['PREVIEW_TEXT'] : '<p>'.$arResult['PREVIEW_TEXT'].'</p>';
	                    }

	                    if ($arResult['DETAIL_TEXT'] != '')
	                    {
		                    echo $arResult['DETAIL_TEXT_TYPE'] === 'html' ? $arResult['DETAIL_TEXT'] : '<p>'.$arResult['DETAIL_TEXT'].'</p>';
	                    }
	                    ?>
                    <? } ?>
                </div>

                <div class="mf-delivery-container mf-product-data-container">
                    <br>
                    <div class="h3">СПОСОБЫ ДОСТАВКИ</div>
	                <?$APPLICATION->IncludeComponent(
		                "bitrix:main.include",
		                "",
		                Array(
			                "AREA_FILE_SHOW" => "file",
			                "PATH" => SITE_DIR.($arResult['IS_SUBSCRIPTION_PRODUCT']?"include/catalog-element-shipping-subscription.php":"include/catalog-element-shipping.php"),
			                "AREA_FILE_RECURSIVE" => "N",
			                "EDIT_MODE" => "html",
		                ),
		                false,
		                Array('HIDE_ICONS' => 'N')
	                );?>
                    <br>
                    <br>
                    <div class="h3">СПОСОБЫ ОПЛАТЫ</div>
	                <?$APPLICATION->IncludeComponent(
		                "bitrix:main.include",
		                "",
		                Array(
			                "AREA_FILE_SHOW" => "file",
			                "PATH" => SITE_DIR.($arResult['IS_SUBSCRIPTION_PRODUCT']?"include/catalog-element-payment-subscription.php":"include/catalog-element-payment.php"),
			                "AREA_FILE_RECURSIVE" => "N",
			                "EDIT_MODE" => "html",
		                ),
		                false,
		                Array('HIDE_ICONS' => 'N')
	                );?>
                </div>


                <div id="mf-review-container" class="mf-review-container mf-product-data-container">

                    <?/*
                	<div class="review-item">
                		<div class="review-item__name">Николай</div>
                		<div class="review-item__rate">
                			<svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                		</div>
                		<div class="review-item__date">10 октября, 10:09</div>
                		<div class="review-item__text">Хорошая майка, сидит по фигуре, но было пару зацепок, но не особо видно из-за цвета. Цвет приятно мне нравится</div>
                		<div class="review-item__usfl">
                			<div>Отзыв полезен?</div>
                			<div class="review-item__usfl-like">
                				<svg class="review-item__usfl-icon" width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><path d="M0 12.4091H2.36364V5.31819H0V12.4091ZM13 5.90909C13 5.25909 12.4682 4.72728 11.8182 4.72728H8.09546L8.68636 2.00909C8.68636 1.95 8.68636 1.89091 8.68636 1.83182C8.68636 1.59546 8.56818 1.35909 8.45002 1.18182L7.8 0.590912L3.9 4.49091C3.66364 4.66819 3.54545 4.96364 3.54545 5.31819V11.2273C3.54545 11.8773 4.07727 12.4091 4.72727 12.4091H10.0455C10.5182 12.4091 10.9318 12.1136 11.1091 11.7L12.8818 7.50453C12.9409 7.38634 12.9409 7.20907 12.9409 7.09089V5.90907H13C13 5.96819 13 5.90909 13 5.90909Z"/></svg>
                				<span class="review-item__usfl-num">1</span>
                			</div>
                			<div class="review-item__usfl-dislike">
                				<svg class="review-item__usfl-icon" width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><path d="M0 0.590904H2.36364V7.68181H0V0.590904ZM13 7.09091C13 7.74091 12.4682 8.27272 11.8182 8.27272H8.09546L8.68636 10.9909C8.68636 11.05 8.68636 11.1091 8.68636 11.1682C8.68636 11.4045 8.56818 11.6409 8.45002 11.8182L7.8 12.4091L3.9 8.50909C3.66364 8.33181 3.54545 8.03636 3.54545 7.68181V1.77272C3.54545 1.12272 4.07727 0.590904 4.72727 0.590904H10.0455C10.5182 0.590904 10.9318 0.886359 11.1091 1.3L12.8818 5.49547C12.9409 5.61366 12.9409 5.79093 12.9409 5.90911V7.09093H13C13 7.03181 13 7.09091 13 7.09091Z"/></svg>
                				<span>1</span>
                			</div>
                		</div>
                	</div>
                	<div class="review-item">
                		<div class="review-item__name">Христофор Христафорович</div>
                		<div class="review-item__rate">
                			<svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                		</div>
                		<div class="review-item__date">9 октября 09:00</div>
                		<div class="review-item__text">Майка классная и телу приятная, но не нашлось подходящего для меня размеров</div>
                		<div class="review-item__usfl">
                			<div>Отзыв полезен?</div>
                			<div class="review-item__usfl-like">
                				<svg class="review-item__usfl-icon" width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><path d="M0 12.4091H2.36364V5.31819H0V12.4091ZM13 5.90909C13 5.25909 12.4682 4.72728 11.8182 4.72728H8.09546L8.68636 2.00909C8.68636 1.95 8.68636 1.89091 8.68636 1.83182C8.68636 1.59546 8.56818 1.35909 8.45002 1.18182L7.8 0.590912L3.9 4.49091C3.66364 4.66819 3.54545 4.96364 3.54545 5.31819V11.2273C3.54545 11.8773 4.07727 12.4091 4.72727 12.4091H10.0455C10.5182 12.4091 10.9318 12.1136 11.1091 11.7L12.8818 7.50453C12.9409 7.38634 12.9409 7.20907 12.9409 7.09089V5.90907H13C13 5.96819 13 5.90909 13 5.90909Z"/></svg>
                				<span class="review-item__usfl-num">0</span>
                			</div>
                			<div class="review-item__usfl-dislike">
                				<svg class="review-item__usfl-icon" width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><path d="M0 0.590904H2.36364V7.68181H0V0.590904ZM13 7.09091C13 7.74091 12.4682 8.27272 11.8182 8.27272H8.09546L8.68636 10.9909C8.68636 11.05 8.68636 11.1091 8.68636 11.1682C8.68636 11.4045 8.56818 11.6409 8.45002 11.8182L7.8 12.4091L3.9 8.50909C3.66364 8.33181 3.54545 8.03636 3.54545 7.68181V1.77272C3.54545 1.12272 4.07727 0.590904 4.72727 0.590904H10.0455C10.5182 0.590904 10.9318 0.886359 11.1091 1.3L12.8818 5.49547C12.9409 5.61366 12.9409 5.79093 12.9409 5.90911V7.09093H13C13 7.03181 13 7.09091 13 7.09091Z"/></svg>
                				<span>2</span>
                			</div>
                		</div>
                	</div>
                	<div class="review-item">
                		<div class="review-item__name">Святослав</div>
                		<div class="review-item__rate">
                			<svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                			<svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
                		</div>
                		<div class="review-item__date">8 октября 08:00</div>
                		<div class="review-item__text">Хорошая майка, сидит по фигуре, но было пару зацепок, но не особо видно из-за цвета. Цвет приятно мне нравится</div>
                		<div class="review-item__usfl">
                			<div>Отзыв полезен?</div>
                			<div class="review-item__usfl-like">
                				<svg class="review-item__usfl-icon" width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><path d="M0 12.4091H2.36364V5.31819H0V12.4091ZM13 5.90909C13 5.25909 12.4682 4.72728 11.8182 4.72728H8.09546L8.68636 2.00909C8.68636 1.95 8.68636 1.89091 8.68636 1.83182C8.68636 1.59546 8.56818 1.35909 8.45002 1.18182L7.8 0.590912L3.9 4.49091C3.66364 4.66819 3.54545 4.96364 3.54545 5.31819V11.2273C3.54545 11.8773 4.07727 12.4091 4.72727 12.4091H10.0455C10.5182 12.4091 10.9318 12.1136 11.1091 11.7L12.8818 7.50453C12.9409 7.38634 12.9409 7.20907 12.9409 7.09089V5.90907H13C13 5.96819 13 5.90909 13 5.90909Z"/></svg>
                				<span class="review-item__usfl-num">66</span>
                			</div>
                			<div class="review-item__usfl-dislike">
                				<svg class="review-item__usfl-icon" width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><path d="M0 0.590904H2.36364V7.68181H0V0.590904ZM13 7.09091C13 7.74091 12.4682 8.27272 11.8182 8.27272H8.09546L8.68636 10.9909C8.68636 11.05 8.68636 11.1091 8.68636 11.1682C8.68636 11.4045 8.56818 11.6409 8.45002 11.8182L7.8 12.4091L3.9 8.50909C3.66364 8.33181 3.54545 8.03636 3.54545 7.68181V1.77272C3.54545 1.12272 4.07727 0.590904 4.72727 0.590904H10.0455C10.5182 0.590904 10.9318 0.886359 11.1091 1.3L12.8818 5.49547C12.9409 5.61366 12.9409 5.79093 12.9409 5.90911V7.09093H13C13 7.03181 13 7.09091 13 7.09091Z"/></svg>
                				<span>999</span>
                			</div>
                		</div>
                	</div>
                    <?/**/?>
                	<div class="review-footer">
                        <? //prA($arParams['CAN_SEND_REVIEW']); ?>
                        <? if ($arParams['CAN_SEND_REVIEW'] == 'Y') { ?>
                		    <button class="write-review js-new-review-modal" type="button" data-mfp-src="/ajax/new-review.php?id=<?= $arResult['ID'] ?>">Оставить отзыв</button>
                        <? } else { ?>
                            <button class="write-review popup-modal-auth js-popup-modal-input" type="button" data-mfp-src="<?= SITE_DIR ?>ajax/auth.php">Оставить отзыв</button>
                        <? } ?>
                		<a class="rules-review" href="<?= SITE_DIR ?>pravila-publikatsii-otzyvov-i-voprosov/" target="_blank">Правила публикации отзывов</a>
                	</div>
                </div>
                <?/*
                <div class="review-modal__bg" style="z-index: 1000; background-color: rgba(0, 0, 0, 0.8); position: fixed; top: 0; bottom: 0; left: 0; right: 0;">
                	<div class="review-modal">
                		<div class="review-modal__header">
                			<span>Оставить отзыв</span>
                			<button class="review-modal__close">
                				<svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17"><path d="M16.071,17.485L9,10.414,1.929,17.485,0.515,16.071,7.586,9,0.515,1.929,1.929,0.515,9,7.586l7.071-7.071,1.414,1.414L10.414,9l7.071,7.071Z"/></svg>
                			</button>
                		</div>
                		<div class="review-modal__item">
                			<span>Оцените товар</span>
                			<div class="review-item__rate">
	                			<svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
	                			<svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
	                			<svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
	                			<svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
	                			<svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
	                		</div>
                		</div>
                		<div class="review-modal__item">
                			<span>Оставьте свой комментарий</span>
                			<textarea placeholder="Что вам понравилось в этом товаре? Кому бы вы его рекомендовали?"></textarea>
                		</div>
                		<input class="review-modal__submit" type="submit">
                	</div>
                </div>
                <div class="review-modal__bg" style="z-index: 1000; background-color: rgba(0, 0, 0, 0.8); position: fixed; top: 0; bottom: 0; left: 0; right: 0;">
                	<div class="review-modal review-modal-msg">
                		<div class="review-modal__header">Спасибо за отзыв</div>
                		<div class="review-modal__item">Ваш отзыв принят и будет опубликован после модерации</div>
                		<button class="write-review">Ок</button>
                	</div>
                </div>
                */?>

            </div>
        </div>

        <div class="mf-product-info-column-2">
            <div class="mf-header-product">
            <?
            if(SITE_ID == 's2'){
                $arResult['SECTION']['SECTION_PAGE_URL'] = str_replace('/ru//ru/', '/ru/', $arResult['SECTION']['SECTION_PAGE_URL']);
            }elseif(SITE_ID == 's3'){
                $arResult['SECTION']['SECTION_PAGE_URL'] = str_replace('/kz//kz/', '/kz/', $arResult['SECTION']['SECTION_PAGE_URL']);
            }else{
                $arResult['SECTION']['SECTION_PAGE_URL'] = $arResult['SECTION']['SECTION_PAGE_URL'];
            }
            //pr($arResult['SECTION']['SECTION_PAGE_URL']);
            ?>

                <a href="<?=/* getDomainUrl() .*/ $arResult['SECTION']['SECTION_PAGE_URL'] ?>" class="mf-back-from-product"><svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 492 492" style="enable-background:new 0 0 492 492;" xml:space="preserve"><path d="M198.608,246.104L382.664,62.04c5.068-5.056,7.856-11.816,7.856-19.024c0-7.212-2.788-13.968-7.856-19.032l-16.128-16.12C361.476,2.792,354.712,0,347.504,0s-13.964,2.792-19.028,7.864L109.328,227.008c-5.084,5.08-7.868,11.868-7.848,19.084c-0.02,7.248,2.76,14.028,7.848,19.112l218.944,218.932c5.064,5.072,11.82,7.864,19.032,7.864c7.208,0,13.964-2.792,19.032-7.864l16.124-16.12c10.492-10.492,10.492-27.572,0-38.06L198.608,246.104z"/></svg> назад</a>
                <h1 class="mf-product-title"><?=$name?></h1>

                <div id="reviews-preview-wr"></div>

                <ul class="mf-product-atts">
                    <li class="mf-product-att-model">Модель : <span><?= $arResult['PROPERTIES']['ARTNUMBER']['VALUE'] ?></span></li>
                    <? if(!empty($arTagsUrls)) { ?>
                        <li class="mf-product-att-tags">Теги : <span><?= implode(', ', $arTagsUrls) ?></span></li>

                    <? } ?>
                </ul>
            </div>
            <div class="mf-body-product">

                <? if(!$arResult['IS_SUBSCRIPTION_PRODUCT']) { ?>
                    <? if($arResult['PRINT_MIN_SKU_PRICE'] != $arResult['PRINT_MAX_SKU_PRICE'] && !$arResult['SKU_PRICE_IS_CHANGING']) { ?>
                        <div class="mf-product-defining-attr">
                            <div class="h3">В зависимости от цвета цена:</div>
                            <p><?= $arResult['PRINT_MIN_SKU_PRICE'] ?> - <?= $arResult['PRINT_MAX_SKU_PRICE'] ?></p>
                        </div>
                    <? } ?>

                    <? if($arResult['IBLOCK_SECTION_ID'] != CERTIFICATES_SECTION_ID) { ?>
                        <div class="mf-product-attr-option mf-product-color">
                            <div class="h3">Цвет: <span><?= $arResult['CURRENT_COLOR'] ?></span></div>
                            <? if(!$arResult['SKU_PRICE_IS_CHANGING']) { ?>
                                <? foreach ($arResult['arPricesColors'] as $priceVal=>$colors) { ?>
                                    <?
//                                    var_dump($showDiscount);
                                    //pr($showDiscount);
                                    ?>

                                    <p class="price">
                                        <? if ($showDiscount) { ?>
                                            <span class="discount" style="padding-right: 7px;"><?= $price['PRINT_RATIO_BASE_PRICE'] ?></span>
                                        <? } ?>
                                        <?= $price['PRINT_RATIO_PRICE'] ?>
                                    </p>
                                    <ul id="1" class="mf-product-colorlist">
                                        <? foreach ($colors as $color) { ?>
                                            <?
                                            if($color['UF_FILE'] != '' && $color['UF_FILE'] != '0')
                                            {
                                                $style = 'background: url('.$color['UF_FILE'].');background-size:contain;';
                                            }
                                            elseif(!empty($color['UF_COLOR_CODE']))
                                            {
                                                $style = 'background: ' . $color['UF_COLOR_CODE'] . ';background-size:contain;';
                                            }
                                            else
                                            {
                                                $style = 'background-size:contain;';
                                            }
                                            ?>
                                            <li>
                                                <? if(!empty($color['DETAIL_PAGE_URL'])) { ?>
                                                    <a href="<?= $color['DETAIL_PAGE_URL'] ?>" class="mf-color-product" title="<?= $color['UF_NAME'] ?>" style="<?= $style ?>" <?/*data-product="<?= $color['PRODUCT_ID'] ?>"*/?>></a>
                                                <? } else { ?>
                                                    <div class="mf-color-product selected" title="<?= $color['UF_NAME'] ?>" style="<?= $style ?>" <?/*data-product="<?= $color['PRODUCT_ID'] ?>"*/?>></div>
                                                <? } ?>
                                            </li>
                                        <? } ?>
                                    </ul>
                                <? }
                                ?>
                            <? } else { ?>
                                <ul id="2" class="mf-product-colorlist">
                                    <? foreach ($arResult['arPricesColors'] as $priceVal=>$colors) { ?>
                                        <? foreach ($colors as $color) { ?><!--<?//pr($color);?>-->
                                            <?
                                            if($color['UF_FILE'] != '' && $color['UF_FILE'] != '0')
                                            {
                                                $style = 'background: url('.$color['UF_FILE'].');background-size:contain;';
                                            }
                                            elseif(!empty($color['UF_COLOR_CODE']))
                                            {
                                                $style = 'background: ' . $color['UF_COLOR_CODE'] . ';background-size:contain;';
                                            }
                                            else
                                            {
                                                $style = 'background-size:contain;';
                                            }
                                            ?>
                                            <li>
                                                <? if(!empty($color['DETAIL_PAGE_URL'])) { ?>
                                                    <a href="<?= $color['DETAIL_PAGE_URL'] ?>" class="mf-color-product" title="<?= $color['UF_NAME'] ?>" style="<?= $style ?>" <?/*data-product="<?= $color['PRODUCT_ID'] ?>"*/?>></a>
                                                <? } else { ?>
                                                    <div class="mf-color-product selected" title="<?= $color['UF_NAME'] ?>" style="<?= $style ?>" <?/*data-product="<?= $color['PRODUCT_ID'] ?>"*/?>></div>
                                                <? } ?>
                                            </li>
                                        <? } ?>
                                    <? } ?>
                                </ul>
                            <? } ?>

                        </div>
                    <? } ?>

                    <? if(!empty($arResult['CURRENT_DECOR'])) { ?>
                        <div class="mf-product-attr-option mf-product-color">
                            <div class="h3">Рисунок: <span><?= $arResult['CURRENT_DECOR'] ?></span></div>
                            <? if(!$arResult['SKU_PRICE_IS_CHANGING']) { ?>
                                <? foreach ($arResult['arPricesDecors'] as $priceVal=>$colors) { ?>
                                    <p class="price"><?= $priceVal ?></p>
                                    <ul id="3" class="mf-product-colorlist">
                                        <? foreach ($colors as $color) { ?>
                                            <?
                                            if($color['UF_FILE'] != '' && $color['UF_FILE'] != '0')
                                            {
                                                $style = 'background: url('.$color['UF_FILE'].');background-size:contain;color:blue;';
                                            }
                                            elseif(!empty($color['UF_COLOR_CODE']))
                                            {
                                                $style = 'background: ' . $color['UF_COLOR_CODE'] . ';background-size:contain;color:red;';
                                            }
                                            else
                                            {
                                                $style = 'background-size:contain;';
                                            }
                                            ?>
                                            <li>
                                                <? if(!empty($color['DETAIL_PAGE_URL'])) { ?>
                                                    <a href="<?= $color['DETAIL_PAGE_URL'] ?>" class="mf-color-product" title="<?= $color['UF_NAME'] ?>" style="<?= $style ?>" <?/*data-product="<?= $color['PRODUCT_ID'] ?>"*/?>></a>
                                                <? } else { ?>
                                                    <div class="mf-color-product selected" title="<?= $color['UF_NAME'] ?>" style="<?= $style ?>" <?/*data-product="<?= $color['PRODUCT_ID'] ?>"*/?>></div>
                                                <? } ?>
                                            </li>
                                        <? } ?>
                                    </ul>
                                <? } ?>
                            <? } else { ?>
                                <ul class="mf-product-colorlist">
                                    <? foreach ($arResult['arPricesDecors'] as $priceVal=>$colors) { ?>
                                        <? foreach ($colors as $color) { ?>
                                            <?
                                            if($color['UF_FILE'] != '' && $color['UF_FILE'] != '0')
                                            {
                                                $style = 'background: url('.$color['UF_FILE'].');background-size:contain;';
                                            }
                                            elseif(!empty($color['UF_COLOR_CODE']))
                                            {
                                                $style = 'background: ' . $color['UF_COLOR_CODE'] . ';background-size:contain;';
                                            }
                                            else
                                            {
                                                $style = 'background-size:contain;';
                                            }
                                            ?>
                                            <li>
                                                <? if(!empty($color['DETAIL_PAGE_URL'])) { ?>
                                                    <a href="<?= $color['DETAIL_PAGE_URL'] ?>" class="mf-color-product" title="<?= $color['UF_NAME'] ?>" style="<?= $style ?>" <?/*data-product="<?= $color['PRODUCT_ID'] ?>"*/?>></a>
                                                <? } else { ?>
                                                    <div class="mf-color-product selected" title="<?= $color['UF_NAME'] ?>" style="<?= $style ?>" <?/*data-product="<?= $color['PRODUCT_ID'] ?>"*/?>></div>
                                                <? } ?>

                                            </li>
                                        <? } ?>
                                    <? } ?>
                                </ul>
                            <? } ?>
                        </div>
                    <? } ?>
                <? } ?>


	            <? if ($haveOffers && !empty($arResult['OFFERS_PROP'])) { ?>
                    <?
		            $hideSkuCnt = false;
                    if (count($arResult['SKU_PROPS']) == 1)
                    {
                        reset($arResult['SKU_PROPS']);
	                    $SKU_PROP = current($arResult['SKU_PROPS']);
	                    if($SKU_PROP['CODE'] == 'EVRO_RAZMER' && count($SKU_PROP['VALUES']) == 2)
                        {
                            reset($SKU_PROP['VALUES']);
                            $firstValue = current($SKU_PROP['VALUES']);
                            if($firstValue['ID'] == '1834')
                            {
                                $hideSkuCnt = true;
                            }
                        }
                    }
                    ?>
                    <div class="js-sku-container" id="<?=$itemIds['TREE_ID']?>"<?= $hideSkuCnt?' style="display:none;"':'' ?>>
			            <?
			            foreach ($arResult['SKU_PROPS'] as $skuProperty)
			            {
				            if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
					            continue;

//				            pr($skuProperty['CODE']);

				            if($skuProperty['CODE'] == 'SIZES_CLOTHES')
				            {
					            $arSizes = $skuProperty;
				            }

//					            pr($skuProperty['CODE']);

				            $propertyId = $skuProperty['ID'];
				            $skuProps[] = array(
					            'ID' => $propertyId,
					            'SHOW_MODE' => $skuProperty['SHOW_MODE'],
					            'VALUES' => $skuProperty['VALUES'],
					            'VALUES_COUNT' => $skuProperty['VALUES_COUNT']
				            );

				            switch ($skuProperty['CODE'])
                            {
                                case 'COLOR_REF':
	                                include '_product-item-scu-container--color_ref.php';
                                    break;

					            case 'PERIOD_PODPISKI':
						            include '_product-item-scu-container--subscription_period.php';
						            break;

                                case 'NOMINALNAYA_STOIMOST':
                                case 'SIZES_CLOTHES':
                                case 'EVRO_RAZMER':
	                                include '_product-item-scu-container--sizes_clothes.php';
                                    break;

                                case 'GROWTH':
	                                include '_product-item-scu-container--growth.php';
                                    break;

                                default:
                                    include '_product-item-scu-container.php';
                                    break;
                            }
				            ?>
                            <? /*
                            <div class="product-item-detail-info-container" data-entity="sku-line-block">
                                <div class="product-item-detail-info-container-title"><?=htmlspecialcharsEx($skuProperty['NAME'])?></div>
                                <div class="product-item-scu-container">
                                    <div class="product-item-scu-block">
                                        <div class="product-item-scu-list">
                                            <ul class="product-item-scu-item-list">
									            <?
									            foreach ($skuProperty['VALUES'] as &$value)
									            {
										            $value['NAME'] = htmlspecialcharsbx($value['NAME']);

										            if ($skuProperty['SHOW_MODE'] === 'PICT')
										            {
											            ?>
                                                        <li class="product-item-scu-item-color-container" title="<?=$value['NAME']?>"
                                                            data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"
                                                            data-onevalue="<?=$value['ID']?>">
                                                            <div class="product-item-scu-item-color-block">
                                                                <div class="product-item-scu-item-color" title="<?=$value['NAME']?>"
                                                                     style="background-image: url('<?=$value['PICT']['SRC']?>');">
                                                                </div>
                                                            </div>
                                                        </li>
											            <?
										            }
										            else
										            {
											            ?>
                                                        <li class="product-item-scu-item-text-container" title="<?=$value['NAME']?>"
                                                            data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"
                                                            data-onevalue="<?=$value['ID']?>">
                                                            <div class="product-item-scu-item-text-block">
                                                                <div class="product-item-scu-item-text"><?=$value['NAME']?></div>
                                                            </div>
                                                        </li>
											            <?
										            }
									            }
									            ?>
                                            </ul>
                                            <div style="clear: both;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <? /**/ ?>
				            <?
			            }
			            ?>
                    </div>
		            <?
	            }
	            ?>

                <?/*
                <div class="mf-product-attr-option mf-product-size">
                    <h3>Размер:</h3>
                    <div class="mf-pr-table-atts">
                        <div class="mf-pr-cell-atts">
                            <span class="mf-link-option-product table-sizes-product">Таблица размеров</span>
                            <? if(!empty($arSizes)) { ?>
                                <ul class="mf-product-sizes">
                                    <li class="disabled-size">XS</li>
                                    <li>S</li>
                                    <li>M</li>
                                    <li>L</li>
                                    <li>XL</li>
                                    <li>XXL</li>
                                </ul>
                            <? } ?>
                            <span class="mf-link-option-product check-size-product">Определить размер</span>
                        </div>
                        <div class="mf-pr-cell-atts">
                            <span class="mf-title-option-product check-height">Выбрать рост:</span>
                            <div class="mf-input-quantity-horizontal">
                                <div class="mf-input-plus">
                                    <span>+</span>
                                </div>
                                <input type="text" name="mf_check_height" maxlength="3" placeholder="156 см" />
                                <div class="mf-input-minus">
                                    <span>-</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
	            <? /**/ ?>


	            <? if ($arParams['USE_PRODUCT_QUANTITY']) { ?>
                    <div class="mf-product-attr-option mf-product-quantity" style="<?=(!$actualItem['CAN_BUY'] ? 'display: none;' : '')?>"  data-entity="quantity-block">
                        <div class="h3"><?=Loc::getMessage('CATALOG_QUANTITY')?>:</div>
                        <div class="mf-input-quantity">
                            <div>
                                <input id="<?=$itemIds['QUANTITY_ID']?>" max="15" type="text" value="<?=$price['MIN_QUANTITY']?>">
                            </div>
                            <div>
                                <div class="up-counter" id="<?=$itemIds['QUANTITY_UP_ID']?>">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" style="enable-background:new 0 0 404.308 404.309;" viewBox="0 0 404.308 404.309" xml:space="preserve"><path d="M404.308,303.229H0L202.157,101.08L404.308,303.229z"/></svg>
                                </div>
                                <div class="down-counter" id="<?=$itemIds['QUANTITY_DOWN_ID']?>">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 404.308 404.309" style="enable-background:new 0 0 404.308 404.309;" xml:space="preserve"><path d="M0,101.08h404.308L202.151,303.229L0,101.08z"/></svg>
                                </div>
                            </div>
                        </div>
                        <?/*
                        <span class="product-item-amount-description-container">
                            <span id="<?=$itemIds['QUANTITY_MEASURE']?>">
                                <?=$actualItem['ITEM_MEASURE']['TITLE']?>
                            </span>
                            <span id="<?=$itemIds['PRICE_TOTAL']?>"></span>
                        </span>
                        */?>
                        <?/*<span class="mf-link-option-product check-in-stores">Проверить наличие в магазинах</span>*/?>

                    </div>

                <? } ?>
                
                <? if(!$arResult['IS_SUBSCRIPTION_PRODUCT']) { ?>
                    <button onclick="yaCounter17899708.reachGoal('openCheckWondow');console.log('openCheckWondow')" id="check-in-stores" type="button" class="btn-clear mf-link-option-product check-in-stores" data-mfp-src="/ajax/check-in-stores.php?ids=<?= implode(',', $arActualOffersIdsForStore) ?>&site_id=<?=SITE_ID;?>&size_code=<?= in_array($arParams['PROP_CODE_SIZES_CLOTHES'], $arParams['OFFER_TREE_PROPS'])?$arParams['PROP_CODE_SIZES_CLOTHES']:$arParams['PROP_CODE_EVRO_RAZMER'] ?>">Проверить наличие в магазинах</button>
                <? } ?>

                <? //pr($price) ?>

                <div class="price product-item-detail-price-current" <?= !$arResult['SKU_PRICE_IS_CHANGING'] ? 'style="display: none"' : ''?>>
		            <? if ($arParams['SHOW_OLD_PRICE'] === 'Y') { ?>
                        <?/*<div style="display: none;">*/?>
                        <?//pr($showDiscount);?>
                        <span class="discount" id="<?=$itemIds['OLD_PRICE_ID']?>" style="padding-right: 7px; display: <?=($showDiscount ? 'block' : 'none')?>;">
				            <?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?>
                        </span>
                        <?/*</div>*/?>
                    <? } ?>
                    <span id="<?=$itemIds['PRICE_ID']?>">
			            <?=$price['PRINT_RATIO_PRICE']?>
                    </span>

                </div>

	            <? if ($arParams['SHOW_OLD_PRICE'] === 'Y') { ?>
                    <div style="display: none;">
                        <div class="item_economy_price" id="<?=$itemIds['DISCOUNT_PRICE_ID']?>"
                             style="display: <?=($showDiscount ? 'block' : 'none')?>;">
				            <?
				            if ($showDiscount)
				            {
					            echo Loc::getMessage('CT_BCE_CATALOG_ECONOMY_INFO2', array('#ECONOMY#' => $price['PRINT_RATIO_DISCOUNT']));
				            }
				            ?>
                        </div>
                    </div>
	            <? } ?>
            </div>

            <div class="product-item-detail-info-container" style="display: none !important;">
                <a class="btn btn-link product-item-detail-buy-button" id="<?=$itemIds['NOT_AVAILABLE_MESS']?>"
                   href="javascript:void(0)"
                   rel="nofollow" style="display: <?=(!$actualItem['CAN_BUY'] ? '' : 'none')?>;">
			        <?=$arParams['MESS_NOT_AVAILABLE']?>
                </a>
            </div>

            <div class="mf-footer-product">
                <div data-entity="main-button-container">
                    <div id="<?=$itemIds['BASKET_ACTIONS_ID']?>"<?=($actualItem['CAN_BUY'] ? '' : 'style="display: none;"')?>>
			            <? if ($showAddBtn) { ?>
                            <div class="product-item-detail-info-container">
                                <a class="button-link black-tr-b" id="<?=$itemIds['ADD_BASKET_LINK']?>" href="javascript:void(0);"><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></a>
                            </div>
                        <? } ?>

                        <? if ($showBuyBtn) { ?>
                            <div class="product-item-detail-info-container">
                                <a class="button-link black-tr-b" id="<?=$itemIds['BUY_LINK']?>" href="javascript:void(0);"><?=$arParams['MESS_BTN_BUY']?></a>
                            </div>
                        <? } ?>
                    </div>

		            <? if ($showSubscribe) { ?>
                        <?if(SITE_ID == 's1'){?>
                            <div class="product-item-detail-info-container">
    				            <? $APPLICATION->IncludeComponent(
    					            'bitrix:catalog.product.subscribe',
    					            '',
    					            array(
    						            'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
    						            'PRODUCT_ID' => $arResult['ID'],
    						            'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
    						            'BUTTON_CLASS' => 'btn btn-default product-item-detail-buy-button',
    						            'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
    						            'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
    					            ),
    					            $component,
    					            array('HIDE_ICONS' => 'Y')
    				            );?>
                            </div>
                        <?} 
                    } ?>
                </div>

                <a <?=($actualItem['CAN_BUY'] ? '' : 'style="display: none;"')?> id="js-btn-to-fav" href="#" class="button-link black-tr-b like-product js-btn-to-fav" data-item="<?= $actualItem['ID'] ?>">
                    <svg enable-background="new 0 0 128 128" version="1.1" viewBox="0 0 128 128" width="128px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><path d="M127,44.205c0-18.395-14.913-33.308-33.307-33.308c-12.979,0-24.199,7.441-29.692,18.276  c-5.497-10.835-16.714-18.274-29.694-18.274C15.912,10.898,1,25.81,1,44.205C1,79,56.879,117.104,64.001,117.104  C71.124,117.104,127,79.167,127,44.205z" fill="rgb(94, 93, 93)"></path></svg>
                </a>

                <div class="product-item-detail-pay-block">
		            <?
		            foreach ($arParams['PRODUCT_PAY_BLOCK_ORDER'] as $blockName)
		            {
			            switch ($blockName)
			            {
				            case 'rating':
					            if ($arParams['USE_VOTE_RATING'] === 'Y')
					            {
						            ?>
                                    <div class="product-item-detail-info-container">
							            <?
							            $APPLICATION->IncludeComponent(
								            'bitrix:iblock.vote',
								            'stars',
								            array(
									            'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
									            'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
									            'IBLOCK_ID' => $arParams['IBLOCK_ID'],
									            'ELEMENT_ID' => $arResult['ID'],
									            'ELEMENT_CODE' => '',
									            'MAX_VOTE' => '5',
									            'VOTE_NAMES' => array('1', '2', '3', '4', '5'),
									            'SET_STATUS_404' => 'N',
									            'DISPLAY_AS_RATING' => $arParams['VOTE_DISPLAY_AS_RATING'],
									            'CACHE_TYPE' => $arParams['CACHE_TYPE'],
									            'CACHE_TIME' => $arParams['CACHE_TIME']
								            ),
								            $component,
								            array('HIDE_ICONS' => 'Y')
							            );
							            ?>
                                    </div>
						            <?
					            }

					            break;

				            case 'price':
					            ?>
					            <?
					            break;

				            case 'priceRanges':
					            if ($arParams['USE_PRICE_COUNT'])
					            {
						            $showRanges = !$haveOffers && count($actualItem['ITEM_QUANTITY_RANGES']) > 1;
						            $useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';
						            ?>
                                    <div class="product-item-detail-info-container"
							            <?=$showRanges ? '' : 'style="display: none;"'?>
                                         data-entity="price-ranges-block">
                                        <div class="product-item-detail-info-container-title">
								            <?=$arParams['MESS_PRICE_RANGES_TITLE']?>
                                            <span data-entity="price-ranges-ratio-header">
														(<?=(Loc::getMessage(
										            'CT_BCE_CATALOG_RATIO_PRICE',
										            array('#RATIO#' => ($useRatio ? $measureRatio : '1').' '.$actualItem['ITEM_MEASURE']['TITLE'])
									            ))?>)
													</span>
                                        </div>
                                        <dl class="product-item-detail-properties" data-entity="price-ranges-body">
								            <?
								            if ($showRanges)
								            {
									            foreach ($actualItem['ITEM_QUANTITY_RANGES'] as $range)
									            {
										            if ($range['HASH'] !== 'ZERO-INF')
										            {
											            $itemPrice = false;

											            foreach ($arResult['ITEM_PRICES'] as $itemPrice)
											            {
												            if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
												            {
													            break;
												            }
											            }

											            if ($itemPrice)
											            {
												            ?>
                                                            <dt>
													            <?
													            echo Loc::getMessage(
															            'CT_BCE_CATALOG_RANGE_FROM',
															            array('#FROM#' => $range['SORT_FROM'].' '.$actualItem['ITEM_MEASURE']['TITLE'])
														            ).' ';

													            if (is_infinite($range['SORT_TO']))
													            {
														            echo Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
													            }
													            else
													            {
														            echo Loc::getMessage(
															            'CT_BCE_CATALOG_RANGE_TO',
															            array('#TO#' => $range['SORT_TO'].' '.$actualItem['ITEM_MEASURE']['TITLE'])
														            );
													            }
													            ?>
                                                            </dt>
                                                            <dd><?=($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE'])?></dd>
												            <?
											            }
										            }
									            }
								            }
								            ?>
                                        </dl>
                                    </div>
						            <?
						            unset($showRanges, $useRatio, $itemPrice, $range);
					            }

					            break;

				            case 'quantityLimit':
					            if ($arParams['SHOW_MAX_QUANTITY'] !== 'N')
					            {
						            if ($haveOffers)
						            {
							            ?>
                                        <div class="product-item-detail-info-container" id="<?=$itemIds['QUANTITY_LIMIT']?>" style="display: none;">
                                            <div class="product-item-detail-info-container-title">
									            <?=$arParams['MESS_SHOW_MAX_QUANTITY']?>:
                                                <span class="product-item-quantity" data-entity="quantity-limit-value"></span>
                                            </div>
                                        </div>
							            <?
						            }
						            else
						            {
							            if (
								            $measureRatio
								            && (float)$actualItem['CATALOG_QUANTITY'] > 0
								            && $actualItem['CATALOG_QUANTITY_TRACE'] === 'Y'
								            && $actualItem['CATALOG_CAN_BUY_ZERO'] === 'N'
							            )
							            {
								            ?>
                                            <div class="product-item-detail-info-container" id="<?=$itemIds['QUANTITY_LIMIT']?>">
                                                <div class="product-item-detail-info-container-title">
										            <?=$arParams['MESS_SHOW_MAX_QUANTITY']?>:
                                                    <span class="product-item-quantity" data-entity="quantity-limit-value">
																<?
																if ($arParams['SHOW_MAX_QUANTITY'] === 'M')
																{
																	if ((float)$actualItem['CATALOG_QUANTITY'] / $measureRatio >= $arParams['RELATIVE_QUANTITY_FACTOR'])
																	{
																		echo $arParams['MESS_RELATIVE_QUANTITY_MANY'];
																	}
																	else
																	{
																		echo $arParams['MESS_RELATIVE_QUANTITY_FEW'];
																	}
																}
																else
																{
																	echo $actualItem['CATALOG_QUANTITY'].' '.$actualItem['ITEM_MEASURE']['TITLE'];
																}
																?>
															</span>
                                                </div>
                                            </div>
								            <?
							            }
						            }
					            }

					            break;

				            case 'quantity':

					            break;

				            case 'buttons':
					            ?>

					            <?
					            break;
			            }
		            }

		            if ($arParams['DISPLAY_COMPARE'])
		            {
			            ?>
                        <div class="product-item-detail-compare-container">
                            <div class="product-item-detail-compare">
                                <div class="checkbox">
                                    <label id="<?=$itemIds['COMPARE_LINK']?>">
                                        <input type="checkbox" data-entity="compare-checkbox">
                                        <span data-entity="compare-title"><?=$arParams['MESS_BTN_COMPARE']?></span>
                                    </label>
                                </div>
                            </div>
                        </div>
			            <?
		            }
		            ?>
                </div>

                <div class="mf-product-social-links">
                    <div class="mf-p-soc-container">
                        <span class="repost-social">Поделиться:</span>
                        <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,twitter" data-bare></div>
                        <?/*
                        <ul class="social-icon-list">
                            <li><a href="#">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-1 -1 50.652 50.652" style="enable-background:new 0 0 49.652 49.652;" xml:space="preserve">
                                            <g><g><path d="M24.826,0C11.137,0,0,11.137,0,24.826c0,13.688,11.137,24.826,24.826,24.826c13.688,0,24.826-11.138,24.826-24.826C49.652,11.137,38.516,0,24.826,0z M31,25.7h-4.039c0,6.453,0,14.396,0,14.396h-5.985c0,0,0-7.866,0-14.396h-2.845v-5.088h2.845v-3.291c0-2.357,1.12-6.04,6.04-6.04l4.435,0.017v4.939c0,0-2.695,0-3.219,0c-0.524,0-1.269,0.262-1.269,1.386v2.99h4.56L31,25.7z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
                                        </svg>
                                </a></li>
                            <li><a href="#">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="-10 -10 496.392 496.392" style="enable-background:new 0 0 486.392 486.392;" xml:space="preserve">
                                            <g><g><path  d="M243.196,0C108.891,0,0,108.891,0,243.196s108.891,243.196,243.196,243.196
                                                    s243.196-108.891,243.196-243.196C486.392,108.861,377.501,0,243.196,0z M364.186,188.598l0.182,7.752
                                                    c0,79.16-60.221,170.359-170.359,170.359c-33.804,0-65.268-9.91-91.776-26.904c4.682,0.547,9.454,0.851,14.288,0.851
                                                    c28.059,0,53.868-9.576,74.357-25.627c-26.204-0.486-48.305-17.814-55.935-41.586c3.678,0.699,7.387,1.034,11.278,1.034
                                                    c5.472,0,10.761-0.699,15.777-2.067c-27.39-5.533-48.031-29.7-48.031-58.701v-0.76c8.086,4.499,17.297,7.174,27.116,7.509
                                                    c-16.051-10.731-26.63-29.062-26.63-49.825c0-10.974,2.949-21.249,8.086-30.095c29.518,36.236,73.658,60.069,123.422,62.562
                                                    c-1.034-4.378-1.55-8.968-1.55-13.649c0-33.044,26.812-59.857,59.887-59.857c17.206,0,32.771,7.265,43.714,18.908
                                                    c13.619-2.706,26.448-7.691,38.03-14.531c-4.469,13.984-13.953,25.718-26.326,33.135c12.069-1.429,23.651-4.682,34.382-9.424
                                                    C386.073,169.659,375.889,180.208,364.186,188.598z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
                                        </svg>
                                </a></li>
                            <li><a href="#">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="-2 -2 100 100" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve"
                                    >
                                            <g>
                                                <path d="M48.875,0C21.883,0,0,21.882,0,48.875S21.883,97.75,48.875,97.75S97.75,75.868,97.75,48.875S75.867,0,48.875,0z
                                                 M73.667,54.161c2.278,2.225,4.688,4.319,6.733,6.774c0.906,1.086,1.76,2.209,2.41,3.472c0.928,1.801,0.09,3.776-1.522,3.883
                                                l-10.013-0.002c-2.586,0.214-4.644-0.829-6.379-2.597c-1.385-1.409-2.67-2.914-4.004-4.371c-0.545-0.598-1.119-1.161-1.803-1.604
                                                c-1.365-0.888-2.551-0.616-3.333,0.81c-0.797,1.451-0.979,3.059-1.055,4.674c-0.109,2.361-0.821,2.978-3.19,3.089
                                                c-5.062,0.237-9.865-0.531-14.329-3.083c-3.938-2.251-6.986-5.428-9.642-9.025c-5.172-7.012-9.133-14.708-12.692-22.625
                                                c-0.801-1.783-0.215-2.737,1.752-2.774c3.268-0.063,6.536-0.055,9.804-0.003c1.33,0.021,2.21,0.782,2.721,2.037
                                                c1.766,4.345,3.931,8.479,6.644,12.313c0.723,1.021,1.461,2.039,2.512,2.76c1.16,0.796,2.044,0.533,2.591-0.762
                                                c0.35-0.823,0.501-1.703,0.577-2.585c0.26-3.021,0.291-6.041-0.159-9.05c-0.28-1.883-1.339-3.099-3.216-3.455
                                                c-0.956-0.181-0.816-0.535-0.351-1.081c0.807-0.944,1.563-1.528,3.074-1.528l11.313-0.002c1.783,0.35,2.183,1.15,2.425,2.946
                                                l0.01,12.572c-0.021,0.695,0.349,2.755,1.597,3.21c1,0.33,1.66-0.472,2.258-1.105c2.713-2.879,4.646-6.277,6.377-9.794
                                                c0.764-1.551,1.423-3.156,2.063-4.764c0.476-1.189,1.216-1.774,2.558-1.754l10.894,0.013c0.321,0,0.647,0.003,0.965,0.058
                                                c1.836,0.314,2.339,1.104,1.771,2.895c-0.894,2.814-2.631,5.158-4.329,7.508c-1.82,2.516-3.761,4.944-5.563,7.471
                                                C71.48,50.992,71.611,52.155,73.667,54.161z"/></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g>
                                        <g>
                                        </g>
                                        </svg>
                                </a></li>
                            <li><a href="#">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                         viewBox="-2 -2 100 100" style="enable-background:new 0 0 97.75 97.75;" xml:space="preserve"
                                    >
                                            <g>
                                                <g>
                                                    <path d="M48.921,40.507c4.667-0.017,8.384-3.766,8.367-8.443c-0.017-4.679-3.742-8.402-8.411-8.406
                                                    c-4.708-0.005-8.468,3.787-8.432,8.508C40.48,36.826,44.239,40.524,48.921,40.507z"/>
                                                    <path d="M48.875,0C21.882,0,0,21.883,0,48.875S21.882,97.75,48.875,97.75S97.75,75.867,97.75,48.875S75.868,0,48.875,0z
                                                     M48.945,14.863c9.52,0.026,17.161,7.813,17.112,17.438c-0.048,9.403-7.814,17.024-17.318,16.992
                                                    c-9.407-0.032-17.122-7.831-17.066-17.253C31.726,22.515,39.445,14.837,48.945,14.863z M68.227,56.057
                                                    c-2.105,2.161-4.639,3.725-7.453,4.816c-2.66,1.031-5.575,1.55-8.461,1.896c0.437,0.474,0.642,0.707,0.914,0.979
                                                    c3.916,3.937,7.851,7.854,11.754,11.802c1.33,1.346,1.607,3.014,0.875,4.577c-0.799,1.71-2.592,2.834-4.351,2.713
                                                    c-1.114-0.077-1.983-0.63-2.754-1.407c-2.956-2.974-5.968-5.895-8.862-8.925c-0.845-0.882-1.249-0.714-1.994,0.052
                                                    c-2.973,3.062-5.995,6.075-9.034,9.072c-1.365,1.346-2.989,1.59-4.573,0.82c-1.683-0.814-2.753-2.533-2.671-4.262
                                                    c0.058-1.166,0.632-2.06,1.434-2.858c3.877-3.869,7.742-7.75,11.608-11.628c0.257-0.257,0.495-0.53,0.868-0.93
                                                    c-5.273-0.551-10.028-1.849-14.099-5.032c-0.506-0.396-1.027-0.778-1.487-1.222c-1.783-1.711-1.962-3.672-0.553-5.69
                                                    c1.207-1.728,3.231-2.19,5.336-1.197c0.408,0.191,0.796,0.433,1.168,0.689c7.586,5.213,18.008,5.356,25.624,0.233
                                                    c0.754-0.576,1.561-1.05,2.496-1.289c1.816-0.468,3.512,0.201,4.486,1.791C69.613,52.874,69.6,54.646,68.227,56.057z"/>
                                                </g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g>
                                        </g>
                                        </svg>
                                </a></li>
                        </ul>
                        */?>
                    </div>
                </div>


            </div>
        </div>

    <div class="bx-catalog-element">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-12">
					<?
					if ($haveOffers)
					{
						if ($arResult['OFFER_GROUP'])
						{
							foreach ($arResult['OFFER_GROUP_VALUES'] as $offerId)
							{
								?>
                                <span id="<?=$itemIds['OFFER_GROUP'].$offerId?>" style="display: none;">
								<?
								$APPLICATION->IncludeComponent(
									'bitrix:catalog.set.constructor',
									'.default',
									array(
										'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
										'IBLOCK_ID' => $arResult['OFFERS_IBLOCK'],
										'ELEMENT_ID' => $offerId,
										'PRICE_CODE' => $arParams['PRICE_CODE'],
										'BASKET_URL' => $arParams['BASKET_URL'],
										'OFFERS_CART_PROPERTIES' => $arParams['OFFERS_CART_PROPERTIES'],
										'CACHE_TYPE' => $arParams['CACHE_TYPE'],
										'CACHE_TIME' => $arParams['CACHE_TIME'],
										'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
										'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME'],
										'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
										'CURRENCY_ID' => $arParams['CURRENCY_ID']
									),
									$component,
									array('HIDE_ICONS' => 'Y')
								);
								?>
							</span>
								<?
							}
						}
					}
					else
					{
						if ($arResult['MODULES']['catalog'] && $arResult['OFFER_GROUP'])
						{
							$APPLICATION->IncludeComponent(
								'bitrix:catalog.set.constructor',
								'.default',
								array(
									'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
									'IBLOCK_ID' => $arParams['IBLOCK_ID'],
									'ELEMENT_ID' => $arResult['ID'],
									'PRICE_CODE' => $arParams['PRICE_CODE'],
									'BASKET_URL' => $arParams['BASKET_URL'],
									'CACHE_TYPE' => $arParams['CACHE_TYPE'],
									'CACHE_TIME' => $arParams['CACHE_TIME'],
									'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
									'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME'],
									'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
									'CURRENCY_ID' => $arParams['CURRENCY_ID']
								),
								$component,
								array('HIDE_ICONS' => 'Y')
							);
						}
					}
					?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 col-md-9">
                    <div class="row" id="<?=$itemIds['TABS_ID']?>">
                        <div class="col-xs-12">
                            <div class="product-item-detail-tabs-container">
                                <ul class="product-item-detail-tabs-list">
									<?
									if ($arParams['USE_COMMENTS'] === 'Y')
									{
										?>
                                        <li class="product-item-detail-tab" data-entity="tab" data-value="comments">
                                            <a href="javascript:void(0);" class="product-item-detail-tab-link">
                                                <span><?=$arParams['MESS_COMMENTS_TAB']?></span>
                                            </a>
                                        </li>
										<?
									}
									?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="<?=$itemIds['TAB_CONTAINERS_ID']?>">
                        <div class="col-xs-12">
							<?

							if ($arParams['USE_COMMENTS'] === 'Y')
							{
								?>
                                <div class="product-item-detail-tab-content" data-entity="tab-container" data-value="comments" style="display: none;">
									<?
									$componentCommentsParams = array(
										'ELEMENT_ID' => $arResult['ID'],
										'ELEMENT_CODE' => '',
										'IBLOCK_ID' => $arParams['IBLOCK_ID'],
										'SHOW_DEACTIVATED' => $arParams['SHOW_DEACTIVATED'],
										'URL_TO_COMMENT' => '',
										'WIDTH' => '',
										'COMMENTS_COUNT' => '5',
										'BLOG_USE' => $arParams['BLOG_USE'],
										'FB_USE' => $arParams['FB_USE'],
										'FB_APP_ID' => $arParams['FB_APP_ID'],
										'VK_USE' => $arParams['VK_USE'],
										'VK_API_ID' => $arParams['VK_API_ID'],
										'CACHE_TYPE' => $arParams['CACHE_TYPE'],
										'CACHE_TIME' => $arParams['CACHE_TIME'],
										'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
										'BLOG_TITLE' => '',
										'BLOG_URL' => $arParams['BLOG_URL'],
										'PATH_TO_SMILE' => '',
										'EMAIL_NOTIFY' => $arParams['BLOG_EMAIL_NOTIFY'],
										'AJAX_POST' => 'Y',
										'SHOW_SPAM' => 'Y',
										'SHOW_RATING' => 'N',
										'FB_TITLE' => '',
										'FB_USER_ADMIN_ID' => '',
										'FB_COLORSCHEME' => 'light',
										'FB_ORDER_BY' => 'reverse_time',
										'VK_TITLE' => '',
										'TEMPLATE_THEME' => $arParams['~TEMPLATE_THEME']
									);
									if(isset($arParams["USER_CONSENT"]))
										$componentCommentsParams["USER_CONSENT"] = $arParams["USER_CONSENT"];
									if(isset($arParams["USER_CONSENT_ID"]))
										$componentCommentsParams["USER_CONSENT_ID"] = $arParams["USER_CONSENT_ID"];
									if(isset($arParams["USER_CONSENT_IS_CHECKED"]))
										$componentCommentsParams["USER_CONSENT_IS_CHECKED"] = $arParams["USER_CONSENT_IS_CHECKED"];
									if(isset($arParams["USER_CONSENT_IS_LOADED"]))
										$componentCommentsParams["USER_CONSENT_IS_LOADED"] = $arParams["USER_CONSENT_IS_LOADED"];
									$APPLICATION->IncludeComponent(
										'bitrix:catalog.comments',
										'',
										$componentCommentsParams,
										$component,
										array('HIDE_ICONS' => 'Y')
									);
									?>
                                </div>
								<?
							}
							?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 col-md-3">
                    <div>
						<?
						if ($arParams['BRAND_USE'] === 'Y')
						{
							$APPLICATION->IncludeComponent(
								'bitrix:catalog.brandblock',
								'.default',
								array(
									'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
									'IBLOCK_ID' => $arParams['IBLOCK_ID'],
									'ELEMENT_ID' => $arResult['ID'],
									'ELEMENT_CODE' => '',
									'PROP_CODE' => $arParams['BRAND_PROP_CODE'],
									'CACHE_TYPE' => $arParams['CACHE_TYPE'],
									'CACHE_TIME' => $arParams['CACHE_TIME'],
									'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
									'WIDTH' => '',
									'HEIGHT' => ''
								),
								$component,
								array('HIDE_ICONS' => 'Y')
							);
						}
						?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
					<?
					if ($arResult['CATALOG'] && $arParams['USE_GIFTS_DETAIL'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled('sale'))
					{
						?>
                        <div data-entity="parent-container">
							<?
							if (!isset($arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE']) || $arParams['GIFTS_DETAIL_HIDE_BLOCK_TITLE'] !== 'Y')
							{
								?>
                                <div class="catalog-block-header" data-entity="header" data-showed="false" style="display: none; opacity: 0;">
									<?=($arParams['GIFTS_DETAIL_BLOCK_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_GIFT_BLOCK_TITLE_DEFAULT'))?>
                                </div>
								<?
							}

							CBitrixComponent::includeComponentClass('bitrix:sale.products.gift');
							$APPLICATION->IncludeComponent(
								'bitrix:sale.products.gift',
								'.default',
								array(
									'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
									'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
									'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],

									'PRODUCT_ROW_VARIANTS' => "",
									'PAGE_ELEMENT_COUNT' => 0,
									'DEFERRED_PRODUCT_ROW_VARIANTS' => \Bitrix\Main\Web\Json::encode(
										SaleProductsGiftComponent::predictRowVariants(
											$arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],
											$arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT']
										)
									),
									'DEFERRED_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_DETAIL_PAGE_ELEMENT_COUNT'],

									'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
									'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
									'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
									'PRODUCT_DISPLAY_MODE' => 'Y',
									'PRODUCT_BLOCKS_ORDER' => $arParams['GIFTS_PRODUCT_BLOCKS_ORDER'],
									'SHOW_SLIDER' => $arParams['GIFTS_SHOW_SLIDER'],
									'SLIDER_INTERVAL' => isset($arParams['GIFTS_SLIDER_INTERVAL']) ? $arParams['GIFTS_SLIDER_INTERVAL'] : '',
									'SLIDER_PROGRESS' => isset($arParams['GIFTS_SLIDER_PROGRESS']) ? $arParams['GIFTS_SLIDER_PROGRESS'] : '',

									'TEXT_LABEL_GIFT' => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],

									'LABEL_PROP_'.$arParams['IBLOCK_ID'] => array(),
									'LABEL_PROP_MOBILE_'.$arParams['IBLOCK_ID'] => array(),
									'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],

									'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
									'MESS_BTN_BUY' => $arParams['~GIFTS_MESS_BTN_BUY'],
									'MESS_BTN_ADD_TO_BASKET' => $arParams['~GIFTS_MESS_BTN_BUY'],
									'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
									'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],

									'SHOW_PRODUCTS_'.$arParams['IBLOCK_ID'] => 'Y',
									'PROPERTY_CODE_'.$arParams['IBLOCK_ID'] => $arParams['LIST_PROPERTY_CODE'],
									'PROPERTY_CODE_MOBILE'.$arParams['IBLOCK_ID'] => $arParams['LIST_PROPERTY_CODE_MOBILE'],
									'PROPERTY_CODE_'.$arResult['OFFERS_IBLOCK'] => $arParams['OFFER_TREE_PROPS'],
									'OFFER_TREE_PROPS_'.$arResult['OFFERS_IBLOCK'] => $arParams['OFFER_TREE_PROPS'],
									'CART_PROPERTIES_'.$arResult['OFFERS_IBLOCK'] => $arParams['OFFERS_CART_PROPERTIES'],
									'ADDITIONAL_PICT_PROP_'.$arParams['IBLOCK_ID'] => (isset($arParams['ADD_PICT_PROP']) ? $arParams['ADD_PICT_PROP'] : ''),
									'ADDITIONAL_PICT_PROP_'.$arResult['OFFERS_IBLOCK'] => (isset($arParams['OFFER_ADD_PICT_PROP']) ? $arParams['OFFER_ADD_PICT_PROP'] : ''),

									'HIDE_NOT_AVAILABLE' => 'Y',
									'HIDE_NOT_AVAILABLE_OFFERS' => 'Y',
									'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
									'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
									'PRICE_CODE' => $arParams['PRICE_CODE'],
									'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
									'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
									'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
									'BASKET_URL' => $arParams['BASKET_URL'],
									'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
									'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
									'PARTIAL_PRODUCT_PROPERTIES' => $arParams['PARTIAL_PRODUCT_PROPERTIES'],
									'USE_PRODUCT_QUANTITY' => 'N',
									'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
									'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
									'POTENTIAL_PRODUCT_TO_BUY' => array(
										'ID' => isset($arResult['ID']) ? $arResult['ID'] : null,
										'MODULE' => isset($arResult['MODULE']) ? $arResult['MODULE'] : 'catalog',
										'PRODUCT_PROVIDER_CLASS' => isset($arResult['PRODUCT_PROVIDER_CLASS']) ? $arResult['PRODUCT_PROVIDER_CLASS'] : 'CCatalogProductProvider',
										'QUANTITY' => isset($arResult['QUANTITY']) ? $arResult['QUANTITY'] : null,
										'IBLOCK_ID' => isset($arResult['IBLOCK_ID']) ? $arResult['IBLOCK_ID'] : null,

										'PRIMARY_OFFER_ID' => isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'])
											? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID']
											: null,
										'SECTION' => array(
											'ID' => isset($arResult['SECTION']['ID']) ? $arResult['SECTION']['ID'] : null,
											'IBLOCK_ID' => isset($arResult['SECTION']['IBLOCK_ID']) ? $arResult['SECTION']['IBLOCK_ID'] : null,
											'LEFT_MARGIN' => isset($arResult['SECTION']['LEFT_MARGIN']) ? $arResult['SECTION']['LEFT_MARGIN'] : null,
											'RIGHT_MARGIN' => isset($arResult['SECTION']['RIGHT_MARGIN']) ? $arResult['SECTION']['RIGHT_MARGIN'] : null,
										),
									),

									'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
									'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
									'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
								),
								$component,
								array('HIDE_ICONS' => 'Y')
							);
							?>
                        </div>
						<?
					}

					if ($arResult['CATALOG'] && $arParams['USE_GIFTS_MAIN_PR_SECTION_LIST'] == 'Y' && \Bitrix\Main\ModuleManager::isModuleInstalled('sale'))
					{
						?>
                        <div data-entity="parent-container">
							<?
							if (!isset($arParams['GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE']) || $arParams['GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE'] !== 'Y')
							{
								?>
                                <div class="catalog-block-header" data-entity="header" data-showed="false" style="display: none; opacity: 0;">
									<?=($arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_GIFTS_MAIN_BLOCK_TITLE_DEFAULT'))?>
                                </div>
								<?
							}

							$APPLICATION->IncludeComponent(
								'bitrix:sale.gift.main.products',
								'.default',
								array(
									'CUSTOM_SITE_ID' => isset($arParams['CUSTOM_SITE_ID']) ? $arParams['CUSTOM_SITE_ID'] : null,
									'PAGE_ELEMENT_COUNT' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
									'LINE_ELEMENT_COUNT' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT'],
									'HIDE_BLOCK_TITLE' => 'Y',
									'BLOCK_TITLE' => $arParams['GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE'],

									'OFFERS_FIELD_CODE' => $arParams['OFFERS_FIELD_CODE'],
									'OFFERS_PROPERTY_CODE' => $arParams['OFFERS_PROPERTY_CODE'],

									'AJAX_MODE' => $arParams['AJAX_MODE'],
									'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
									'IBLOCK_ID' => $arParams['IBLOCK_ID'],

									'ELEMENT_SORT_FIELD' => 'ID',
									'ELEMENT_SORT_ORDER' => 'DESC',
									//'ELEMENT_SORT_FIELD2' => $arParams['ELEMENT_SORT_FIELD2'],
									//'ELEMENT_SORT_ORDER2' => $arParams['ELEMENT_SORT_ORDER2'],
									'FILTER_NAME' => 'searchFilter',
									'SECTION_URL' => $arParams['SECTION_URL'],
									'DETAIL_URL' => $arParams['DETAIL_URL'],
									'BASKET_URL' => $arParams['BASKET_URL'],
									'ACTION_VARIABLE' => $arParams['ACTION_VARIABLE'],
									'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
									'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],

									'CACHE_TYPE' => $arParams['CACHE_TYPE'],
									'CACHE_TIME' => $arParams['CACHE_TIME'],

									'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],
									'SET_TITLE' => $arParams['SET_TITLE'],
									'PROPERTY_CODE' => $arParams['PROPERTY_CODE'],
									'PRICE_CODE' => $arParams['PRICE_CODE'],
									'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
									'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],

									'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
									'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
									'CURRENCY_ID' => $arParams['CURRENCY_ID'],
									'HIDE_NOT_AVAILABLE' => 'Y',
									'HIDE_NOT_AVAILABLE_OFFERS' => 'Y',
									'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
									'PRODUCT_BLOCKS_ORDER' => $arParams['GIFTS_PRODUCT_BLOCKS_ORDER'],

									'SHOW_SLIDER' => $arParams['GIFTS_SHOW_SLIDER'],
									'SLIDER_INTERVAL' => isset($arParams['GIFTS_SLIDER_INTERVAL']) ? $arParams['GIFTS_SLIDER_INTERVAL'] : '',
									'SLIDER_PROGRESS' => isset($arParams['GIFTS_SLIDER_PROGRESS']) ? $arParams['GIFTS_SLIDER_PROGRESS'] : '',

									'ADD_PICT_PROP' => (isset($arParams['ADD_PICT_PROP']) ? $arParams['ADD_PICT_PROP'] : ''),
									'LABEL_PROP' => (isset($arParams['LABEL_PROP']) ? $arParams['LABEL_PROP'] : ''),
									'LABEL_PROP_MOBILE' => (isset($arParams['LABEL_PROP_MOBILE']) ? $arParams['LABEL_PROP_MOBILE'] : ''),
									'LABEL_PROP_POSITION' => (isset($arParams['LABEL_PROP_POSITION']) ? $arParams['LABEL_PROP_POSITION'] : ''),
									'OFFER_ADD_PICT_PROP' => (isset($arParams['OFFER_ADD_PICT_PROP']) ? $arParams['OFFER_ADD_PICT_PROP'] : ''),
									'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : ''),
									'SHOW_DISCOUNT_PERCENT' => (isset($arParams['SHOW_DISCOUNT_PERCENT']) ? $arParams['SHOW_DISCOUNT_PERCENT'] : ''),
									'DISCOUNT_PERCENT_POSITION' => (isset($arParams['DISCOUNT_PERCENT_POSITION']) ? $arParams['DISCOUNT_PERCENT_POSITION'] : ''),
									'SHOW_OLD_PRICE' => (isset($arParams['SHOW_OLD_PRICE']) ? $arParams['SHOW_OLD_PRICE'] : ''),
									'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
									'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
									'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
									'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
									'ADD_TO_BASKET_ACTION' => (isset($arParams['ADD_TO_BASKET_ACTION']) ? $arParams['ADD_TO_BASKET_ACTION'] : ''),
									'SHOW_CLOSE_POPUP' => (isset($arParams['SHOW_CLOSE_POPUP']) ? $arParams['SHOW_CLOSE_POPUP'] : ''),
									'DISPLAY_COMPARE' => (isset($arParams['DISPLAY_COMPARE']) ? $arParams['DISPLAY_COMPARE'] : ''),
									'COMPARE_PATH' => (isset($arParams['COMPARE_PATH']) ? $arParams['COMPARE_PATH'] : ''),
								)
								+ array(
									'OFFER_ID' => empty($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'])
										? $arResult['ID']
										: $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'],
									'SECTION_ID' => $arResult['SECTION']['ID'],
									'ELEMENT_ID' => $arResult['ID'],

									'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
									'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
									'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
								),
								$component,
								array('HIDE_ICONS' => 'Y')
							);
							?>
                        </div>
						<?
					}
					?>
                </div>
            </div>
        </div>
		<?/*
	<!--Small Card-->
	<div class="product-item-detail-short-card-fixed hidden-xs" id="<?=$itemIds['SMALL_CARD_PANEL_ID']?>">
		<div class="product-item-detail-short-card-content-container">
			<table>
				<tr>
					<td rowspan="2" class="product-item-detail-short-card-image">
						<img src="" style="height: 65px;" data-entity="panel-picture">
					</td>
					<td class="product-item-detail-short-title-container" data-entity="panel-title">
						<span class="product-item-detail-short-title-text"><?=$name?></span>
					</td>
					<td rowspan="2" class="product-item-detail-short-card-price">
						<?
						if ($arParams['SHOW_OLD_PRICE'] === 'Y')
						{
							?>
							<div class="product-item-detail-price-old" style="display: <?=($showDiscount ? '' : 'none')?>;"
								data-entity="panel-old-price">
								<?=($showDiscount ? $price['PRINT_RATIO_BASE_PRICE'] : '')?>
							</div>
							<?
						}
						?>
						<div class="product-item-detail-price-current" data-entity="panel-price">
							<?=$price['PRINT_RATIO_PRICE']?>
						</div>
					</td>
					<?
					if ($showAddBtn)
					{
						?>
						<td rowspan="2" class="product-item-detail-short-card-btn"
							style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;"
							data-entity="panel-add-button">
							<a class="btn <?=$showButtonClassName?> product-item-detail-buy-button"
								id="<?=$itemIds['ADD_BASKET_LINK']?>"
								href="javascript:void(0);">
								<span><?=$arParams['MESS_BTN_ADD_TO_BASKET']?></span>
							</a>
						</td>
						<?
					}

					if ($showBuyBtn)
					{
						?>
						<td rowspan="2" class="product-item-detail-short-card-btn"
							style="display: <?=($actualItem['CAN_BUY'] ? '' : 'none')?>;"
							data-entity="panel-buy-button">
							<a class="btn <?=$buyButtonClassName?> product-item-detail-buy-button" id="<?=$itemIds['BUY_LINK']?>"
								href="javascript:void(0);">
								<span><?=$arParams['MESS_BTN_BUY']?></span>
							</a>
						</td>
						<?
					}
					?>
					<td rowspan="2" class="product-item-detail-short-card-btn"
						style="display: <?=(!$actualItem['CAN_BUY'] ? '' : 'none')?>;"
						data-entity="panel-not-available-button">
						<a class="btn btn-link product-item-detail-buy-button" href="javascript:void(0)"
							rel="nofollow">
							<?=$arParams['MESS_NOT_AVAILABLE']?>
						</a>
					</td>
				</tr>
				<?
				if ($haveOffers)
				{
					?>
					<tr>
						<td>
							<div class="product-item-selected-scu-container" data-entity="panel-sku-container">
								<?
								$i = 0;

								foreach ($arResult['SKU_PROPS'] as $skuProperty)
								{
									if (!isset($arResult['OFFERS_PROP'][$skuProperty['CODE']]))
									{
										continue;
									}

									$propertyId = $skuProperty['ID'];

									foreach ($skuProperty['VALUES'] as $value)
									{
										$value['NAME'] = htmlspecialcharsbx($value['NAME']);
										if ($skuProperty['SHOW_MODE'] === 'PICT')
										{
											?>
											<div class="product-item-selected-scu product-item-selected-scu-color selected"
												title="<?=$value['NAME']?>"
												style="background-image: url('<?=$value['PICT']['SRC']?>'); display: none;"
												data-sku-line="<?=$i?>"
												data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"
												data-onevalue="<?=$value['ID']?>">
											</div>
											<?
										}
										else
										{
											?>
											<div class="product-item-selected-scu product-item-selected-scu-text selected"
												title="<?=$value['NAME']?>"
												style="display: none;"
												data-sku-line="<?=$i?>"
												data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"
												data-onevalue="<?=$value['ID']?>">
												<?=$value['NAME']?>
											</div>
											<?
										}
									}

									$i++;
								}
								?>
							</div>
						</td>
					</tr>
					<?
				}
				?>
			</table>
		</div>
	</div>
	<!--Top tabs-->
	<div class="product-item-detail-tabs-container-fixed hidden-xs" id="<?=$itemIds['TABS_PANEL_ID']?>">
		<ul class="product-item-detail-tabs-list">
			<?
			if ($arParams['USE_COMMENTS'] === 'Y')
			{
				?>
				<li class="product-item-detail-tab" data-entity="tab" data-value="comments">
					<a href="javascript:void(0);" class="product-item-detail-tab-link">
						<span><?=$arParams['MESS_COMMENTS_TAB']?></span>
					</a>
				</li>
				<?
			}
			?>
		</ul>
	</div>
    */?>

        <meta itemprop="name" content="<?=$name?>" />
        <meta itemprop="category" content="<?=$arResult['CATEGORY_PATH']?>" />
		<?
		if ($haveOffers)
		{
			foreach ($arResult['JS_OFFERS'] as $offer)
			{
				$currentOffersList = array();

				if (!empty($offer['TREE']) && is_array($offer['TREE']))
				{
					foreach ($offer['TREE'] as $propName => $skuId)
					{
						$propId = (int)substr($propName, 5);

						foreach ($skuProps as $prop)
						{
							if ($prop['ID'] == $propId)
							{
								foreach ($prop['VALUES'] as $propId => $propValue)
								{
									if ($propId == $skuId)
									{
										$currentOffersList[] = $propValue['NAME'];
										break;
									}
								}
							}
						}
					}
				}

				$offerPrice = $offer['ITEM_PRICES'][$offer['ITEM_PRICE_SELECTED']];
				?>
                <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
				<meta itemprop="sku" content="<?=htmlspecialcharsbx(implode('/', $currentOffersList))?>" />
				<meta itemprop="price" content="<?=$offerPrice['RATIO_PRICE']?>" />
				<meta itemprop="priceCurrency" content="<?=$offerPrice['CURRENCY']?>" />
				<link itemprop="availability" href="http://schema.org/<?=($offer['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
			</span>
				<?
			}

			unset($offerPrice, $currentOffersList);
		}
		else
		{
			?>
            <span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
			<meta itemprop="price" content="<?=$price['RATIO_PRICE']?>" />
			<meta itemprop="priceCurrency" content="<?=$price['CURRENCY']?>" />
			<link itemprop="availability" href="http://schema.org/<?=($actualItem['CAN_BUY'] ? 'InStock' : 'OutOfStock')?>" />
		</span>
			<?
		}
		?>
    </div>
	<?
	if ($haveOffers)
	{
		$offerIds = array();
		$offerCodes = array();

		$useRatio = $arParams['USE_RATIO_IN_RANGES'] === 'Y';

		foreach ($arResult['JS_OFFERS'] as $ind => &$jsOffer)
		{
			$offerIds[] = (int)$jsOffer['ID'];
			$offerCodes[] = $jsOffer['CODE'];

			$fullOffer = $arResult['OFFERS'][$ind];
			$measureName = $fullOffer['ITEM_MEASURE']['TITLE'];

			$strAllProps = '';
			$strMainProps = '';
			$strPriceRangesRatio = '';
			$strPriceRanges = '';

			if ($arResult['SHOW_OFFERS_PROPS'])
			{
				if (!empty($jsOffer['DISPLAY_PROPERTIES']))
				{
					foreach ($jsOffer['DISPLAY_PROPERTIES'] as $property)
					{
						$current = '<dt>'.$property['NAME'].'</dt><dd>'.(
							is_array($property['VALUE'])
								? implode(' / ', $property['VALUE'])
								: $property['VALUE']
							).'</dd>';
						$strAllProps .= $current;

						if (isset($arParams['MAIN_BLOCK_OFFERS_PROPERTY_CODE'][$property['CODE']]))
						{
							$strMainProps .= $current;
						}
					}

					unset($current);
				}
			}

			if ($arParams['USE_PRICE_COUNT'] && count($jsOffer['ITEM_QUANTITY_RANGES']) > 1)
			{
				$strPriceRangesRatio = '('.Loc::getMessage(
						'CT_BCE_CATALOG_RATIO_PRICE',
						array('#RATIO#' => ($useRatio
								? $fullOffer['ITEM_MEASURE_RATIOS'][$fullOffer['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']
								: '1'
							).' '.$measureName)
					).')';

				foreach ($jsOffer['ITEM_QUANTITY_RANGES'] as $range)
				{
					if ($range['HASH'] !== 'ZERO-INF')
					{
						$itemPrice = false;

						foreach ($jsOffer['ITEM_PRICES'] as $itemPrice)
						{
							if ($itemPrice['QUANTITY_HASH'] === $range['HASH'])
							{
								break;
							}
						}

						if ($itemPrice)
						{
							$strPriceRanges .= '<dt>'.Loc::getMessage(
									'CT_BCE_CATALOG_RANGE_FROM',
									array('#FROM#' => $range['SORT_FROM'].' '.$measureName)
								).' ';

							if (is_infinite($range['SORT_TO']))
							{
								$strPriceRanges .= Loc::getMessage('CT_BCE_CATALOG_RANGE_MORE');
							}
							else
							{
								$strPriceRanges .= Loc::getMessage(
									'CT_BCE_CATALOG_RANGE_TO',
									array('#TO#' => $range['SORT_TO'].' '.$measureName)
								);
							}

							$strPriceRanges .= '</dt><dd>'.($useRatio ? $itemPrice['PRINT_RATIO_PRICE'] : $itemPrice['PRINT_PRICE']).'</dd>';
						}
					}
				}

				unset($range, $itemPrice);
			}

			$jsOffer['DISPLAY_PROPERTIES'] = $strAllProps;
			$jsOffer['DISPLAY_PROPERTIES_MAIN_BLOCK'] = $strMainProps;
			$jsOffer['PRICE_RANGES_RATIO_HTML'] = $strPriceRangesRatio;
			$jsOffer['PRICE_RANGES_HTML'] = $strPriceRanges;
		}

		$templateData['OFFER_IDS'] = $offerIds;
		$templateData['OFFER_CODES'] = $offerCodes;
		unset($jsOffer, $strAllProps, $strMainProps, $strPriceRanges, $strPriceRangesRatio, $useRatio);

		$jsParams = array(
			'CONFIG' => array(
				'USE_CATALOG' => $arResult['CATALOG'],
				'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
				'SHOW_PRICE' => true,
				'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
				'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
				'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
				'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
				'SHOW_SKU_PROPS' => $arResult['SHOW_OFFERS_PROPS'],
				'OFFER_GROUP' => $arResult['OFFER_GROUP'],
				'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
				'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
				'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
				'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
				'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
				'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
				'USE_STICKERS' => true,
				'USE_SUBSCRIBE' => $showSubscribe,
				'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
				'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
				'ALT' => $alt,
				'TITLE' => $title,
				'MAGNIFIER_ZOOM_PERCENT' => 200,
				'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
				'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
				'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
					? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
					: null
			),
			'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
			'VISUAL' => $itemIds,
			'DEFAULT_PICTURE' => array(
				'PREVIEW_PICTURE' => $arResult['DEFAULT_PICTURE'],
				'DETAIL_PICTURE' => $arResult['DEFAULT_PICTURE']
			),
			'PRODUCT' => array(
				'ID' => $arResult['ID'],
				'ACTIVE' => $arResult['ACTIVE'],
				'NAME' => $arResult['~NAME'],
				'CATEGORY' => $arResult['CATEGORY_PATH']
			),
			'BASKET' => array(
				'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'BASKET_URL' => $arParams['BASKET_URL'],
				'SKU_PROPS' => $arResult['OFFERS_PROP_CODES'],
				'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
				'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
			),
			'OFFERS' => $arResult['JS_OFFERS'],
			'OFFER_SELECTED' => $arResult['OFFERS_SELECTED'],
			'TREE_PROPS' => $skuProps
		);
	}
	else
	{
		$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
		if ($arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y' && !$emptyProductProperties)
		{
			?>
            <div id="<?=$itemIds['BASKET_PROP_DIV']?>" style="display: none;">
				<?
				if (!empty($arResult['PRODUCT_PROPERTIES_FILL']))
				{
					foreach ($arResult['PRODUCT_PROPERTIES_FILL'] as $propId => $propInfo)
					{
						?>
                        <input type="hidden" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]" value="<?=htmlspecialcharsbx($propInfo['ID'])?>">
						<?
						unset($arResult['PRODUCT_PROPERTIES'][$propId]);
					}
				}

				$emptyProductProperties = empty($arResult['PRODUCT_PROPERTIES']);
				if (!$emptyProductProperties)
				{
					?>
                    <table>
						<?
						foreach ($arResult['PRODUCT_PROPERTIES'] as $propId => $propInfo)
						{
							?>
                            <tr>
                                <td><?=$arResult['PROPERTIES'][$propId]['NAME']?></td>
                                <td>
									<?
									if (
										$arResult['PROPERTIES'][$propId]['PROPERTY_TYPE'] === 'L'
										&& $arResult['PROPERTIES'][$propId]['LIST_TYPE'] === 'C'
									)
									{
										foreach ($propInfo['VALUES'] as $valueId => $value)
										{
											?>
                                            <label>
                                                <input type="radio" name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]"
                                                       value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"checked"' : '')?>>
												<?=$value?>
                                            </label>
                                            <br>
											<?
										}
									}
									else
									{
										?>
                                        <select name="<?=$arParams['PRODUCT_PROPS_VARIABLE']?>[<?=$propId?>]">
											<?
											foreach ($propInfo['VALUES'] as $valueId => $value)
											{
												?>
                                                <option value="<?=$valueId?>" <?=($valueId == $propInfo['SELECTED'] ? '"selected"' : '')?>>
													<?=$value?>
                                                </option>
												<?
											}
											?>
                                        </select>
										<?
									}
									?>
                                </td>
                            </tr>
							<?
						}
						?>
                    </table>
					<?
				}
				?>
            </div>
            
			<?
		}

		$jsParams = array(
			'CONFIG' => array(
				'USE_CATALOG' => $arResult['CATALOG'],
				'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
				'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
				'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
				'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
				'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
				'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
				'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
				'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
				'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
				'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
				'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
				'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
				'USE_STICKERS' => true,
				'USE_SUBSCRIBE' => $showSubscribe,
				'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
				'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
				'ALT' => $alt,
				'TITLE' => $title,
				'MAGNIFIER_ZOOM_PERCENT' => 200,
				'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
				'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
				'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
					? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
					: null
			),
			'VISUAL' => $itemIds,
			'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
			'PRODUCT' => array(
				'ID' => $arResult['ID'],
				'ACTIVE' => $arResult['ACTIVE'],
				'PICT' => reset($arResult['MORE_PHOTO']),
				'NAME' => $arResult['~NAME'],
				'SUBSCRIPTION' => true,
				'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
				'ITEM_PRICES' => $arResult['ITEM_PRICES'],
				'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
				'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
				'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
				'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
				'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
				'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
				'SLIDER' => $arResult['MORE_PHOTO'],
				'CAN_BUY' => $arResult['CAN_BUY'],
				'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
				'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
				'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
				'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
				'CATEGORY' => $arResult['CATEGORY_PATH']
			),
			'BASKET' => array(
				'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
				'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
				'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
				'EMPTY_PROPS' => $emptyProductProperties,
				'BASKET_URL' => $arParams['BASKET_URL'],
				'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
				'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
			)
		);
		unset($emptyProductProperties);
	}

	if ($arParams['DISPLAY_COMPARE'])
	{
		$jsParams['COMPARE'] = array(
			'COMPARE_URL_TEMPLATE' => $arResult['~COMPARE_URL_TEMPLATE'],
			'COMPARE_DELETE_URL_TEMPLATE' => $arResult['~COMPARE_DELETE_URL_TEMPLATE'],
			'COMPARE_PATH' => $arParams['COMPARE_PATH']
		);
	}
	?>
</div>
    <button id="ajax_basket" class="ajax_basket" data-mfp-src="/ajax/basket-popup.php" style="display: none;">CART</button>
    <script>
	BX.message({
		ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		SITE_ID: '<?=CUtil::JSEscape($component->getSiteId())?>'
	});

	var <?=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
</script>
<?
unset($actualItem, $itemIds, $jsParams);