<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Sale;
use Bitrix\Main\Grid\Declension;

CModule::IncludeModule("sale");
/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */

global $APPLICATION;

// вносим ID тегов коллекций
$GLOBALS['COLLECION_TAG_IDS'] = $arResult['COLLECION_TAG_IDS'];
$GLOBALS['ADDITIONAL_IMAGE_IDS'] = $arResult['ADDITIONAL_IMAGE_IDS'];

$itemsInCart = $itemsInFav = $itemsInCompare = [0];

// находим избранные товары
$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$basketItems = $basket->getBasketItems(); // массив объектов Sale\BasketItem
foreach ($basketItems as $basketItem)
{
//	echo $basketItem->getField('NAME') . ' - ' . $basketItem->getQuantity() . '<br />';
	if($basketItem->isDelay())
    {
	    $itemsInFav[] = $basketItem->getProductId();
    }
}
?><script>var favList = <?= json_encode($itemsInFav) ?>;</script><?

//if (isset($templateData['TEMPLATE_THEME']))
//{
//	$APPLICATION->SetAdditionalCSS($templateFolder.'/themes/'.$templateData['TEMPLATE_THEME'].'/style.css');
//	$APPLICATION->SetAdditionalCSS('/bitrix/css/main/themes/'.$templateData['TEMPLATE_THEME'].'/style.css', true);
//}


// получаем отзывы по товару
//pr($templateData['ITEM']['ID']);

//  находим отзывы к товару
$REVIEWS = loadProductReviews($templateData['ITEM']['ID']);
$ratingTotal = 0;
foreach ($REVIEWS as $arReview)
	$ratingTotal += $arReview['UF_RATING'];

$AVERAGE_RATING = intval($ratingTotal / count($REVIEWS));
$reviewsDeclension = new Declension('отзыв', 'отзыва', 'отзывов');
?>
<?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("mf-reviews-detail");?>

<a id="js-reviews-preview" class="reviews-preview js-reviews-preview b-ghost" href="#tab-reviews">
    <div class="review-item__rate">
		<?
		for ($i = 1; $i <= 5; $i++)
		{
			$_cl = 'review-item__rate-icon';
			if ($AVERAGE_RATING >= $i)
				$_cl .= ' filled'
			?><svg class="<?= $_cl ?>" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg><?
		}
		?>
    </div>
    <div class="reviews-quantity">
		<?= count($REVIEWS) . ' ' .  $reviewsDeclension->get(count($REVIEWS)); ?>
    </div>
</a>

<span id="reviews-product-count" class="b-ghost"><?= count($REVIEWS) . ' ' .  $reviewsDeclension->get(count($REVIEWS)); ?></span>

<div id="mf-review-container-wr" class="b-ghost">
	<? foreach ($REVIEWS as $arReview) { ?>
        <div class="review-item">
            <div class="review-item__name"><?= $arReview['USER_INFO']['NAME'] . ' ' . $arReview['USER_INFO']['LAST_NAME'] ?></div>
            <div class="review-item__rate">
				<?
				for ($i = 1; $i <= 5; $i++)
				{
					$_cl = 'review-item__rate-icon';
					if ($arReview['UF_RATING'] != 0 && $arReview['UF_RATING'] >= $i)
						$_cl .= ' filled'
					?><svg class="<?= $_cl ?>" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg><?
				}
				?>
            </div>
            <div class="review-item__date"><?= CIBlockFormatProperties::DateFormat('j F H:i', MakeTimeStamp($arReview['UF_CREATED']->toString(), CSite::GetDateFormat())); ?></div>
            <div class="review-item__text"><?= nl2br($arReview['UF_REVIEW']) ?></div>
            <div class="review-item__usfl">
                <div>Отзыв полезен?</div>
                <button type="button" class="review-item__usfl-like js-btn-review-helpful" data-helpful="yes" data-review="<?= $arReview['ID'] ?>">
                    <svg class="review-item__usfl-icon" width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><path d="M0 12.4091H2.36364V5.31819H0V12.4091ZM13 5.90909C13 5.25909 12.4682 4.72728 11.8182 4.72728H8.09546L8.68636 2.00909C8.68636 1.95 8.68636 1.89091 8.68636 1.83182C8.68636 1.59546 8.56818 1.35909 8.45002 1.18182L7.8 0.590912L3.9 4.49091C3.66364 4.66819 3.54545 4.96364 3.54545 5.31819V11.2273C3.54545 11.8773 4.07727 12.4091 4.72727 12.4091H10.0455C10.5182 12.4091 10.9318 12.1136 11.1091 11.7L12.8818 7.50453C12.9409 7.38634 12.9409 7.20907 12.9409 7.09089V5.90907H13C13 5.96819 13 5.90909 13 5.90909Z"/></svg>
                    <span class="review-item__usfl-num"><?= $arReview['UF_HELPFUL_YES'] ?></span>
                </button>
                <button type="button" class="review-item__usfl-dislike js-btn-review-helpful" data-helpful="no" data-review="<?= $arReview['ID'] ?>">
                    <svg class="review-item__usfl-icon" width="13" height="13" viewBox="0 0 13 13" xmlns="http://www.w3.org/2000/svg"><path d="M0 0.590904H2.36364V7.68181H0V0.590904ZM13 7.09091C13 7.74091 12.4682 8.27272 11.8182 8.27272H8.09546L8.68636 10.9909C8.68636 11.05 8.68636 11.1091 8.68636 11.1682C8.68636 11.4045 8.56818 11.6409 8.45002 11.8182L7.8 12.4091L3.9 8.50909C3.66364 8.33181 3.54545 8.03636 3.54545 7.68181V1.77272C3.54545 1.12272 4.07727 0.590904 4.72727 0.590904H10.0455C10.5182 0.590904 10.9318 0.886359 11.1091 1.3L12.8818 5.49547C12.9409 5.61366 12.9409 5.79093 12.9409 5.90911V7.09093H13C13 7.03181 13 7.09091 13 7.09091Z"/></svg>
                    <span><?= $arReview['UF_HELPFUL_NO'] ?></span>
                </button>
            </div>
        </div>
	<? } ?>
</div>

<script>
    $('#js-reviews-preview').detach().appendTo('#reviews-preview-wr').removeClass('b-ghost');
    $('#mf-review-container-wr').detach().prependTo('#mf-review-container').removeClass('b-ghost');

    $('#review-product').text('');
    $('#reviews-product-count').detach().prependTo('#review-product').removeClass('b-ghost');
    $('#review-product').removeClass('not-visible');

</script>
<?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("mf-reviews-detail", "");?>
<?

if (!empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;

	if (!empty($templateData['CURRENCIES']))
	{
		$loadCurrency = Loader::includeModule('currency');
	}

	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
		?>
		<script>
			BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
		</script>
		<?
	}
}

if (isset($templateData['JS_OBJ']))
{
	?>
	<script>
		BX.ready(BX.defer(function(){
			if (!!window.<?=$templateData['JS_OBJ']?>)
			{
				window.<?=$templateData['JS_OBJ']?>.allowViewedCount(true);
			}
		}));
	</script>
	<?
	// check compared state
	if ($arParams['DISPLAY_COMPARE'])
	{
		$compared = false;
		$comparedIds = array();
		$item = $templateData['ITEM'];

		if (!empty($_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]))
		{
			if (!empty($item['JS_OFFERS']))
			{
				foreach ($item['JS_OFFERS'] as $key => $offer)
				{
					if (array_key_exists($offer['ID'], $_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']))
					{
						if ($key == $item['OFFERS_SELECTED'])
						{
							$compared = true;
						}

						$comparedIds[] = $offer['ID'];
					}
				}
			}
			elseif (array_key_exists($item['ID'], $_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']))
			{
				$compared = true;
			}
		}

		if ($templateData['JS_OBJ'])
		{
			?>
			<script>
				BX.ready(BX.defer(function(){
					if (!!window.<?=$templateData['JS_OBJ']?>)
					{
						window.<?=$templateData['JS_OBJ']?>.setCompared('<?=$compared?>');

						<? if (!empty($comparedIds)): ?>
						window.<?=$templateData['JS_OBJ']?>.setCompareInfo(<?=CUtil::PhpToJSObject($comparedIds, false, true)?>);
						<? endif ?>
					}
				}));
			</script>
			<?
		}
	}

	// select target offer
	$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
	$offerNum = false;
	$offerId = (int)$this->request->get('OFFER_ID');
	$offerCode = $this->request->get('OFFER_CODE');

	if ($offerId > 0 && !empty($templateData['OFFER_IDS']) && is_array($templateData['OFFER_IDS']))
	{
		$offerNum = array_search($offerId, $templateData['OFFER_IDS']);
	}
	elseif (!empty($offerCode) && !empty($templateData['OFFER_CODES']) && is_array($templateData['OFFER_CODES']))
	{
		$offerNum = array_search($offerCode, $templateData['OFFER_CODES']);
	}

	if (!empty($offerNum))
	{
		?>
		<script>
			BX.ready(function(){
				if (!!window.<?=$templateData['JS_OBJ']?>)
				{
					window.<?=$templateData['JS_OBJ']?>.setOffer(<?=$offerNum?>);
				}
			});
		</script>
		<?
	}
}
?>
<?ob_start();?>
<div id='stack' style='display:none;'><?=$_SESSION['city'];?></div>
<script type="text/javascript"> (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() { try { rrApi.groupView([<?$counter = 0; $countGoods = count($arResult['OFFERS_ID']); foreach($arResult['OFFERS_ID'] as $aroffer): $counter++?><?=$aroffer;?><?if($counter!=$countGoods):?>,<?endif;?><?endforeach;?>], {stockId: "<?=$_SESSION['city'];?>"}); } catch(e) {} }) </script>
<div class="fix-block">
<div data-retailrocket-markup-block="5d1dba6597a5280cd4e57053"
data-product-id="<?$counter = 0; $countGoods = count($arResult['OFFERS_ID']); foreach($arResult['OFFERS_ID'] as $aroffer): $counter++?><?=$aroffer;?><?if($counter!=$countGoods):?>,<?endif;?><?endforeach;?>" data-stock-id="<?=$_SESSION['city'];?>"></div>
<div data-retailrocket-markup-block="5d1dba5f97a5280cd4e57051"
data-product-id="<?$counter = 0; $countGoods = count($arResult['OFFERS_ID']); foreach($arResult['OFFERS_ID'] as $aroffer): $counter++?><?=$aroffer;?><?if($counter!=$countGoods):?>,<?endif;?><?endforeach;?>" data-stock-id="<?=$_SESSION['city'];?>"></div>
</div>
<script>retailrocket.markup.render();</script>
        <?
    $out1 = ob_get_contents();
    ob_end_clean();
$APPLICATION->AddViewContent('hit_fav',$out1);?>

<style>
    .mf-product-gallery-list .mf-product-img:last-child {
        margin-bottom: 0;
    }
</style>

