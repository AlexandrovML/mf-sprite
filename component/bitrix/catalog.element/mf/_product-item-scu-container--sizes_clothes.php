<div class="mf-product-attr-option mf-product-size">
    <div id="mf-pr-table-atts" class="mf-pr-table-atts">
        <div class="mf-pr-cell-atts">
            <div class="h3"><?= $skuProperty['CODE']=='NOMINALNAYA_STOIMOST'?'Номинальная стоимость':'Размер' ?>:</div>
            <?  ?>

            <?/*<span class="mf-link-option-product table-sizes-product">Таблица размеров</span>*/?>

            <ul class="mf-product-sizes mf-product-sizes--<?= $skuProperty['CODE'] ?>" data-prop="<?= $skuProperty['CODE'] ?>">
                <? foreach ($skuProperty['VALUES'] as &$value) { ?>
                    <?/*<li class="disabled-size" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>"><?=$value['NAME']?></li>*/?>
                    <li class="js_prop_val_<?= $skuProperty['CODE'] ?>" data-prop="<?= $skuProperty['CODE'] ?>" data-treevalue="<?=$propertyId?>_<?=$value['ID']?>" <?= $value['NAME']=='-'?'style="display:none;"':'' ?>><?=$value['NAME']?></li>
                <? } ?>
            </ul>
            <?/*<span class="mf-link-option-product check-size-product">Определить размер</span>*/?>
	        <? if($arResult['SIZES_ELEMENT_ID'] > 0) { ?>
                <button type="button" id="check-size-product" class="btn-clear mf-link-option-product check-size-product" data-mfp-src="/ajax/check-size-product.php?ELEMENT_ID=<?= $arResult['SIZES_ELEMENT_ID'] ?>">Определить размер</button>
	        <? } ?>
        </div>

        <?/*
        <div class="mf-pr-cell-atts">
            <span class="mf-title-option-product check-height">Выбрать рост:</span>
            <div class="mf-input-quantity-horizontal">
                <div class="mf-input-plus">
                    <span>+</span>
                </div>
                <input type="text" name="mf_check_height" maxlength="3" placeholder="156 см" />
                <div class="mf-input-minus">
                    <span>-</span>
                </div>
            </div>
        </div>
        */?>
    </div>
</div>
<div style="display: none"><? include '_product-item-scu-container.php'?></div>

