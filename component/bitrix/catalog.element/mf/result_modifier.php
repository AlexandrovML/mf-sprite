<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

/**
 * @param string $refProp
 * @param array  $arResult
 * @param array  $arSizes
 */


$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

// префикс для старой цены
switch (SITE_ID)
{
	case's3':
		$prefix='_KZ';
		break;

	case's2':
		$prefix='_RU';
		break;

	case's1':
	default:
		$prefix='_BY';
		break;
}

if(empty($arParams['CURRENCY_ID'])) $arParams['CURRENCY_ID'] = 'BYN';


// дефолтное ограничение на макс. кол-во для покупки
if (empty($arParams['MAX_QUANTITY_PER_PRODUCT']) || $arParams['MAX_QUANTITY_PER_PRODUCT'] == 0)
	$arParams['MAX_QUANTITY_PER_PRODUCT'] = 15;

//foreach ($arResult['OFFERS'] as $key=>$arOffer)
//{
//	if ($arOffer['ID'] == 6340 ||$arOffer['ID'] == 6342)
//	{
//		unset($arResult['OFFERS'][$key]);
//	}
//}
//
//foreach ($arResult['JS_OFFERS'] as $key=>$arOffer)
//{
//	if ($arOffer['ID'] == 6340 ||$arOffer['ID'] == 6342)
//	{
//		unset($arResult['JS_OFFERS'][$key]);
//	}
//}
//pr($arParams['OFFER_TREE_PROPS']);
//pr($arResult['OFFERS_PROP']);


/*
 * ищем в ТП варианты, когда товар недоступен к покупке
 * и доступен при одинаковых комбинациях свойств
 *
 * А если все из них не в наличии - то оставить одну,
 * которая самая дорогая их них
 *
 * А если все в наличии то оставлем самую дешевую.
 *
 */

$arPresentedSkuPropValues = [];
$arPresentedSkuPrices = [];

foreach ($arResult['OFFERS'] as $k=>&$arOffer)
{
	$propsKey = '';
	foreach ($arParams['OFFER_TREE_PROPS'] as $proCode)
	{
		if(!empty($arOffer['PROPERTIES'][$proCode]['VALUE']))
		{
			$propsKey .= $proCode . '-' . $arOffer['PROPERTIES'][$proCode]['VALUE'].'|';
		}
	}

	$arPresentedSkuPropValues[($propsKey)][$arOffer['ID']] = $arOffer['CAN_BUY']===true?1:0;

	if($arOffer['CAN_BUY']=='Y' && $arOffer['ITEM_PRICES_CAN_BUY']=='Y')
	{
		//рассчитываем старую цену
		if(!empty($arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE']) && floatval($arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE']) > $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['RATIO_BASE_PRICE'])
		{
			$arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['PERCENT'] = 5;
			$arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['PRINT_RATIO_BASE_PRICE'] = CurrencyFormat(floatval($arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE']), $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['CURRENCY']);
			$arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['RATIO_BASE_PRICE'] = floatval($arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE']);

			foreach ($arResult['JS_OFFERS'] as &$arJsOffer)
			{
				if ($arOffer['ID'] == $arJsOffer['ID'])
				{
					$arJsOffer['ITEM_PRICES'][$arJsOffer['ITEM_PRICE_SELECTED']]['PERCENT'] = 5;
					$arJsOffer['ITEM_PRICES'][$arJsOffer['ITEM_PRICE_SELECTED']]['PRINT_RATIO_BASE_PRICE'] = CurrencyFormat(floatval($arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE']), $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['CURRENCY']);
					$arJsOffer['ITEM_PRICES'][$arJsOffer['ITEM_PRICE_SELECTED']]['RATIO_BASE_PRICE'] = floatval($arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE']);
					break;
				}
			}
			unset($arJsOffer);

		}

//		pr($arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['RATIO_PRICE']);

		$arPresentedSkuPrices[$arOffer['ID']] = $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['RATIO_PRICE'];
	}
}
unset($arOffer);

checkSkuOnProductPage($arPresentedSkuPropValues, $arPresentedSkuPrices, $arResult);

/*
// находим ТП которые недоступны к покупке и коорые тут нужно удалить
$arIdsToRemove = [];
foreach ($arPresentedSkuPropValues as &$arPropsItem)
{
	if(count($arPropsItem) == 1) continue;
	foreach ($arPropsItem as $offerId => $canBuy)
	{
		if($canBuy == 0)
		{
			$arIdsToRemove[] = $offerId;
			unset($arPropsItem[$offerId]);
		}
	}
}
unset($arPropsItem);

//pr($arIdsToRemove);
//pr($arPresentedSkuPrices);
//pr($arPresentedSkuPropValues);

// вторая проверка на то, что нужно убрать из выдачи ТП
// с меньшей ценой, если есть 2ТП одной модели но разного артикула
foreach ($arPresentedSkuPropValues as $arPropsItem)
{
	if (count($arPropsItem) > 1)
	{
		$_minPrice = $_offerId = 0;
		$bFirstStep = false;

		$arBuyYes = $arBuyNo = [];
		foreach ($arPropsItem as $offerId=>$canBuy)
		{
			if ($canBuy) $arBuyYes[] = $offerId;
				else $arBuyNo[] = $offerId;
		}

		if (count($arBuyYes) > 1)
		{
//			pr($arPropsItem);
			foreach ($arPropsItem as $offerId=>$canBuy)
			{
				if ($canBuy == 0) continue;

				if ($_minPrice == 0)
				{
					$_minPrice = $arPresentedSkuPrices[$offerId];
					$_offerId = $offerId;
//				$bFirstStep = true;

				}
				elseif ($_minPrice > $arPresentedSkuPrices[$offerId])
				{
					$_minPrice = $arPresentedSkuPrices[$offerId];
					$_offerId = $offerId;
//				$bFirstStep = false;
				}
			}

//		pr($_minPrice);

			if (count($arPropsItem) > 2)
			{
				if ($_offerId > 0)
				{
					pr($_offerId);
				$arIdsToRemove[] = $_offerId;
				}
			}
		}
	}
}

//pr($arIdsToRemove);
//pr($arPresentedSkuPropValues);
//pr($arPresentedSkuPrices);
//pr($arResult['OFFERS_SELECTED']);
//pr($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]);
$OFFERS_SELECTED_ID = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'];
// если есть ТП для удаления и указан активный ТП то ищем его "соседа"
if (count($arIdsToRemove) && isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]))
{
	$id = $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]['ID'];

	foreach ($arPresentedSkuPropValues as $arPropsItem)
	{
		$thisPropsValues = false;

		foreach ($arPropsItem as $offerId => $canBuy)
		{
			if ($offerId == $id)
			{
				$thisPropsValues = true;
				break;
			}
		}

		if ($thisPropsValues)
		{
			foreach ($arPropsItem as $offerId => $canBuy)
			{
				if ($offerId != $id && $canBuy == 1)
				{
					foreach ($arResult['OFFERS'] as $k1 => $arOffer)
					{
						if ($arOffer['ID'] == $offerId)
						{
							$OFFERS_SELECTED_ID = $arOffer['ID'];
							break;
						}
					}
					break;
				}
			}
		}
	}
}

//pr($OFFERS_SELECTED_ID);

if (count($arIdsToRemove))
{
// перестраиваем массивы ТП без удаленных ТП
	$arOffers = [];
	foreach ($arResult['OFFERS'] as $k=>$arOffer)
	{
		if (!in_array($arOffer['ID'], $arIdsToRemove))
		{
			$arOffers[] = $arOffer;
		}
	}

	$arResult['OFFERS'] = $arOffers;

	$arOffers = [];
	foreach ($arResult['JS_OFFERS'] as $k=>$arOffer)
	{
		if (!in_array($arOffer['ID'], $arIdsToRemove))
		{
			$arOffers[] = $arOffer;
		}
	}

	$arResult['JS_OFFERS'] = $arOffers;


	// отмечаем активное ТП
	foreach ($arResult['OFFERS'] as $k=>$arOffer)
	{
//		pr($arOffer['ID']);
		if ($arOffer['ID'] == $OFFERS_SELECTED_ID)
		{
			$arResult['OFFERS_SELECTED'] = $k;
			break;
		}
	}

}
//pr($arResult['OFFERS_SELECTED']);

//pr(count($arResult['OFFERS']));
//pr(count($arResult['JS_OFFERS']));
//pr($arResult['OFFERS_SELECTED']);
//pr($arResult['OFFERS'][$arResult['OFFERS_SELECTED']]);
//pr($arResult);
/**/


// проверяем есть ли отличия в цене у SKU
$arResult['SKU_PRICE_IS_CHANGING'] = false;
$arResult['IS_SUBSCRIPTION_PRODUCT'] = false;
$offerPrice = -1;
foreach ($arResult['OFFERS'] as $arOffer)
{
	$price = $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']];

	if($offerPrice == -1)
	{
		$offerPrice = $price['RATIO_PRICE'];
	}
	elseif($offerPrice != $price['RATIO_PRICE'])
	{
//		$arResult['SKU_PRICE_IS_CHANGING'] = true;
		break;
	}

	if (!empty($arOffer['PROPERTIES']['PERIOD_PODPISKI']['VALUE']))
	{

		$arResult['IS_SUBSCRIPTION_PRODUCT'] = true;
//		$arResult['SKU_PRICE_IS_CHANGING'] = true;
	}
}


$CURRENCY = 'BYN';
if(!empty($arResult['OFFERS'][0]))
{
	$CURRENCY = $arResult['OFFERS'][0]['ITEM_PRICES'][0]['CURRENCY'];
}

// получаем доп. инфомацию из справочника цветов

// ищем доп товары по такой же ID модели
$arRelatedModels = [];
$TABLE_NAME_COLOR_REF = $TABLE_NAME_DECOR_REF = '';

$arColorCodes = [$arResult['PROPERTIES']['COLOR_REF']['VALUE']];
$TABLE_NAME_COLOR_REF = $arResult['PROPERTIES']['COLOR_REF']['USER_TYPE_SETTINGS']['TABLE_NAME'];

if(!empty($arResult['PROPERTIES']['DECOR_REF']['VALUE']))
{
	$arDecorCodes = [$arResult['PROPERTIES']['DECOR_REF']['VALUE']];
}
else
{
	$arDecorCodes = [];
}

$TABLE_NAME_DECOR_REF = $arResult['PROPERTIES']['DECOR_REF']['USER_TYPE_SETTINGS']['TABLE_NAME'];

// если заполнено св-во ID модели
if(!empty($arResult['PROPERTIES']['MODEL_ID']['VALUE']))
{
	$arSelect = ["ID", "IBLOCK_ID", "NAME", "DETAIL_PAGE_URL"];
	$arFilter = [
		"IBLOCK_ID"=>$arParams['IBLOCK_ID'],
		"ACTIVE_DATE"=>"Y",
		"ACTIVE"=>"Y",
		'PROPERTY_MODEL_ID'=>$arResult['PROPERTIES']['MODEL_ID']['VALUE'],
		'!ID' => $arResult['ID'],
	];

	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$arProps = $ob->GetProperties();
		$arFields['PROPERTIES'] = $arProps;
		$arColorCodes[] = $arProps['COLOR_REF']['VALUE'];
		$arDecorCodes[] = $arProps['DECOR_REF']['VALUE'];

		if(empty($TABLE_NAME_COLOR_REF)) $TABLE_NAME_COLOR_REF = $arProps['COLOR_REF']['USER_TYPE_SETTINGS']['TABLE_NAME'];
		if(empty($TABLE_NAME_DECOR_REF)) $TABLE_NAME_DECOR_REF = $arProps['DECOR_REF']['USER_TYPE_SETTINGS']['TABLE_NAME'];

		$arRelatedModels[] = $arFields;
	}
}


$arUserGroups = $USER->GetUserGroupArray();

if(!empty($TABLE_NAME_COLOR_REF) && !empty($arColorCodes))
{
	$arColorCodes = array_unique($arColorCodes);
	$arFilter = ['UF_XML_ID'=>$arColorCodes];
	$arColorRefs = getDataFromReference($TABLE_NAME_COLOR_REF, ['*'], 'UF_XML_ID', $arFilter);
}
else
{
	$arColorRefs = [];
}

if(!empty($TABLE_NAME_DECOR_REF) && !empty($arDecorCodes))
{
	$arDecorCodes = array_unique($arDecorCodes);
	$arFilter = ['UF_XML_ID'=>$arDecorCodes];
	$arDecorRefs = getDataFromReference($TABLE_NAME_DECOR_REF, ['*'], 'UF_XML_ID', $arFilter);
}
else
{
	$arDecorRefs = [];
}

$arResult['arPricesColors'] = [];
$arResult['MIN_SKU_PRICE'] = $arResult['MAX_SKU_PRICE'] = 0;
$arResult['PRINT_MIN_SKU_PRICE'] = $arResult['PRINT_MAX_SKU_PRICE'] = 0;

// находим min / max цены в ТП и строим массив цена-цвет

// ищем мин цену сначала среди текущих ТП
$minPrice = $maxPrice = 0;
$minPricePrint = $maxPricePrint = '';

foreach ($arResult['OFFERS'] as $arOffer)
{
//	pr($arOffer['ITEM_PRICES']);
	if($minPrice == 0)
	{
		$minPrice = $arOffer['ITEM_PRICES'][0]['RATIO_PRICE'];
		$minPricePrint = $arOffer['ITEM_PRICES'][0]['PRINT_RATIO_PRICE'];
	}
	elseif ($arOffer['ITEM_PRICES'][0]['RATIO_PRICE'] < $minPrice)
	{
		$minPrice = $arOffer['ITEM_PRICES'][0]['RATIO_PRICE'];
		$minPricePrint = $arOffer['ITEM_PRICES'][0]['PRINT_RATIO_PRICE'];
	}

	if ($arOffer['ITEM_PRICES'][0]['RATIO_PRICE'] > $maxPrice)
	{
		$maxPrice = $arOffer['ITEM_PRICES'][0]['RATIO_PRICE'];
		$maxPricePrint = $arOffer['ITEM_PRICES'][0]['PRINT_RATIO_PRICE'];
	}
}

// находим min / max цены и строим массив цена-цвет и цена-рисунок
$arResult['arPricesColors'] = [
	$minPricePrint => [
		$arResult['PROPERTIES']['COLOR_REF']['VALUE'] => $arColorRefs[$arResult['PROPERTIES']['COLOR_REF']['VALUE']],
	],
];
$arResult['arPricesColors'][$minPricePrint][$arResult['PROPERTIES']['COLOR_REF']['VALUE']]['PRODUCT_ID'] = $arResult['ID'];

if(!empty($arResult['PROPERTIES']['DECOR_REF']['VALUE']))
{
	$arResult['arPricesDecors'] = [
		$minPricePrint => [
			$arResult['PROPERTIES']['DECOR_REF']['VALUE'] => $arDecorRefs[$arResult['PROPERTIES']['DECOR_REF']['VALUE']],
		],
	];
	$arResult['arPricesDecors'][$minPricePrint][$arResult['PROPERTIES']['DECOR_REF']['VALUE']]['PRODUCT_ID'] = $arResult['ID'];
}

$arResult['MIN_SKU_PRICE'] = $minPrice;
$arResult['PRINT_MIN_SKU_PRICE'] = $minPricePrint;
$arResult['MAX_SKU_PRICE'] = $maxPrice;
$arResult['PRINT_MAX_SKU_PRICE'] = $maxPricePrint;

//pr($arResult['PROPERTIES']['COLOR_REF']['VALUE']);
//pr($arResult['arPricesColors']);
//pr($arResult['arPricesDecors']);
foreach ($arRelatedModels as $model)
{
//	$minPrice = $maxPrice = 0;
//	$minPricePrint = $maxPricePrint = '0';

	/*
	// ищем все ТП товара
	$arOffersModel = CCatalogSKU::getOffersList($model['ID']);

	if(!empty($arOffersModel[$model['ID']]))
	{
		foreach ($arOffersModel[$model['ID']] as $offerItem)
		{
			$arPrice = CCatalogProduct::GetOptimalPrice($offerItem['ID'], 1, $arUserGroups);

//			pr($arPrice);

			if($offerPrice == -1)
			{
				$offerPrice = $arPrice['RESULT_PRICE']['DISCOUNT_PRICE'];
			}
			elseif($offerPrice != $arPrice['RESULT_PRICE']['DISCOUNT_PRICE'])
			{
				$arResult['SKU_PRICE_IS_CHANGING'] = true;
			}


			if($minPrice == 0)
			{
				$minPrice = $arPrice['RESULT_PRICE']['DISCOUNT_PRICE'];
				$minPricePrint = CCurrencyLang::CurrencyFormat($arPrice['RESULT_PRICE']['DISCOUNT_PRICE'], $arPrice['RESULT_PRICE']['CURRENCY']);
			}
			elseif ($arPrice['RESULT_PRICE']['DISCOUNT_PRICE'] < $minPrice)
			{
				$minPrice = $arPrice['RESULT_PRICE']['DISCOUNT_PRICE'];
				$minPricePrint = CCurrencyLang::CurrencyFormat($arPrice['RESULT_PRICE']['DISCOUNT_PRICE'], $arPrice['RESULT_PRICE']['CURRENCY']);
			}

			if ($arPrice['RESULT_PRICE']['DISCOUNT_PRICE'] > $maxPrice)
			{
				$maxPrice = $arPrice['RESULT_PRICE']['DISCOUNT_PRICE'];
				$maxPricePrint = CCurrencyLang::CurrencyFormat($arPrice['RESULT_PRICE']['DISCOUNT_PRICE'], $arPrice['RESULT_PRICE']['CURRENCY']);
			}
		}
	}

	if($arResult['MIN_SKU_PRICE'] == 0)
	{
		$arResult['MIN_SKU_PRICE'] = $minPrice;
		$arResult['PRINT_MIN_SKU_PRICE'] = $minPricePrint;
	}
	elseif($minPrice < $arResult['MIN_SKU_PRICE'])
	{
		$arResult['MIN_SKU_PRICE'] = $minPrice;
		$arResult['PRINT_MIN_SKU_PRICE'] = $minPricePrint;
	}

	if($maxPrice > $arResult['MAX_SKU_PRICE'])
	{
		$arResult['MAX_SKU_PRICE'] = $maxPrice;
		$arResult['PRINT_MAX_SKU_PRICE'] = $maxPricePrint;
	}
	*/
	if(empty($arResult['arPricesColors'][$minPricePrint][$model['PROPERTIES']['COLOR_REF']['VALUE']]))
	{
		$arResult['arPricesColors'][$minPricePrint][$model['PROPERTIES']['COLOR_REF']['VALUE']] = $arColorRefs[$model['PROPERTIES']['COLOR_REF']['VALUE']];
		$arResult['arPricesColors'][$minPricePrint][$model['PROPERTIES']['COLOR_REF']['VALUE']]['PRODUCT_ID'] = $model['ID'];
		$arResult['arPricesColors'][$minPricePrint][$model['PROPERTIES']['COLOR_REF']['VALUE']]['DETAIL_PAGE_URL'] = $model['DETAIL_PAGE_URL'];
	}

//	if(!empty($arResult['PROPERTIES']['DECOR_REF']['VALUE']) && empty($arResult['arPricesDecors'][$minPrice][$model['PROPERTIES']['DECOR_REF']['VALUE']]))
//	pr($model['ID'] . ' | ' . $model['PROPERTIES']['DECOR_REF']['VALUE'] . ' | ' . $model['PROPERTIES']['COLOR_REF']['VALUE']);

	if(empty($arResult['arPricesDecors'][$minPricePrint][$model['PROPERTIES']['DECOR_REF']['VALUE']]))
	{
		$arResult['arPricesDecors'][$minPricePrint][$model['PROPERTIES']['DECOR_REF']['VALUE']] = $arDecorRefs[$model['PROPERTIES']['DECOR_REF']['VALUE']];
		$arResult['arPricesDecors'][$minPricePrint][$model['PROPERTIES']['DECOR_REF']['VALUE']]['PRODUCT_ID'] = $model['ID'];
		$arResult['arPricesDecors'][$minPricePrint][$model['PROPERTIES']['DECOR_REF']['VALUE']]['DETAIL_PAGE_URL'] = $model['DETAIL_PAGE_URL'];
	}
}


// сортируем полученный массив по возрастанию ключа
ksort($arResult['arPricesColors']);
foreach ($arResult['arPricesColors'] as &$ar)
{
	ksort($ar);
}
unset($ar);

ksort($arResult['arPricesDecors']);
foreach ($arResult['arPricesDecors'] as &$ar)
{
	ksort($ar);
}
unset($ar);


$arResult['CURRENT_COLOR'] = $arColorRefs[$arResult['PROPERTIES']['COLOR_REF']['VALUE']]['UF_NAME'];

if(!empty($arResult['PROPERTIES']['DECOR_REF']['VALUE']))
{
	$arResult['CURRENT_DECOR'] = $arDecorRefs[$arResult['PROPERTIES']['DECOR_REF']['VALUE']]['UF_NAME'];
}
else
{
	$arResult['CURRENT_DECOR'] = '';
}

// получаем теги
$arResult['PROPERTIES']['TAGS']['DISPLAY_VALUE'] = $collectionTagIds = [];
if(!empty($arResult['PROPERTIES']['TAGS']['VALUE']))
{
	switch (SITE_ID)
	{
		case 's3':
			$PROPERTY_REGION_VALUE = 'kz';
			break;

		case 's2':
			$PROPERTY_REGION_VALUE = 'ru';
			break;

		case 's1':
		default:
			$PROPERTY_REGION_VALUE = 'by';
			break;
	}

	$arFilter = [
		"IBLOCK_ID" => $arResult['PROPERTIES']['TAGS']['LINK_IBLOCK_ID'],
		"ID" => $arResult['PROPERTIES']['TAGS']['VALUE']
	];
//	pr($arFilter);
	$arSelect = ["ID", "IBLOCK_ID", "SECTION_ID", "NAME", "DETAIL_PAGE_URL"];
	$res = CIBlockElement::GetList(['SORT'=>'asc'], $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$arProps = $ob->GetProperties();
		if (!empty($arProps['REGION']['VALUE']) && !in_array($PROPERTY_REGION_VALUE, $arProps['REGION']['VALUE'])) continue;

		$arResult['PROPERTIES']['TAGS']['DISPLAY_VALUE'][] = [
			'ID' => $arFields['ID'],
			'NAME' => $arFields['NAME'],
			'DETAIL_PAGE_URL' => $arFields['DETAIL_PAGE_URL'],
			'IBLOCK_SECTION_ID' => $arFields['IBLOCK_SECTION_ID'],
		];

		if($arFields['IBLOCK_SECTION_ID'] == '164')
		{
			$collectionTagIds[] = $arFields['ID'];
		}
	}
}

//pr($arResult['PROPERTIES']['COLOR_REF']);


if (!empty($arResult['PROPERTIES']['COLOR_REF']['VALUE']) && !empty($arColorRefs[$arResult['PROPERTIES']['COLOR_REF']['VALUE']]))
{
// дополняем ИД товаров для блока "Дополни образ"
//pr($arResult['IBLOCK_SECTION_ID']);
	$additionalImageIds = [];
	$arSectionsMap = [
		915 => 909, // Трусы -> Бюстгальтеры
		909 => 915  // Бюстгальтеры -> Трусы
	];

// находим предков текущего раздела
	$rsSection = CIBlockSection::GetByID($arResult['IBLOCK_SECTION_ID']);
	if ($thisSection = $rsSection->GetNext())
	{
		$rsSect = \CIBlockSection::GetList(array('left_margin' => 'asc'), array(
			'IBLOCK_ID' => $arParams['IBLOCK_ID'],
			"<=LEFT_BORDER" => $thisSection["LEFT_MARGIN"],
			">=RIGHT_BORDER" => $thisSection["RIGHT_MARGIN"],
			"<DEPTH_LEVEL" => $thisSection["DEPTH_LEVEL"]
		));

		while($arSect = $rsSect->Fetch())
		{
//		pr($arSect);
			if (!empty($arSectionsMap[$arSect['ID']]))
			{
				$arSectionIds = [];

				// находим потомков нужного раздела
				$rsParentSection = CIBlockSection::GetByID($arSectionsMap[$arSect['ID']]);
				if ($arParentSection = $rsParentSection->GetNext())
				{
//				pr($arSect['ID']);
//				pr($arSectionsMap[$arSect['ID']]);
//				pr($arParentSection);
					$arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
					$rsSect1 = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter);
					while ($arSect1 = $rsSect1->GetNext())
					{
						// получаем подразделы
						$arSectionIds[] = $arSect1['ID'];
					}
//				pr($arSectionIds);


					$arSelect = Array(
						"ID",
						"IBLOCK_ID",
						"NAME",
//					"PROPERTY_*"
					);

					$arFilter = Array(
						"IBLOCK_ID"=>$arParams['IBLOCK_ID'],
						"IBLOCK_SECTION_ID"=>$arSectionIds,
						"ACTIVE_DATE"=>"Y",
						"ACTIVE"=>"Y",
						"PROPERTY_COLOR_REF"=>$arColorRefs[$arResult['PROPERTIES']['COLOR_REF']['VALUE']]['UF_XML_ID']
					);

//				pr($arResult['PROPERTIES']['COLOR_REF']['VALUE']);
//				pr($arColorRefs[$arResult['PROPERTIES']['COLOR_REF']['VALUE']]);
//				pr($arFilter);

					$res1 = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

					while($ob1 = $res1->GetNextElement())
					{
						$arFields = $ob1->GetFields();
						$additionalImageIds[] = $arFields['ID'];
//					pr($arFields);
//						$arProps = $ob1->GetProperties();
//					pr($arProps['COLOR_REF']['VALUE']);

//					break;
					}
				}


				break;
			}
		}
	}

//	pr($additionalImageIds);

}


// получаем ID материала где хранится размерная сетка
$arResult['SIZES_ELEMENT_ID'] = 0;
$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'],'ID'=>$arResult['IBLOCK_SECTION_ID']);
$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter, false, ['ID', 'NAME', 'IBLOCK_ID', 'UF_SIZES']);
if ($arSect = $rsSect->GetNext())
{
	if(!empty($arSect['UF_SIZES']))
	{
		$arResult['SIZES_ELEMENT_ID'] = $arSect['UF_SIZES'];
	}
}

if($arResult['SIZES_ELEMENT_ID'] == 0)
{
	$arIds = [];
	$nav = CIBlockSection::GetNavChain(false, $arResult['IBLOCK_SECTION_ID'], ['ID']);
	while($arItem = $nav->Fetch()){
		$ITEMS[] = $arItem;
		$arIds[] = $arItem['ID'];
	}

	$arFilter = array('IBLOCK_ID' => $arParams['IBLOCK_ID'],'ID'=>$arIds);
	$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter, false, ['ID', 'NAME', 'IBLOCK_ID', 'UF_SIZES']);
	while ($arSect = $rsSect->GetNext())
	{
		if(!empty($arSect['UF_SIZES']))
		{
			$arResult['SIZES_ELEMENT_ID'] = $arSect['UF_SIZES'];
		}
	}
}

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
}
else
{
	$actualItem = $arResult;
}

foreach ($arResult['MORE_PHOTO'] as &$photo)
{
//	pr($photo);
	$arFile = CFile::ResizeImageGet($photo['ID'], Array("width"=>704, "height"=>880), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
	$photo['SRC'] = $arFile['src'];
}
unset($photo);

foreach ($arResult['OFFERS'] as &$arOffer)
{
	foreach ($arOffer['MORE_PHOTO'] as &$photo)
	{
		$arFile = CFile::ResizeImageGet($photo['ID'], Array("width"=>704, "height"=>880), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
		$photo['SRC'] = $arFile['src'];
	}
	unset($photo);
}
unset($arOffer);
//pr($arResult['OFFERS']);


//var_dump($arResult['SKU_PRICE_IS_CHANGING']);

// проставляем ограничение по максимальному кол-ву, доступному для заказа
if(!empty($arResult['JS_OFFERS']))
{
	foreach ($arResult['JS_OFFERS'] as &$arJsOffer)
	{
		if ($arJsOffer['MAX_QUANTITY'] > $arParams['MAX_QUANTITY_PER_PRODUCT'])
			$arJsOffer['MAX_QUANTITY'] = $arParams['MAX_QUANTITY_PER_PRODUCT'];
	}
	unset($arJsOffer);
}

// проверяем, чтобы о ТП цена была отлична от 0
foreach ($arResult['OFFERS'] as $k=>&$arOffer)
{
	if (!empty($arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]))
	{
		$price = $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']];

		if($price['BASE_PRICE'] == 0)
		{
			$arOffer['CAN_BUY'] = false;
		}
	}
}
unset($arOffer);

if(!empty($arResult['JS_OFFERS']))
{
	foreach ($arResult['JS_OFFERS'] as &$arOffer)
	{
		if (!empty($arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]))
		{
			$price = $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']];

			if($price['BASE_PRICE'] == 0)
			{
				$arOffer['CAN_BUY'] = false;
			}
		}

	}
	unset($arOffer);
}

foreach($arResult['OFFERS'] as $arOffer){
    $arOffersId[]=$arOffer['ID'];
}

//  находим отзывы к товару
$arResult['REVIEWS'] = loadProductReviews($arResult['ID']);
$ratingTotal = 0;
foreach ($arResult['REVIEWS'] as $arReview)
	$ratingTotal += $arReview['UF_RATING'];

$arResult['AVERAGE_RATING'] = intval($ratingTotal / count($arResult['REVIEWS']));


$this->__component->arResult["COLLECION_TAG_IDS"] = $collectionTagIds;
$this->__component->arResult["ADDITIONAL_IMAGE_IDS"] = $additionalImageIds;
$this->__component->arResult["OFFERS_ID"] = $arOffersId;
$this->__component->SetResultCacheKeys([
	"COLLECION_TAG_IDS",
	"ADDITIONAL_IMAGE_IDS",
    "OFFERS_ID",
]);


//$ar = CIBlockSectionPropertyLink::GetArray($arParams['IBLOCK_ID'], $arResult['IBLOCK_SECTION_ID']);