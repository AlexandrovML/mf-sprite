<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

//use Bitrix\Main\Loader;
//use Bitrix\Sale;
use Bitrix\Main\Grid\Declension;
//CModule::IncludeModule("sale");
/**
 * @var array $templateData
 * @var array $arParams
 * @var string $templateFolder
 * @global CMain $APPLICATION
 */
/*
global $APPLICATION;

// вносим ID тегов коллекций
$GLOBALS['COLLECION_TAG_IDS'] = $arResult['COLLECION_TAG_IDS'];

$itemsInCart = $itemsInFav = $itemsInCompare = [0];

// находим избранные товары
$basket = Sale\Basket::loadItemsForFUser(Sale\Fuser::getId(), Bitrix\Main\Context::getCurrent()->getSite());
$basketItems = $basket->getBasketItems(); // массив объектов Sale\BasketItem
foreach ($basketItems as $basketItem)
{
//	echo $basketItem->getField('NAME') . ' - ' . $basketItem->getQuantity() . '<br />';
	if($basketItem->isDelay())
    {
	    $itemsInFav[] = $basketItem->getProductId();
    }
}
?><script>var favList = <?= json_encode($itemsInFav) ?>;</script><?

//if (isset($templateData['TEMPLATE_THEME']))
//{
//	$APPLICATION->SetAdditionalCSS($templateFolder.'/themes/'.$templateData['TEMPLATE_THEME'].'/style.css');
//	$APPLICATION->SetAdditionalCSS('/bitrix/css/main/themes/'.$templateData['TEMPLATE_THEME'].'/style.css', true);
//}

if (!empty($templateData['TEMPLATE_LIBRARY']))
{
	$loadCurrency = false;

	if (!empty($templateData['CURRENCIES']))
	{
		$loadCurrency = Loader::includeModule('currency');
	}

	CJSCore::Init($templateData['TEMPLATE_LIBRARY']);
	if ($loadCurrency)
	{
		?>
		<script>
			BX.Currency.setCurrencies(<?=$templateData['CURRENCIES']?>);
		</script>
		<?
	}
}

if (isset($templateData['JS_OBJ']))
{
	?>
	<script>
		BX.ready(BX.defer(function(){
			if (!!window.<?=$templateData['JS_OBJ']?>)
			{
				window.<?=$templateData['JS_OBJ']?>.allowViewedCount(true);
			}
		}));
	</script>

	<?
	// check compared state
	if ($arParams['DISPLAY_COMPARE'])
	{
		$compared = false;
		$comparedIds = array();
		$item = $templateData['ITEM'];

		if (!empty($_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]))
		{
			if (!empty($item['JS_OFFERS']))
			{
				foreach ($item['JS_OFFERS'] as $key => $offer)
				{
					if (array_key_exists($offer['ID'], $_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']))
					{
						if ($key == $item['OFFERS_SELECTED'])
						{
							$compared = true;
						}

						$comparedIds[] = $offer['ID'];
					}
				}
			}
			elseif (array_key_exists($item['ID'], $_SESSION[$arParams['COMPARE_NAME']][$item['IBLOCK_ID']]['ITEMS']))
			{
				$compared = true;
			}
		}

		if ($templateData['JS_OBJ'])
		{
			?>
			<script>
				BX.ready(BX.defer(function(){
					if (!!window.<?=$templateData['JS_OBJ']?>)
					{
						window.<?=$templateData['JS_OBJ']?>.setCompared('<?=$compared?>');

						<? if (!empty($comparedIds)): ?>
						window.<?=$templateData['JS_OBJ']?>.setCompareInfo(<?=CUtil::PhpToJSObject($comparedIds, false, true)?>);
						<? endif ?>
					}
				}));
			</script>
			<?
		}
	}

	// select target offer
	$request = Bitrix\Main\Application::getInstance()->getContext()->getRequest();
	$offerNum = false;
	$offerId = (int)$this->request->get('OFFER_ID');
	$offerCode = $this->request->get('OFFER_CODE');

	if ($offerId > 0 && !empty($templateData['OFFER_IDS']) && is_array($templateData['OFFER_IDS']))
	{
		$offerNum = array_search($offerId, $templateData['OFFER_IDS']);
	}
	elseif (!empty($offerCode) && !empty($templateData['OFFER_CODES']) && is_array($templateData['OFFER_CODES']))
	{
		$offerNum = array_search($offerCode, $templateData['OFFER_CODES']);
	}

	if (!empty($offerNum))
	{
		?>
		<script>
			BX.ready(function(){
				if (!!window.<?=$templateData['JS_OBJ']?>)
				{
					window.<?=$templateData['JS_OBJ']?>.setOffer(<?=$offerNum?>);
				}
			});
		</script>
		<?
	}
}
/**/

$REVIEWS = loadProductReviews($templateData['ITEM']['ID']);
$ratingTotal = 0;
foreach ($REVIEWS as $arReview)
	$ratingTotal += $arReview['UF_RATING'];

$AVERAGE_RATING = intval($ratingTotal / count($REVIEWS));
$reviewsDeclension = new Declension('отзыв', 'отзыва', 'отзывов');

?>
<div id="review-item-inner" class="b-ghost">
	<div class="review-item__rate">
		<?
		for ($i = 1; $i <= 5; $i++)
		{
			$_cl = 'review-item__rate-icon';
			if ($AVERAGE_RATING >= $i)
				$_cl .= ' filled'
			?><svg class="<?= $_cl ?>" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg><?
		}
		?>
		<?/*
        <svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"></path></svg>
        <svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"></path></svg>
        <svg class="review-item__rate-icon filled" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"></path></svg>
        <svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"></path></svg>
        <svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"></path></svg>
        */?>
	</div>
	<div class="reviews-quantity">
		<?= count($REVIEWS) . ' ' .  $reviewsDeclension->get(count($REVIEWS)); ?>
	</div>
</div>
<div id='stack' style='display:none;'><?=$_SESSION['city'];?></div>
<script type="text/javascript"> (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() { try { rrApi.groupView([<?$counter = 0; $countGoods = count($arResult['OFFERS_ID']); foreach($arResult['OFFERS_ID'] as $aroffer): $counter++?><?=$aroffer;?><?if($counter!=$countGoods):?>,<?endif;?><?endforeach;?>], {stockId: "<?=$_SESSION['city'];?>"}); } catch(e) {} });</script>
<script>retailrocket.markup.render();</script>