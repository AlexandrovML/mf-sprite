<div class="mf-product-defining-attr">
	<div class="h3">В зависимости от цвета цена:</div>
	<p><?= $arResult['PRINT_MIN_SKU_PRICE'] ?> - <?= $arResult['PRINT_MAX_SKU_PRICE'] ?></p>
</div>
<div class="mf-product-attr-option mf-product-color">
	<div class="h3">Цвет: <span>ЯРКИЙ КРАСНЫЙ</span></div>
	<? foreach ($arResult['arPricesColors'] as $priceVal=>$colors) { ?>
		<p class="price"><?= $priceVal ?></p>
		<ul class="mf-product-colorlist">
			<? foreach ($colors as $color) { ?>
				<?
				if($color['PICT']['ID'] != 0)
				{
					$style = 'background: url('.$color['PICT']['SRC'].');';
				}
				elseif(!empty($color['COLOR_CODE']))
				{
					$style = 'background: ' . $color['COLOR_CODE'] . ';';
				}
				else
				{
					$style = 'background: url('.$color['PICT']['SRC'].');background-size:contain;';
				}
				?>
				<li>
					<div class="mf-color-product js-mf-color-product js_prop_val_<?= $skuProperty['CODE'] ?>" data-prop="<?= $skuProperty['CODE'] ?>" data-colorname="<?= $color['NAME'] ?>" data-treevalue="<?= $arResult['SKU_PROPS']['COLOR_REF']['ID'].'_'.$color['ID'] ?>" title="<?= $color['NAME'] ?>" style="<?= $style ?>"></div>
				</li>
			<? } ?>
		</ul>
	<? }
	?>
</div>
<div style="display: none"><? include '_product-item-scu-container.php'?></div>