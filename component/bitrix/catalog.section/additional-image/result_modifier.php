<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

$domain = getDomainUrl();
foreach ($arResult['ITEMS'] as &$arItem)
{
	$arItem['DETAIL_PAGE_URL'] = $domain . $arItem['DETAIL_PAGE_URL'];
}
unset($arItem);