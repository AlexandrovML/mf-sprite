<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

// префикс для старой цены
if(SITE_ID=='s1'){
	$prefix='_BY';
}elseif(SITE_ID=='s2'){
	$prefix='_EN';
}elseif(SITE_ID=='s3'){
	$prefix='_KZ';
}

if(empty($arParams['CURRENCY_ID'])) $arParams['CURRENCY_ID']='BYN';





foreach ($arResult['ITEMS'] as &$arItem)
{
	// уменьшаем картинку
	if(!empty($arItem["PREVIEW_PICTURE"]))
	{
		$arFile = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]['ID'], Array("width"=>110, "height"=>160), BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
		$arItem["PREVIEW_PICTURE"]['SRC'] = $arFile['src'];
	}

	if(!empty($arItem['OFFERS']))
	{
		unset($minOfferOldPrice);
		$minOfferOldPrice = 0;
//        pr($arItem['OFFERS_SELECTED']);
//        pr($prefix);

		foreach ($arItem['OFFERS'] as $key=>&$arOffer)
		{
//			pr($arOffer['PROPERTIES']);
			if($arOffer['CATALOG_CAN_BUY_1']=='Y' && $key == $arItem['OFFERS_SELECTED'])
			{
				//рассчитываем старую цену
				if(!empty($arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE'])){
					if(empty($minOfferOldPrice)){
						$minOfferOldPrice=$arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE'];
					}
					if($minOfferOldPrice < $arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE']){
						$minOfferOldPrice = $arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE'];
					}
				}
			}
		}
		unset($arOffer);

//		pr($minOfferOldPrice);


		if(!empty($minOfferOldPrice)){
			$arItem['PRINT_OLD_PRICE'] = CurrencyFormat($minOfferOldPrice, $arParams['CURRENCY_ID']);
			$arItem['OLD_PRICE'] = $minOfferOldPrice;
		}
	}

}
unset($arItem);

if($arParams['BY_PHOTO']=='Y'&&!empty($_SESSION['goodsForSearch'])){
    foreach($_SESSION['goodsForSearch'] as $item){
        foreach($arResult['ITEMS'] as $key => $arItem){
            if($arItem['ID'] == $item){
                $arResult['NEW_ITEMS'][$key] = $arItem;
            }
        }
    }
    $arResult['ITEMS'] = $arResult['NEW_ITEMS'];
}