<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();
/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */
$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();


// получаем из справочника все нужные цвета
$arColorCodes = [];
$TABLE_NAME_COLOR_REF = '';
// префикс для старой цены
if(SITE_ID=='s1'){
    $prefix='_BY';
}elseif(SITE_ID=='s2'){
    $prefix='_RU';
}elseif(SITE_ID=='s3'){
    $prefix='_KZ';
}
if(empty($arParams['CURRENCY_ID'])){
      $arParams['CURRENCY_ID']='BYN'; // Бред полнейший, почему оно пустое ?
}
//pr($arParams['ELEMENTS_VIEW_TYPE']);

if($arParams['ELEMENTS_VIEW_TYPE'] == 'tile-big')
{
	$imgSizeBig = Array("width"=>610, "height"=>746);
	$imgSizeSmall = Array("width"=>186, "height"=>260);
}
else
{
	$imgSizeBig = Array("width"=>354, "height"=>432);
	$imgSizeSmall = Array("width"=>158, "height"=>198);

}

$domainUrl = getDomainUrl();

foreach ($arResult['ITEMS'] as &$arItem)
{

	$arItem['RELATED_PRODUCTS_COLORS'] = [];
	if(!empty($arItem['PROPERTIES']['COLOR_REF']['VALUE']))
	{
		$arColorCodes[$arItem['PROPERTIES']['COLOR_REF']['VALUE']] = $arItem['PROPERTIES']['COLOR_REF']['VALUE'];

		if(empty($TABLE_NAME_COLOR_REF)) $TABLE_NAME_COLOR_REF = $arItem['PROPERTIES']['COLOR_REF']['USER_TYPE_SETTINGS']['TABLE_NAME'];
		foreach ($arResult['ITEMS'] as $arRelItem)
		{
			if($arRelItem['PROPERTIES']['MODEL_ID']['VALUE'] == $arItem['PROPERTIES']['MODEL_ID']['VALUE'] && !empty($arRelItem['PROPERTIES']['COLOR_REF']['VALUE']))
			{
				$arFile = CFile::ResizeImageGet($arRelItem["PREVIEW_PICTURE"]['ID'], $imgSizeBig, BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				$arRelItem["PREVIEW_PICTURE"]['SRC'] = $arFile['src'];

				$arItem['RELATED_PRODUCTS_COLORS'][$arRelItem['PROPERTIES']['COLOR_REF']['VALUE']] = $arRelItem['PREVIEW_PICTURE']['SRC'];
			}
		}

	}



	// уменьшаем картинку
	if(!empty($arItem["PREVIEW_PICTURE"]))
	{
		$arFile = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"]['ID'], $imgSizeBig, BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
		$arItem["PREVIEW_PICTURE"]['SRC'] = $arFile['src'];
	}

	foreach ($arItem['MORE_PHOTO'] as &$arPhoto)
	{
		$arFile = CFile::ResizeImageGet($arPhoto['ID'], $imgSizeSmall, BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
		$arPhoto['SRC'] = $arFile['src'];

		$arFile = CFile::ResizeImageGet($arPhoto['ID'], $imgSizeBig, BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
		$arPhoto['SRC_BIG'] = $arFile['src'];


	}
	unset($arPhoto);

	if(!empty($arItem['OFFERS']))
	{
        unset($minOfferOldPrice);
        $minOfferOldPrice = 0;
      
		foreach ($arItem['OFFERS'] as $key=>&$arOffer)
		{
            
            if($arOffer['CAN_BUY']=='Y' && $arOffer['ITEM_PRICES_CAN_BUY']=='Y' && $key == $arItem['OFFERS_SELECTED'])
            {
               //рассчитываем старую цену
               
                if(!empty($arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE'])){
                    if(empty($minOfferOldPrice)){
                       $minOfferOldPrice=$arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE'];
                    }
                    if($minOfferOldPrice < $arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE']){
                        $minOfferOldPrice = $arOffer['PROPERTIES']['OLD_PRICE'.$prefix]['VALUE'];
                    }
                }
            }

			foreach ($arOffer['MORE_PHOTO'] as &$arPhoto)
			{
				$arFile = CFile::ResizeImageGet($arPhoto['ID'], $imgSizeSmall, BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				$arPhoto['SRC'] = $arFile['src'];

				$arFile = CFile::ResizeImageGet($arPhoto['ID'], $imgSizeBig, BX_RESIZE_IMAGE_PROPORTIONAL_ALT);
				$arPhoto['SRC_BIG'] = $arFile['src'];
			}
			unset($arPhoto);
		}
		unset($arOffer);


        if(!empty($minOfferOldPrice)){
           $arItem['PRINT_OLD_PRICE'] = CurrencyFormat($minOfferOldPrice, $arParams['CURRENCY_ID']);
           $arItem['OLD_PRICE'] = $minOfferOldPrice;
        }
        //pr($arItem['OLD_PRICE']);
        
	}


	// делаем абсолютный урл
	$arItem['DETAIL_PAGE_URL'] = $domainUrl . $arItem['DETAIL_PAGE_URL'];


	if ($arItem['IBLOCK_SECTION_ID'] == CERTIFICATES_SECTION_ID)
	{
		$arItem['NAME'] = str_replace(['ПОДАРОЧНЫЙ СЕРТИФИКАТ '], ['ПОДАРОЧНЫЙ СЕРТИФИКАТ<br>'], $arItem['NAME']);
	}
}
unset($arItem);

if(!empty($TABLE_NAME_COLOR_REF))
{
	$arFilter = ['UF_XML_ID'=>$arColorCodes];
	$arColorRefs = getDataFromReference($TABLE_NAME_COLOR_REF, ['*'], 'UF_XML_ID', $arFilter);
}
else
{
	$arColorRefs = [];
}

foreach ($arResult['ITEMS'] as &$arItem)
{
	$arItem['COLORS'] = [];

	foreach ($arItem['RELATED_PRODUCTS_COLORS'] as $colorCode=>$PREVIEW_PICTURE)
	{
		$arItem['COLORS'][$colorCode] = $arColorRefs[$colorCode];
		unset($arItem['COLORS'][$colorCode]['UF_DESCRIPTION']);
		unset($arItem['COLORS'][$colorCode]['UF_FULL_DESCRIPTION']);
		unset($arItem['COLORS'][$colorCode]['UF_LINK']);
		unset($arItem['COLORS'][$colorCode]['UF_SORT']);
		unset($arItem['COLORS'][$colorCode]['UF_DEF']);
		unset($arItem['COLORS'][$colorCode]['UF_XML_ID']);
		$arItem['COLORS'][$colorCode]['PREVIEW_PICTURE'] = $PREVIEW_PICTURE;
	}

	
	// нужно убрать из выдачи более дешевые ТП одинаковой модели
	// в подписке на носки этого не нужно делать
	if ($arResult['ID'] != 1134)
	{
//	pr($arItem['OFFERS']);
		$arPresentedSkuPropValues = [];
		$arPresentedSkuPrices = [];
//	pr($arItem['OFFERS_SELECTED']);
		foreach ($arItem['OFFERS'] as $arOffer)
		{
			$propsKey = '';
			foreach ($arParams['OFFER_TREE_PROPS'] as $proCode)
			{
				if(!empty($arOffer['PROPERTIES'][$proCode]['VALUE']))
				{
					$propsKey .= $proCode . '-' . $arOffer['PROPERTIES'][$proCode]['VALUE'].'|';
				}
			}

			$arPresentedSkuPropValues[($propsKey)][$arOffer['ID']] = $arOffer['CAN_BUY']===true?1:0;
			$arPresentedSkuPrices[$arOffer['ID']] = $arOffer['ITEM_PRICES'][$arOffer['ITEM_PRICE_SELECTED']]['RATIO_PRICE'];
		}

	//pr($arPresentedSkuPropValues);
	//pr($arPresentedSkuPrices);

		$arIdsToRemove = [];

		foreach ($arPresentedSkuPropValues as $arPropsItem)
		{
			if (count($arPropsItem) > 1)
			{
				$_minPrice = $_offerId = 0;
				foreach ($arPropsItem as $offerId=>$canBuy)
				{
					if ($canBuy == 0) continue;
					if ($_minPrice == 0)
					{
						$_minPrice = $arPresentedSkuPrices[$offerId];
						//$_offerId = $offerId;

					}
					elseif ($_minPrice < $arPresentedSkuPrices[$offerId])
					{
						//$_minPrice = $arPresentedSkuPrices[$offerId];
						$_offerId = $offerId;
					}
				}

				if ($_offerId > 0)
				{
					$arIdsToRemove[] = $_offerId;
				}
			}
		}
      //pr($arIdsToRemove);

		$OFFERS_SELECTED_ID = $arItem['OFFERS_SELECTED'];

		if (count($arIdsToRemove) && isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']]))
		{
			$id = $arItem['OFFERS'][$arItem['OFFERS_SELECTED']]['ID'];
			foreach ($arPresentedSkuPropValues as $arPropsItem)
			{
				$thisPropsValues = false;

				foreach ($arPropsItem as $offerId => $canBuy)
				{
					if ($offerId == $id)
					{
						$thisPropsValues = true;
						break;
					}
				}

				if ($thisPropsValues)
				{
					foreach ($arPropsItem as $offerId => $canBuy)
					{
						if ($offerId != $id && $canBuy == 1)
						{
							foreach ($arItem['OFFERS'] as $k1 => $arOffer)
							{
								if ($arOffer['ID'] == $offerId)
								{
									$OFFERS_SELECTED_ID = $arOffer['ID'];
									break;
								}
							}
							break;
						}
					}
				}
			}
		}

		if (count($arIdsToRemove))
		{
			// перестраиваем массивы ТП без удаленных ТП
			$arOffers = [];
			foreach ($arItem['OFFERS'] as $k=>$arOffer)
			{
				if (!in_array($arOffer['ID'], $arIdsToRemove))
				{
					$arOffers[] = $arOffer;
				}
			}
			$arItem['OFFERS'] = $arOffers;

			$arOffers = [];
			foreach ($arItem['JS_OFFERS'] as $k=>$arOffer)
			{
				if (!in_array($arOffer['ID'], $arIdsToRemove))
				{
					$arOffers[] = $arOffer;
				}
			}

			$arItem['JS_OFFERS'] = $arOffers;

			// отмечаем активное ТП
			foreach ($arItem['OFFERS'] as $k=>$arOffer)
			{
				if ($arItem['ID'] == $OFFERS_SELECTED_ID)
				{
                    
					$arItem['OFFERS_SELECTED'] = $k;
					break;
				}
			}
		}
	}

	/**/
	// END нужно убрать из выдачи более дешевые ТП одинаковой модели

}
unset($arItem);
