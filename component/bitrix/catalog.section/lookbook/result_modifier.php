<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogSectionComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

// формируем нашу последовательность товаров, заданную в админке
$new_arResult = array();
$item_sort = $GLOBALS['PRODUCT_IDS_FOR_FILTER_LOOKBOOK'];
foreach ($item_sort as $sort){
    foreach ($arResult['ITEMS'] as $item){
        if ($item['ID'] == $sort){
            $new_arResult[$sort] = $item;
            break;
        }
    }
}
$arResult['ITEMS'] = $new_arResult;