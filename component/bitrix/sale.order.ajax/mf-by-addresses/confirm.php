<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;
use Bitrix\Main,
    Bitrix\Sale,
    Bitrix\Sale\Order;
/**
 * @var array $arParams
 * @var array $arResult
 * @var $APPLICATION CMain
 */

if ($arParams["SET_TITLE"] == "Y")
{
	$APPLICATION->SetTitle(Loc::getMessage("SOA_ORDER_COMPLETE"));
}

$isEripPayment = false;
if ($arResult["ORDER"]["IS_ALLOW_PAY"] === 'Y')
{
	if (!empty($arResult["PAYMENT"]))
	{
		foreach ($arResult["PAYMENT"] as $payment)
		{
			if ($payment["PAID"] != 'Y')
			{
				if (!empty($arResult['PAY_SYSTEM_LIST'])
					&& array_key_exists($payment["PAY_SYSTEM_ID"], $arResult['PAY_SYSTEM_LIST'])
				)
				{
					$arPaySystem = $arResult['PAY_SYSTEM_LIST_BY_PAYMENT_ID'][$payment["ID"]];

					// метод оплаты Оплата через ЕРИП
					if($arPaySystem["PAY_SYSTEM_ID"] == 14) $isEripPayment = true;
				}
			}
		}
	}
}

//if()
//$arResult["ORDER"]["IS_ALLOW_PAY"] = 'N'
?>
<div class="container container-order-done">
<? if (!empty($arResult["ORDER"])): ?>
    <div style="opacity: 0; visibility: hidden">
	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=Loc::getMessage("SOA_ORDER_SUC", array(
					"#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"]->toUserTime()->format('d.m.Y H:i'),
					"#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]
				))?>
				<? if (!empty($arResult['ORDER']["PAYMENT_ID"]) && !$isEripPayment): ?>
					<?=Loc::getMessage("SOA_PAYMENT_SUC", array(
						"#PAYMENT_ID#" => $arResult['PAYMENT'][$arResult['ORDER']["PAYMENT_ID"]]['ACCOUNT_NUMBER']
					))?>
				<? endif ?>
				<? if ($arParams['NO_PERSONAL'] !== 'Y'): ?>
					<br /><br />
					<?=Loc::getMessage('SOA_ORDER_SUC1', ['#LINK#' => $arParams['PATH_TO_PERSONAL']])?>
				<? endif; ?>
			</td>
		</tr>
	</table>

	<?
	if ($arResult["ORDER"]["IS_ALLOW_PAY"] === 'Y')
	{
		if (!empty($arResult["PAYMENT"]))
		{
			foreach ($arResult["PAYMENT"] as $payment)
			{
				if ($payment["PAID"] != 'Y')
				{
					if (!empty($arResult['PAY_SYSTEM_LIST'])
						&& array_key_exists($payment["PAY_SYSTEM_ID"], $arResult['PAY_SYSTEM_LIST'])
					)
					{
						$arPaySystem = $arResult['PAY_SYSTEM_LIST_BY_PAYMENT_ID'][$payment["ID"]];

						// метод оплаты Оплата через ЕРИП
						if($arPaySystem["PAY_SYSTEM_ID"] == 14) continue;


						if (empty($arPaySystem["ERROR"]))
						{
							?>
							<br /><br />

							<table class="sale_order_full_table">
								<tr>
									<td class="ps_logo">
										<div class="pay_name"><?=Loc::getMessage("SOA_PAY") ?></div>
										<?=CFile::ShowImage($arPaySystem["LOGOTIP"], 100, 100, "border=0\" style=\"width:100px\"", "", false) ?>
										<div class="paysystem_name"><?=$arPaySystem["NAME"] ?></div>
										<br/>
									</td>
								</tr>
								<tr>
									<td>
										<? if (strlen($arPaySystem["ACTION_FILE"]) > 0 && $arPaySystem["NEW_WINDOW"] == "Y" && $arPaySystem["IS_CASH"] != "Y"): ?>
											<?
											$orderAccountNumber = urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]));
											$paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
											?>
											<script>
												window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=$orderAccountNumber?>&PAYMENT_ID=<?=$paymentAccountNumber?>');
											</script>
										<?=Loc::getMessage("SOA_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber))?>
										<? if (CSalePdf::isPdfAvailable() && $arPaySystem['IS_AFFORD_PDF']): ?>
										<br/>
											<?=Loc::getMessage("SOA_PAY_PDF", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&pdf=1&DOWNLOAD=Y"))?>
										<? endif ?>
										<? else: ?>
											<?=$arPaySystem["BUFFERED_OUTPUT"]?>
										<? endif ?>
									</td>
								</tr>
							</table>

							<?
						}
						else
						{
							?>
							<span style="color:red;"><?=Loc::getMessage("SOA_ORDER_PS_ERROR")?></span>
							<?
						}
					}
					else
					{
						?>
						<span style="color:red;"><?=Loc::getMessage("SOA_ORDER_PS_ERROR")?></span>
						<?
					}
				}
			}
		}
	}
	else
	{
		?>
		<br /><strong><?=$arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR']?></strong>
		<?
	}
	?>
    </div>
         <?
            $arOrder = array();
            $order = Sale\Order::loadByAccountNumber($arResult["ORDER"]["ACCOUNT_NUMBER"]);
            $basket = $order->getBasket();
            $basketItems = $basket->getBasketItems();
            $allPrice = $order->getPrice(); 
            $deliveryPrice = $order->getDeliveryPrice(); 
            foreach ($basket as $basketItem) {
                $idGood = $basketItem->getProductId();
                $arId[]=$idGood;
                $arOrder[$idGood]['ID'] = $idGood;
                $arOrder[$idGood]['PRICE'] = $basketItem->getPrice();
                $arOrder[$idGood]['QUANTITY'] = $basketItem->getQuantity();
                $arOrder[$idGood]['CURRENCY'] = $arResult['ORDER']["CURRENCY"];
                $currency = $arResult['ORDER']["CURRENCY"];
                if($currency == 'KZT'){
                    $reTCurrency = 'KZN';
                }
                if($currency == 'RUB'){
                    $reTCurrency = 'RUN';
                }else{
                    $reTCurrency = $currency;
                }
                $arOrder[$idGood]['PRICE_USD'] = number_format(CCurrencyRates::ConvertCurrency($arOrder[$idGood]['PRICE'], $currency, "USD"),2);
            }
            //pr($arOrder);
            if(!empty($arId)){
                if(CModule::IncludeModule("iblock")){
                    $arFilter = array('IBLOCK_ID' => '3', 'ACTIVE'=>'Y', "GLOBAL_ACTIVE"=>'Y', '=ID'=>$arId);
                    $arSelect=array('ID', "PROPERTY_CML2_LINK", "NAME", "PROPERTY_SIZES_CLOTHES", "PROPERTY_ARTNUMBER");
                    $rsEl = CIBlockElement::GetList(array('sort' => 'asc'),$arFilter, false, false, $arSelect);
                    while($arEl = $rsEl->GetNext()){
                        $arIDGoods[]=$arEl['PROPERTY_CML2_LINK_VALUE'];
                        $arItemsAll[$arEl['ID']]=$arEl;
                        $arOrder[$arEl['ID']]['NAME'] = $arEl['NAME'];
                        $arOrder[$arEl['ID']]['RAZMER'] = $arEl['PROPERTY_SIZES_CLOTHES_VALUE'];
                        $arOrder[$arEl['ID']]['ARTNUMBER'] = $arEl['PROPERTY_ARTNUMBER_VALUE'];
                    }
                    $arFilter = array('IBLOCK_ID' => '2', 'ACTIVE'=>'Y', "GLOBAL_ACTIVE"=>'Y', '=ID'=>$arIDGoods);
                    $arSelect=array('ID', "PROPERTY_SPECIALOFFER", "PROPERTY_SECTION_L1", "PROPERTY_SECTION_L2", "PROPERTY_SECTION_L3", "PROPERTY_SECTION_L4", "PROPERTY_SECTION_L5");
                    $rsEl = CIBlockElement::GetList(array('sort' => 'asc'),$arFilter, false, false, $arSelect);
                    while($arEl = $rsEl->GetNext()){
                        if(!empty($arEl['PROPERTY_SECTION_L1_VALUE'])){
                            $arSectionID[] = $arEl['PROPERTY_SECTION_L1_VALUE'];
                        }
                        if(!empty($arEl['PROPERTY_SECTION_L2_VALUE'])){
                            $arSectionID[] = $arEl['PROPERTY_SECTION_L2_VALUE'];
                        }
                        if(!empty($arEl['PROPERTY_SECTION_L3_VALUE'])){
                            $arSectionID[] = $arEl['PROPERTY_SECTION_L3_VALUE'];
                        }
                        if(!empty($arEl['PROPERTY_SECTION_L4_VALUE'])){
                            $arSectionID[] = $arEl['PROPERTY_SECTION_L4_VALUE'];
                        }
                        if(!empty($arEl['PROPERTY_SECTION_L5_VALUE'])){
                            $arSectionID[] = $arEl['PROPERTY_SECTION_L5_VALUE'];
                        }
                        foreach($arItemsAll as $key=>$arOffers){
                            if($arOffers['PROPERTY_CML2_LINK_VALUE'] == $arEl['ID']){
                                $arItemsAll[$key]['SPECIALOFFER'] = $arEl['PROPERTY_SPECIALOFFER_VALUE'];
                                if($arEl['PROPERTY_SPECIALOFFER_VALUE'] == 'да'){
                                    $cat = 2;
                                }else{
                                    $cat = 1;
                                }
                                $arOrder[$key]['CATEGORY_ADMITAB'] = $cat;
                                
                                if(!empty($arEl['PROPERTY_SECTION_L1_VALUE'])){
                                    $arOrder[$key]['CATEGORY'][$arEl['PROPERTY_SECTION_L1_VALUE']] = $arEl['PROPERTY_SECTION_L1_VALUE'];
                                }
                                if(!empty($arEl['PROPERTY_SECTION_L2_VALUE'])){
                                    $arOrder[$key]['CATEGORY'][$arEl['PROPERTY_SECTION_L2_VALUE']] = $arEl['PROPERTY_SECTION_L2_VALUE'];
                                }
                                if(!empty($arEl['PROPERTY_SECTION_L3_VALUE'])){
                                    $arOrder[$key]['CATEGORY'][$arEl['PROPERTY_SECTION_L3_VALUE']] = $arEl['PROPERTY_SECTION_L3_VALUE'];
                                }
                                if(!empty($arEl['PROPERTY_SECTION_L4_VALUE'])){
                                     $arOrder[$key]['CATEGORY'][$arEl['PROPERTY_SECTION_L4_VALUE']] = $arEl['PROPERTY_SECTION_L4_VALUE'];
                                }
                                if(!empty($arEl['PROPERTY_SECTION_L5_VALUE'])){
                                    $arOrder[$key]['CATEGORY'][$arEl['PROPERTY_SECTION_L5_VALUE']] = $arEl['PROPERTY_SECTION_L5_VALUE'];
                                }
                            }
                        }
                    }
                }
            }
            if(!empty($arSectionID)){
                if(CModule::IncludeModule("iblock")){
                    $arFilter = array('IBLOCK_ID' => 2,  '=ID'=>$arSectionID);
                    $arSelect=array('ID', 'NAME');
                    $rsSect=CIBlockSection::GetList(array(),$arFilter, false, $arSelect);
                    while ($arSect = $rsSect->GetNext())
                    {
                        $arSection[$arSect['ID']] = $arSect['NAME'];
                    }
                }
            }
            
            foreach($arOrder as $key=>$ar){
                $countCat = count($ar['CATEGORY']);
                $counterCat = 0;
                $arOrder[$key]['CATEGORY_PATH'] = '';
                foreach($ar['CATEGORY'] as $cat){
                    $counterCat++;
                    $arOrder[$key]['CATEGORY_PATH'].= trim($arSection[$cat]);
                    if($counterCat!=$countCat){
                       $arOrder[$key]['CATEGORY_PATH'].= '/';
                    }
                }
            }
            $countGoods = count($arOrder);
            $counter = 0;
            
            /*?>
            <script type="text/javascript">
            gtag('event','purchase',{"transaction_id":"<?=$arResult["ORDER"]["ACCOUNT_NUMBER"];?>","affiliation":"Mark Formelle","value":<?=$allPrice?>,"currency":"<?=$currency;?>","shipping":<?=$deliveryPrice;?>,"items":[<?foreach ($arOrder as $arItemOrder): $counter++?>{"id":"<?=$arItemOrder['ARTNUMBER'];?>","name":"<?=$arItemOrder['NAME'];?>","brand":"Mark Formelle","category":"<?=$arItemOrder['CATEGORY_PATH'];?>","variant":"<?=$arItemOrder['RAZMER'];?>","list_position":<?=$counter;?>,"quantity":<?=$arItemOrder['QUANTITY'];?>,"price":'<?=$arItemOrder['PRICE'];?>'}<?if($counter!=$countGoods):?>,<?endif;?><?endforeach;?>]});
            </script>
            <?*/
            $counter = 0;
            ?>
            <script>
            $.ajax({
                 url: '/ajax/put_to_log.php?order=<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?>&mess=<?foreach ($arOrder as $arItemOrder):?>id - <?=$arItemOrder['ID'];?> qnt - <?=$arItemOrder['QUANTITY'];?> price - <?=$arItemOrder['PRICE'];?> | <?endforeach;?>',
                 data: '',
                 cache: false,
                 contentType: false,
                 processData: false,
                 type: 'GET',
                 success: function(response) {
                 }
                    });
            </script>
    <div id="order-done-popup">
        <div class="popup_shop popup_subscribe">
            <div class="img"></div>
            <div class="text">
	            <?=Loc::getMessage("SOA_ORDER_SUC", array(
		            "#ORDER_DATE#" => $arResult["ORDER"]["DATE_INSERT"]->toUserTime()->format('d.m.Y H:i'),
		            "#ORDER_ID#" => $arResult["ORDER"]["ACCOUNT_NUMBER"]
	            ))?>
	            <? if (!empty($arResult['ORDER']["PAYMENT_ID"]) && !$isEripPayment): ?>
		            <?=Loc::getMessage("SOA_PAYMENT_SUC", array(
			            "#PAYMENT_ID#" => $arResult['PAYMENT'][$arResult['ORDER']["PAYMENT_ID"]]['ACCOUNT_NUMBER']
		            ))?>
	            <? endif ?>
	            <? if ($arParams['NO_PERSONAL'] !== 'Y'): ?>
                    <br /><br />
		            <?= !$isEripPayment ? Loc::getMessage('SOA_ORDER_SUC1', ['#LINK#' => $arParams['PATH_TO_PERSONAL']]) : Loc::getMessage('SOA_ORDER_SUC_ERIP') ?>
	            <? endif; ?>

	            <?
	            if ($arResult["ORDER"]["IS_ALLOW_PAY"] === 'Y')
	            {
		            if (!empty($arResult["PAYMENT"]))
		            {
			            foreach ($arResult["PAYMENT"] as $payment)
			            {
				            if ($payment["PAID"] != 'Y')
				            {
					            if (!empty($arResult['PAY_SYSTEM_LIST'])
						            && array_key_exists($payment["PAY_SYSTEM_ID"], $arResult['PAY_SYSTEM_LIST'])
					            )
					            {
						            $arPaySystem = $arResult['PAY_SYSTEM_LIST_BY_PAYMENT_ID'][$payment["ID"]];

                                    // метод оплаты Оплата через ЕРИП
						            if($arPaySystem["PAY_SYSTEM_ID"] == 14) continue;


						            if (empty($arPaySystem["ERROR"]))
						            {
							            ?>
                                        <hr>
                                        <table class="sale_order_full_table">
                                            <tr>
                                                <td class="ps_logo">
                                                    <div class="pay_name"><?=Loc::getMessage("SOA_PAY") ?></div>
										            <?//=CFile::ShowImage($arPaySystem["LOGOTIP"], 100, 100, "border=0\" style=\"width:100px\"", "", false) ?>
                                                    <div class="paysystem_name"><b><?=$arPaySystem["NAME"] ?></b></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
										            <? if (strlen($arPaySystem["ACTION_FILE"]) > 0 && $arPaySystem["NEW_WINDOW"] == "Y" && $arPaySystem["IS_CASH"] != "Y"): ?>
											            <?
											            $orderAccountNumber = urlencode(urlencode($arResult["ORDER"]["ACCOUNT_NUMBER"]));
											            $paymentAccountNumber = $payment["ACCOUNT_NUMBER"];
											            ?>
                                                        <script>
                                                            window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=$orderAccountNumber?>&PAYMENT_ID=<?=$paymentAccountNumber?>');
                                                        </script>
										            <?=Loc::getMessage("SOA_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&PAYMENT_ID=".$paymentAccountNumber))?>
										            <? if (CSalePdf::isPdfAvailable() && $arPaySystem['IS_AFFORD_PDF']): ?>
                                                    <br/>
											            <?=Loc::getMessage("SOA_PAY_PDF", array("#LINK#" => $arParams["PATH_TO_PAYMENT"]."?ORDER_ID=".$orderAccountNumber."&pdf=1&DOWNLOAD=Y"))?>
										            <? endif ?>
										            <? else: ?>
											            <?=$arPaySystem["BUFFERED_OUTPUT"]?>
										            <? endif ?>
                                                </td>
                                            </tr>
                                        </table>

							            <?
						            }
						            else
						            {
							            ?>
                                        <span style="color:red;"><?=Loc::getMessage("SOA_ORDER_PS_ERROR")?></span>
							            <?
						            }
					            }
					            else
					            {
						            ?>
                                    <span style="color:red;"><?=Loc::getMessage("SOA_ORDER_PS_ERROR")?></span>
						            <?
					            }
				            }
			            }
		            }
	            }
	            else
	            {
		            ?>
                    <br /><strong><?=$arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR']?></strong>
		            <?
	            }
	            ?>
            </div>
            <a href="<?= $arParams['PATH_TO_PERSONAL'] ?>" title="Close (Esc)" type="button" class="mfp-close">×</a>
        </div>
    </div>

    <script>
        $(document).ready(function () {
            $.magnificPopup.open({
                mainClass: 'popup_shop-body popup_input popup_subscribe_done',
                modal: true,
                closeBtnInside:true,
                items: {
                    src: $('#order-done-popup'),
                    type: 'inline'
                }
            });
        });
    </script>

<? else: ?>

	<b><?=Loc::getMessage("SOA_ERROR_ORDER")?></b>
	<br /><br />

	<table class="sale_order_full_table">
		<tr>
			<td>
				<?=Loc::getMessage("SOA_ERROR_ORDER_LOST", ["#ORDER_ID#" => htmlspecialcharsbx($arResult["ACCOUNT_NUMBER"])])?>
				<?=Loc::getMessage("SOA_ERROR_ORDER_LOST1")?>
			</td>
		</tr>
	</table>

<? endif ?>
</div>
<?/*
<div class="mfp-container mfp-ajax-holder mfp-s-ready">
    <div class="mfp-content">
        <div class="popup_shop popup_subscribe">
            <div class="img"></div>
            <div class="text">
                <span>Спасибо</span>
                Вы успешно подписались на рассылку
                <a href="#" class="link">Закрыть</a>
            </div>
            <button title="Close (Esc)" type="button" class="mfp-close">×</button>
        </div>
    </div>
    <div class="mfp-preloader">Loading...</div>
</div>*/?>