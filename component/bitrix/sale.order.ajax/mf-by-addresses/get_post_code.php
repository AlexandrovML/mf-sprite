<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
//use Bitrix\Sale;
//if(!CModule::IncludeModule("sale") || !CModule::IncludeModule("catalog") || !CModule::IncludeModule("iblock")) { echo "failure"; return;}


//session_start();
//session_name('PHPSESSID');

$arJson['res'] = 'error';
$task = htmlspecialchars($_REQUEST["task"]);
$street = !empty($_REQUEST['street']) ? htmlspecialcharsbx($_REQUEST['street']) : '';
$house = !empty($_REQUEST['house']) ? htmlspecialcharsbx($_REQUEST['house']) : '';
$locationCode = !empty($_REQUEST['locationCode']) ? htmlspecialcharsbx($_REQUEST['locationCode']) : '';
$cityName = !empty($_REQUEST['altLocationValue']) ? htmlspecialcharsbx($_REQUEST['altLocationValue']) : '';

if ($street != '' && $house != '' && $locationCode != '')
{
	$postCode = saleHandlers::getPostCode($locationCode, $street, $house, $cityName);
	$arJson['res'] = 'ok';
	$arJson['postCode'] = $postCode;
}

echo \Bitrix\Main\Web\Json::encode($arJson);
//$jsonBred = json_encode($arJson);
//$jsonBred = htmlspecialchars($jsonBred);
//echo htmlspecialchars_decode($jsonBred);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");