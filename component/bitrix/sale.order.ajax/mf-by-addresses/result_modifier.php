<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);
$arSelect = ['*', 'UF_COUNTRY', 'UF_REGION', 'UF_CITY'];

$arFilter = [
	'ACTIVE' => 'Y',
	'ISSUING_CENTER' => 'Y',
];

$dbResult = CCatalogStore::GetList(
	array('PRODUCT_ID'=>'ASC','ID' => 'ASC'),
	$arFilter,
	false,
	false,
	$arSelect
);

while ($row = $dbResult->getNext())
{
	foreach ($arResult['JS_DATA']['STORE_LIST'] as &$arStore)
	{
		if($arStore['ID'] == $row['ID'])
		{
			$arStore['UF_COUNTRY'] = $row['UF_COUNTRY'];
			$arStore['UF_REGION'] = $row['UF_REGION'];
			$arStore['UF_CITY'] = $row['UF_CITY'];
		}
	}
	unset($arStore);
}

if(!empty($_SESSION['BASKET_USER_DATA']))
{
	foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as &$arProp)
	{
//		pr($arProp['CODE']);
		switch ($arProp['CODE'])
		{
			case 'EMAIL':

				if(!empty($_SESSION['BASKET_USER_DATA']['email']))
				{
					$arProp['VALUE'][0] = $_SESSION['BASKET_USER_DATA']['email'];
				}
				break;

			case 'PHONE':

				if(!empty($_SESSION['BASKET_USER_DATA']['phone']))
				{
					$arProp['VALUE'][0] = $_SESSION['BASKET_USER_DATA']['phone'];
				}
				break;

			case 'NAME':
				if(!empty($_SESSION['BASKET_USER_DATA']['username']))
				{
					$arProp['VALUE'][0] = $_SESSION['BASKET_USER_DATA']['username'];
				}
				break;
		}
	}

}

//убираем ЕРИП из методов оплаты вообще
//$PAY_SYSTEM = [];
//foreach ($arResult['JS_DATA']['PAY_SYSTEM'] as $item)
//	if ($item['PAY_SYSTEM_ID'] != 14) $PAY_SYSTEM[] = $item;
//
//$arResult['JS_DATA']['PAY_SYSTEM'] = $PAY_SYSTEM;

if ($USER->IsAuthorized())
{
//	var_dump($USER->getId());
	$rsUser = CUser::GetByID($USER->getId());
	$arUser = $rsUser->Fetch();
//	echo '<pre>'; print_r($arUser); echo '</pre>';
	foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as &$property)
	{
		switch ($property['CODE'])
		{
			case 'FIO':
				// фамилия
				if (empty($property['VALUE'][0]) && !empty($arUser['LAST_NAME']))
				{
					$property['VALUE'][0] = $arUser['LAST_NAME'];
				}
				break;
		}
	}
	unset($property);


}
