<?php

CJSCore::Init(array('currency'));

if(!empty($arResult['BASKET_ITEMS']))
{
	$CURRENCY = $arResult['BASKET_ITEMS'][0]['CURRENCY'];
}
else
{
	$CURRENCY = 'BYN';
}

$currencyFormat = CCurrencyLang::GetFormatDescription($CURRENCY);
?>
<script type="text/javascript">BX.Currency.setCurrencyFormat('<?= $CURRENCY ?>', <? echo CUtil::PhpToJSObject($currencyFormat, false, true); ?>);</script>
