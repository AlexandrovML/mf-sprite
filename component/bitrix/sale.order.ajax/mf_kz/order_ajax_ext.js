var storeCityValue = '',
    storeItemValue = '';

(function () {
    'use strict';

    var initParent = BX.Sale.OrderAjaxComponent.init,
        getBlockFooterParent = BX.Sale.OrderAjaxComponent.getBlockFooter,
        editOrderParent = BX.Sale.OrderAjaxComponent.editOrder,
        initOptionsParent = BX.Sale.OrderAjaxComponent.initOptions,
        editDeliveryInfoParent = BX.Sale.OrderAjaxComponent.editDeliveryInfo,
        editRegionBlockParent = BX.Sale.OrderAjaxComponent.editRegionBlock,
        editPickUpListParent = BX.Sale.OrderAjaxComponent.editPickUpList;

    BX.namespace('BX.Sale.OrderAjaxComponentExt');

    BX.Sale.OrderAjaxComponentExt = BX.Sale.OrderAjaxComponent;


    BX.Sale.OrderAjaxComponentExt.init = function (parameters) {
        console.log('init');

        this.couponsBlockNode = BX(parameters.couponsBlockId);
        this.couponsHiddenBlockNode = BX(parameters.couponsBlockId + '-hidden');

        // this.emailBlockNode = BX(parameters.emailBlockId);
        // this.emailHiddenBlockNode = BX(parameters.emailBlockId + '-hidden');

        this.phoneMaskBy = "+375 (__) __-__-__";
        this.phoneMaskRu = "+7(___)___-__-__";

        // this.isEmailEntered = false;

        initParent.apply(this, arguments);

        if($('body').find('select[name="PROFILE_ID"]').length)
            $('body').find('select[name="PROFILE_ID"]').styler();

    };

    /**
     * Binds main events for scrolling/resizing
     */
    // BX.Sale.OrderAjaxComponentExt.bindEvents = function()
    // {
    //     BX.bind(this.orderSaveBlockNode.querySelector('[data-save-button]'), 'click', BX.proxy(this.clickOrderSaveAction, this));
    //     BX.bind(window, 'scroll', BX.proxy(this.totalBlockScrollCheck, this));
    //     BX.bind(window, 'resize', BX.throttle(function(){
    //         this.totalBlockResizeCheck();
    //         this.alignBasketColumns();
    //         this.basketBlockScrollCheck();
    //         this.mapsReady && this.resizeMapContainers();
    //     }, 50, this));
    //     BX.addCustomEvent('onDeliveryExtraServiceValueChange', BX.proxy(this.sendRequest, this));
    //     BX.addCustomEvent('OnBasketChange', BX.proxy(this.sendRequest, this));
    // };


    // BX.Sale.OrderAjaxComponentExt.getBlockFooter = function (node) {
    //     var parentNodeSection = BX.findParent(node, {className: 'bx-soa-section'});
    //
    //     getBlockFooterParent.apply(this, arguments);
    //
    //     if (/bx-soa-auth|bx-soa-region|bx-soa-properties|bx-soa-basket|bx-soa-delivery|bx-soa-pickup|bx-soa-paysystem/.test(parentNodeSection.id)) {
    //         BX.remove(parentNodeSection.querySelector('.pull-left'));
    //         BX.remove(parentNodeSection.querySelector('.pull-right'));
    //     }
    // };



    /**
     * Edit order block nodes with this.result/this.params data
     */
    BX.Sale.OrderAjaxComponentExt.editOrder = function()
    {
        if (!this.orderBlockNode || !this.result)
            return;

        console.log('editOrder');

        if (this.result.DELIVERY.length > 0)
        {
            BX.addClass(this.deliveryBlockNode, 'bx-active');
            this.deliveryBlockNode.removeAttribute('style');
        }
        else
        {
            BX.removeClass(this.deliveryBlockNode, 'bx-active');
            this.deliveryBlockNode.style.display = 'none';
        }

        this.orderSaveBlockNode.style.display = this.result.SHOW_AUTH ? 'none' : '';
        this.mobileTotalBlockNode.style.display = this.result.SHOW_AUTH ? 'none' : '';

        this.checkPickUpShow();

        var sections = this.orderBlockNode.querySelectorAll('.bx-soa-section.bx-active'), i;
        for (i in sections)
        {
            if (sections.hasOwnProperty(i))
            {
                this.editSection(sections[i]);
            }
        }

        this.editTotalBlock();
        this.totalBlockFixFont();

        this.showErrors(this.result.ERROR, false);
        this.showWarnings();
        // this.show(BX('bx-soa-coupons'));

        this.showOrderSaveBtns();
        // // если нужно показываем кнопки оформить заказ
        // if((BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-visited="true"]').length - BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-disablednode="true"]').length) == (BX('bx-soa-order').querySelectorAll('.mf-checkout-el').length - BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-disablednode="true"]').length))
        // {
        //     BX('bx-soa-orderSave').style.display = 'block';
        //     BX('bx-soa-orderSave-sb').style.display = 'block';
        // }
    };

    BX.Sale.OrderAjaxComponentExt.editActivePropsBlock = function(activeNodeMode)
    {
        console.log('editActivePropsBlock');

        var node = activeNodeMode ? this.propsBlockNode : this.propsHiddenBlockNode,
            propsContent, propsNode, selectedDelivery, showPropMap = false, i, validationErrors;

        if (this.initialized.props)
        {
            BX.remove(BX.lastChild(node));
            node.appendChild(BX.firstChild(this.propsHiddenBlockNode));
            this.maps && setTimeout(BX.proxy(this.maps.propsMapFocusWaiter, this.maps), 200);
        }
        else
        {
            propsContent = node.querySelector('.bx-soa-section-content');
            if (!propsContent)
            {
                propsContent = this.getNewContainer();
                node.appendChild(propsContent);
            }
            else
                BX.cleanNode(propsContent);

            this.getErrorContainer(propsContent);

            // propsNode = BX.create('DIV', {props: {className: 'checkout-user-data mf-form-body'}});
            propsNode = BX.create('DIV');
            selectedDelivery = this.getSelectedDelivery();

            if (
                selectedDelivery && this.params.SHOW_MAP_IN_PROPS === 'Y'
                && this.params.SHOW_MAP_FOR_DELIVERIES && this.params.SHOW_MAP_FOR_DELIVERIES.length
            )
            {
                for (i = 0; i < this.params.SHOW_MAP_FOR_DELIVERIES.length; i++)
                {
                    if (parseInt(selectedDelivery.ID) === parseInt(this.params.SHOW_MAP_FOR_DELIVERIES[i]))
                    {
                        showPropMap = true;
                        break;
                    }
                }
            }

            this.editPropsItems(propsNode);
            showPropMap && this.editPropsMap(propsNode);
            this.editPropsComment(propsNode);

            propsContent.appendChild(propsNode);
            this.getBlockFooter(propsContent);

            if (this.propsBlockNode.getAttribute('data-visited') === 'true')
            {
                validationErrors = this.isValidPropertiesBlock(true);
                // if (validationErrors.length)
                //     BX.addClass(this.propsBlockNode, 'bx-step-error');
                // else
                //     BX.removeClass(this.propsBlockNode, 'bx-step-error');
            }

            var regionNodeCol = BX.create('DIV', {props: {className: 'col-xs-12 person-type-col'}});

            this.getPersonTypeControl(regionNodeCol);
            // propsContent.appendChild(regionNodeCol);
        }

        if ($('#soa-property-3').length && !$('#soa-property-3').hasClass('js-mask-init'))
        {
            $('#soa-property-3').mask(this.params.PHONE_MASK).addClass('js-mask-init');
        }
    };

    BX.Sale.OrderAjaxComponentExt.editFadePropsBlock = function()
    {
        var propsContent = this.propsBlockNode.querySelector('.bx-soa-section-content'), newContent;

        if (this.initialized.props)
        {
            this.propsHiddenBlockNode.appendChild(propsContent);
        }
        else
        {
            this.editActivePropsBlock(false);
            BX.remove(BX.lastChild(this.propsBlockNode));
        }

        newContent = this.getNewContainer();
        this.propsBlockNode.appendChild(newContent);

        if(this.propsBlockNode.getAttribute('data-visited')=='true')
        {
            this.editFadePropsContent(newContent);
        }
        else if(this.deliveryBlockNode.getAttribute('data-visited')=='true')
        {
            this.propsBlockNode.setAttribute('data-disabled', 'false');
        }
        else
        {
            this.propsBlockNode.setAttribute('data-disabled', 'true');
        }
    }

    BX.Sale.OrderAjaxComponentExt.editFadePropsContent = function(node)
    {
        console.log('editFadePropsContent');

        if (!node || !this.locationsInitialized)
            return;

        var errorNode = this.propsHiddenBlockNode.querySelector('.alert'),
            personType = this.getSelectedPersonType(),
            fadeParamName, props,
            group, property, groupIterator, propsIterator, i, validPropsErrors;

        BX.cleanNode(node);

        if (errorNode)
            node.appendChild(errorNode.cloneNode(true));

        if (personType)
        {
            fadeParamName = 'PROPS_FADE_LIST_' + personType.ID;
            props = this.params[fadeParamName];
        }

        if (!props || props.length === 0)
        {
            node.innerHTML += '<strong>' + BX.message('SOA_ORDER_PROPS') + '</strong>';
        }
        else
        {
            var nodeDisabled = BX.create('DIV', {props: {className: 'checkout-user-data checkout-user-data--disabled1'}}),
                editIcon = BX.create('DIV', {
                    props : {
                        className : 'checkout-change-data',
                    },
                    html : this.params.ICON_EDIT + this.params.MESS_EDIT,
                    events: {click: BX.proxy(this.showByClick, this)},
                });

            nodeDisabled.appendChild(editIcon);


            groupIterator = this.fadedPropertyCollection.getGroupIterator();
            while (group = groupIterator())
            {
                propsIterator = group.getIterator();
                while (property = propsIterator())
                {
                    for (i = 0; i < props.length; i++)
                        if (props[i] == property.getId() && property.getSettings()['IS_ZIP'] != 'Y')
                            this.getPropertyRowNode(property, nodeDisabled, true);
                }
            }


            node.appendChild(nodeDisabled);
        }

        if (this.propsBlockNode.getAttribute('data-visited') === 'true')
        {
            validPropsErrors = this.isValidPropertiesBlock();
            if (validPropsErrors.length)
                this.showError(this.propsBlockNode, validPropsErrors);
        }

        BX.bind(node.querySelector('.alert.alert-danger'), 'click', BX.proxy(this.showByClick, this));
        BX.bind(node.querySelector('.alert.alert-warning'), 'click', BX.proxy(this.showByClick, this));
    }


    BX.Sale.OrderAjaxComponentExt.editPropsItems = function(propsNode)
    {
        if (!this.result.ORDER_PROP || !this.propertyCollection)
            return;

        var propsItemsContainer = BX.create('DIV', {props: {className: 'bx-soa-customer'}}),
            itemsPersonal = BX.create('DIV', {props: {className: 'checkout-user-data mf-form-body'}}),
            itemsAddr = BX.create('DIV', {props: {className: 'checkout-user-address mf-form-body'}}),
            group, property, groupIterator = this.propertyCollection.getGroupIterator(), propsIterator,
            subHeader, showAddrBlock = false;

        if (!propsItemsContainer)
            propsItemsContainer = this.propsBlockNode.querySelector('.bx-soa-customer');

        subHeader = BX.create('DIV', {
            props: {
                className: 'checkout-subheader',
            },
            children: [BX.create('H4', {html: 'Контактное лицо:'})]
        });

        itemsPersonal.appendChild(subHeader);

        // while (group = groupIterator())
        // {
        //     propsIterator =  group.getIterator();
        //     while (property = propsIterator())
        //     {
        //         if (
        //             this.deliveryLocationInfo.loc == property.getId()
        //             || this.deliveryLocationInfo.zip == property.getId()
        //             || this.deliveryLocationInfo.city == property.getId()
        //             || 2 == property.getId()
        //             || 13 == property.getId()
        //         )
        //             continue;
        //
        //
        //         switch (property.getGroupId()) {
        //             case '1':
        //                 // this.getPropertyRowNodePersonal(property, itemsPersonal, false);
        //                 break;
        //
        //             case '2':
        //                 showAddrBlock = true;
        //                 // this.getPropertyRowNodeAddr(property, itemsAddr, false);
        //                 break;
        //
        //             // default:
        //             //     this.getPropertyRowNode(property, itemsAddr, false);
        //
        //         }
        //
        //         // this.getPropertyRowNode(property, propsItemsContainer, false);
        //     }
        // }

        subHeader = BX.create('DIV', {
            props: {
                className: 'checkout-subheader',
            },
            children: [BX.create('H4', {html: 'Адрес:'})]
        });

        itemsAddr.appendChild(subHeader);

        while (group = groupIterator())
        {
            propsIterator =  group.getIterator();
            while (property = propsIterator())
            {
                if (
                    this.deliveryLocationInfo.loc == property.getId()
                    || this.deliveryLocationInfo.zip == property.getId()
                    || this.deliveryLocationInfo.city == property.getId()
                // || 2 == property.getId()
                // || 13 == property.getId()
                )
                    continue;


                switch (property.getGroupId()) {
                    case '1':
                        this.getPropertyRowNodePersonal(property, itemsPersonal, false);
                        break;

                    case '2':
                        showAddrBlock = true;
                        this.getPropertyRowNodeAddr(property, itemsAddr, false);
                        break;

                    // default:
                    //     this.getPropertyRowNode(property, itemsAddr, false);

                }

                // this.getPropertyRowNode(property, propsItemsContainer, false);
            }
        }

        if(showAddrBlock === false)
        {
            itemsAddr = BX.create('DIV', {props: {className: 'checkout-user-address mf-form-body'}});
        }


        // propsNode.appendChild(propsItemsContainer);
        propsNode.appendChild(itemsPersonal);
        propsNode.appendChild(itemsAddr);
    };

    BX.Sale.OrderAjaxComponentExt.getPropertyRowNodePersonal = function(property, propsItemsContainer, disabled)
    {
        var propsItemNode = BX.create('DIV'),
            textHtml = '',
            propertyType = property.getType() || '',
            propertyDesc = property.getDescription() || '',
            label;


        // добавим перенос строки перед блоком email
        if(2 == property.getId())
        {
            propsItemsContainer.appendChild(BX.create('BR'));
        }

        if (disabled)
        {
            propsItemNode.innerHTML = '<strong>' + BX.util.htmlspecialchars(property.getName()) + ':</strong> ';
        }
        else
        {
            BX.addClass(propsItemNode, "form-group bx-soa-customer-field");

            // if (property.isRequired())
            //     textHtml += '<span class="bx-authform-starrequired">*</span> ';
            //
            // textHtml += BX.util.htmlspecialchars(property.getName());

            if (property.isRequired())
            {
                textHtml += '<span class="bx-authform-starrequired mf-form-required">';
                textHtml += BX.util.htmlspecialchars(property.getName());
                if (propertyDesc.length && propertyType != 'STRING' && propertyType != 'NUMBER' && propertyType != 'DATE')
                    textHtml += ' <small>(' + BX.util.htmlspecialchars(propertyDesc) + ')</small>';

                textHtml += '<sup>*</sup>';
                textHtml += '</span>';
            }
            else
            {
                textHtml += '<span>';
                textHtml += BX.util.htmlspecialchars(property.getName());
                if (propertyDesc.length && propertyType != 'STRING' && propertyType != 'NUMBER' && propertyType != 'DATE')
                    textHtml += ' <small>(' + BX.util.htmlspecialchars(propertyDesc) + ')</small>';
                textHtml += '</span>';
            }

            label = BX.create('LABEL', {
                attrs: {'for': 'soa-property-' + property.getId()},
                props: {className: 'bx-soa-custom-label'},
                html: textHtml,
            });
            propsItemNode.setAttribute('data-property-id-row', property.getId());
            propsItemNode.appendChild(label);
        }

        switch (propertyType)
        {
            case 'LOCATION':
                this.insertLocationProperty(property, propsItemNode, disabled);
                break;
            case 'DATE':
                this.insertDateProperty(property, propsItemNode, disabled);
                break;
            case 'FILE':
                this.insertFileProperty(property, propsItemNode, disabled);
                break;
            case 'STRING':
                this.insertStringProperty(property, propsItemNode, disabled);
                break;
            case 'ENUM':
                this.insertEnumProperty(property, propsItemNode, disabled);
                break;
            case 'Y/N':
                this.insertYNProperty(property, propsItemNode, disabled);
                break;
            case 'NUMBER':
                this.insertNumberProperty(property, propsItemNode, disabled);
        }

        propsItemsContainer.appendChild(propsItemNode);
    };

    BX.Sale.OrderAjaxComponentExt.insertStringProperty = function(property, propsItemNode, disabled)
    {
        var prop, inputs, values, i, propContainer;

        if (disabled)
        {
            prop = this.propsHiddenBlockNode.querySelector('div[data-property-id-row="' + property.getId() + '"]');
            if (prop)
            {
                values = [];
                inputs = prop.querySelectorAll('input[type=text]');
                if (inputs.length == 0)
                    inputs = prop.querySelectorAll('textarea');

                if (inputs.length)
                {
                    for (i = 0; i < inputs.length; i++)
                    {
                        if (inputs[i].value.length)
                            values.push(inputs[i].value);
                    }
                }

                propsItemNode.innerHTML += this.valuesToString(values);
            }
        }
        else
        {
            propContainer = BX.create('DIV', {props: {className: 'soa-property-container'}});
            property.appendTo(propContainer);
            propsItemNode.appendChild(propContainer);
            this.alterProperty(property.getSettings(), propContainer);
            this.bindValidation(property.getId(), propContainer);
        }
    };

    BX.Sale.OrderAjaxComponentExt.insertStringPropertyEmail = function(property, propsItemNode, disabled)
    {
        var prop, inputs, values, i, propContainer;

        if (disabled)
        {
            prop = this.emailHiddenBlockNode.querySelector('div[data-property-id-row="' + property.getId() + '"]');
            if (prop)
            {
                values = [];
                inputs = prop.querySelectorAll('input[type=text]');
                if (inputs.length == 0)
                    inputs = prop.querySelectorAll('textarea');

                if (inputs.length)
                {
                    for (i = 0; i < inputs.length; i++)
                    {
                        if (inputs[i].value.length)
                            values.push(inputs[i].value);
                    }
                }

                propsItemNode.innerHTML += this.valuesToString(values);
            }
            else if(property.getValue() !== '')
            {
                propsItemNode.innerHTML += property.getValue();
            }
        }
        else
        {
            propContainer = BX.create('DIV', {props: {className: 'soa-property-container'}});
            property.appendTo(propContainer);
            propsItemNode.appendChild(propContainer);
            this.alterProperty(property.getSettings(), propContainer);
            this.bindValidation(property.getId(), propContainer);
        }
    };


    BX.Sale.OrderAjaxComponentExt.getPropertyRowNodeAddr = function(property, propsItemsContainer, disabled)
    {
        var propsItemNode = BX.create('DIV'),
            textHtml = '',
            propertyType = property.getType() || '',
            propertyDesc = property.getDescription() || '',
            label;

        if (disabled)
        {
            propsItemNode.innerHTML = '<strong>' + BX.util.htmlspecialchars(property.getName()) + ':</strong> ';
        }
        else
        {
            BX.addClass(propsItemNode, "form-group bx-soa-customer-field bx-soa-customer-field--"+property.getId());

            // if (property.isRequired())
            //     textHtml += '<span class="bx-authform-starrequired">*</span> ';
            //
            // textHtml += BX.util.htmlspecialchars(property.getName());

            if (property.isRequired())
            {
                textHtml += '<span class="bx-authform-starrequired mf-form-required">';
                textHtml += BX.util.htmlspecialchars(property.getName());
                if (propertyDesc.length && propertyType != 'STRING' && propertyType != 'NUMBER' && propertyType != 'DATE')
                    textHtml += ' <small>(' + BX.util.htmlspecialchars(propertyDesc) + ')</small>';

                textHtml += '<sup>*</sup>';
                textHtml += '</span>';
            }
            else
            {
                textHtml += '<span>';
                textHtml += BX.util.htmlspecialchars(property.getName());
                if (propertyDesc.length && propertyType != 'STRING' && propertyType != 'NUMBER' && propertyType != 'DATE')
                    textHtml += ' <small>(' + BX.util.htmlspecialchars(propertyDesc) + ')</small>';
                textHtml += '</span>';
            }


            label = BX.create('LABEL', {
                attrs: {'for': 'soa-property-' + property.getId()},
                props: {className: 'bx-soa-custom-label'},
                html: textHtml
            });
            propsItemNode.setAttribute('data-property-id-row', property.getId());
            propsItemNode.appendChild(label);
        }

        switch (propertyType)
        {
            case 'LOCATION':
                this.insertLocationProperty(property, propsItemNode, disabled);
                break;
            case 'DATE':
                this.insertDateProperty(property, propsItemNode, disabled);
                break;
            case 'FILE':
                this.insertFileProperty(property, propsItemNode, disabled);
                break;
            case 'STRING':
                this.insertStringProperty(property, propsItemNode, disabled);
                break;
            case 'ENUM':
                this.insertEnumProperty(property, propsItemNode, disabled);
                break;
            case 'Y/N':
                this.insertYNProperty(property, propsItemNode, disabled);
                break;
            case 'NUMBER':
                this.insertNumberProperty(property, propsItemNode, disabled);
        }

        propsItemsContainer.appendChild(propsItemNode);
    };

    BX.Sale.OrderAjaxComponentExt.getPropertyRowNodeEmail = function(property, propsItemsContainer, disabled)
    {
        console.log('getPropertyRowNodeEmail');
        // console.log(property.getValue());
        var propsItemNode = BX.create('DIV'),
            textHtml = '',
            propertyType = property.getType() || '',
            propertyDesc = property.getDescription() || '',
            label;

        if (disabled)
        {
            BX.addClass(propsItemNode, 'checkout-user-data checkout-user-data--disabled');
            propsItemNode.innerHTML = '<strong>' + BX.util.htmlspecialchars(property.getName()) + ':</strong> ';
        }
        else
        {
            BX.addClass(propsItemNode, "form-group bx-soa-customer-field");

            // textHtml += BX.util.htmlspecialchars(property.getName());


            if (property.isRequired())
            {
                textHtml += '<span class="bx-authform-starrequired mf-form-required">';
                textHtml += BX.util.htmlspecialchars(this.params.MESS_EMAIL_LABEL);
                textHtml += '<sup>*</sup>';
                textHtml += '</span>';
            }
            else
            {
                textHtml += '<span>';
                textHtml += BX.util.htmlspecialchars(this.params.MESS_EMAIL_LABEL);
                textHtml += '</span>';
            }


            label = BX.create('LABEL', {
                attrs: {'for': 'soa-property-' + property.getId()},
                props: {className: 'bx-soa-custom-label'},
                html: textHtml,
            });
            propsItemNode.setAttribute('data-property-id-row', property.getId());
            propsItemNode.appendChild(label);
        }

        switch (propertyType)
        {
            case 'LOCATION':
                this.insertLocationProperty(property, propsItemNode, disabled);
                break;
            case 'DATE':
                this.insertDateProperty(property, propsItemNode, disabled);
                break;
            case 'FILE':
                this.insertFileProperty(property, propsItemNode, disabled);
                break;
            case 'STRING':
                this.insertStringPropertyEmail(property, propsItemNode, disabled);
                break;
            case 'ENUM':
                this.insertEnumProperty(property, propsItemNode, disabled);
                break;
            case 'Y/N':
                this.insertYNProperty(property, propsItemNode, disabled);
                break;
            case 'NUMBER':
                this.insertNumberProperty(property, propsItemNode, disabled);
        }

        if(disabled)
        {
            var editIcon = BX.create('DIV', {
                props : {
                    className : 'checkout-change-data',
                },
                html : this.params.ICON_EDIT + this.params.MESS_EDIT,
                events: {click: BX.proxy(this.showByClick, this)},
            });

            propsItemNode.appendChild(editIcon);
        }

        propsItemsContainer.appendChild(propsItemNode);
    };

    /**
     * Hiding current block node and showing next available block node
     */
    BX.Sale.OrderAjaxComponentExt.clickNextAction = function(event)
    {
        console.log('clickNextAction');
        // console.log(event);
        var target = event.target || event.srcElement,
            actionSection = BX.findParent(target, {className : "bx-active"}),
            section = this.getNextSection(actionSection),
            allSections, titleNode, editStep,
            scrollTop = BX.GetWindowScrollPos().scrollTop;

        console.log(actionSection);
        console.log(section.next);
        // console.log(section);
        // console.log(actionSection == this.deliveryBlockNode);


        if(actionSection == this.regionBlockNode)
        {
            // console.log(this.isValidRegionBlock());
            if(this.isValidRegionBlock().length)
            {
                this.showError(this.regionBlockNode, this.isValidRegionBlock());

                setTimeout(BX.delegate(function(){
                    if (BX.pos(actionSection).top < scrollTop)
                        this.animateScrollTo(actionSection, 300);
                }, this), 320);

                return BX.PreventDefault(event);
            }
            else
            {
                this.deliveryBlockNode.setAttribute('data-disabled', false);
                this.propsBlockNode.setAttribute('data-disabled', false);
                this.couponsBlockNode.setAttribute('data-disabled', false);
            }


            // this.propsBlockNode.setAttribute('data-disabled', false);
            // this.paySystemBlockNode.setAttribute('data-disabled', false);
        }
        else if(actionSection == this.couponsBlockNode)
        {
            this.deliveryBlockNode.setAttribute('data-disabled', false);
            this.propsBlockNode.setAttribute('data-disabled', false);
        }
        else if(actionSection == this.deliveryBlockNode)
        {
            //нажали далее в блоке Доставка
            var validPropsErrors = this.isValidPickupStore(),
                errorContainer = this.deliveryBlockNode.querySelector('.alert.alert-danger');

            errorContainer.style.display = 'none';

            // if(actionSection.querySelector('select.js-store-city'))
            // {
            //     if(actionSection.querySelector('select.js-store-city').value == '')
            //     {
            //         validPropsErrors.push('Укажите город самовывоза');
            //     }
            // }
            //
            // if(actionSection.querySelector('select.js-store-items'))
            // {
            //     if(actionSection.querySelector('select.js-store-items').value == '')
            //     {
            //         validPropsErrors.push('Укажите пункт самовывоза');
            //     }
            // }

            if (validPropsErrors.length) {
                this.showError(this.deliveryBlockNode, validPropsErrors);

                setTimeout(BX.delegate(function(){
                    if (BX.pos(actionSection).top < scrollTop)
                        this.animateScrollTo(actionSection, 300);
                }, this), 320);

                return BX.PreventDefault(event);
            }
        }
        else if(actionSection == this.propsBlockNode)
        {
            if (this.propsBlockNode.getAttribute('data-visited') === 'true')
            {
                this.propsBlockNode.querySelector('.alert.alert-danger').style.display = 'none';

                validPropsErrors = this.isValidPropertiesBlock();

                if (validPropsErrors.length)
                {
                    this.showError(this.propsBlockNode, validPropsErrors);

                    setTimeout(BX.delegate(function(){
                        if (BX.pos(actionSection).top < scrollTop)
                            this.animateScrollTo(actionSection, 300);
                    }, this), 320);

                    return BX.PreventDefault(event);
                }
                else
                {
                    this.paySystemBlockNode.setAttribute('data-disabled', false);
                }
            }
        }


        this.reachGoal('next', actionSection);

        if (
            (!this.result.IS_AUTHORIZED || typeof this.result.LAST_ORDER_DATA.FAIL !== 'undefined')
            && section.next.getAttribute('data-visited') == 'false'
        )
        {
            titleNode = section.next.querySelector('.bx-soa-section-title-container');
            BX.bind(titleNode, 'click', BX.proxy(this.showByClick, this));
            editStep = section.next.querySelector('.bx-soa-editstep');
            if (editStep)
                editStep.style.display = '';

            allSections = this.orderBlockNode.querySelectorAll('.bx-soa-section.bx-active');
            if (section.next.id == allSections[allSections.length - 1].id)
                this.switchOrderSaveButtons(true);
        }

        this.fade(actionSection, section.next);
        this.show(section.next);

        this.showOrderSaveBtns();
        // if((BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-visited="true"]').length - BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-disablednode="true"]').length) == (BX('bx-soa-order').querySelectorAll('.mf-checkout-el:not([data-disablednode="true"])').length - BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-disablednode="true"]').length))
        // {
        //     BX('bx-soa-orderSave').style.display = 'block';
        //     BX('bx-soa-orderSave-sb').style.display = 'block';
        // }

        return BX.PreventDefault(event);
    };

    BX.Sale.OrderAjaxComponentExt.clickNextActionEmail = function(event)
    {
        console.log('clickNextActionEmail');
        // console.log(event);
        var target = event.target || event.srcElement,
            actionSection = BX.findParent(target, {className : "bx-active"}),
            section = this.getNextSection(actionSection),
            allSections, titleNode, editStep,
            validPropsErrors,
            errorContainer = this.emailBlockNode.querySelector('.alert.alert-danger');

        errorContainer.style.display = 'none';

        // if (this.propsBlockNode.getAttribute('data-visited') === 'true')
        // {
        validPropsErrors = this.isValidEmailBlock();

        if (validPropsErrors.length) {
            this.showError(this.emailBlockNode, validPropsErrors);

            this.deliveryBlockNode.setAttribute('data-disabled', true);
            this.propsBlockNode.setAttribute('data-disabled', true);
            this.paySystemBlockNode.setAttribute('data-disabled', true);

            this.isEmailEntered = false;

            return BX.PreventDefault(event);
        } else {
            this.deliveryBlockNode.setAttribute('data-disabled', false);
            this.propsBlockNode.setAttribute('data-disabled', false);
            this.paySystemBlockNode.setAttribute('data-disabled', false);

            this.isEmailEntered = true;
            this.activeSectionId = this.emailBlockNode.id;
        }

        // }

        // console.log(actionSection);
        // console.log(section.next);

        this.reachGoal('next', actionSection);

        if (
            (!this.result.IS_AUTHORIZED || typeof this.result.LAST_ORDER_DATA.FAIL !== 'undefined')
            && section.next.getAttribute('data-visited') == 'false'
        )
        {
            titleNode = section.next.querySelector('.bx-soa-section-title-container');
            BX.bind(titleNode, 'click', BX.proxy(this.showByClick, this));
            editStep = section.next.querySelector('.bx-soa-editstep');
            if (editStep)
                editStep.style.display = '';

            allSections = this.orderBlockNode.querySelectorAll('.bx-soa-section.bx-active');
            if (section.next.id == allSections[allSections.length - 1].id)
                this.switchOrderSaveButtons(true);
        }

        this.fade(actionSection, section.next);
        this.show(section.next);
        return BX.PreventDefault(event);
    };

    BX.Sale.OrderAjaxComponentExt.isValidEmailBlock = function(excludeLocation)
    {
        if (!this.options.propertyValidation)
            return [];

        var props = this.emailBlockNode.querySelectorAll('.bx-soa-customer-field[data-property-id-row]'),
            propsErrors = [],
            id, propContainer, arProperty, data, i;

        for (i = 0; i < props.length; i++)
        {
            id = props[i].getAttribute('data-property-id-row');

            if (!!excludeLocation && this.locations[id])
                continue;

            propContainer = props[i].querySelector('.soa-property-container');
            if (propContainer)
            {
                arProperty = this.validation.properties[id];
                data = this.getValidationData(arProperty, propContainer);
                propsErrors = propsErrors.concat(this.isValidProperty(data, true));
            }
        }

        return propsErrors;
    }

    BX.Sale.OrderAjaxComponentExt.isValidProperty = function(data, fieldName)
    {
        var propErrors = [], inputErrors, i;

        if (!data || !data.inputs)
            return propErrors;

        for (i = 0; i < data.inputs.length; i++)
        {
            inputErrors = data.func(data.inputs[i], !!fieldName);
            if (inputErrors.length)
                propErrors[i] = inputErrors.join('<br>');
        }

        // this.showValidationResult(data.inputs, propErrors);

        return propErrors;
    }



    BX.Sale.OrderAjaxComponentExt.isValidPickupStore = function()
    {
        var validErrors = [],
            buyerStoreInput = BX('BUYER_STORE'),
            isCutyErrorPushed = false;

        if(this.deliveryBlockNode.querySelector('select.js-store-city'))
        {
            if(this.deliveryBlockNode.querySelector('select.js-store-city').value == '')
            {
                validErrors.push('Укажите город самовывоза');
            }
        }

        if(this.deliveryBlockNode.querySelector('select.js-store-items'))
        {
            if(this.deliveryBlockNode.querySelector('select.js-store-items').value == '')
            {
                validErrors.push('Укажите пункт самовывоза');
                isCutyErrorPushed = true
            }
        }

        // if(parseInt(buyerStoreInput.value) == 0 && !isCutyErrorPushed)
        // {
        //     validErrors.push('Укажите пункт самовывоза');
        // }

        return validErrors;
    }

    BX.Sale.OrderAjaxComponentExt.isValidForm = function()
    {
        if (!this.options.propertyValidation)
            return true;

        var regionErrors = this.isValidRegionBlock(),
            propsErrors = this.isValidPropertiesBlock(),
            pickupErrors = this.isValidPickupStore(),
            navigated = false, tooltips, i,
            maxCommentLength = 250;

        if (BX('orderDescription').value.length > maxCommentLength)
            propsErrors.push('Максимальная длина комментария - '+maxCommentLength+' символов');

        if (regionErrors.length)
        {
            navigated = true;
            this.animateScrollTo(this.regionBlockNode, 800, 50);
        }

        if (propsErrors.length && !navigated)
        {
            if (this.activeSectionId == this.propsBlockNode.id)
            {
                tooltips = this.propsBlockNode.querySelectorAll('div.tooltip');
                for (i = 0; i < tooltips.length; i++)
                {
                    if (tooltips[i].getAttribute('data-state') == 'opened')
                    {
                        this.animateScrollTo(BX.findParent(tooltips[i], {className: 'form-group bx-soa-customer-field'}), 800, 50);
                        break;
                    }
                }
            }
            else
                this.animateScrollTo(this.propsBlockNode, 800, 50);
        }

        if (regionErrors.length)
        {
            this.showError(this.regionBlockNode, regionErrors);
            BX.addClass(this.regionBlockNode, 'bx-step-error');
        }

        if (propsErrors.length)
        {
            if (this.activeSectionId !== this.propsBlockNode.id)
                this.showError(this.propsBlockNode, propsErrors);

            BX.addClass(this.propsBlockNode, 'bx-step-error');
        }

        if (pickupErrors.length)
        {
            this.showError(this.deliveryBlockNode, pickupErrors);

            if (!navigated)
                this.animateScrollTo(this.deliveryBlockNode, 800, 50);

            BX.addClass(this.deliveryBlockNode, 'bx-step-error');
        }

        // console.log(!(regionErrors.length + propsErrors.length + pickupErrors.length));

        // return false;

        return !(regionErrors.length + propsErrors.length + pickupErrors.length);
    }


    BX.Sale.OrderAjaxComponentExt.initPropsListForLocation = function () {
        // console.log('initPropsListForLocation');
        if (BX.saleOrderAjax && this.result.ORDER_PROP && this.result.ORDER_PROP.properties)
        {
            var i, k, curProp, attrObj;

            BX.saleOrderAjax.cleanUp();

            for (i = 0; i < this.result.ORDER_PROP.properties.length; i++)
            {
                curProp = this.result.ORDER_PROP.properties[i];

                if (curProp.TYPE == 'LOCATION' && curProp.MULTIPLE == 'Y' && curProp.IS_LOCATION != 'Y')
                {
                    for (k = 0; k < this.locations[curProp.ID].length; k++)
                    {
                        BX.saleOrderAjax.addPropertyDesc({
                            id: curProp.ID + '_' + k,
                            attributes: {
                                id: curProp.ID + '_' + k,
                                type: curProp.TYPE,
                                valueSource: curProp.SOURCE == 'DEFAULT' ? 'default' : 'form'
                            }
                        });
                    }
                }
                else
                {
                    attrObj = {
                        id: curProp.ID,
                        type: curProp.TYPE,
                        valueSource: curProp.SOURCE == 'DEFAULT' ? 'default' : 'form'
                    };

                    if (!this.deliveryLocationInfo.city && parseInt(curProp.INPUT_FIELD_LOCATION) > 0)
                    {
                        attrObj.altLocationPropId = parseInt(curProp.INPUT_FIELD_LOCATION);
                        this.deliveryLocationInfo.city = curProp.INPUT_FIELD_LOCATION;
                    }

                    if (!this.deliveryLocationInfo.loc && curProp.IS_LOCATION == 'Y')
                        this.deliveryLocationInfo.loc = curProp.ID;

                    if (!this.deliveryLocationInfo.zip && curProp.IS_ZIP == 'Y')
                    {
                        attrObj.isZip = true;
                        this.deliveryLocationInfo.zip = curProp.ID;
                    }

                    BX.saleOrderAjax.addPropertyDesc({
                        id: curProp.ID,
                        attributes: attrObj
                    });
                }
            }
        }
    };

    BX.Sale.OrderAjaxComponentExt.prepareLocations = function(locations)
    {
        // console.log('prepareLocations');

        this.locations = {};
        this.cleanLocations = {};

        var temporaryLocations,
            i, k, output;

        if (BX.util.object_keys(locations).length)
        {
            for (i in locations)
            {
                if (!locations.hasOwnProperty(i))
                    continue;

                this.locationsTemplate = locations[i].template || '';
                temporaryLocations = [];
                output = locations[i].output;

                if (output.clean)
                {
                    this.cleanLocations[i] = BX.processHTML(output.clean, false);
                    delete output.clean;
                }

                for (k in output)
                {
                    if (output.hasOwnProperty(k))
                    {
                        temporaryLocations.push({
                            output: BX.processHTML(output[k], false),
                            showAlt: locations[i].showAlt,
                            lastValue: locations[i].lastValue,
                            coordinates: locations[i].coordinates || false
                        });
                    }
                }

                this.locations[i] = temporaryLocations;
            }
        }
    };

    BX.Sale.OrderAjaxComponentExt.locationsCompletion = function()
    {
        console.log('locationsCompletion');
        // console.log($('#bx-soa-region').find('.bx-ui-slst-pool').find('.bx-ui-slst-input-block:first-child').find('.bx-combobox-fake-as-input').text());

        var i, locationNode, clearButton, inputStep, inputSearch,
            arProperty, data, section, $countryBlock, selectedCountry, blockClasses = '';

        this.locationsInitialized = true;
        this.fixLocationsStyle(this.regionBlockNode, this.regionHiddenBlockNode);
        this.fixLocationsStyle(this.propsBlockNode, this.propsHiddenBlockNode);

        for (i in this.locations)
        {
            if (!this.locations.hasOwnProperty(i))
                continue;

            locationNode = this.orderBlockNode.querySelector('div[data-property-id-row="' + i + '"]');
            if (!locationNode)
                continue;

            clearButton = locationNode.querySelector('div.bx-ui-sls-clear');
            inputStep = locationNode.querySelector('div.bx-ui-slst-pool');
            inputSearch = locationNode.querySelector('input.bx-ui-sls-fake[type=text]');

            locationNode.removeAttribute('style');
            this.bindValidation(i, locationNode);
            if (clearButton)
            {
                BX.bind(clearButton, 'click', function(e){
                    var target = e.target || e.srcElement,
                        parent = BX.findParent(target, {tagName: 'DIV', className: 'form-group'}),
                        locationInput;

                    if (parent)
                        locationInput = parent.querySelector('input.bx-ui-sls-fake[type=text]');

                    if (locationInput)
                        BX.fireEvent(locationInput, 'keyup');
                });
            }

            if (!this.firstLoad && this.options.propertyValidation)
            {
                if (inputStep)
                {
                    arProperty = this.validation.properties[i];
                    data = this.getValidationData(arProperty, locationNode);
                    section = BX.findParent(locationNode, {className: 'bx-soa-section'});

                    if (section && section.getAttribute('data-visited') == 'true')
                        this.isValidProperty(data);
                }

                if (inputSearch)
                    BX.fireEvent(inputSearch, 'keyup');
            }
        }

        if (this.firstLoad && this.result.IS_AUTHORIZED && typeof this.result.LAST_ORDER_DATA.FAIL === 'undefined')
            this.showActualBlock();

        this.checkNotifications();

        $countryBlock = $('#bx-soa-region').find('.bx-ui-slst-pool').find('.bx-ui-slst-input-block:first-child');
        selectedCountry = $countryBlock.find('.bx-combobox-fake-as-input').text();

        if(!$countryBlock.hasClass('country')) {
            $countryBlock.addClass('country')
        }

        $countryBlock.removeClass('country--by');
        $countryBlock.removeClass('country--ru');
        $countryBlock.removeClass('country--kz');

        switch (selectedCountry)
        {
            case 'Беларусь':
                blockClasses += ' country--by';
                break;

            case 'Россия':
                blockClasses += ' country--ru';
                break;

            case 'Казахстан':
                blockClasses += ' country--kz';
                break;
        }

        $countryBlock.addClass(blockClasses);


        if (this.activeSectionId !== this.regionBlockNode.id)
            this.editFadeRegionContent(this.regionBlockNode.querySelector('.bx-soa-section-content'));

        if (this.activeSectionId != this.propsBlockNode.id)
            this.editFadePropsContent(this.propsBlockNode.querySelector('.bx-soa-section-content'));
    };

    BX.Sale.OrderAjaxComponentExt.initOptions11 = function() {
        initOptionsParent.apply(this, arguments);
        this.propertyDeliveryCollection = new BX.Sale.PropertyCollection(BX.merge({publicMode: true}, this.result.DELIVERY_PROPS));
    };


    // BX.Sale.OrderAjaxComponentExt.editPickUpList = function(isNew)
    // {
    //     // console.log('editPickUpList');
    //     // editPickUpListParent.apply(isNew); //вызываем родителя
    //
    //     if (!this.pickUpPagination.currentPage || !this.pickUpPagination.currentPage.length)
    //         return;
    //
    //     BX.remove(BX('pickUpLoader'));
    //
    //     var pickUpList = BX.create('DIV', {props: {className: 'bx-soa-pickup-list main'}}),
    //         buyerStoreInput = BX('BUYER_STORE'),
    //         selectedStore,
    //         container, i, found = false,
    //         recommendList, selectedDelivery, currentStore, storeNode;
    //
    //     if (buyerStoreInput)
    //         selectedStore = buyerStoreInput.value;
    //
    //     recommendList = this.pickUpBlockNode.querySelector('.bx-soa-pickup-list.recommend');
    //     if (!recommendList)
    //         recommendList = this.pickUpHiddenBlockNode.querySelector('.bx-soa-pickup-list.recommend');
    //
    //     if (!recommendList || !recommendList.querySelector('.bx-soa-pickup-list-item.bx-selected'))
    //     {
    //         selectedDelivery = this.getSelectedDelivery();
    //         if (selectedDelivery && selectedDelivery.STORE)
    //         {
    //             for (i = 0; i < selectedDelivery.STORE.length; i++)
    //                 if (selectedDelivery.STORE[i] == selectedStore)
    //                     found = true;
    //         }
    //     }
    //     else
    //         found = true;
    //
    //     for (i = 0; i < this.pickUpPagination.currentPage.length; i++)
    //     {
    //         currentStore = this.pickUpPagination.currentPage[i];
    //
    //         if (currentStore.ID == selectedStore || parseInt(selectedStore) == 0 || !found)
    //         {
    //             selectedStore = buyerStoreInput.value = currentStore.ID;
    //             found = true;
    //         }
    //
    //         storeNode = this.createPickUpItem(currentStore, {selected: currentStore.ID == selectedStore});
    //         pickUpList.appendChild(storeNode);
    //     }
    //
    //     if (!!isNew)
    //     {
    //         container = this.pickUpHiddenBlockNode.querySelector('.bx_soa_pickup>.col-xs-12');
    //         if (!container)
    //             container = this.pickUpBlockNode.querySelector('.bx_soa_pickup>.col-xs-12');
    //
    //         container.appendChild(
    //             BX.create('DIV', {
    //                 props: {className: 'bx-soa-pickup-subTitle'},
    //                 html: this.params.MESS_PICKUP_LIST
    //             })
    //         );
    //         container.appendChild(pickUpList);
    //     }
    //     else
    //     {
    //         container = this.pickUpBlockNode.querySelector('.bx-soa-pickup-list.main');
    //         BX.insertAfter(pickUpList, container);
    //         BX.remove(container);
    //     }
    //
    //     this.pickUpPagination.show && this.showPagination('pickUp', pickUpList);
    //
    //     if(!$('#bx-soa-pickup').find('.js-stores-group').length) {
    //         if($('#stores-selects').html() === undefined) {
    //             setTimeout(function () {
    //                 $('#bx-soa-pickup').prepend($('#stores-selects').html());
    //                 // $('#bx-soa-pickup select').styler();
    //             }, 300);
    //         } else {
    //             $('#bx-soa-pickup').prepend($('#stores-selects').html());
    //             // $('#bx-soa-pickup select').styler();
    //
    //         }
    //     }
    // };


    /**
     * поле коммент к заказу
     *
     * @param propsNode
     */
    BX.Sale.OrderAjaxComponentExt.editPropsComment = function(propsNode)
    {
        var propsCommentContainer, label, input, div;

        propsCommentContainer = BX.create('DIV', {props: {className: 'checkout-user-data mf-form-body mf-form-body--comment'}});
        label = BX.create('LABEL', {
            attrs: {for: 'orderDescription'},
            props: {className: 'bx-soa-customer-label'},
            html: '<span>' + this.params.MESS_ORDER_DESC + '</span>',
        });

        input = BX.create('TEXTAREA', {
            props: {
                id: 'orderDescription',
                // cols: '4',
                className: 'form-control bx-soa-customer-textarea bx-ios-fix',
                name: 'ORDER_DESCRIPTION'
            },
            text: this.result.ORDER_DESCRIPTION ? this.result.ORDER_DESCRIPTION : ''
        });

        div = BX.create('DIV', {
            props: {className: 'form-group bx-soa-customer-field'},
            children: [label, input]
        });

        propsCommentContainer.appendChild(div);
        propsNode.appendChild(propsCommentContainer);
    };

    BX.Sale.OrderAjaxComponentExt.getPersonTypeControl = function(node)
    {
        if (!this.result.PERSON_TYPE)
            return;

        this.result.PERSON_TYPE = this.getPersonTypeSortedArray(this.result.PERSON_TYPE);

        var personTypesCount = this.result.PERSON_TYPE.length,
            currentType, oldPersonTypeId, i,
            input, options = [], label, delimiter = false;

        if (personTypesCount > 1)
        {
            input = BX.create('DIV', {
                props: {className: 'form-group bx-soa-section-hide'},
                children: [
                    BX.create('LABEL', {props: {className: 'bx-soa-custom-label'}, html: this.params.MESS_PERSON_TYPE}),
                    BX.create('BR')
                ]
            });
            node.appendChild(input);
            node = input;
        }

        if (personTypesCount > 2)
        {
            for (i in this.result.PERSON_TYPE)
            {
                if (this.result.PERSON_TYPE.hasOwnProperty(i))
                {
                    currentType = this.result.PERSON_TYPE[i];
                    options.push(BX.create('OPTION', {
                        props: {
                            value: currentType.ID,
                            selected: currentType.CHECKED == 'Y'
                        },
                        text: currentType.NAME
                    }));

                    if (currentType.CHECKED == 'Y')
                        oldPersonTypeId = currentType.ID;
                }

            }
            node.appendChild(BX.create('SELECT', {
                props: {name: 'PERSON_TYPE', className: 'form-control'},
                children: options,
                events: {change: BX.proxy(this.sendRequest, this)}
            }));

            this.regionBlockNotEmpty = true;
        }
        else if (personTypesCount == 2)
        {
            for (i in this.result.PERSON_TYPE)
            {
                if (this.result.PERSON_TYPE.hasOwnProperty(i))
                {
                    currentType = this.result.PERSON_TYPE[i];
                    label = BX.create('LABEL', {
                        children: [
                            BX.create('INPUT', {
                                attrs: {checked: currentType.CHECKED == 'Y'},
                                props: {type: 'radio', name: 'PERSON_TYPE', value: currentType.ID}
                            }),
                            BX.util.htmlspecialchars(currentType.NAME)
                        ],
                        events: {change: BX.proxy(this.sendRequest, this)}
                    });

                    if (delimiter)
                        node.appendChild(BX.create('BR'));

                    node.appendChild(BX.create('DIV', {props: {className: 'radio-inline'}, children: [label]}));
                    delimiter = true;

                    if (currentType.CHECKED == 'Y')
                        oldPersonTypeId = currentType.ID;
                }
            }

            this.regionBlockNotEmpty = true;
        }
        else
        {
            for (i in this.result.PERSON_TYPE)
                if (this.result.PERSON_TYPE.hasOwnProperty(i))
                    node.appendChild(BX.create('INPUT', {props: {type: 'hidden', name: 'PERSON_TYPE', value: this.result.PERSON_TYPE[i].ID}}));
        }

        if (oldPersonTypeId)
        {
            node.appendChild(
                BX.create('INPUT', {
                    props: {
                        type: 'hidden',
                        name: 'PERSON_TYPE_OLD',
                        value: oldPersonTypeId

                    }
                })
            );
        }
    };

    BX.Sale.OrderAjaxComponentExt.editFadeRegionContent = function(node)
    {
        if (!node || !this.locationsInitialized)
            return;

        var selectedPersonType = this.getSelectedPersonType(),
            errorNode = this.regionHiddenBlockNode.querySelector('.alert.alert-danger'),
            addedHtml = '', props = [], locationProperty,
            input, zipValue = '', zipProperty,
            fadeParamName, i, k, locationString, validRegionErrors;

        BX.cleanNode(node);

        if (errorNode)
            node.appendChild(errorNode.cloneNode(true));

        // if (selectedPersonType && selectedPersonType.NAME && this.result.PERSON_TYPE.length > 1)
        // {
        //     addedHtml += '<strong>' + this.params.MESS_PERSON_TYPE + ':</strong> '
        //         + BX.util.htmlspecialchars(selectedPersonType.NAME) + '<br>';
        // }

        if (selectedPersonType)
        {
            fadeParamName = 'PROPS_FADE_LIST_' + selectedPersonType.ID;
            props = this.params[fadeParamName] || [];
        }

        for (i in this.result.ORDER_PROP.properties)
        {
            if (this.result.ORDER_PROP.properties.hasOwnProperty(i))
            {
                if (this.result.ORDER_PROP.properties[i].IS_LOCATION == 'Y'
                    && this.result.ORDER_PROP.properties[i].ID == this.deliveryLocationInfo.loc)
                {
                    locationProperty = this.result.ORDER_PROP.properties[i];
                }
                else if (this.result.ORDER_PROP.properties[i].IS_ZIP == 'Y'
                    && this.result.ORDER_PROP.properties[i].ID == this.deliveryLocationInfo.zip)
                {
                    zipProperty = this.result.ORDER_PROP.properties[i];
                    for (k = 0; k < props.length; k++)
                    {
                        if (props[k] == zipProperty.ID)
                        {
                            input = BX('zipProperty');
                            zipValue = input && input.value && input.value.length ? input.value : BX.message('SOA_NOT_SPECIFIED');
                            break;
                        }
                    }
                }
            }
        }

        locationString = this.getLocationString(this.regionHiddenBlockNode);
        if (locationProperty && locationString.length)
            addedHtml += '<strong>' + BX.util.htmlspecialchars(locationProperty.NAME) + ':</strong> '
                + BX.util.htmlspecialchars(locationString) + '<br>';

        if (zipProperty && zipValue.length)
            addedHtml += '<strong>' + BX.util.htmlspecialchars(zipProperty.NAME) + ':</strong> '
                + BX.util.htmlspecialchars(zipValue);

        node.innerHTML += addedHtml;

        if (this.regionBlockNode.getAttribute('data-visited') == 'true')
        {
            validRegionErrors = this.isValidRegionBlock();

            if (validRegionErrors.length)
            {
                BX.addClass(this.regionBlockNode, 'bx-step-error');
                this.showError(this.regionBlockNode, validRegionErrors);
            }
            else
                BX.removeClass(this.regionBlockNode, 'bx-step-error');
        }

        BX.bind(node.querySelector('.alert.alert-danger'), 'click', BX.proxy(this.showByClick, this));
        BX.bind(node.querySelector('.alert.alert-warning'), 'click', BX.proxy(this.showByClick, this));
    };

    /**
     * Replacing current active block node with generated fade block node
     */
    BX.Sale.OrderAjaxComponentExt.fade = function(node, nextSection)
    {
        if (!node || !node.id || this.activeSectionId != node.id)
            return;

        this.hasErrorSection[node.id] = false;

        var objHeightOrig = node.offsetHeight,
            objHeight;

        switch (node.id)
        {
            case this.authBlockNode.id:
                this.authBlockNode.style.display = 'none';
                BX.removeClass(this.authBlockNode, 'bx-active');
                break;
            case this.basketBlockNode.id:
                this.editFadeBasketBlock();
                break;
            case this.regionBlockNode.id:
                this.editFadeRegionBlock();
                break;
            case this.couponsBlockNode.id:
                this.editFadeCouponsBlock();
                break;
            // case this.emailBlockNode.id:
            //     BX.remove(this.emailBlockNode.querySelector('.alert.alert-warning.alert-hide'));
            //     this.editFadeEmailBlock();
            //     break;
            case this.paySystemBlockNode.id:
                BX.remove(this.paySystemBlockNode.querySelector('.alert.alert-warning.alert-hide'));
                this.editFadePaySystemBlock();
                break;
            case this.deliveryBlockNode.id:
                BX.remove(this.deliveryBlockNode.querySelector('.alert.alert-warning.alert-hide'));
                this.editFadeDeliveryBlock();
                break;
            case this.pickUpBlockNode.id:
                this.editFadePickUpBlock();
                break;
            case this.propsBlockNode.id:
                this.editFadePropsBlock();
                break;
        }

        BX.addClass(node, 'bx-step-completed');
        BX.removeClass(node, 'bx-selected');

        objHeight = node.offsetHeight;
        node.style.height = objHeightOrig + 'px';

        // calculations of scrolling animation
        if (nextSection)
        {
            var windowScrollTop = BX.GetWindowScrollPos().scrollTop,
                orderPos = BX.pos(this.orderBlockNode),
                nodePos = BX.pos(node),
                diff, scrollTo, nextSectionHeightBefore, nextSectionHeightAfter, nextSectionHidden, offset;

            nextSectionHidden = BX(nextSection.id + '-hidden');
            nextSectionHidden.style.left = '-10000';
            nextSectionHidden.style.position = 'absolute';
            this.orderBlockNode.appendChild(nextSectionHidden);
            nextSectionHeightBefore = nextSection.offsetHeight;
            nextSectionHeightAfter = nextSectionHidden.offsetHeight + 57;
            BX(node.id + '-hidden').parentNode.appendChild(nextSectionHidden);
            nextSectionHidden.removeAttribute('style');

            diff = objHeight + nextSectionHeightAfter - objHeightOrig - nextSectionHeightBefore;

            offset = window.innerHeight - orderPos.height - diff;
            if (offset > 0)
                scrollTo = orderPos.top - offset/2;
            else
            {
                if (nodePos.top > windowScrollTop)
                    scrollTo = nodePos.top;
                else
                    scrollTo = nodePos.bottom + 6 - objHeightOrig + objHeight;

                if (scrollTo + window.innerHeight > orderPos.bottom + 25 + diff)
                    scrollTo = orderPos.bottom + 25 + diff - window.innerHeight;
            }

            scrollTo -= this.isMobile ? 50 : 0;
        }

        new BX.easing({
            duration: nextSection ? 800 : 600,
            start: {height: objHeightOrig, scrollTop: windowScrollTop},
            finish: {height: objHeight, scrollTop: scrollTo},
            transition: BX.easing.makeEaseOut(BX.easing.transitions.quad),
            step: function(state){
                node.style.height = state.height + "px";
                // if (nextSection)
                //     window.scrollTo(0, state.scrollTop);
            },
            complete: function(){
                node.style.height = '';
            }
        }).animate();

        this.checkBlockErrors(node);
    };

    /**
     * Showing active data in certain block node
     */
    BX.Sale.OrderAjaxComponentExt.show = function(node)
    {
        if (!node || !node.id || this.activeSectionId == node.id)
            return;

        this.activeSectionId = node.id;
        BX.removeClass(node, 'bx-step-error bx-step-warning');

        switch (node.id)
        {
            case this.authBlockNode.id:
                this.authBlockNode.style.display = '';
                BX.addClass(this.authBlockNode, 'bx-active');
                break;
            case this.basketBlockNode.id:
                this.editActiveBasketBlock(true);
                this.alignBasketColumns();
                break;
            case this.regionBlockNode.id:
                this.editActiveRegionBlock(true);
                break;
            case this.couponsBlockNode.id:
                this.editActiveCouponsBlock(true);
                break;
            // case this.emailBlockNode.id:
            //     this.editActiveEmailBlock(true);
            //     break;
            case this.deliveryBlockNode.id:
                this.editActiveDeliveryBlock(true);
                break;
            case this.paySystemBlockNode.id:
                this.editActivePaySystemBlock(true);
                break;
            case this.pickUpBlockNode.id:
                this.editActivePickUpBlock(true);
                break;
            case this.propsBlockNode.id:
                this.editActivePropsBlock(true);
                break;
        }

        if (node.getAttribute('data-visited') === 'false')
        {
            this.showBlockErrors(node);
            this.notifyAboutWarnings(node);
        }

        node.setAttribute('data-visited', 'true');
        BX.addClass(node, 'bx-selected');
        BX.removeClass(node, 'bx-step-completed');
    };

    BX.Sale.OrderAjaxComponentExt.showByClick = function(event)
    {
        console.log('showByClick');

        var target = event.target || event.srcElement,
            showNode = BX.findParent(target, {className: "bx-active"}),
            fadeNode = BX(this.activeSectionId),
            scrollTop = BX.GetWindowScrollPos().scrollTop;

        console.log(this.activeSectionId);

        if(this.activeSectionId == 'bx-soa-delivery')
        {
            var validPropsErrors = this.isValidPickupStore(),
                errorContainer = this.deliveryBlockNode.querySelector('.alert.alert-danger');

            errorContainer.style.display = 'none';

            // if(fadeNode.querySelector('select.js-store-city'))
            // {
            //     if(fadeNode.querySelector('select.js-store-city').value == '')
            //     {
            //         validPropsErrors.push('Укажите город самовывоза');
            //     }
            // }
            //
            // if(fadeNode.querySelector('select.js-store-items'))
            // {
            //     if(fadeNode.querySelector('select.js-store-items').value == '')
            //     {
            //         validPropsErrors.push('Укажите пункт самовывоза');
            //     }
            // }

            if (validPropsErrors.length) {
                this.showError(this.deliveryBlockNode, validPropsErrors);

                setTimeout(BX.delegate(function(){
                    if (BX.pos(fadeNode).top < scrollTop)
                        this.animateScrollTo(fadeNode, 300);
                }, this), 320);


                return BX.PreventDefault(event);
            }
        } else if(this.activeSectionId == 'bx-soa-properties') {
            if (this.propsBlockNode.getAttribute('data-visited') === 'true')
            {
                this.propsBlockNode.querySelector('.alert.alert-danger').style.display = 'none';
                validPropsErrors = this.isValidPropertiesBlock();

                if (validPropsErrors.length)
                {
                    this.showError(this.propsBlockNode, validPropsErrors);

                    setTimeout(BX.delegate(function(){
                        if (BX.pos(fadeNode).top < scrollTop)
                            this.animateScrollTo(fadeNode, 300);
                    }, this), 320);

                    return BX.PreventDefault(event);
                }

            }

        }

        // if(showNode.id == this.emailBlockNode.id) {
        //     // если переключили на изменение мейла - отключаем следующие блоки
        //     this.deliveryBlockNode.setAttribute('data-disabled', true);
        //     this.propsBlockNode.setAttribute('data-disabled', true);
        //     this.paySystemBlockNode.setAttribute('data-disabled', true);
        //
        //     this.isEmailEntered = false;
        // }

        // if (!showNode || BX.hasClass(showNode, 'bx-selected'))
        if (!showNode || BX.hasClass(showNode, 'bx-selected') || showNode.getAttribute('data-disabled')=='true')
            return BX.PreventDefault(event);

        this.reachGoal('edit', showNode);

        fadeNode && this.fade(fadeNode);
        this.show(showNode);

        // console.log(showNode.id);

        if(showNode.id == 'bx-soa-delivery')
        {
            $('#bx-soa-delivery').find('select').styler();
        }

        setTimeout(BX.delegate(function(){
            if (BX.pos(showNode).top < scrollTop)
                this.animateScrollTo(showNode, 300);
        }, this), 320);

        this.showOrderSaveBtns();
        // if((BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-visited="true"]').length - BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-disablednode="true"]').length) == (BX('bx-soa-order').querySelectorAll('.mf-checkout-el').length - BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-disablednode="true"]').length))
        // {
        //     BX('bx-soa-orderSave').style.display = 'block';
        //     BX('bx-soa-orderSave-sb').style.display = 'block';
        // }

        return BX.PreventDefault(event);
    };


    BX.Sale.OrderAjaxComponentExt.editActiveCouponsBlock = function(activeNodeMode)
    {
        console.log('editActiveCouponsBlock');
        var node = activeNodeMode ? this.couponsBlockNode : this.couponsHiddenBlockNode,
            regionContent, regionNode, regionNodeCol,
            couponsContent, couponsNode, couponsNodeCol;

        couponsContent = node.querySelector('.bx-soa-section-content');
        if (!couponsContent)
        {
            couponsContent = this.getNewContainer();
            node.appendChild(couponsContent);
        }
        else
            BX.cleanNode(couponsContent);

        this.getErrorContainer(couponsContent);

        couponsNode = BX.create('DIV', {props: {className: 'bx_soa_location row'}});
        couponsNodeCol = BX.create('DIV', {props: {className: 'col-xs-12'}});

        // this.getPersonTypeControl(couponsNodeCol);
        //
        // this.getProfilesControl(couponsNodeCol);
        //
        // this.getDeliveryLocationInput(couponsNodeCol);
        //
        // if (!this.result.SHOW_AUTH)
        // {
        //     if (this.regionBlockNotEmpty)
        //     {
        //         BX.addClass(this.regionBlockNode, 'bx-active');
        //         this.regionBlockNode.style.display = '';
        //     }
        //     else
        //     {
        //         BX.removeClass(this.regionBlockNode, 'bx-active');
        //         this.regionBlockNode.style.display = 'none';
        //
        //         if (!this.result.IS_AUTHORIZED || typeof this.result.LAST_ORDER_DATA.FAIL !== 'undefined')
        //             this.initFirstSection();
        //     }
        // }

        this.editCoupons(couponsContent);
        // couponsNode.appendChild(couponsNodeCol);
        // couponsContent.appendChild(couponsNode);
        this.getBlockFooter(couponsContent);

        BX.removeClass(couponsContent, 'bx-soa-section-content--p0');
    };

    BX.Sale.OrderAjaxComponentExt.editFadeCouponsBlock = function()
    {
        console.log('editFadeCouponsBlock');
        // return;
        var couponsContent = this.couponsBlockNode.querySelector('.bx-soa-section-content'), newContent;


        this.couponsHiddenBlockNode.appendChild(couponsContent);

        newContent = this.getNewContainer(true);
        this.couponsBlockNode.appendChild(newContent);


        if(this.couponsBlockNode.getAttribute('data-visited')=='true')
        {
            this.editFadeCouponsContent(newContent);
        }
        else
        {
            this.couponsBlockNode.setAttribute('data-disabled', 'true');
        }
    };

    BX.Sale.OrderAjaxComponentExt.editFadeCouponsContent = function(node)
    {
        console.log('editFadeCouponsContent');

        if (!node)
            return;

        BX.cleanNode(node);

        var couponText = this.getCouponText(),
            addedHtml = '';

        if(couponText !== '')
        {
            addedHtml = '<div class="checkout-partbody"><p>' + couponText + '</p></div>'
        }
        else
        {
            BX.addClass(node, 'bx-soa-section-content--p0');
        }

        node.innerHTML += addedHtml;
    };

    BX.Sale.OrderAjaxComponentExt.editActiveEmailBlock = function(activeNodeMode)
    {
        console.log('editActiveEmailBlock');
        var node = activeNodeMode ? this.emailBlockNode : this.emailHiddenBlockNode,
            regionContent, regionNode, regionNodeCol,
            emailContent, couponsNode, couponsNodeCol;

        if (this.initialized.email)
        {
            BX.remove(BX.lastChild(node));
            node.appendChild(BX.firstChild(this.emailHiddenBlockNode));
        }
        else
        {
            emailContent = node.querySelector('.bx-soa-section-content');
            if (!emailContent)
            {
                emailContent = this.getNewContainer();
                node.appendChild(emailContent);
            }
            else
                BX.cleanNode(emailContent);

            this.getErrorContainer(emailContent);

            // couponsNode = BX.create('DIV', {props: {className: 'bx_soa_location row'}});
            // couponsNodeCol = BX.create('DIV', {props: {className: 'col-xs-12'}});

            this.editEmail(emailContent);
            // couponsNode.appendChild(couponsNodeCol);
            // couponsContent.appendChild(couponsNode);
            // this.getBlockFooter(emailContent);
            // BX.bind(this.emailBlockNode.querySelector('.bx-soa-customer-field'), 'click', BX.proxy(this.showByClick, this));
        }
    };

    BX.Sale.OrderAjaxComponentExt.editFadeEmailBlock = function()
    {
        console.log('editFadeEmailBlock');
        // return;
        var emailContent = this.emailBlockNode.querySelector('.bx-soa-section-content'), newContent;

        // BX.cleanNode(this.emailHiddenBlockNode);

        this.emailHiddenBlockNode.appendChild(emailContent);

        newContent = this.getNewContainer(true);
        this.emailBlockNode.appendChild(newContent);
        this.editFadeEmailContent(newContent);
    };

    BX.Sale.OrderAjaxComponentExt.editFadeEmailContent = function(node){
        console.log('editFadeEmailContent');

        if (!node)
            return;

        var errorNode = this.emailHiddenBlockNode.querySelector('.alert'),
            personType = this.getSelectedPersonType(),
            fadeParamName, props,
            group, property, propsIterator, i, validPropsErrors,
            groupIterator = this.propertyCollection.getGroupIterator();

        BX.cleanNode(node);

        if (errorNode)
            node.appendChild(errorNode.cloneNode(true));

        if (personType)
        {
            fadeParamName = 'PROPS_FADE_LIST_' + personType.ID;
            props = this.params[fadeParamName];
        }

        // if (!props || props.length === 0)
        // {
        //     node.innerHTML += '<strong>' + BX.message('SOA_ORDER_PROPS') + '</strong>';
        // }
        // else
        // {
        //     groupIterator = this.fadedPropertyCollection.getGroupIterator();
        //     // console.log(node);
        //     while (group = groupIterator())
        //     {
        //         propsIterator = group.getIterator();
        //         while (property = propsIterator())
        //         {
        //             for (i = 0; i < props.length; i++)
        //                 // console.log(props[i] + ' ' + property.getId());
        //                 // if (props[i] == property.getId() && (2 == property.getId() || 13 == property.getId())) {
        //                 if ((2 == property.getId() || 13 == property.getId())) {
        //                     this.getPropertyRowNodeEmail(property, node, true);
        //                 }
        //                 // if (props[i] == property.getId() && property.getSettings()['IS_ZIP'] != 'Y')
        //                 //     this.getPropertyRowNodeEmail(property, node, true);
        //         }
        //     }
        // }


        // var propsItemsContainer = this.emailBlockNode.querySelector('.checkout-user-data');

        // console.log(this.emailBlockNode);
        // console.log(propsItemsContainer);

        while (group = groupIterator())
        {
            propsIterator =  group.getIterator();
            while (property = propsIterator())
            {
                // console.log(property.getId());
                // нам нужно только поле мейла
                if (2 == property.getId() || 13 == property.getId()) {
                    this.getPropertyRowNodeEmail(property, node, true);
                }
            }
        }


        if (this.emailBlockNode.getAttribute('data-visited') === 'true')
        {
            validPropsErrors = this.isValidEmailBlock();
            if (validPropsErrors.length)
                this.showError(this.emailBlockNode, validPropsErrors);
        }

        BX.bind(node.querySelector('.alert.alert-danger'), 'click', BX.proxy(this.showByClick, this));
        BX.bind(node.querySelector('.alert.alert-warning'), 'click', BX.proxy(this.showByClick, this));
    };

    BX.Sale.OrderAjaxComponentExt.editActivePaySystemBlock = function(activeNodeMode)
    {
        var node = activeNodeMode ? this.paySystemBlockNode : this.paySystemHiddenBlockNode,
            paySystemContent, paySystemNode,
            acceptCard;

        if (this.initialized.paySystem)
        {
            BX.remove(BX.lastChild(node));
            node.appendChild(BX.firstChild(this.paySystemHiddenBlockNode));
        }
        else
        {
            paySystemContent = node.querySelector('.bx-soa-section-content');
            if (!paySystemContent)
            {
                paySystemContent = this.getNewContainer();
                node.appendChild(paySystemContent);
            }
            else
                BX.cleanNode(paySystemContent);

            this.getErrorContainer(paySystemContent);
            paySystemNode = BX.create('DIV', {props: {className: 'bx-soa-pp clearfix'}});

            console.log(this.result.LOCATION_COUNTRY);

            acceptCard = BX.create('DIV', {
                props: {
                    className: 'checkout-el-body'
                },
                children: [
                    BX.create('DIV', {
                        props: {
                            className: 'desc-pay'
                        },
                        html: this.params.MESS_WE_ACCEPT_PAYMENT + (this.result.LOCATION_COUNTRY == 'Россия' ? this.params.MESS_WE_ACCEPT_PAYMENT_BELCART : '')
                    })
                ]
            });

            this.editPaySystemItems(paySystemNode);

            paySystemContent.appendChild(acceptCard);
            paySystemContent.appendChild(paySystemNode);
            // this.editPaySystemInfo(paySystemNode);

            // if (this.params.SHOW_COUPONS_PAY_SYSTEM == 'Y')
            //     this.editCoupons(paySystemContent);

            // this.getBlockFooter(paySystemContent);
        }
    };

    BX.Sale.OrderAjaxComponentExt.editFadePaySystemBlock = function()
    {
        console.log('editFadePaySystemBlock');

        var paySystemContent = this.paySystemBlockNode.querySelector('.bx-soa-section-content'), newContent;

        if (this.initialized.paySystem)
        {
            this.paySystemHiddenBlockNode.appendChild(paySystemContent);
        }
        else
        {
            this.editActivePaySystemBlock(false);
            BX.remove(BX.lastChild(this.paySystemBlockNode));
        }

        newContent = this.getNewContainer(true);
        this.paySystemBlockNode.appendChild(newContent);


        if(this.paySystemBlockNode.getAttribute('data-visited')=='true')
        {
            this.editFadePaySystemContent(newContent);
        }
        else
        {
            // console.log(this.paySystemBlockNode);
            BX.addClass(newContent, 'bx-soa-section-content--p0');
            // if(!this.isEmailEntered)
            // {
            //     this.paySystemBlockNode.setAttribute('data-disabled', 'true');
            // }
            // else
            // {
            //     this.paySystemBlockNode.setAttribute('data-disabled', 'false');
            // }
            this.paySystemBlockNode.setAttribute('data-disabled', 'true');
        }


        if (this.params.SHOW_COUPONS_PAY_SYSTEM == 'Y')
            this.editCouponsFade(newContent);
    },


        BX.Sale.OrderAjaxComponentExt.editPaySystemItems = function(paySystemNode)
        {
            if (!this.result.PAY_SYSTEM || this.result.PAY_SYSTEM.length <= 0)
                return;

            var paySystemItemsContainer = BX.create('UL', {props: {className: 'bx-soa-pp-item-container variations-pay'}}),
                paySystemItemNode, i;

            for (i = 0; i < this.paySystemPagination.currentPage.length; i++)
            {
                paySystemItemNode = this.createPaySystemItem(this.paySystemPagination.currentPage[i]);
                paySystemItemsContainer.appendChild(paySystemItemNode);
            }

            if (this.paySystemPagination.show)
                this.showPagination('paySystem', paySystemItemsContainer);

            paySystemNode.appendChild(paySystemItemsContainer);
        };

    BX.Sale.OrderAjaxComponentExt.createPaySystemItem = function(item)
    {
        var checked = item.CHECKED == 'Y',
            logotype, logoNode,
            paySystemId = parseInt(item.ID),
            title, label, itemNode, descr;

        // logoNode = BX.create('DIV', {props: {className: 'bx-soa-pp-company-image'}});
        // logotype = this.getImageSources(item, 'PSA_LOGOTIP');
        //
        // if (logotype && logotype.src_2x)
        // {
        //     logoNode.setAttribute('style',
        //         'background-image: url(' + logotype.src_1x + ');' +
        //         'background-image: -webkit-image-set(url(' + logotype.src_1x + ') 1x, url(' + logotype.src_2x + ') 2x)'
        //     );
        // }
        // else
        // {
        //     logotype = logotype && logotype.src_1x || this.defaultPaySystemLogo;
        //     logoNode.setAttribute('style', 'background-image: url(' + logotype + ');');
        // }

        label = BX.create('DIV', {
            props: {className: 'vari-pay-container' + (checked ? ' selected' : '')},
            children: [
                BX.create('DIV', {
                    props: {
                        className: 'vari-pay-figure'
                    }
                }),
                BX.create('INPUT', {
                    props: {
                        id: 'ID_PAY_SYSTEM_ID_' + paySystemId,
                        name: 'PAY_SYSTEM_ID',
                        type: 'checkbox',
                        className: 'bx-soa-pp-company-checkbox',
                        value: paySystemId,
                        checked: checked
                    }
                }),
                // logoNode
            ]
        });

        if (this.params.SHOW_PAY_SYSTEM_LIST_NAMES == 'Y')
        {
            title = BX.create('H4', {text: item.NAME});

            label.appendChild(title);
        }

        descr = BX.create('P', {text: item.DESCRIPTION});
        label.appendChild(descr);


        itemNode = BX.create('LI', {
            props: {className: 'bx-soa-pp-company'},
            children: [label],
            // children: [label, title],
            events: {
                click: BX.proxy(this.selectPaySystem, this)
            }
        });

        if (checked)
            BX.addClass(itemNode, 'bx-selected');

        return itemNode;
    };

    BX.Sale.OrderAjaxComponentExt.editFadePaySystemContent = function(node)
    {
        console.log('editFadePaySystemContent');
        var selectedPaySystem = this.getSelectedPaySystem(),
            errorNode = this.paySystemHiddenBlockNode.querySelector('div.alert.alert-danger'),
            warningNode = this.paySystemHiddenBlockNode.querySelector('div.alert.alert-warning.alert-show'),
            addedHtml = '', logotype, imgSrc, innerNode,
            editIcon = BX.create('DIV', {
                props : {
                    className : 'checkout-change-data',
                },
                html : this.params.ICON_EDIT + this.params.MESS_EDIT,
                events: {click: BX.proxy(this.showByClick, this)},
            });

        if (errorNode)
            node.appendChild(errorNode.cloneNode(true));
        else
            this.getErrorContainer(node);

        if (warningNode && warningNode.innerHTML)
            node.appendChild(warningNode.cloneNode(true));

        if (this.isSelectedInnerPayment())
        {
            logotype = this.getImageSources(this.result.INNER_PAY_SYSTEM, 'LOGOTIP');
            imgSrc = logotype && logotype.src_1x || this.defaultPaySystemLogo;

            // addedHtml += '<div class="bx-soa-pp-company-selected">';
            // addedHtml += '<img src="' + imgSrc + '" style="height:18px;" alt="">';
            addedHtml += '<strong>' + this.result.INNER_PAY_SYSTEM.NAME + '</strong><br>';
            // addedHtml += '</div>';
        }

        if (selectedPaySystem && selectedPaySystem.NAME)
        {
            logotype = this.getImageSources(selectedPaySystem, 'PSA_LOGOTIP');
            imgSrc = logotype && logotype.src_1x || this.defaultPaySystemLogo;

            // addedHtml += '<div class="bx-soa-pp-company-selected">';
            // addedHtml += '<img src="' + imgSrc + '" style="height:18px;" alt="">';
            addedHtml += '<strong>' + BX.util.htmlspecialchars(selectedPaySystem.NAME) + '</strong>';
            // addedHtml += '</div>';
        }

        if (!addedHtml.length)
            addedHtml = '<strong>' + BX.message('SOA_PS_SELECT_ERROR') + '</strong>';

        innerNode = BX.create('DIV', {
            props: {className: 'checkout-user-data checkout-user-data--disabled'},
            html: addedHtml
        });

        innerNode.appendChild(editIcon);

        node.appendChild(innerNode);

        // node.innerHTML += addedHtml;

        node.appendChild(BX.create('DIV', {style: {clear: 'both'}}));
        BX.bind(node.querySelector('.alert.alert-danger'), 'click', BX.proxy(this.showByClick, this));
        BX.bind(node.querySelector('.alert.alert-warning'), 'click', BX.proxy(this.showByClick, this));
    }


    BX.Sale.OrderAjaxComponentExt.editActiveDeliveryBlock = function(activeNodeMode)
    {
        var node = activeNodeMode ? this.deliveryBlockNode : this.deliveryHiddenBlockNode,
            deliveryContent, deliveryNode;

        if (this.initialized.delivery)
        {
            BX.remove(BX.lastChild(node));
            node.appendChild(BX.firstChild(this.deliveryHiddenBlockNode));
        }
        else
        {
            deliveryContent = node.querySelector('.bx-soa-section-content');
            if (!deliveryContent)
            {
                deliveryContent = this.getNewContainer();
                node.appendChild(deliveryContent);
            }
            else
                BX.cleanNode(deliveryContent);

            this.getErrorContainer(deliveryContent);

            deliveryNode = BX.create('DIV', {props: {className: 'bx-soa-pp'}});
            this.editDeliveryItems(deliveryNode);
            deliveryContent.appendChild(deliveryNode);
            this.editDeliveryInfo(deliveryNode);

            // if (this.params.SHOW_COUPONS_DELIVERY == 'Y')
            //     this.editCoupons(deliveryContent);

            this.getBlockFooter(deliveryContent);

            $('#bx-soa-delivery').find('select').styler();
        }
    };

    BX.Sale.OrderAjaxComponentExt.editDeliveryItems = function(deliveryNode)
    {
        if (!this.result.DELIVERY || this.result.DELIVERY.length <= 0)
            return;

        var deliveryItemsContainer = BX.create('UL', {props: {className: 'bx-soa-pp-item-container list-checkout-delivery'}}),
            deliveryItemNode, k;

        for (k = 0; k < this.deliveryPagination.currentPage.length; k++)
        {
            deliveryItemNode = this.createDeliveryItem(this.deliveryPagination.currentPage[k]);
            deliveryItemsContainer.appendChild(deliveryItemNode);
        }

        if (this.deliveryPagination.show)
            this.showPagination('delivery', deliveryItemsContainer);

        deliveryNode.appendChild(deliveryItemsContainer);
    };

    BX.Sale.OrderAjaxComponentExt.createDeliveryItem = function(item)
    {
        var checked = item.CHECKED == 'Y',
            deliveryId = parseInt(item.ID),
            labelNodes = [
                BX.create('DIV', {
                    props: {
                        className: 'vari-deliv-figure'
                    }
                }),
                BX.create('INPUT', {
                    props: {
                        id: 'ID_DELIVERY_ID_' + deliveryId,
                        name: 'DELIVERY_ID',
                        type: 'checkbox',
                        className: 'bx-soa-pp-company-checkbox',
                        value: deliveryId,
                        checked: checked
                    }
                })
            ],
            deliveryCached = this.deliveryCachedInfo[deliveryId],
            logotype, label, title, itemNode, logoNode, descr, cost;

        logoNode = BX.create('DIV', {props: {className: 'bx-soa-pp-company-image'}});
        logotype = this.getImageSources(item, 'LOGOTIP');
        logotype = false;

        if (logotype && logotype.src_2x)
        {
            logoNode.setAttribute('style',
                'background-image: url(' + logotype.src_1x + ');' +
                'background-image: -webkit-image-set(url(' + logotype.src_1x + ') 1x, url(' + logotype.src_2x + ') 2x)'
            );
        }
        else
        {
            logotype = logotype && logotype.src_1x || this.defaultDeliveryLogo;
            logoNode.setAttribute('style', 'background-image: url(' + logotype + ');');
        }


        // labelNodes.push(logoNode);

        // if (item.PRICE >= 0 || typeof item.DELIVERY_DISCOUNT_PRICE !== 'undefined')
        // {
        //     labelNodes.push(
        //         BX.create('DIV', {
        //             props: {className: 'bx-soa-pp-delivery-cost'},
        //             html: typeof item.DELIVERY_DISCOUNT_PRICE !== 'undefined'
        //                 ? item.DELIVERY_DISCOUNT_PRICE_FORMATED
        //                 : item.PRICE_FORMATED})
        //     );
        // }
        // else if (deliveryCached && (deliveryCached.PRICE >= 0 || typeof deliveryCached.DELIVERY_DISCOUNT_PRICE !== 'undefined'))
        // {
        //     labelNodes.push(
        //         BX.create('DIV', {
        //             props: {className: 'bx-soa-pp-delivery-cost'},
        //             html: typeof deliveryCached.DELIVERY_DISCOUNT_PRICE !== 'undefined'
        //                 ? deliveryCached.DELIVERY_DISCOUNT_PRICE_FORMATED
        //                 : deliveryCached.PRICE_FORMATED})
        //     );
        // }

        // label = BX.create('DIV', {
        //     props: {
        //         className: 'bx-soa-pp-company-graf-container label_delivery_block'
        //             + (item.CALCULATE_ERRORS || deliveryCached && deliveryCached.CALCULATE_ERRORS ? ' bx-bd-waring' : '')},
        //     children: labelNodes
        // });

        // console.log(this.result.STORE_LIST);
        // console.log(item);
        // console.log(checked);
        // console.log(BX('stores-selects').html());

        if (this.params.SHOW_DELIVERY_LIST_NAMES == 'Y')
        {
            title = BX.create('H4', {
                text: this.params.SHOW_DELIVERY_PARENT_NAMES != 'N' ? item.NAME : item.OWN_NAME
            });

            labelNodes.push(title);


            // добавляем описание, если оно есть
            if(item.DESCRIPTION)
            {
                title = BX.create('DIV', {
                    html: item.DESCRIPTION,
                    props: {className: 'label_delivery__description'},
                });

                labelNodes.push(title);
            }
        }

        label = BX.create('DIV', {
            props: {className: 'label_delivery_block'},

            children: [
                BX.create('SPAN', {
                    props: {className: 'label_delivery' + (checked ? ' selected' : '')},
                    children: labelNodes
                }),
            ]
        });

        if (deliveryCached && (deliveryCached.PRICE >= 0 || typeof deliveryCached.DELIVERY_DISCOUNT_PRICE !== 'undefined'))
        {
            cost = typeof deliveryCached.DELIVERY_DISCOUNT_PRICE !== 'undefined'
                ? deliveryCached.DELIVERY_DISCOUNT_PRICE_FORMATED
                : deliveryCached.PRICE_FORMATED;
        }

        if (item.PRICE >= 0 || typeof item.DELIVERY_DISCOUNT_PRICE !== 'undefined')
        {
            cost = typeof item.DELIVERY_DISCOUNT_PRICE !== 'undefined'
                ? item.DELIVERY_DISCOUNT_PRICE_FORMATED
                : item.PRICE_FORMATED;
        }
        else if (deliveryCached && (deliveryCached.PRICE >= 0 || typeof deliveryCached.DELIVERY_DISCOUNT_PRICE !== 'undefined'))
        {
            cost = typeof deliveryCached.DELIVERY_DISCOUNT_PRICE !== 'undefined'
                ? deliveryCached.DELIVERY_DISCOUNT_PRICE_FORMATED
                : deliveryCached.PRICE_FORMATED;
        }

        if(item.ID == '32' && checked) {
            descr = BX.create('DIV', {
                props: {className: 'desc_delivery'},

                children: [
                    BX.create('DIV', {
                        props: {className: 'desc_delivery__stores'},
                        children: [
                            BX.create('DIV', {
                                html: this.result.CITIES_SELECT
                            }),
                            BX.create('DIV', {
                                html: this.result.STORES_SELECT
                            }),
                        ],
                    }),
                ]
            });

            // descr.appendChild(BX('stores-selects'));
        } else {
            if(item.ID == '14' && checked){
                descr = BX.create('DIV', {
                    props: {className: 'bx-soa-pp-list'},
                    children: [
                        BX.create('P', {
                            html: 
                            cost ? this.params.MESS_SOA_DELIVERY_COST + ' ' + cost : '',
                        }),
                        BX.create('DIV', {
                            props: {className: 'bx-soa-pp-company bx-soa-pp-list-description'},                        
                            html: item.PERIOD_TEXT,
                        })
                    ]
                });
            }else{
                descr = BX.create('DIV', {
                    children: [
                        BX.create('P', {
                            html: 
                            cost ? this.params.MESS_SOA_DELIVERY_COST + ' ' + cost : '',
                        }),
                        BX.create('DIV', {                      
                            html: item.PERIOD_TEXT,
                        })
                    ]
                });
            }
        }


        itemNode = BX.create('LI', {
            props: {className: 'bx-soa-pp-company'},
            // children: [label, title],
            children: [label, descr],
            events: {click: BX.proxy(this.selectDelivery, this)}
        });

        checked && BX.addClass(itemNode, 'bx-selected');

        if (checked && this.result.LAST_ORDER_DATA.PICK_UP)
            this.lastSelectedDelivery = deliveryId;

        return itemNode;
    };

    BX.Sale.OrderAjaxComponentExt.editDeliveryInfo = function(deliveryNode)
    {
        return;
        if (!this.result.DELIVERY)
            return;

        var deliveryInfoContainer = BX.create('DIV', {props: {className: 'col-sm-5 bx-soa-pp-desc-container'}}),
            currentDelivery, logotype, name, logoNode,
            subTitle, label, title, price, period,
            clear, infoList, extraServices, extraServicesNode;

        BX.cleanNode(deliveryInfoContainer);
        currentDelivery = this.getSelectedDelivery();

        logoNode = BX.create('DIV', {props: {className: 'bx-soa-pp-company-image'}});
        logotype = this.getImageSources(currentDelivery, 'LOGOTIP');
        if (logotype && logotype.src_2x)
        {
            logoNode.setAttribute('style',
                'background-image: url(' + logotype.src_1x + ');' +
                'background-image: -webkit-image-set(url(' + logotype.src_1x + ') 1x, url(' + logotype.src_2x + ') 2x)'
            );
        }
        else
        {
            logotype = logotype && logotype.src_1x || this.defaultDeliveryLogo;
            logoNode.setAttribute('style', 'background-image: url(' + logotype + ');');
        }

        name = this.params.SHOW_DELIVERY_PARENT_NAMES != 'N' ? currentDelivery.NAME : currentDelivery.OWN_NAME;

        if (this.params.SHOW_DELIVERY_INFO_NAME == 'Y')
            subTitle = BX.create('DIV', {props: {className: 'bx-soa-pp-company-subTitle'}, text: name});

        label = BX.create('DIV', {
            props: {className: 'bx-soa-pp-company-logo'},
            children: [
                BX.create('DIV', {
                    props: {className: 'bx-soa-pp-company-graf-container'},
                    children: [logoNode]
                })
            ]
        });
        title = BX.create('DIV', {
            props: {className: 'bx-soa-pp-company-block'},
            children: [
                BX.create('DIV', {props: {className: 'bx-soa-pp-company-desc'}, html: currentDelivery.DESCRIPTION}),
                currentDelivery.CALCULATE_DESCRIPTION
                    ? BX.create('DIV', {props: {className: 'bx-soa-pp-company-desc'}, html: currentDelivery.CALCULATE_DESCRIPTION})
                    : null
            ]
        });

        if (currentDelivery.PRICE >= 0)
        {
            price = BX.create('LI', {
                children: [
                    BX.create('DIV', {
                        props: {className: 'bx-soa-pp-list-termin'},
                        html: this.params.MESS_PRICE + ':'
                    }),
                    BX.create('DIV', {
                        props: {className: 'bx-soa-pp-list-description'},
                        children: this.getDeliveryPriceNodes(currentDelivery)
                    })
                ]
            });
        }

        if (currentDelivery.PERIOD_TEXT && currentDelivery.PERIOD_TEXT.length)
        {
            period = BX.create('LI', {
                children: [
                    BX.create('DIV', {props: {className: 'bx-soa-pp-list-termin'}, html: this.params.MESS_PERIOD + ':'}),
                    BX.create('DIV', {props: {className: 'bx-soa-pp-list-description'}, html: currentDelivery.PERIOD_TEXT})
                ]
            });
        }

        clear = BX.create('DIV', {style: {clear: 'both'}});
        infoList = BX.create('UL', {props: {className: 'bx-soa-pp-list'}, children: [price, period]});
        extraServices = this.getDeliveryExtraServices(currentDelivery);

        if (extraServices.length)
        {
            extraServicesNode = BX.create('DIV', {
                props: {className: 'bx-soa-pp-company-block'},
                children: extraServices
            });
        }

        deliveryInfoContainer.appendChild(
            BX.create('DIV', {
                props: {className: 'bx-soa-pp-company'},
                children: [subTitle, label, title, clear, extraServicesNode, infoList]
            })
        );
        deliveryNode.appendChild(deliveryInfoContainer);

        if (this.params.DELIVERY_NO_AJAX != 'Y')
            this.deliveryCachedInfo[currentDelivery.ID] = currentDelivery;
    };

    BX.Sale.OrderAjaxComponentExt.editFadeDeliveryBlock = function()
    {
        console.log('editFadeDeliveryBlock');

        var deliveryContent = this.deliveryBlockNode.querySelector('.bx-soa-section-content'), newContent;

        if (this.initialized.delivery)
        {
            this.deliveryHiddenBlockNode.appendChild(deliveryContent);
        }
        else
        {
            this.editActiveDeliveryBlock(false);
            BX.remove(BX.lastChild(this.deliveryBlockNode));
        }

        newContent = this.getNewContainer(true);
        this.deliveryBlockNode.appendChild(newContent);

        if(this.deliveryBlockNode.getAttribute('data-visited')=='true')
        {
            this.editFadeDeliveryContent(newContent);
        }
        else
        {
            BX.addClass(newContent, 'bx-soa-section-content--p0');
            // if(!this.isEmailEntered)
            // {
            //     this.deliveryBlockNode.setAttribute('data-disabled', 'true');
            // }
            // else
            // {
            //     this.deliveryBlockNode.setAttribute('data-disabled', 'false');
            // }
            this.deliveryBlockNode.setAttribute('data-disabled', 'true');
            console.log("this.deliveryBlockNode.setAttribute('data-disabled', 'true');")
        }
    };

    BX.Sale.OrderAjaxComponentExt.editFadeDeliveryContent = function(node)
    {
        console.log('editFadeDeliveryContent');
        var selectedDelivery = this.getSelectedDelivery(),
            name = this.params.SHOW_DELIVERY_PARENT_NAMES != 'N' ? selectedDelivery.NAME : selectedDelivery.OWN_NAME,
            errorNode = this.deliveryHiddenBlockNode.querySelector('div.alert.alert-danger'),
            warningNode = this.deliveryHiddenBlockNode.querySelector('div.alert.alert-warning.alert-show'),
            extraService, logotype, imgSrc, arNodes, i,
            editIcon = BX.create('DIV', {
                props : {
                    className : 'checkout-change-data',
                },
                html : this.params.ICON_EDIT + this.params.MESS_EDIT,
                events: {click: BX.proxy(this.showByClick, this)},
            });


        if (errorNode && errorNode.innerHTML)
            node.appendChild(errorNode.cloneNode(true));
        else
            this.getErrorContainer(node);

        if (warningNode && warningNode.innerHTML)
            node.appendChild(warningNode.cloneNode(true));

        if (selectedDelivery && selectedDelivery.NAME)
        {
            // выбран самовывоз
            if(selectedDelivery.ID == '32')
            {
                // name += ': ' + pickupCity + ', ' + pickupStore;
                name += ': ';
            }


            logotype = this.getImageSources(selectedDelivery, 'LOGOTIP');
            imgSrc = logotype && logotype.src_1x || this.defaultDeliveryLogo;
            arNodes = [
                // BX.create('IMG', {props: {src: imgSrc, alt: ''}, style: {height: '18px'}}),
                BX.create('STRONG', {text: name + ' - '})
            ];

            if (this.params.DELIVERY_FADE_EXTRA_SERVICES == 'Y' && BX.util.object_keys(selectedDelivery.EXTRA_SERVICES).length)
            {
                arNodes.push(BX.create('BR'));

                for (i in selectedDelivery.EXTRA_SERVICES)
                {
                    if (selectedDelivery.EXTRA_SERVICES.hasOwnProperty(i))
                    {
                        extraService = selectedDelivery.EXTRA_SERVICES[i];
                        if (extraService.value && extraService.value != 'N' && extraService.canUserEditValue)
                        {
                            arNodes.push(BX.create('BR'));
                            arNodes.push(BX.create('STRONG', {text: extraService.name + ': '}));
                            arNodes.push(extraService.viewControl);
                        }
                    }
                }
            }

            // выбран самовывоз
            if(selectedDelivery.ID == '32')
            {
                if(storeCityValue !== '' && storeItemValue !== '')
                {
                    $('select.js-store-city').val(storeCityValue).trigger('refresh');
                    $('select.js-store-items').val(storeItemValue).trigger('refresh');
                }

                var pickupCity = $('select.js-store-city').find('option:selected').text(),
                    pickupStore = $('select.js-store-items').find('option:selected').text();

                arNodes.push(BX.create('SPAN', {text: (pickupCity + ', ' + pickupStore)}));
                arNodes.push(editIcon);

                node.appendChild(
                    BX.create('DIV', {
                        props: {className: 'checkout-user-data checkout-user-data--disabled1'},
                        children: arNodes,
                    })
                );

            }
            else
            {
                console.log(selectedDelivery);
                // arNodes.push(BX.create('SPAN', {text: (' - ' + this.getDeliveryPriceNodes(selectedDelivery))}));
                arNodes.push(BX.create('SPAN', {children: this.getDeliveryPriceNodes(selectedDelivery)}));
                arNodes.push(editIcon);

                console.log(arNodes);

                node.appendChild(
                    BX.create('DIV', {
                        props: {className: 'checkout-user-data checkout-user-data--disabled1'},
                        children: arNodes,
                    })
                );
            }


            // if(selectedDelivery.ID != '7')
            // {
            //     node.appendChild(
            //         BX.create('DIV', {
            //             props: {className: 'col-sm-3 bx-soa-pp-price'},
            //             children: this.getDeliveryPriceNodes(selectedDelivery)
            //         })
            //     );
            // }
        }
        else
            node.appendChild(BX.create('STRONG', {text: BX.message('SOA_DELIVERY_SELECT_ERROR')}));

        // node.appendChild(BX.create('DIV', {style: {clear: 'both'}}));
        BX.bind(node.querySelector('.alert.alert-danger'), 'click', BX.proxy(this.showByClick, this));
        BX.bind(node.querySelector('.alert.alert-warning'), 'click', BX.proxy(this.showByClick, this));
    }

    BX.Sale.OrderAjaxComponentExt.getDeliveryPriceNodes = function(delivery)
    {
        var priceNodesArray;

        if (typeof delivery.DELIVERY_DISCOUNT_PRICE !== 'undefined'
            && parseFloat(delivery.DELIVERY_DISCOUNT_PRICE) != parseFloat(delivery.PRICE))
        {
            if (parseFloat(delivery.DELIVERY_DISCOUNT_PRICE) > parseFloat(delivery.PRICE))
                priceNodesArray = [delivery.DELIVERY_DISCOUNT_PRICE_FORMATED];
            else
                priceNodesArray = [
                    delivery.DELIVERY_DISCOUNT_PRICE_FORMATED,
                    // BX.create('BR'),
                    // BX.create('SPAN', {props: {className: 'bx-price-old'}, html: delivery.PRICE_FORMATED})
                ];
        }
        else
        {
            priceNodesArray = [delivery.PRICE_FORMATED];
        }

        return priceNodesArray;
    }


    BX.Sale.OrderAjaxComponentExt.getDeliveryLocationInput = function(node)
    {
        var currentProperty, locationId, altId, location, k, altProperty,
            labelHtml, currentLocation, insertedLoc,
            labelTextHtml, label, input, altNode;

        for (k in this.result.ORDER_PROP.properties)
        {
            if (this.result.ORDER_PROP.properties.hasOwnProperty(k))
            {
                currentProperty = this.result.ORDER_PROP.properties[k];
                if (currentProperty.IS_LOCATION == 'Y')
                {
                    locationId = currentProperty.ID;
                    altId = parseInt(currentProperty.INPUT_FIELD_LOCATION);
                    break;
                }
            }
        }

        location = this.locations[locationId];
        if (location && location[0] && location[0].output)
        {
            this.regionBlockNotEmpty = true;

            if(currentProperty.REQUIRED == 'Y')
            {
                labelHtml = '<label class="bx-soa-custom-label" for="soa-property-' + parseInt(locationId) + '">'
                    + '<span class="bx-authform-starrequired mf-form-required">'
                    + BX.util.htmlspecialchars(currentProperty.NAME)
                    + '<sup>*</sup>'
                    + '</span>'
                    + '</label>';
            }
            else
            {
                labelHtml = '<label class="bx-soa-custom-label" for="soa-property-' + parseInt(locationId) + '">'
                    + '<span>'
                    + BX.util.htmlspecialchars(currentProperty.NAME)
                    + '<sup>*</sup>'
                    + '</span>'
                    + '</label>';
            }

            currentLocation = location[0].output;
            insertedLoc = BX.create('DIV', {
                attrs: {'data-property-id-row': locationId},
                props: {className: 'form-group bx-soa-location-input-container'},
                style: {visibility: 'hidden'},
                html:  labelHtml + currentLocation.HTML
            });
            node.appendChild(insertedLoc);
            node.appendChild(BX.create('INPUT', {
                props: {
                    type: 'hidden',
                    name: 'RECENT_DELIVERY_VALUE',
                    value: location[0].lastValue
                }
            }));

            for (k in currentLocation.SCRIPT)
                if (currentLocation.SCRIPT.hasOwnProperty(k))
                    BX.evalGlobal(currentLocation.SCRIPT[k].JS);
        }

        if (location && location[0] && location[0].showAlt && altId > 0)
        {
            for (k in this.result.ORDER_PROP.properties)
            {
                if (parseInt(this.result.ORDER_PROP.properties[k].ID) == altId)
                {
                    altProperty = this.result.ORDER_PROP.properties[k];
                    break;
                }
            }
        }

        if (altProperty)
        {
            altNode = BX.create('DIV', {
                attrs: {'data-property-id-row': altProperty.ID},
                props: {className: "form-group bx-soa-location-input-container"}
            });

            labelTextHtml = altProperty.REQUIRED == 'Y' ? '<span class="bx-authform-starrequired">*</span> ' : '';
            labelTextHtml += BX.util.htmlspecialchars(altProperty.NAME);

            label = BX.create('LABEL', {
                attrs: {for: 'altProperty'},
                props: {className: 'bx-soa-custom-label'},
                html: labelTextHtml
            });

            input = BX.create('INPUT', {
                props: {
                    id: 'altProperty',
                    type: 'text',
                    placeholder: altProperty.DESCRIPTION,
                    autocomplete: 'city',
                    className: 'form-control bx-soa-customer-input bx-ios-fix',
                    name: 'ORDER_PROP_' + altProperty.ID,
                    value: altProperty.VALUE
                }
            });

            altNode.appendChild(label);
            altNode.appendChild(input);
            node.appendChild(altNode);

            this.bindValidation(altProperty.ID, altNode);
        }

        this.getZipLocationInput(node);

        if (location && location[0])
        {
            node.appendChild(
                BX.create('DIV', {
                    props: {className: 'bx-soa-reference'},
                    html: this.params.MESS_REGION_REFERENCE
                })
            );
        }
    }


    BX.Sale.OrderAjaxComponentExt.editCouponsBlock = function(active)
    {
        console.log('editCouponsBlock');
        if (!this.couponsBlockNode || !this.couponsHiddenBlockNode)
            return;

        if (active)
            this.editActiveCouponsBlock(true);
        else
            this.editFadeCouponsBlock();

        // this.editActiveCouponsBlock(true);
    };

    BX.Sale.OrderAjaxComponentExt.editCoupons = function(basketItemsNode)
    {
        console.log('editCoupons');
        var couponsList = this.getCouponsList(true),
            couponsLabel = this.getCouponsLabel(true),
            // discountLabel = '<div>Скидка <b>10%</b> на весь заказ по промокоду</div>',
            discountLabel = 'Скидка <b>10%</b> на весь заказ по промокоду',

            couponsBlock = BX.create('DIV', {
                props: {className: 'bx-soa-coupon-block'},
                children: [
                    BX.create('DIV', {
                        props: {className: 'bx-soa-coupon-input'},
                        children: [
                            BX.create('INPUT', {
                                props: {
                                    className: 'form-control bx-ios-fix',
                                    type: 'text'
                                },
                                events: {
                                    change: BX.delegate(function(event){
                                        var newCoupon = BX.getEventTarget(event);
                                        if (newCoupon && newCoupon.value)
                                        {
                                            this.sendRequest('enterCoupon', newCoupon.value);
                                            newCoupon.value = '';
                                        }
                                    }, this)
                                }
                            }),
                        ]
                    }),
                    BX.create('SPAN', {props: {className: 'bx-soa-coupon-item'}, children: couponsList})
                ]
            });

        couponsBlock = BX.create('LABEL', {
            props: {className: 'mf-form-label mf-form-label--coupon', style: 'font-size: 12pt; color: rgb(51, 51, 51);'},
            children: [
                BX.create('SPAN', {
                    html: this.params.MESS_USE_COUPON_LABEL
                }),
                BX.create('INPUT', {
                    props: {
                        id: 'coupon-control',
                        className: 'form-control bx-ios-fix',
                        type: 'text'
                    },
                    // events: {
                    //     change: BX.delegate(function(event){
                    //         var newCoupon = BX.getEventTarget(event);
                    //         if (newCoupon && newCoupon.value)
                    //         {
                    //             this.sendRequest('enterCoupon', newCoupon.value);
                    //             newCoupon.value = '';
                    //         }
                    //     }, this)
                    // }
                }),
                BX.create('SPAN', {
                    text: 'Применить',
                    props: {
                        className: 'coupon-btn-txt',
                    },
                    events: {
                        click: BX.delegate(function(event){
                            var newCoupon = BX('coupon-control');
                            if (newCoupon && newCoupon.value)
                            {
                                this.sendRequest('enterCoupon', newCoupon.value);
                                newCoupon.value = '';
                            }
                        }, this)
                    }
                }),
                BX.create('SPAN', {
                    text: '(после ввода промокода нажмите "Применить")',
                    props: {
                        className: 'coupon-btn-note',
                    },
                }),
            ]
        });
        /**/


        basketItemsNode.appendChild(
            BX.create('DIV', {
                props: {className: 'checkout-partbody'},
                children: [
                    BX.create('DIV', {
                        props: {className: 'bx-soa-coupon'},
                        children: [
                            couponsLabel,
                            couponsBlock,
                            BX.create('P', {
                                html: this.getCouponText()
                                // html: this.params.MESS_SOA_COUPON_INFO
                            }),
                            BX.create('SPAN', {props: {className: 'bx-soa-coupon-item'}, children: couponsList})
                        ]
                    }),
                ]
            })
        );

        basketItemsNode.appendChild(
            BX.create('DIV', {
                props: {className: 'checkout-partbody checkout-partbody-desc'},
                // html: this.params.MESS_CHECKOUT_DESC
                html: this.getCouponDesc()
            })
        );
    };

    BX.Sale.OrderAjaxComponentExt.getCouponText = function()
    {
        var result = '';

        if(this.result.COUPON_LIST.length) {
            var coupon = this.result.COUPON_LIST[0],
                value = '';

            if(typeof coupon.COUPON_INFO !== 'undefined')
            {
                switch (coupon.COUPON_INFO.VALUE_TYPE) {
                    case 'P':
                        value = coupon.COUPON_INFO.VALUE + '%';
                        break;

                    case 'F':
                    case 'S':
                        value = BX.Currency.currencyFormat(coupon.COUPON_INFO.VALUE, 'BYN', true);
                        break;
                }

                if(value !== '') {
                    result = this.params.MESS_SOA_COUPON_INFO.replace('{{VALUE}}', value);
                }
            }
        }

        return result;
    };

    BX.Sale.OrderAjaxComponentExt.getCouponDesc = function()
    {
        var result = '';

        if(this.result.COUPON_LIST.length) {
            var coupon = this.result.COUPON_LIST[0],
                value = '';

            if(typeof coupon.COUPON_INFO !== 'undefined' && typeof coupon.COUPON_INFO.TEXT !== 'undefined')
            {
                result = coupon.COUPON_INFO.TEXT;
            }
        }

        return result;
    };


    BX.Sale.OrderAjaxComponentExt.editCouponsFade = function(basketItemsNode)
    {
        console.log('editCouponsFade');
        if (this.result.COUPON_LIST.length < 1)
            return;

        var couponsList = this.getCouponsList(false),
            couponsLabel, couponsBlock;

        if (couponsList.length)
        {
            couponsLabel = this.getCouponsLabel(false);
            couponsBlock = BX.create('DIV', {
                props: {className: 'bx-soa-coupon-block'},
                children: [
                    BX.create('DIV', {
                        props: {className: 'bx-soa-coupon-list'},
                        children: [
                            BX.create('DIV', {
                                props: {className: 'bx-soa-coupon-item'},
                                children: [couponsLabel].concat(couponsList)
                            })
                        ]
                    })
                ]
            });

            basketItemsNode.appendChild(
                BX.create('DIV', {
                    props: {className: 'bx-soa-coupon bx-soa-coupon-item-fixed'},
                    children: [couponsBlock]
                })
            );
        }
    };

    BX.Sale.OrderAjaxComponentExt.getCouponsLabel = function(active)
    {
        return BX.create('DIV', {
            props: {className: 'h4'},
            html: this.params.MESS_USE_COUPON
        });
    };

    BX.Sale.OrderAjaxComponentExt.editEmailBlock = function(active)
    {
        console.log('editEmailBlock');
        // console.log(active);
        // console.log(this.isEmailEntered);

        if (!this.emailBlockNode || !this.emailHiddenBlockNode)
            return;

        // if(!this.isEmailEntered)
        // {
        //     this.editActiveEmailBlock(true);
        // }
        // else
        // {
        //     if (active)
        //         this.editActiveEmailBlock(true);
        //     else
        //         this.editFadeEmailBlock();
        // }

        // this.initialized.email = true;

        if (active)
            this.editActiveEmailBlock(true);
        else
            this.editFadeEmailBlock();


    };

    BX.Sale.OrderAjaxComponentExt.editEmail = function(propsNode)
    {
        console.log('editEmail');

        if (!this.result.ORDER_PROP || !this.propertyCollection)
            return;

        // var propsItemsContainer = BX.create('DIV', {props: {className: 'bx-soa-customer'}}),
        var propsItemsContainer = BX.create('DIV', {props: {className: 'checkout-user-data mf-form-body'}}),
            group, property, groupIterator = this.propertyCollection.getGroupIterator(), propsIterator,
            btnContainer = BX.create('DIV', {
                props: {
                    className: 'mf-btn-wr',
                },
                children: [
                    BX.create('A', {
                        props: {
                            className: 'button-link black-tr-b js-bnt-email-add',
                            href: 'javascript:void(0)',
                        },
                        html: this.params.MESS_FURTHER,
                        events: {click: BX.proxy(this.clickNextActionEmail, this)},
                    })
                ],
            });

        if (!propsItemsContainer)
            propsItemsContainer = this.propsBlockNode.querySelector('.bx-soa-customer');

        while (group = groupIterator())
        {
            propsIterator =  group.getIterator();
            while (property = propsIterator())
            {
                // нам нужно только поле мейла
                if (2 == property.getId() || 13 == property.getId()) {
                    this.getPropertyRowNodeEmail(property, propsItemsContainer, false);
                }
            }
        }
        propsItemsContainer.appendChild(btnContainer);
        propsNode.appendChild(propsItemsContainer);
        // propsNode.appendChild(btnContainer);
    };

    BX.Sale.OrderAjaxComponentExt.getLocationString = function(node)
    {
        if (!node)
            return '';

        var locationInputNode = node.querySelector('.bx-ui-sls-route'),
            locationString = '',
            locationSteps, i, altLoc;

        if (locationInputNode && locationInputNode.value && locationInputNode.value.length)
            locationString = locationInputNode.value;
        else
        {
            locationSteps = node.querySelectorAll('.bx-ui-combobox-fake.bx-combobox-fake-as-input');
            for (i = locationSteps.length; i--;)
            {
                if (locationSteps[i].innerHTML.indexOf('...') >= 0)
                    continue;

                if (locationSteps[i].innerHTML.indexOf('---') >= 0)
                {
                    altLoc = BX('altProperty');
                    if (altLoc && altLoc.value.length)
                        locationString += altLoc.value;

                    continue;
                }

                if (locationString.length)
                    locationString += ', ';

                if(i === 0) {
                    var blockClasses = 'country';

                    switch (locationSteps[i].innerHTML)
                    {
                        case 'Беларусь':
                            blockClasses += ' country--by';
                            break;

                        case 'Россия':
                            blockClasses += ' country--ru';
                            break;

                        case 'Казахстан':
                            blockClasses += ' country--kz';
                            break;
                    }
                    $(locationSteps[i]).closest('.bx-ui-slst-input-block').addClass(blockClasses);
                }

                locationString += locationSteps[i].innerHTML;
            }

            if (locationString.length == 0)
                locationString = BX.message('SOA_NOT_SPECIFIED');
        }

        // console.log(locationString);

        return locationString;
    };

    /**
     * Edit certain block node
     */
    BX.Sale.OrderAjaxComponentExt.editSection = function(section)
    {
        console.log('editSection');

        if (!section || !section.id)
            return;

        if (this.result.SHOW_AUTH && section.id != this.authBlockNode.id && section.id != this.basketBlockNode.id)
            section.style.display = 'none';
        else if (section.id != this.pickUpBlockNode.id)
            section.style.display = '';

        var active = section.id == this.activeSectionId,
            titleNode = section.querySelector('.bx-soa-section-title-container'),
            editButton, errorContainer;

        BX.unbindAll(titleNode);
        if (this.result.SHOW_AUTH)
        {
            BX.bind(titleNode, 'click', BX.delegate(function(){
                this.animateScrollTo(this.authBlockNode);
                this.addAnimationEffect(this.authBlockNode, 'bx-step-good');
            }, this));
        }
        else
        {
            BX.bind(titleNode, 'click', BX.proxy(this.showByClick, this));
            editButton = titleNode.querySelector('.bx-soa-editstep');
            editButton && BX.bind(editButton, 'click', BX.proxy(this.showByClick, this));
        }

        errorContainer = section.querySelector('.alert.alert-danger');
        this.hasErrorSection[section.id] = errorContainer && errorContainer.style.display != 'none';

        switch (section.id)
        {
            case this.authBlockNode.id:
                this.editAuthBlock();
                break;
            case this.basketBlockNode.id:
                this.editBasketBlock(active);
                break;
            case this.regionBlockNode.id:
                this.editRegionBlock(active);
                break;
            case this.couponsBlockNode.id:
                this.editCouponsBlock(active);
                break;
            // case this.emailBlockNode.id:
            //     this.editEmailBlock(active);
            //     break;
            case this.paySystemBlockNode.id:
                this.editPaySystemBlock(active);
                break;
            case this.deliveryBlockNode.id:
                this.editDeliveryBlock(active);
                break;
            case this.pickUpBlockNode.id:
                this.editPickUpBlock(active);
                break;
            case this.propsBlockNode.id:
                this.editPropsBlock(active);
                break;
        }

        if (active)
            section.setAttribute('data-visited', 'true');
    };

    BX.Sale.OrderAjaxComponentExt.getNewContainer = function(notFluid)
    {
        return BX.create('DIV', {props: {className: 'bx-soa-section-content checkout-el-body'}});
    };




    /**
     * Returns footer node with navigation buttons
     */
    BX.Sale.OrderAjaxComponentExt.getBlockFooter = function(node)
    {
        var sections = this.orderBlockNode.querySelectorAll('.bx-soa-section.bx-active'),
            firstSection = sections[0],
            lastSection = sections[sections.length - 1],
            currentSection = BX.findParent(node, {className: "bx-soa-section"}),
            isLastNode = false,
            buttons = [];

        // if (currentSection && currentSection.id.indexOf(firstSection.id) == '-1')
        // {
        //     buttons.push(
        //         BX.create('A', {
        //             props: {
        //                 href: 'javascript:void(0)',
        //                 // className: 'pull-left btn btn-default btn-md'
        //                 className: 'pull-left button-link black-tr-b'
        //             },
        //             html: this.params.MESS_BACK,
        //             events: {
        //                 click: BX.proxy(this.clickPrevAction, this)
        //             }
        //         })
        //     );
        // }

        if (currentSection && currentSection.id.indexOf(lastSection.id) != '-1')
            isLastNode = true;

        if (!isLastNode)
        {
            buttons.push(
                BX.create('A', {
                    // props: {href: 'javascript:void(0)', className: 'pull-right btn btn-default btn-md'},
                    props: {href: 'javascript:void(0)', className: 'pull-right button-link black-tr-b'},
                    html: this.params.MESS_FURTHER,
                    events: {click: BX.proxy(this.clickNextAction, this)}
                })
            );
        }

        node.appendChild(
            BX.create('DIV', {
                props: {className: 'bx-soa-more'},
                children: [
                    BX.create('DIV', {
                        props: {className: 'bx-soa-more-btn clearfix'},
                        children: buttons
                    })
                ]
            })
        );
    };

    BX.Sale.OrderAjaxComponentExt.getPickUpInfoArray = function(storeIds)
    {
        if (!storeIds || storeIds.length <= 0)
            return [];

        var arr = [], i;

        // for (i = 0; i < storeIds.length; i++)
        //     if (this.result.STORE_LIST[storeIds[i]])
        //         arr.push(this.result.STORE_LIST[storeIds[i]]);

        return arr;
    }

    BX.Sale.OrderAjaxComponentExt.getErrorContainer = function(node)
    {
        if (!node)
            return;

        node.appendChild(
            BX.create('DIV', {
                props: {
                    className: 'alert alert-danger checkout-user-info'
                },
                style: {
                    display: 'none'
                },
            })
        );
    }

    BX.Sale.OrderAjaxComponentExt.showError = function(node, msg, border)
    {
        if (BX.type.isArray(msg))
            msg = msg.join('<br>');

        var errorContainer = node.querySelector('.alert.alert-danger'), animate;
        if (errorContainer && msg.length)
        {
            BX.cleanNode(errorContainer);
            errorContainer.appendChild(BX.create('SPAN', {
                props : {
                    className : 'icon-warning-cnt'
                },
                html: this.params.ICON_WARNING
            }));
            errorContainer.appendChild(BX.create('P', {html: msg}));

            animate = !this.hasErrorSection[node.id];
            if (animate)
            {
                errorContainer.style.opacity = 0;
                errorContainer.style.display = '';
                new BX.easing({
                    duration: 300,
                    start: {opacity: 0},
                    finish: {opacity: 100},
                    transition: BX.easing.makeEaseOut(BX.easing.transitions.quad),
                    step: function(state){
                        errorContainer.style.opacity = state.opacity / 100;
                    },
                    complete: function(){
                        errorContainer.removeAttribute('style');
                    }
                }).animate();
            }
            else
                errorContainer.style.display = '';

            if (!!border)
                BX.addClass(node, 'bx-step-error');
        }
    }

    BX.Sale.OrderAjaxComponentExt.showBlockWarning = function(node, warnings, hide)
    {
        console.log('showBlockWarning');

        var errorNode = node.querySelector('.alert.alert-danger'),
            warnStr = '',
            i, warningNode, existedWarningNodes;

        if (errorNode)
        {
            if (BX.type.isString(warnings))
            {
                warnStr = warnings;
            }
            else
            {
                for (i in warnings)
                {
                    if (warnings.hasOwnProperty(i) && warnings[i])
                    {
                        warnStr += warnings[i] + '<br>';
                    }
                }
            }

            if (!warnStr)
            {
                return;
            }

            existedWarningNodes = node.querySelectorAll('.alert.alert-warning');
            for (i in existedWarningNodes)
            {
                if (existedWarningNodes.hasOwnProperty(i) && BX.type.isDomNode(existedWarningNodes[i]))
                {
                    if (existedWarningNodes[i].innerHTML.indexOf(warnStr) !== -1)
                    {
                        return;
                    }
                }
            }

            // errorContainer.appendChild(BX.create('SPAN', {
            //     props : {
            //         className : 'icon-warning-cnt'
            //     },
            //     html: this.params.ICON_WARNING
            // }));


            // warningNode = BX.create('DIV', {
            //     props: {className: 'alert alert-warning' + (!!hide ? ' alert-hide' : ' alert-show')},
            //     html: warnStr
            // });

            warningNode = BX.create('DIV', {
                props: {className: 'alert alert-warning checkout-user-info'},
                // html: warnStr,
                children: [
                    BX.create('SPAN', {
                        props : {
                            className : 'icon-warning-cnt'
                        },
                        html: this.params.ICON_WARNING
                    }),
                    BX.create('P', {
                        html: warnStr
                    }),
                ]
            });

            BX.prepend(warningNode, errorNode.parentNode);
            BX.addClass(node, 'bx-step-warning');
        }
    }


    BX.Sale.OrderAjaxComponentExt.editTotalBlock = function()
    {
        console.log('editTotalBlock');

        /*
                <div class="mf-form-container">
                    <div class="mf-form-bck js-mf-form-bck-total"></div>
                    <div class="mf-form js-mf-form-total">
                        <div class="mf-form-body"></div>
                        <div class="mf-form-footer">
                            <span class="mf-submit-el">
                                <input type="submit" id="mf-submit-cart" class="button-link black-tr-b" value="Оформить заказ" />
                            </span>
                        </div>

                    </div>
                </div>

        */
        if (!this.totalInfoBlockNode || !this.result.TOTAL)
            return;



        var total = this.result.TOTAL,
            priceHtml, params = {},
            discText, valFormatted, i,
            curDelivery, deliveryError, deliveryValue,
            showOrderButton = this.params.SHOW_TOTAL_ORDER_BUTTON === 'Y',
            basketItem, basketItems,
            formContainer, formBck, formCnt, formBody, formFooter;



        formContainer = BX.create('DIV', {props: {className: 'mf-form-container'}});
        formBck = BX.create('DIV', {props: {className: 'mf-form-bck js-mf-form-bck-total'}});
        formCnt = BX.create('DIV', {props: {className: 'mf-form js-mf-form-total'}});
        formBody = BX.create('DIV', {props: {className: 'mf-form-body'}});
        formFooter = BX.create('DIV', {props: {className: 'mf-form-footer', id: 'bx-soa-orderSave-sb'}});


        formCnt.appendChild(formBody);
        formCnt.appendChild(formFooter);
        formContainer.appendChild(formBck);
        formContainer.appendChild(formCnt);


        BX.cleanNode(this.totalInfoBlockNode);

        basketItems = BX.create('DIV', {
            props: {className: 'basket-items-mini'}
        });


        for (i in this.result.GRID.ROWS)
        {
            if (this.result.GRID.ROWS.hasOwnProperty(i))
            {
                // console.log(this.result.GRID.ROWS[i].data);
                basketItem = BX.create('DIV', {
                    props: {
                        className: 'basket-item-mini'
                    },
                    children: [
                        BX.create('SPAN', {
                            props: {
                                className: 'basket-item-mini__cell basket-item-mini__name'
                            },
                            text: this.result.GRID.ROWS[i].data.NAME
                        }),
                        BX.create('SPAN', {
                            props: {
                                className: 'basket-item-mini__cell basket-item-mini__summ'
                            },
                            text: this.result.GRID.ROWS[i].data.SUM
                        }),
                    ]
                });

                basketItems.appendChild(basketItem);
                // console.log(this.result.GRID.ROWS[i]);
                // this.createBasketItem(basketItemsNode, this.result.GRID.ROWS[i], index++, !!active);
            }
        }

        formBody.appendChild(basketItems);

        if (parseFloat(total.ORDER_PRICE) === 0)
        {
            priceHtml = this.params.MESS_PRICE_FREE;
            params.free = true;
        }
        else
        {
            priceHtml = total.ORDER_PRICE_FORMATED;
        }

        if (this.options.showPriceWithoutDiscount)
        {
            priceHtml += '<br><span class="bx-price-old">' + total.PRICE_WITHOUT_DISCOUNT + '</span>';
        }

        formBody.appendChild(this.createTotalUnit(BX.message('SOA_SUM_SUMMARY'), priceHtml, params));

        if (this.options.showOrderWeight)
        {
            formBody.appendChild(this.createTotalUnit(BX.message('SOA_SUM_WEIGHT_SUM'), total.ORDER_WEIGHT_FORMATED));
        }

        if (this.options.showTaxList)
        {
            for (i = 0; i < total.TAX_LIST.length; i++)
            {
                valFormatted = total.TAX_LIST[i].VALUE_MONEY_FORMATED || '';
                formBody.appendChild(
                    this.createTotalUnit(
                        total.TAX_LIST[i].NAME + (!!total.TAX_LIST[i].VALUE_FORMATED ? ' ' + total.TAX_LIST[i].VALUE_FORMATED : '') + ':',
                        valFormatted
                    )
                );
            }
        }

        params = {};
        curDelivery = this.getSelectedDelivery();
        deliveryError = curDelivery && curDelivery.CALCULATE_ERRORS && curDelivery.CALCULATE_ERRORS.length;

        if (deliveryError)
        {
            deliveryValue = BX.message('SOA_NOT_CALCULATED');
            params.error = deliveryError;
        }
        else
        {
            if (parseFloat(total.DELIVERY_PRICE) === 0)
            {
                deliveryValue = this.params.MESS_PRICE_FREE;
                params.free = true;
            }
            else
            {
                deliveryValue = total.DELIVERY_PRICE_FORMATED;
            }

            if (
                curDelivery && typeof curDelivery.DELIVERY_DISCOUNT_PRICE !== 'undefined'
                && parseFloat(curDelivery.PRICE) > parseFloat(curDelivery.DELIVERY_DISCOUNT_PRICE)
            )
            {
                deliveryValue += '<br><span class="bx-price-old">' + curDelivery.PRICE_FORMATED + '</span>';
            }
        }

        if (this.result.DELIVERY.length)
        {
            formBody.appendChild(this.createTotalUnit(BX.message('SOA_SUM_DELIVERY'), deliveryValue, params));
        }

        if (this.options.showDiscountPrice)
        {
            discText = this.params.MESS_ECONOMY;
            if (total.DISCOUNT_PERCENT_FORMATED && parseFloat(total.DISCOUNT_PERCENT_FORMATED) > 0)
                discText += total.DISCOUNT_PERCENT_FORMATED;

            formBody.appendChild(this.createTotalUnit(discText + ':', total.DISCOUNT_PRICE_FORMATED, {highlighted: true}));
        }

        if (this.options.showPayedFromInnerBudget)
        {
            formBody.appendChild(this.createTotalUnit(BX.message('SOA_SUM_IT'), total.ORDER_TOTAL_PRICE_FORMATED));
            formBody.appendChild(this.createTotalUnit(BX.message('SOA_SUM_PAYED'), total.PAYED_FROM_ACCOUNT_FORMATED));
            formBody.appendChild(this.createTotalUnit(BX.message('SOA_SUM_LEFT_TO_PAY'), total.ORDER_TOTAL_LEFT_TO_PAY_FORMATED, {total: true}));
        }
        else
        {
            formBody.appendChild(this.createTotalUnit(BX.message('SOA_SUM_IT'), total.ORDER_TOTAL_PRICE_FORMATED, {total: true}));
        }

        if (parseFloat(total.PAY_SYSTEM_PRICE) >= 0 && this.result.DELIVERY.length)
        {
            formBody.appendChild(this.createTotalUnit(BX.message('SOA_PAYSYSTEM_PRICE'), '~' + total.PAY_SYSTEM_PRICE_FORMATTED));
        }

        if (!this.result.SHOW_AUTH)
        {
            formFooter.appendChild(
                BX.create('SPAN', {
                    props: {className: 'bx-soa-cart-total-button-container'},
                    children: [
                        BX.create('A', {
                            props: {
                                href: 'javascript:void(0)',
                                className: 'button-link black-tr-b mf-submit-cart'
                            },
                            html: this.params.MESS_ORDER,
                            events: {
                                click: BX.proxy(this.clickOrderSaveAction, this)
                            }
                        })

                    ]
                })
            );
        }

        this.totalInfoBlockNode.appendChild(formContainer);

        // console.log(formCnt.style.height);
        if($('body').find('.js-mf-form-bck-total').length) {
            $('body').find('.js-mf-form-bck-total').css({
                'height' : $('body').find('.js-mf-form-total')[0].clientHeight - 24
            });
        }


        this.editMobileTotalBlock();
    }

    /**
     * Binds main events for scrolling/resizing
     */
    BX.Sale.OrderAjaxComponentExt.bindEvents = function()
    {
        BX.bind(this.orderSaveBlockNode.querySelector('[data-save-button]'), 'click', BX.proxy(this.clickOrderSaveAction, this));
        // BX.bind(window, 'scroll', BX.proxy(this.totalBlockScrollCheck, this));
        BX.bind(window, 'resize', BX.throttle(function(){
            this.totalBlockResizeCheck();
            this.alignBasketColumns();
            // this.basketBlockScrollCheck();
            this.mapsReady && this.resizeMapContainers();
        }, 50, this));
        BX.addCustomEvent('onDeliveryExtraServiceValueChange', BX.proxy(this.sendRequest, this));
    }

    /**
     * Checks possibility to skip section
     */
    BX.Sale.OrderAjaxComponentExt.shouldSkipSection = function(section)
    {
        var skip = false;
        if (this.params.SKIP_USELESS_BLOCK === 'Y')
        {
            if (section.id === this.pickUpBlockNode.id)
            {
                var delivery = this.getSelectedDelivery();
                if (delivery)
                {
                    skip = this.getPickUpInfoArray(delivery.STORE).length === 1;
                }
            }

            if (section.id === this.deliveryBlockNode.id)
            {
                skip = this.result.DELIVERY && this.result.DELIVERY.length === 1
                    && this.result.DELIVERY[0].EXTRA_SERVICES.length === 0
                    && !this.result.DELIVERY[0].CALCULATE_ERRORS;
            }

            // if (section.id === this.paySystemBlockNode.id)
            // {
            //     skip = this.result.PAY_SYSTEM && this.result.PAY_SYSTEM.length === 1 && this.result.PAY_FROM_ACCOUNT !== 'Y';
            // }

            if (section.id === this.couponsBlockNode.id)
            {
                skip = section.getAttribute('data-disablednode')==='true';
            }
        }

        return skip;
    }

    BX.Sale.OrderAjaxComponentExt.getProfilesControl = function(node)
    {
        var profilesLength = BX.util.object_keys(this.result.USER_PROFILES).length,
            i, label, options = [],
            profileChangeInput, input;

        if (profilesLength)
        {
            if (
                this.params.ALLOW_USER_PROFILES === 'Y'
                && (profilesLength > 1 || this.params.ALLOW_NEW_PROFILE === 'Y')
            )
            {
                this.regionBlockNotEmpty = true;

                label = BX.create('LABEL', {props: {className: 'bx-soa-custom-label'}, html: this.params.MESS_SELECT_PROFILE});

                for (i in this.result.USER_PROFILES)
                {
                    if (this.result.USER_PROFILES.hasOwnProperty(i))
                    {
                        options.unshift(
                            BX.create('OPTION', {
                                props: {
                                    value: this.result.USER_PROFILES[i].ID,
                                    selected: this.result.USER_PROFILES[i].CHECKED === 'Y'
                                },
                                html: this.result.USER_PROFILES[i].NAME
                            })
                        );
                    }
                }

                if (this.params.ALLOW_NEW_PROFILE === 'Y')
                {
                    options.unshift(BX.create('OPTION', {props: {value: 0}, text: BX.message('SOA_PROP_NEW_PROFILE')}));
                }

                profileChangeInput = BX.create('INPUT', {
                    props: {
                        type: 'hidden',
                        value: 'N',
                        id: 'profile_change',
                        name: 'profile_change'
                    }
                });
                input = BX.create('SELECT', {
                    props: {className: 'form-control', name: 'PROFILE_ID'},
                    children: options,
                    events:{
                        change: BX.delegate(function(){
                            BX('profile_change').value = 'Y';
                            this.sendRequest();
                        }, this)
                    }
                });

                node.appendChild(
                    BX.create('DIV', {
                        props: {className: 'form-group bx-soa-location-input-container bx-soa-location-input-container--profiles'},
                        children: [label, profileChangeInput, input]
                    })
                );
            }
            else
            {
                for (i in this.result.USER_PROFILES)
                {
                    if (
                        this.result.USER_PROFILES.hasOwnProperty(i)
                        && this.result.USER_PROFILES[i].CHECKED === 'Y'
                    )
                    {
                        node.appendChild(
                            BX.create('INPUT', {
                                props: {
                                    name: 'PROFILE_ID',
                                    type: 'hidden',
                                    value: this.result.USER_PROFILES[i].ID}
                            })
                        );
                    }
                }
            }
        }
    }


    /**
     * Animating scroll to certain node
     */
    BX.Sale.OrderAjaxComponentExt.animateScrollTo = function(node, duration, shiftToTop)
    {
        if (!node)
            return;

        var scrollTop = BX.GetWindowScrollPos().scrollTop,
            orderBlockPos = BX.pos(this.orderBlockNode),
            ghostTop = BX.pos(node).top - (this.isMobile ? 50 : 100);

        if (shiftToTop)
            ghostTop -= parseInt(shiftToTop);

        if (ghostTop + window.innerHeight > orderBlockPos.bottom)
            ghostTop = orderBlockPos.bottom - window.innerHeight + 17;

        new BX.easing({
            duration: duration || 800,
            start: {scroll: scrollTop},
            finish: {scroll: ghostTop},
            transition: BX.easing.makeEaseOut(BX.easing.transitions.quad),
            step: BX.delegate(function(state){
                window.scrollTo(0, state.scroll);
            }, this)
        }).animate();
    }

    /**
     * если нужно показываем кнопки оформить заказ
     */
    BX.Sale.OrderAjaxComponentExt.showOrderSaveBtns = function()
    {
        if((BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-visited="true"]').length - BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-disablednode="true"]').length) == (BX('bx-soa-order').querySelectorAll('.mf-checkout-el:not([data-disablednode="true"])').length - BX('bx-soa-order').querySelectorAll('.bx-soa-section[data-disablednode="true"]').length))
        {
            BX('bx-soa-orderSave').style.display = 'block';
            BX('bx-soa-orderSave-sb').style.display = 'block';
        }
    }
})();

$(document).ready(function () {
    $('body').on('focus', 'input[name="ORDER_PROP_5"]', function () {
        $(this).closest('.form-group').addClass('focused');
        // console.log('focc');
    });


    $('body').on('blur', 'input[name="ORDER_PROP_5"]', function () {
        // console.log('blur');
        if($(this).val()=='') {
            $(this).closest('.form-group').removeClass('focused');
        }
    });
});

function FormStyling()
{
    $('#bx-soa-paysystem input[type="checkbox"]').styler();
    $('#bx-soa-paysystem input[type="radio"]').styler();
    // $('.js-stores-group select').styler();
    $('#stores-selects select').styler('destroy');
}


function initRequiredFields()
{
    $('.bx-soa-customer-field').each(function () {
        if($(this).find('.form-control').length) {
            if($(this).find('.bx-authform-starrequired').length) {
                $(this).find('.form-control').attr('required', true);
            }
        }
    });
}



$(document).ready(function () {
    var $body = $('body'),
        $bxSoaDelivery = $('#bx-soa-delivery');

    // FormStyling();
    // initRequiredFields();

    // console.log(IS_B2B);

    // $body.on('focus', '.bx-soa-customer-input', InputFocus);
    // $body.on('blur', '.bx-soa-customer-input', InputBlur);
    // window.InputCheckState();

    // if($body.find('.js-mf-form-bck-total').length) {
    //     $body.find('.js-mf-form-bck-total').css({
    //         'height' : $body.find('.js-mf-form-total')[0].clientHeight - 24
    //     });
    // }



    $bxSoaDelivery.on('change', 'select.js-store-city', function () {
        var city = $(this).val();

        storeCityValue = city;

        $bxSoaDelivery.find('.js-store-items').val('').find('option:not(:first-child)').attr('disabled', true);
        $bxSoaDelivery.find('.js-store-items').find('option').each(function () {
            if($(this).data('city') == city) {
                $(this).attr('disabled', false);
            }
        });
        $bxSoaDelivery.find('select.js-store-items').trigger('refresh');
        $('#BUYER_STORE').val('0');
    });

    $bxSoaDelivery.on('change', 'select.js-store-items', function () {
        storeItemValue = $(this).val();
        $('#BUYER_STORE').val(parseInt($(this).val()));
    });

    // Перемещаем коммент к заказу
    // $('#bx-order-comment').append( $('#orderDescription').closest('.form-group') );

    // $('#bx-soa-pickup').find('.bx-soa-section-title-container')[0].click();

});

BX.addCustomEvent('onAjaxSuccess', function(e) {
    console.log('onAjaxSuccess');
    if($('body').find('select[name="PROFILE_ID"]').length)
    {
        $('body').find('select[name="PROFILE_ID"]').styler('destroy');
        $('body').find('select[name="PROFILE_ID"]').styler();
    }
    // FormStyling();
    // initRequiredFields();

    // $( '.mf-checkout-el' ).accordion({
    //     collapsible: true,
    //     active: false,
    //     header: '.checkout-el-header',
    //     heightStyle: 'content'
    // });
    // $( '.mf-checkout-el' ).accordion( 'option', 'active', 0 );
});
