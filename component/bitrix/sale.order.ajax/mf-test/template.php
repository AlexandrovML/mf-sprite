<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main,
	Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 * @var array $arResult
 * @var CMain $APPLICATION
 * @var CUser $USER
 * @var SaleOrderAjax $component
 * @var string $templateFolder
 */

if (!function_exists("getDiscountCartList")) {
	function getDiscountCartList($arOrder, $USER_ID = null, $ORDER_PRICE = 0, $ORDER_WEIGHT = 0)
	{
		$discountList = array();
		$arIDS = array();

		$groupDiscountIterator = Bitrix\Sale\Internals\DiscountGroupTable::getList(array(
			'select' => array('DISCOUNT_ID'),
			'filter' => array('@GROUP_ID' => CUser::GetUserGroup($USER_ID), '=ACTIVE' => 'Y')
		));
		while ($groupDiscount = $groupDiscountIterator->fetch()) {
			$groupDiscount['DISCOUNT_ID'] = (int)$groupDiscount['DISCOUNT_ID'];
			if ($groupDiscount['DISCOUNT_ID'] > 0)
				$arIDS[$groupDiscount['DISCOUNT_ID']] = true;
		}

		if ($arIDS) {
			$discountIterator = Bitrix\Sale\Internals\DiscountTable::getList(array(
				'select' => array(
					"ID", "NAME", "PRIORITY", "SORT", "LAST_DISCOUNT", "UNPACK", "APPLICATION", "USE_COUPONS"
				),
				'filter' => array(
					'@ID' => $arIDS
				),
				'order' => array(
					"PRIORITY" => "DESC",
					"SORT" => "ASC",
					"ID" => "ASC"
				)
			));
			foreach ($arOrder as &$order) {
				$order['PRICE'] = $order['FULL_PRICE'];
			}
			$arOrderAll = array(
				'SITE_ID' => SITE_ID,
				'USER_ID' => $USER_ID,
				'ORDER_PRICE' => $ORDER_PRICE,
				'ORDER_WEIGHT' => $ORDER_WEIGHT,
				'BASKET_ITEMS' => $arOrder
			);
			while ($discount = $discountIterator->fetch()) {
				$checkOrder = null;
				$strUnpack = $discount['UNPACK'];
				if (empty($strUnpack))
					continue;
				eval('$checkOrder=' . $strUnpack . ';');
				if (!is_callable($checkOrder))
					continue;
				$boolRes = $checkOrder($arOrderAll);
				unset($checkOrder);

				if ($boolRes) {
					$discountList[] = $discount;
				}
			}
		}
		return $discountList;
	}
}

//$USER_ID = $GLOBALS["USER"]->GetID();
//$discountListCart = getDiscountCartList($arResult["ITEMS"]["AnDelCanBuy"], $USER_ID,
//	$arResult['allSum'] + $arResult['DISCOUNT_PRICE_ALL'], $arResult['allWeight']);
//
//
//pr($discountListCart);

$context = Main\Application::getInstance()->getContext();
$request = $context->getRequest();

use Bitrix\Sale\Internals\DiscountTable;

$arDiscounts = DiscountTable::getList()->fetchAll();
//pr($arDiscounts);


//$couponList = \Bitrix\Sale\DiscountCouponsManager::get(true, array(), true, false);
$MESS_WE_ACCEPT_PAYMENT = Loc::getMessage('MESS_WE_ACCEPT_PAYMENT');
$MESS_WE_ACCEPT_PAYMENT .= '<img src="'.SITE_TEMPLATE_PATH.'/images/checkout/mastercard.png">';
$MESS_WE_ACCEPT_PAYMENT .= '<img src="'.SITE_TEMPLATE_PATH.'/images/checkout/visa.png">';
//$MESS_WE_ACCEPT_PAYMENT .= '<img src="'.SITE_TEMPLATE_PATH.'/images/checkout/belkart.png">';

$arParams['MESS_SOA_COUPON_INFO'] = Loc::getMessage('MESS_SOA_COUPON_INFO');
$arParams['MESS_SOA_DELIVERY_COST'] = Loc::getMessage('MESS_SOA_DELIVERY_COST');
$arParams['MESS_WE_ACCEPT_PAYMENT'] = $MESS_WE_ACCEPT_PAYMENT;
$arParams['MESS_WE_ACCEPT_PAYMENT_BELCART'] = '<img src="'.SITE_TEMPLATE_PATH.'/images/checkout/belkart.png">';
$arParams['ICON_EDIT'] = '<svg viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M15.1376 0.861C14.5836 0.3055 13.8446 0 13.0601 0C12.2756 0 11.5376 0.305 10.9831 0.86L2.14656 9.6455C2.09406 9.6975 2.05406 9.76 2.02906 9.829L0.0290632 15.329C-0.0354368 15.5065 0.00606316 15.707 0.136563 15.8445C0.232563 15.9455 0.364063 16 0.499563 16C0.548563 16 0.597563 15.993 0.646063 15.978L5.62056 14.452C5.69856 14.4285 5.76956 14.3855 5.82756 14.328L15.1381 5.0175C15.6941 4.463 15.9996 3.724 15.9996 2.939C15.9991 2.1545 15.6941 1.416 15.1376 0.861ZM5.04556 13.5825L3.18356 14.1545C3.07506 13.836 2.90606 13.554 2.67556 13.3235C2.47506 13.124 2.24256 12.969 1.99756 12.843L2.84956 10.5H3.99956V11.5C3.99956 11.7765 4.22306 12 4.49956 12H5.38206L5.04556 13.5825ZM12.4056 6.3355L6.31356 12.4275L6.48856 11.604C6.51956 11.456 6.48256 11.3025 6.38756 11.1855C6.29256 11.068 6.15006 11 5.99956 11H4.99956V10C4.99956 9.724 4.77556 9.5 4.49956 9.5H3.71056L9.65706 3.5875L9.66306 3.593C10.0291 3.2265 10.5166 3.025 11.0341 3.025C11.5516 3.025 12.0396 3.2265 12.4051 3.593C12.7706 3.9595 12.9741 4.4465 12.9741 4.9645C12.9741 5.4825 12.7726 5.969 12.4056 6.3355ZM14.4306 4.3105L13.9646 4.777C13.9196 4.0625 13.6231 3.3965 13.1131 2.886C12.6026 2.375 11.9361 2.079 11.2196 2.0345L11.6886 1.569L11.6896 1.568C12.0546 1.2015 12.5421 1 13.0601 1C13.5781 1 14.0651 1.2015 14.4306 1.568C14.7976 1.9345 14.9996 2.4215 14.9996 2.939C14.9996 3.4575 14.7986 3.9445 14.4306 4.3105ZM11.1461 4.1465L6.14606 9.1465C5.95056 9.342 5.95056 9.658 6.14606 9.8535C6.24306 9.951 6.37156 10 6.49956 10C6.62756 10 6.75506 9.951 6.85306 9.8535L11.8531 4.8535C12.0486 4.658 12.0486 4.342 11.8531 4.1465C11.6571 3.951 11.3416 3.951 11.1461 4.1465Z"/></svg>';
$arParams['ICON_WARNING'] = '<svg viewBox="0 0 27 25" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M26.7609 21.7747L15.0312 1.45493C14.3513 0.277537 12.6487 0.277537 11.9688 1.45493L0.239091 21.7747C-0.440814 22.9521 0.410449 24.428 1.77026 24.428H25.2297C26.5895 24.428 27.4408 22.9521 26.7609 21.7747ZM13.5166 7.71227C14.2683 7.71227 14.8764 8.33689 14.8543 9.08866L14.6332 16.8053C14.6166 17.4133 14.1191 17.8942 13.511 17.8942C12.903 17.8942 12.4055 17.4078 12.3889 16.8053L12.1733 9.08866C12.1568 8.33689 12.7593 7.71227 13.5166 7.71227ZM13.5 21.6697C12.7316 21.6697 12.107 21.045 12.107 20.2767C12.107 19.5083 12.7316 18.8837 13.5 18.8837C14.2683 18.8837 14.893 19.5083 14.893 20.2767C14.893 21.045 14.2683 21.6697 13.5 21.6697Z" /></svg>';


if(!empty($arResult['JS_DATA']['COUPON_LIST']))
{
	foreach ($arResult['JS_DATA']['COUPON_LIST'] as &$arCoupon)
	{
		$arCoupon['COUPON_INFO'] = [];

		if($arCoupon['JS_STATUS'] == 'APPLIED')
		{
			foreach ($arDiscounts as $arDiscount)
			{
				if($arDiscount['ID'] == $arCoupon['DISCOUNT_ID'])
				{
//			        pr($arDiscount['SHORT_DESCRIPTION_STRUCTURE']);

					$arCoupon['COUPON_INFO'] = $arDiscount['SHORT_DESCRIPTION_STRUCTURE'];

//				    switch ($arDiscount['SHORT_DESCRIPTION_STRUCTURE']['VALUE_TYPE'])
//                    {
//                        case 'P':
//                            $value = $arDiscount['SHORT_DESCRIPTION_STRUCTURE']['VALUE'] . '%';
//                            break;
//
//                        case 'F':
//                        case 'S':
//	                        $CURRENCY = $arResult['BASKET_ITEMS'][0]['CURRENCY'];
//                            $value =  CCurrencyLang::CurrencyFormat($arDiscount['SHORT_DESCRIPTION_STRUCTURE']['VALUE'], $CURRENCY);
//                            break;
//
//                    }

//				    $arParams['MESS_SOA_COUPON_INFO'] = Loc::getMessage('MESS_SOA_COUPON_INFO', ['#VALUE#' => $value]);
				}
			}
		}

	}
	unset($arCoupon);
}

//pr($arResult['JS_DATA']['COUPON_LIST']);
//pr($arResult['BASKET_ITEMS'][0]['CURRENCY']);


//$cc = \Bitrix\Sale\Discount::getApplyResult();
//pr($cc);



if (empty($arParams['TEMPLATE_THEME']))
{
	$arParams['TEMPLATE_THEME'] = Main\ModuleManager::isModuleInstalled('bitrix.eshop') ? 'site' : 'blue';
}

if ($arParams['TEMPLATE_THEME'] === 'site')
{
	$templateId = Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', $component->getSiteId());
	$templateId = preg_match('/^eshop_adapt/', $templateId) ? 'eshop_adapt' : $templateId;
	$arParams['TEMPLATE_THEME'] = Main\Config\Option::get('main', 'wizard_'.$templateId.'_theme_id', 'blue', $component->getSiteId());
}

if (!empty($arParams['TEMPLATE_THEME']))
{
	if (!is_file(Main\Application::getDocumentRoot().'/bitrix/css/main/themes/'.$arParams['TEMPLATE_THEME'].'/style.css'))
	{
		$arParams['TEMPLATE_THEME'] = 'blue';
	}
}

$arParams['ALLOW_USER_PROFILES'] = $arParams['ALLOW_USER_PROFILES'] === 'Y' ? 'Y' : 'N';
$arParams['SKIP_USELESS_BLOCK'] = $arParams['SKIP_USELESS_BLOCK'] === 'N' ? 'N' : 'Y';

if (!isset($arParams['SHOW_ORDER_BUTTON']))
{
	$arParams['SHOW_ORDER_BUTTON'] = 'final_step';
}

$arParams['HIDE_ORDER_DESCRIPTION'] = isset($arParams['HIDE_ORDER_DESCRIPTION']) && $arParams['HIDE_ORDER_DESCRIPTION'] === 'Y' ? 'Y' : 'N';
$arParams['SHOW_TOTAL_ORDER_BUTTON'] = $arParams['SHOW_TOTAL_ORDER_BUTTON'] === 'Y' ? 'Y' : 'N';
$arParams['SHOW_PAY_SYSTEM_LIST_NAMES'] = $arParams['SHOW_PAY_SYSTEM_LIST_NAMES'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_PAY_SYSTEM_INFO_NAME'] = $arParams['SHOW_PAY_SYSTEM_INFO_NAME'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_DELIVERY_LIST_NAMES'] = $arParams['SHOW_DELIVERY_LIST_NAMES'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_DELIVERY_INFO_NAME'] = $arParams['SHOW_DELIVERY_INFO_NAME'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_DELIVERY_PARENT_NAMES'] = $arParams['SHOW_DELIVERY_PARENT_NAMES'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_STORES_IMAGES'] = $arParams['SHOW_STORES_IMAGES'] === 'N' ? 'N' : 'Y';

if (!isset($arParams['BASKET_POSITION']) || !in_array($arParams['BASKET_POSITION'], array('before', 'after')))
{
	$arParams['BASKET_POSITION'] = 'after';
}

$arParams['EMPTY_BASKET_HINT_PATH'] = isset($arParams['EMPTY_BASKET_HINT_PATH']) ? (string)$arParams['EMPTY_BASKET_HINT_PATH'] : '/';
$arParams['SHOW_BASKET_HEADERS'] = $arParams['SHOW_BASKET_HEADERS'] === 'Y' ? 'Y' : 'N';
$arParams['HIDE_DETAIL_PAGE_URL'] = isset($arParams['HIDE_DETAIL_PAGE_URL']) && $arParams['HIDE_DETAIL_PAGE_URL'] === 'Y' ? 'Y' : 'N';
$arParams['DELIVERY_FADE_EXTRA_SERVICES'] = $arParams['DELIVERY_FADE_EXTRA_SERVICES'] === 'Y' ? 'Y' : 'N';
$arParams['SHOW_COUPONS_BASKET'] = $arParams['SHOW_COUPONS_BASKET'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_COUPONS_DELIVERY'] = $arParams['SHOW_COUPONS_DELIVERY'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_COUPONS_PAY_SYSTEM'] = $arParams['SHOW_COUPONS_PAY_SYSTEM'] === 'Y' ? 'Y' : 'N';
$arParams['SHOW_NEAREST_PICKUP'] = $arParams['SHOW_NEAREST_PICKUP'] === 'Y' ? 'Y' : 'N';
$arParams['DELIVERIES_PER_PAGE'] = isset($arParams['DELIVERIES_PER_PAGE']) ? intval($arParams['DELIVERIES_PER_PAGE']) : 9;
$arParams['PAY_SYSTEMS_PER_PAGE'] = isset($arParams['PAY_SYSTEMS_PER_PAGE']) ? intval($arParams['PAY_SYSTEMS_PER_PAGE']) : 9;
$arParams['PICKUPS_PER_PAGE'] = isset($arParams['PICKUPS_PER_PAGE']) ? intval($arParams['PICKUPS_PER_PAGE']) : 5;
$arParams['SHOW_PICKUP_MAP'] = $arParams['SHOW_PICKUP_MAP'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_MAP_IN_PROPS'] = $arParams['SHOW_MAP_IN_PROPS'] === 'Y' ? 'Y' : 'N';
$arParams['USE_YM_GOALS'] = $arParams['USE_YM_GOALS'] === 'Y' ? 'Y' : 'N';
$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

$useDefaultMessages = !isset($arParams['USE_CUSTOM_MAIN_MESSAGES']) || $arParams['USE_CUSTOM_MAIN_MESSAGES'] != 'Y';

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_BLOCK_NAME']))
{
	$arParams['MESS_AUTH_BLOCK_NAME'] = Loc::getMessage('AUTH_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REG_BLOCK_NAME']))
{
	$arParams['MESS_REG_BLOCK_NAME'] = Loc::getMessage('REG_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_BASKET_BLOCK_NAME']))
{
	$arParams['MESS_BASKET_BLOCK_NAME'] = Loc::getMessage('BASKET_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REGION_BLOCK_NAME']))
{
	$arParams['MESS_REGION_BLOCK_NAME'] = Loc::getMessage('REGION_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_COUPONS_BLOCK_NAME']))
{
	$arParams['MESS_COUPONS_BLOCK_NAME'] = Loc::getMessage('MESS_COUPONS_BLOCK_NAME');
}

if ($useDefaultMessages || !isset($arParams['MESS_EMAIL_BLOCK_NAME']))
{
	$arParams['MESS_EMAIL_BLOCK_NAME'] = Loc::getMessage('MESS_EMAIL_BLOCK_NAME');
}

if ($useDefaultMessages || !isset($arParams['MESS_PAYMENT_BLOCK_NAME']))
{
	$arParams['MESS_PAYMENT_BLOCK_NAME'] = Loc::getMessage('PAYMENT_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_BLOCK_NAME']))
{
	$arParams['MESS_DELIVERY_BLOCK_NAME'] = Loc::getMessage('DELIVERY_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_BUYER_BLOCK_NAME']))
{
	$arParams['MESS_BUYER_BLOCK_NAME'] = Loc::getMessage('BUYER_BLOCK_NAME_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_BACK']))
{
	$arParams['MESS_BACK'] = Loc::getMessage('BACK_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_FURTHER']))
{
	$arParams['MESS_FURTHER'] = Loc::getMessage('FURTHER_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_EDIT']))
{
	$arParams['MESS_EDIT'] = Loc::getMessage('EDIT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ORDER']))
{
	$arParams['MESS_ORDER'] = $arParams['~MESS_ORDER'] = Loc::getMessage('ORDER_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PRICE']))
{
	$arParams['MESS_PRICE'] = Loc::getMessage('PRICE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PERIOD']))
{
	$arParams['MESS_PERIOD'] = Loc::getMessage('PERIOD_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_NAV_BACK']))
{
	$arParams['MESS_NAV_BACK'] = Loc::getMessage('NAV_BACK_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_NAV_FORWARD']))
{
	$arParams['MESS_NAV_FORWARD'] = Loc::getMessage('NAV_FORWARD_DEFAULT');
}

$useDefaultMessages = !isset($arParams['USE_CUSTOM_ADDITIONAL_MESSAGES']) || $arParams['USE_CUSTOM_ADDITIONAL_MESSAGES'] != 'Y';

if ($useDefaultMessages || !isset($arParams['MESS_PRICE_FREE']))
{
	$arParams['MESS_PRICE_FREE'] = Loc::getMessage('PRICE_FREE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ECONOMY']))
{
	$arParams['MESS_ECONOMY'] = Loc::getMessage('ECONOMY_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REGISTRATION_REFERENCE']))
{
	$arParams['MESS_REGISTRATION_REFERENCE'] = Loc::getMessage('REGISTRATION_REFERENCE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_1']))
{
	$arParams['MESS_AUTH_REFERENCE_1'] = Loc::getMessage('AUTH_REFERENCE_1_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_2']))
{
	$arParams['MESS_AUTH_REFERENCE_2'] = Loc::getMessage('AUTH_REFERENCE_2_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_AUTH_REFERENCE_3']))
{
	$arParams['MESS_AUTH_REFERENCE_3'] = Loc::getMessage('AUTH_REFERENCE_3_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ADDITIONAL_PROPS']))
{
	$arParams['MESS_ADDITIONAL_PROPS'] = Loc::getMessage('ADDITIONAL_PROPS_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_USE_COUPON']))
{
	$arParams['MESS_USE_COUPON'] = Loc::getMessage('USE_COUPON_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_COUPON']))
{
	$arParams['MESS_COUPON'] = Loc::getMessage('COUPON_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PERSON_TYPE']))
{
	$arParams['MESS_PERSON_TYPE'] = Loc::getMessage('PERSON_TYPE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_SELECT_PROFILE']))
{
	$arParams['MESS_SELECT_PROFILE'] = Loc::getMessage('SELECT_PROFILE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_REGION_REFERENCE']))
{
	$arParams['MESS_REGION_REFERENCE'] = Loc::getMessage('REGION_REFERENCE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PICKUP_LIST']))
{
	$arParams['MESS_PICKUP_LIST'] = Loc::getMessage('PICKUP_LIST_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_NEAREST_PICKUP_LIST']))
{
	$arParams['MESS_NEAREST_PICKUP_LIST'] = Loc::getMessage('NEAREST_PICKUP_LIST_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_SELECT_PICKUP']))
{
	$arParams['MESS_SELECT_PICKUP'] = Loc::getMessage('SELECT_PICKUP_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_INNER_PS_BALANCE']))
{
	$arParams['MESS_INNER_PS_BALANCE'] = Loc::getMessage('INNER_PS_BALANCE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_ORDER_DESC']))
{
	$arParams['MESS_ORDER_DESC'] = Loc::getMessage('ORDER_DESC_DEFAULT');
}

$useDefaultMessages = !isset($arParams['USE_CUSTOM_ERROR_MESSAGES']) || $arParams['USE_CUSTOM_ERROR_MESSAGES'] != 'Y';

if ($useDefaultMessages || !isset($arParams['MESS_PRELOAD_ORDER_TITLE']))
{
	$arParams['MESS_PRELOAD_ORDER_TITLE'] = Loc::getMessage('PRELOAD_ORDER_TITLE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_SUCCESS_PRELOAD_TEXT']))
{
	$arParams['MESS_SUCCESS_PRELOAD_TEXT'] = Loc::getMessage('SUCCESS_PRELOAD_TEXT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_FAIL_PRELOAD_TEXT']))
{
	$arParams['MESS_FAIL_PRELOAD_TEXT'] = Loc::getMessage('FAIL_PRELOAD_TEXT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_CALC_ERROR_TITLE']))
{
	$arParams['MESS_DELIVERY_CALC_ERROR_TITLE'] = Loc::getMessage('DELIVERY_CALC_ERROR_TITLE_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_DELIVERY_CALC_ERROR_TEXT']))
{
	$arParams['MESS_DELIVERY_CALC_ERROR_TEXT'] = Loc::getMessage('DELIVERY_CALC_ERROR_TEXT_DEFAULT');
}

if ($useDefaultMessages || !isset($arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR']))
{
	$arParams['MESS_PAY_SYSTEM_PAYABLE_ERROR'] = Loc::getMessage('PAY_SYSTEM_PAYABLE_ERROR_DEFAULT');
}

$arParams['MESS_CHECKOUT_DESC'] = Loc::getMessage('MESS_CHECKOUT_DESC');
$arParams['MESS_USE_COUPON_LABEL'] = Loc::getMessage('USE_COUPON_LABEL');
$arParams['MESS_EMAIL_LABEL'] = Loc::getMessage('MESS_EMAIL_LABEL');


$arStoreCities = [];
foreach ($arResult['JS_DATA']['STORE_LIST'] as $store) {
	$arStoreCities[$store['UF_CITY']] = $store['UF_CITY'];
}

//pr($arResult['JS_DATA']['STORE_LIST']);
//pr($arStoreCities);


$scheme = $request->isHttps() ? 'https' : 'http';

switch (LANGUAGE_ID)
{
	case 'ru':
		$locale = 'ru-RU'; break;
	case 'ua':
		$locale = 'ru-UA'; break;
	case 'tk':
		$locale = 'tr-TR'; break;
	default:
		$locale = 'en-US'; break;
}

//$this->addExternalCss('/bitrix/css/main/bootstrap.css');
//$APPLICATION->SetAdditionalCSS('/bitrix/css/main/themes/'.$arParams['TEMPLATE_THEME'].'/style.css', true);
$APPLICATION->SetAdditionalCSS($templateFolder.'/style.css', true);
//$this->addExternalJs(SITE_TEMPLATE_PATH.'/js/jquery-ui/jquery-ui.min.js');
$this->addExternalJs($templateFolder.'/order_ajax.js');
$this->addExternalJs($templateFolder.'/order_ajax_ext.js');
\Bitrix\Sale\PropertyValueCollection::initJs();
$this->addExternalJs($templateFolder.'/script.js');
?>
<?/*<div class="fix-block"><? include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/include/_bc.php'; ?></div>*/?>
<section class="cart-section-background">
    <div class="fix-block">
        <NOSCRIPT>
            <div style="color:red"><?=Loc::getMessage('SOA_NO_JS')?></div>
        </NOSCRIPT>
		<?

		if (strlen($request->get('ORDER_ID')) > 0)
		{
			include(Main\Application::getDocumentRoot().$templateFolder.'/confirm.php');
		}
        elseif ($arParams['DISABLE_BASKET_REDIRECT'] === 'Y' && $arResult['SHOW_EMPTY_BASKET'])
		{
			include(Main\Application::getDocumentRoot().$templateFolder.'/empty.php');
		}
		else
		{
			$hideDelivery = empty($arResult['DELIVERY']);
			?>
            <form action="<?=POST_FORM_ACTION_URI?>" method="POST" name="ORDER_FORM" id="bx-soa-order-form" enctype="multipart/form-data">
				<?
				echo bitrix_sessid_post();

				if (strlen($arResult['PREPAY_ADIT_FIELDS']) > 0)
				{
					echo $arResult['PREPAY_ADIT_FIELDS'];
				}
				?>
                <input type="hidden" name="<?=$arParams['ACTION_VARIABLE']?>" value="saveOrderAjax">
                <input type="hidden" name="location_type" value="code">
                <input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?=$arResult['BUYER_STORE']?>">
                <div id="bx-soa-order" class="bx-<?=$arParams['TEMPLATE_THEME']?> clearfix" style="opacity: 0">
                    <!--	MAIN BLOCK	-->
                    <div class="bx-soa cart-content">
                        <div id="bx-soa-main-notifications">
                            <div class="alert alert-danger" style="display:none"></div>
                            <div data-type="informer" style="display:none"></div>
                        </div>
                        <!--	AUTH BLOCK	-->
                        <div id="bx-soa-auth" class="bx-soa-section bx-soa-auth" style="display:none">
                            <div class="bx-soa-section-title-container">
                                <h2 class="bx-soa-section-title col-sm-9">
                                    <span class="bx-soa-section-title-count"></span><?=$arParams['MESS_AUTH_BLOCK_NAME']?>
                                </h2>
                            </div>
                            <div class="bx-soa-section-content container-fluid"></div>
                        </div>

                        <!--	DUPLICATE MOBILE ORDER SAVE BLOCK	-->
                        <div id="bx-soa-total-mobile" style="margin-bottom: 6px;"></div>

						<? if ($arParams['BASKET_POSITION'] === 'before'): ?>
                            <!--	BASKET ITEMS BLOCK	-->
                            <div style="display: none !important;">
                                <div id="bx-soa-basket" data-visited="false" class="bx-soa-section bx-active">
                                    <div class="bx-soa-section-title-container">
                                        <h2 class="bx-soa-section-title col-sm-9">
                                            <span class="bx-soa-section-title-count"></span><?=$arParams['MESS_BASKET_BLOCK_NAME']?>
                                        </h2>
                                        <div class="col-xs-12 col-sm-3 text-right"><a href="javascript:void(0)" class="bx-soa-editstep"><?=$arParams['MESS_EDIT']?></a></div>
                                    </div>
                                    <div class="bx-soa-section-content container-fluid"></div>
                                </div>
                            </div>
						<? endif ?>

                        <!--	REGION BLOCK	-->
                        <div id="bx-soa-region" data-visited="false" class="bx-soa-section bx-active mf-checkout-el checkout-regionlist">
                            <div class="bx-soa-section-title-container checkout-el-header">
                                <span class="bx-soa-section-title"><?=$arParams['MESS_REGION_BLOCK_NAME']?>:</span>

                                <div class="che-el-icon">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"></path></g></svg>
                                </div>
                            </div>
                            <div class="bx-soa-section-content checkout-el-body"></div>
                        </div>


                        <div id="bx-soa-coupons" data-visited="false" class="bx-soa-section bx-active mf-checkout-el checkout-actioncode" <?/*data-disablednode="true"*/?>>
                            <div class="bx-soa-section-title-container checkout-el-header">
                                <span class="bx-soa-section-title"><?=$arParams['MESS_COUPONS_BLOCK_NAME']?>:</span>

                                <div class="che-el-icon">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"></path></g></svg>
                                </div>
                            </div>
                            <div class="bx-soa-section-content checkout-el-body"></div>
                        </div>
						<?/**/?>

                        <!--	DELIVERY BLOCK	-->
                        <div id="bx-soa-delivery" data-visited="false" class="bx-soa-section bx-active mf-checkout-el" <?=($hideDelivery ? 'style="display:none"' : '')?>>
                            <div class="bx-soa-section-title-container checkout-el-header">
                                <span class="bx-soa-section-title"><?=$arParams['MESS_DELIVERY_BLOCK_NAME']?>:</span>

                                <div class="che-el-icon">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"></path></g></svg>
                                </div>
                            </div>
                            <div class="bx-soa-section-content checkout-el-body"></div>
                        </div>
                        <!--	PICKUP BLOCK	-->
                        <div style="display: none;">
                            <div id="bx-soa-pickup" data-visited="false" class="bx-soa-section" style="display:none">
                                <div class="bx-soa-section-title-container">
                                    <h2 class="bx-soa-section-title col-sm-9">
                                        <span class="bx-soa-section-title-count"></span>
                                    </h2>
                                    <div class="col-xs-12 col-sm-3 text-right"><a href="" class="bx-soa-editstep"><?=$arParams['MESS_EDIT']?></a></div>
                                </div>
                                <div class="bx-soa-section-content container-fluid"></div>
                            </div>
                        </div>

                        <!--	BUYER PROPS BLOCK	-->
                        <div id="bx-soa-properties" data-visited="false" class="bx-soa-section bx-active mf-checkout-el">
                            <div class="bx-soa-section-title-container checkout-el-header">
                                <span class="bx-soa-section-title"><?=$arParams['MESS_BUYER_BLOCK_NAME']?>:</span>

                                <div class="che-el-icon">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"></path></g></svg>
                                </div>
                            </div>
                            <div class="bx-soa-section-content checkout-el-body"></div>
                        </div>

                        <!--	PAY SYSTEMS BLOCK	-->
                        <div id="bx-soa-paysystem" data-visited="false" class="bx-soa-section bx-active mf-checkout-el">
                            <div class="bx-soa-section-title-container checkout-el-header">
                                <span class="bx-soa-section-title"><?=$arParams['MESS_PAYMENT_BLOCK_NAME']?>:</span>

                                <div class="che-el-icon">
                                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"></path></g></svg>
                                </div>
                            </div>
                            <div class="bx-soa-section-content checkout-el-body"></div>
                        </div>


						<? if ($arParams['BASKET_POSITION'] === 'after'): ?>
                            <!--	BASKET ITEMS BLOCK	-->
                            <div style="display: none !important;">
                                <div id="bx-soa-basket" data-visited="false" class="bx-soa-section bx-active">
                                    <div class="bx-soa-section-title-container">
                                        <h2 class="bx-soa-section-title col-sm-9">
                                            <span class="bx-soa-section-title-count"></span><?=$arParams['MESS_BASKET_BLOCK_NAME']?>
                                        </h2>
                                        <div class="col-xs-12 col-sm-3 text-right"><a href="javascript:void(0)" class="bx-soa-editstep"><?=$arParams['MESS_EDIT']?></a></div>
                                    </div>
                                    <div class="bx-soa-section-content container-fluid"></div>
                                </div>
                            </div>
						<? endif ?>

                        <!--	ORDER SAVE BLOCK	-->
                        <div id="bx-soa-orderSave" class="mf-checkout-button">
                            <a href="javascript:void(0)" class="button-link black-tr-b mf-submit-checkout" data-save-button="true">
								<?=$arParams['MESS_ORDER']?>
                            </a>

							<?/*<input type="submit" id="mf-submit-checkout" class="button-link black-tr-b" value="Оформить заказ">*/?>

                            <p class="confirm-order"><?=Loc::getMessage('MESS_CONFIRM_ORDER_TEXT')?></p>

                            <div class="checkbox">
								<?
								if ($arParams['USER_CONSENT'] === 'Y')
								{
									$APPLICATION->IncludeComponent(
										'bitrix:main.userconsent.request',
										'',
										array(
											'ID' => $arParams['USER_CONSENT_ID'],
											'IS_CHECKED' => $arParams['USER_CONSENT_IS_CHECKED'],
											'IS_LOADED' => $arParams['USER_CONSENT_IS_LOADED'],
											'AUTO_SAVE' => 'N',
											'SUBMIT_EVENT_NAME' => 'bx-soa-order-save',
											'REPLACE' => array(
												'button_caption' => isset($arParams['~MESS_ORDER']) ? $arParams['~MESS_ORDER'] : $arParams['MESS_ORDER'],
												'fields' => $arResult['USER_CONSENT_PROPERTY_DATA']
											)
										)
									);
								}
								?>
                            </div>
                        </div>

                        <div style="display: none;">
                            <div id='bx-soa-basket-hidden' class="bx-soa-section"></div>
                            <div id='bx-soa-region-hidden' class="bx-soa-section"></div>
                            <div id='bx-soa-coupons-hidden' class="bx-soa-section"></div>
                            <div id='bx-soa-paysystem-hidden' class="bx-soa-section"></div>
                            <div id='bx-soa-delivery-hidden' class="bx-soa-section"></div>
                            <div id='bx-soa-pickup-hidden' class="bx-soa-section"></div>
                            <div id="bx-soa-properties-hidden" class="bx-soa-section"></div>
                            <div id="bx-soa-auth-hidden" class="bx-soa-section">
                                <div class="bx-soa-section-content container-fluid reg"></div>
                            </div>
                        </div>
                    </div>

                    <!--	SIDEBAR BLOCK	-->
                    <div id="bx-soa-total" class="cart-form-container bx-soa-sidebar">
                        <div class="bx-soa-cart-total-ghost"></div>
                        <div class="bx-soa-cart-total"></div>
                    </div>
                </div>
            </form>

            <div id="bx-soa-saved-files" style="display:none"></div>
            <div id="bx-soa-soc-auth-services" style="display:none">
				<?
				$arServices = false;
				$arResult['ALLOW_SOCSERV_AUTHORIZATION'] = Main\Config\Option::get('main', 'allow_socserv_authorization', 'Y') != 'N' ? 'Y' : 'N';
				$arResult['FOR_INTRANET'] = false;

				if (Main\ModuleManager::isModuleInstalled('intranet') || Main\ModuleManager::isModuleInstalled('rest'))
					$arResult['FOR_INTRANET'] = true;

				if (Main\Loader::includeModule('socialservices') && $arResult['ALLOW_SOCSERV_AUTHORIZATION'] === 'Y')
				{
					$oAuthManager = new CSocServAuthManager();
					$arServices = $oAuthManager->GetActiveAuthServices(array(
						'BACKURL' => $this->arParams['~CURRENT_PAGE'],
						'FOR_INTRANET' => $arResult['FOR_INTRANET'],
					));

					if (!empty($arServices))
					{
						$APPLICATION->IncludeComponent(
							'bitrix:socserv.auth.form',
							'flat',
							array(
								'AUTH_SERVICES' => $arServices,
								'AUTH_URL' => $arParams['~CURRENT_PAGE'],
								'POST' => $arResult['POST'],
							),
							$component,
							array('HIDE_ICONS' => 'Y')
						);
					}
				}
				?>
            </div>

            <div style="display: none">
				<?
				// we need to have all styles for sale.location.selector.steps, but RestartBuffer() cuts off document head with styles in it
				$APPLICATION->IncludeComponent(
					'bitrix:sale.location.selector.steps',
					'.default',
					array(),
					false
				);
				$APPLICATION->IncludeComponent(
					'bitrix:sale.location.selector.search',
					'.default',
					array(),
					false
				);
				?>
            </div>
		<?
		$signer = new Main\Security\Sign\Signer;
		$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.order.ajax');
		$messages = Loc::loadLanguageFile(__FILE__);
		?>
            <script>
                BX.message(<?=CUtil::PhpToJSObject($messages)?>);
                BX.Sale.OrderAjaxComponentExt.init({
                    result: <?=CUtil::PhpToJSObject($arResult['JS_DATA'])?>,
                    locations: <?=CUtil::PhpToJSObject($arResult['LOCATIONS'])?>,
                    params: <?=CUtil::PhpToJSObject($arParams)?>,
                    signedParamsString: '<?=CUtil::JSEscape($signedParams)?>',
                    siteID: '<?=CUtil::JSEscape($component->getSiteId())?>',
                    ajaxUrl: '<?=CUtil::JSEscape($component->getPath().'/ajax.php')?>',
                    templateFolder: '<?=CUtil::JSEscape($templateFolder)?>',
                    propertyValidation: true,
                    showWarnings: true,
                    pickUpMap: {
                        defaultMapPosition: {
                            lat: 55.76,
                            lon: 37.64,
                            zoom: 7
                        },
                        secureGeoLocation: false,
                        geoLocationMaxTime: 5000,
                        minToShowNearestBlock: 3,
                        nearestPickUpsToShow: 3
                    },
                    propertyMap: {
                        defaultMapPosition: {
                            lat: 55.76,
                            lon: 37.64,
                            zoom: 7
                        }
                    },
                    orderBlockId: 'bx-soa-order',
                    authBlockId: 'bx-soa-auth',
                    basketBlockId: 'bx-soa-basket',
                    regionBlockId: 'bx-soa-region',
                    couponsBlockId: 'bx-soa-coupons',
                    paySystemBlockId: 'bx-soa-paysystem',
                    deliveryBlockId: 'bx-soa-delivery',
                    pickUpBlockId: 'bx-soa-pickup',
                    propsBlockId: 'bx-soa-properties',
                    totalBlockId: 'bx-soa-total'
                });
            </script>
            <script>
				<?
				// spike: for children of cities we place this prompt
				$city = \Bitrix\Sale\Location\TypeTable::getList(array('filter' => array('=CODE' => 'CITY'), 'select' => array('ID')))->fetch();
				?>
                BX.saleOrderAjax.init(<?=CUtil::PhpToJSObject(array(
					'source' => $component->getPath().'/get.php',
					'cityTypeId' => intval($city['ID']),
					'messages' => array(
						'otherLocation' => '--- '.Loc::getMessage('SOA_OTHER_LOCATION'),
						'moreInfoLocation' => '--- '.Loc::getMessage('SOA_NOT_SELECTED_ALT'), // spike: for children of cities we place this prompt
						'notFoundPrompt' => '<div class="-bx-popup-special-prompt">'.Loc::getMessage('SOA_LOCATION_NOT_FOUND').'.<br />'.Loc::getMessage('SOA_LOCATION_NOT_FOUND_PROMPT', array(
								'#ANCHOR#' => '<a href="javascript:void(0)" class="-bx-popup-set-mode-add-loc">',
								'#ANCHOR_END#' => '</a>'
							)).'</div>'
					)
				))?>);
            </script>
		<?
		if ($arParams['SHOW_PICKUP_MAP'] === 'Y' || $arParams['SHOW_MAP_IN_PROPS'] === 'Y')
		{
		if ($arParams['PICKUP_MAP_TYPE'] === 'yandex')
		{
		$this->addExternalJs($templateFolder.'/scripts/yandex_maps.js');
		?>
            <script src="<?=$scheme?>://api-maps.yandex.ru/2.1.50/?load=package.full&lang=<?=$locale?>"></script>
            <script>
                (function bx_ymaps_waiter(){
                    if (typeof ymaps !== 'undefined' && BX.Sale && BX.Sale.OrderAjaxComponentExt)
                        ymaps.ready(BX.proxy(BX.Sale.OrderAjaxComponentExt.initMaps, BX.Sale.OrderAjaxComponentExt));
                    else
                        setTimeout(bx_ymaps_waiter, 100);
                })();
            </script>
		<?
		}

		if ($arParams['PICKUP_MAP_TYPE'] === 'google')
		{
		$this->addExternalJs($templateFolder.'/scripts/google_maps.js');
		$apiKey = htmlspecialcharsbx(Main\Config\Option::get('fileman', 'google_map_api_key', ''));
		?>
            <script async defer
                    src="<?=$scheme?>://maps.googleapis.com/maps/api/js?key=<?=$apiKey?>&callback=bx_gmaps_waiter">
            </script>
            <script>
                function bx_gmaps_waiter()
                {
                    if (BX.Sale && BX.Sale.OrderAjaxComponentExt)
                        BX.Sale.OrderAjaxComponentExt.initMaps();
                    else
                        setTimeout(bx_gmaps_waiter, 100);
                }
            </script>
		<?
		}
		}

		if ($arParams['USE_YM_GOALS'] === 'Y')
		{
		?>
            <script>
                (function bx_counter_waiter(i){
                    i = i || 0;
                    if (i > 50)
                        return;

                    if (typeof window['yaCounter<?=$arParams['YM_GOALS_COUNTER']?>'] !== 'undefined')
                        BX.Sale.OrderAjaxComponentExt.reachGoal('initialization');
                    else
                        setTimeout(function(){bx_counter_waiter(++i)}, 100);
                })();
            </script>
			<?
		}
		}
		?>
    </div>
</section>