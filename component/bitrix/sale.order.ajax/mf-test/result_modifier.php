<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Service\GeoIp;

/**
 * @var array $arParams
 * @var array $arResult
 * @var SaleOrderAjax $component
 */

$component = $this->__component;
$component::scaleImages($arResult['JS_DATA'], $arParams['SERVICES_IMAGES_SCALING']);
$arSelect = ['*', 'UF_COUNTRY', 'UF_REGION', 'UF_CITY'];

$arFilter = [
	'ACTIVE' => 'Y',
	'ISSUING_CENTER' => 'Y',
];

$dbResult = CCatalogStore::GetList(
	array('PRODUCT_ID'=>'ASC','ID' => 'ASC'),
	$arFilter,
	false,
	false,
	$arSelect
);

while ($row = $dbResult->getNext())
{
	foreach ($arResult['JS_DATA']['STORE_LIST'] as &$arStore)
	{
		if($arStore['ID'] == $row['ID'])
		{
			$arStore['UF_COUNTRY'] = $row['UF_COUNTRY'];
			$arStore['UF_REGION'] = $row['UF_REGION'];
			$arStore['UF_CITY'] = $row['UF_CITY'];
		}
	}
	unset($arStore);
}

// по IP вычисляем страну
$ipAddress = GeoIp\Manager::getRealIp();
$geoResult = GeoIp\Manager::getDataResult($ipAddress, "ru", ["countryName"]);
if($geoResult)
{
	$geoData = $geoResult->getGeoData();
	$arResult['JS_DATA']['LOCATION_COUNTRY'] = $geoData->countryName;
}


if(!empty($_SESSION['BASKET_USER_DATA']))
{
	foreach ($arResult['JS_DATA']['ORDER_PROP']['properties'] as &$arProp)
	{
//		pr($arProp['CODE']);
		switch ($arProp['CODE'])
		{
			case 'EMAIL':

				if(!empty($_SESSION['BASKET_USER_DATA']['email']))
				{
					$arProp['VALUE'][0] = $_SESSION['BASKET_USER_DATA']['email'];
				}
				break;

			case 'PHONE':

				if(!empty($_SESSION['BASKET_USER_DATA']['phone']))
				{
					$arProp['VALUE'][0] = $_SESSION['BASKET_USER_DATA']['phone'];
				}
				break;

			case 'NAME':
				if(!empty($_SESSION['BASKET_USER_DATA']['username']))
				{
					$arProp['VALUE'][0] = $_SESSION['BASKET_USER_DATA']['username'];
				}
				break;
		}
	}

}
//var_dump($_SESSION['BASKET_USER_DATA']);