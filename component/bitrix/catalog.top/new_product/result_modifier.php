<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogTopComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

//pr($arResult['ITEMS']);

foreach ($arResult['ITEMS'] as &$arItem)
{
	if(is_array($arItem["PREVIEW_PICTURE"]))
	{
		$arFile = CFile::ResizeImageGet($arItem["DETAIL_PICTURE"]['ID'], Array("width"=>335, "height"=>418), BX_RESIZE_IMAGE_EXACT);
		$arItem["DETAIL_PICTURE"]['SRC'] = $arFile['src'];
	}
}
unset($arItem);