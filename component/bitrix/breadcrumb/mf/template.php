<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$strReturn = '';

$domain = getDomainUrl();

//we can't use $APPLICATION->SetAdditionalCSS() here because we are inside the buffered function GetNavChain()
//$css = $APPLICATION->GetCSSArray();
//if(!is_array($css) || !in_array("/bitrix/css/main/font-awesome.css", $css))
//{
//	$strReturn .= '<link href="'.CUtil::GetAdditionalFileURL("/bitrix/css/main/font-awesome.css").'" type="text/css" rel="stylesheet" />'."\n";
//}

$strReturn .= '<ul class="nav-breadcrumbs-list" itemprop="http://schema.org/breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">';

$itemSize = count($arResult);
for($index = 0; $index < $itemSize; $index++)
{
	if($arResult[$index]["LINK"] == '/catalog/detyam/devochkam/po-vozrastu/' ||$arResult[$index]["LINK"] == '/catalog/detyam/malchikam/po-vozrastu/') continue;

	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);
	$arrow = ($index > 0? '<span class="bc-char">></span>' : '');

	if($arResult[$index]["LINK"] <> "" && $index != $itemSize-1)
	{
		$arResult[$index]["LINK"] = $domain . $arResult[$index]["LINK"];
		$strReturn .= '
			<li id="bx_breadcrumb_'.$index.'" itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
				'.$arrow.'
				<a href="'.$arResult[$index]["LINK"].'" title="'.$title.'" itemprop="item">
					<span itemprop="name">'.$title.'</span>
				</a>
				<meta itemprop="position" content="'.($index + 1).'" />
			</li>';
	}
	else
	{
		$strReturn .= '
			<li>
				'.$arrow.'
				<span class="bc-current-link">'.$title.'</span>
			</li>';
//		$strReturn .= '
//			<li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
//				'.$arrow.'
//				<span class="bc-current-link" itemprop="name">'.$title.'</span>
//				<meta itemprop="position" content="'.($index + 1).'" />
//			</li>';
	}
}

$strReturn .= '</ul>';

return $strReturn;
