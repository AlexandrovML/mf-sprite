<?
$MESS ['P_ID'] = "Код";
$MESS ['P_NAME'] = "Название";
$MESS ['P_PERSON_TYPE_ID'] = "Тип плательщика";
$MESS ['P_DATE_UPDATE'] = "Дата обновления";
$MESS ['SALE_ACTION'] = "Действия";
$MESS ['SALE_DETAIL'] = "Изменить";
$MESS ['SALE_DELETE'] = "Удалить";
$MESS ['SALE_DETAIL_DESCR'] = "Изменить профиль";
$MESS ['SALE_DELETE_DESCR'] = "Удалить профиль";
$MESS["STPPL_DELETE_CONFIRM"]="Вы уверены, что хотите удалить этот профиль?";
$MESS["STPPL_EMPTY_PROFILE_LIST"]="Список профилей пуст";

$MESS["ADD_PHOTO"] = "добавить фото";
$MESS["MY_DATA"] = "Мои данные";
$MESS["MY_DATA_SMALL"] = "управление личной информацией";
$MESS["MY_FAVORITE"] = "Мои избранные";
$MESS["MY_FAVORITE_SMALL"] = "просмотр отложенных товаров";
$MESS["MY_BUYES"] = "Мои покупки";
$MESS["HISTORY_BUY"] = "история заказов";
$MESS["EXIT"] = "Выйти";
$MESS["SECURITY"] = "Безопасность";


$MESS["PROFILE_BUY"] = "Адреса доставки";
$MESS["PROFILE_BUY_SMALL"] = "Профили покупателя";
$MESS["MY_ADRESSES"] = "Мои адреса доставки";

$MESS["STREET"] = "Улица";
$MESS["CITY"] = "Город";
$MESS["HOUSE"] = "Дом";
$MESS["CORPUS"] = "Корпус";
$MESS["KV"] = "Квартира";
$MESS["NAME_PROFILE"] = "Название профиля";


$MESS["NO_ADRESS"] = "Адреса доставки отсутствуют. Адреса доставки будут заполнены после оформлении заказа и доступны в данном разделе для редактирования.";




?>
