<?
$i = 0;
foreach ($arResult['PROFILES'] as $item) {
    $db_propVals = CSaleOrderUserPropsValue::GetList(array("ID" => "ASC"), Array("USER_PROPS_ID" => $item['ID']));
    while ($arPropVals = $db_propVals->Fetch()) {
        // местоположение
        if ($arPropVals['ORDER_PROPS_ID'] == 6) {
            $arLocs = CSaleLocation::GetByID($arPropVals['VALUE'], LANGUAGE_ID);
            if (strlen($arLocs['CITY_NAME_LANG']) > 0) {
                $arResult['PROFILES'][$i]['CITY'] = $arLocs['CITY_NAME_LANG'];
            } else {
                if (strlen($arLocs['REGION_NAME_LANG']) > 0) {
                    $arResult['PROFILES'][$i]['CITY'] = $arLocs['REGION_NAME_LANG'];
                } else {
                    if (strlen($arLocs['COUNTRY_NAME_LANG']) > 0) {
                        $arResult['PROFILES'][$i]['CITY'] = $arLocs['COUNTRY_NAME_LANG'];
                    } else {
                        $arResult['PROFILES'][$i]['CITY'] = '';
                    }
                }
            }
        }
        // улица
        if ($arPropVals['ORDER_PROPS_ID'] == 22) {
            $arResult['PROFILES'][$i]['STREET'] = $arPropVals['VALUE'];
        }
        // дом
        if ($arPropVals['ORDER_PROPS_ID'] == 23) {
            $arResult['PROFILES'][$i]['HOUSE'] = $arPropVals['VALUE'];
        }
        // корпус
        if ($arPropVals['ORDER_PROPS_ID'] == 24) {
            $arResult['PROFILES'][$i]['CORPUS'] = $arPropVals['VALUE'];
        }
        // квартира
        if ($arPropVals['ORDER_PROPS_ID'] == 25) {
            $arResult['PROFILES'][$i]['KV'] = $arPropVals['VALUE'];
        }
    }
    $i++;
}
