<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
    <div class="search_page-input <?if($arResult['SEARCH_AREA']=='4'):?>camera--active<?endif;?>">
        <form action="" method="get">
            <div class="api-search-fields">

				<?if($arParams["USE_SUGGEST"] === "Y"):
					if(strlen($arResult["REQUEST"]["~QUERY"]) && is_object($arResult["NAV_RESULT"]))
					{
						$arResult["FILTER_MD5"] = $arResult["NAV_RESULT"]->GetFilterMD5();
						$obSearchSuggest = new CSearchSuggest($arResult["FILTER_MD5"], $arResult["REQUEST"]["~QUERY"]);
						$obSearchSuggest->SetResultCount($arResult["NAV_RESULT"]->NavRecordCount);
					}
					?>
					<?$APPLICATION->IncludeComponent(
					"bitrix:search.suggest.input",
					"",
					array(
						"NAME" => "q",
						"VALUE" => $arResult["REQUEST"]["~QUERY"],
						"INPUT_SIZE" => 40,
						"DROPDOWN_SIZE" => 10,
						"FILTER_MD5" => $arResult["FILTER_MD5"],
					),
					$component, array("HIDE_ICONS" => "Y")
				);?>
				<?else:?>
                <?if($arResult['SEARCH_AREA']=='4'):?>
                    <?unset($arResult["REQUEST"]["QUERY"]);?>
                <?endif;?>
                    <input <?if($arResult['SEARCH_AREA']=='4'):?> disabled <?endif;?> type="text" class="input_text api-search-input" name="q" value="<?=$arResult["REQUEST"]["QUERY"]?>" placeholder="Поиск" size="40" />
				<?endif;?>
                
                <div class="camera-popup camera-popup__search_page" >
                    <a href="/ajax/popup_camera.php" class="camera-popup__link popup-ajax"></a>
                </div>
                
                <div class="text_search">
                    <span>Искать по:</span>
                    <select name="search_area" id="" class="custom_sel search-area-control">
                        <option value="1"<?= $arResult['SEARCH_AREA']=='1'?' selected':'' ?>>Сайту</option>
                        <option value="2"<?= $arResult['SEARCH_AREA']=='2'?' selected':'' ?>>Каталогу</option>
                        <option value="3"<?= $arResult['SEARCH_AREA']=='3'?' selected':'' ?>>Новостям</option>
                        <option value="4"<?= $arResult['SEARCH_AREA']=='4'?' selected':'' ?>>Фото</option>
                    </select>
                </div>
            </div>
            <button type="submit" <?if($arResult['SEARCH_AREA']=='4'):?> disabled <?endif;?> class="input_sbmt" value="<?=GetMessage("SEARCH_GO")?>"> </button>
            <input type="hidden" name="how" value="<?echo $arResult["REQUEST"]["HOW"]=="d"? "d": "r"?>" />
			<?if($arParams["SHOW_WHEN"]):?>
                <script>
                    var switch_search_params = function()
                    {
                        var sp = document.getElementById('search_params');
                        var flag;
                        var i;

                        if(sp.style.display == 'none')
                        {
                            flag = false;
                            sp.style.display = 'block'
                        }
                        else
                        {
                            flag = true;
                            sp.style.display = 'none';
                        }

                        var from = document.getElementsByName('from');
                        for(i = 0; i < from.length; i++)
                            if(from[i].type.toLowerCase() == 'text')
                                from[i].disabled = flag;

                        var to = document.getElementsByName('to');
                        for(i = 0; i < to.length; i++)
                            if(to[i].type.toLowerCase() == 'text')
                                to[i].disabled = flag;

                        return false;
                    }
                </script>
                <br /><a class="search-page-params" href="#" onclick="return switch_search_params()"><?echo GetMessage('CT_BSP_ADDITIONAL_PARAMS')?></a>
                <div id="search_params" class="search-page-params" style="display:<?echo $arResult["REQUEST"]["FROM"] || $arResult["REQUEST"]["TO"]? 'block': 'none'?>">
					<?$APPLICATION->IncludeComponent(
						'bitrix:main.calendar',
						'',
						array(
							'SHOW_INPUT' => 'Y',
							'INPUT_NAME' => 'from',
							'INPUT_VALUE' => $arResult["REQUEST"]["~FROM"],
							'INPUT_NAME_FINISH' => 'to',
							'INPUT_VALUE_FINISH' =>$arResult["REQUEST"]["~TO"],
							'INPUT_ADDITIONAL_ATTR' => 'size="10"',
						),
						null,
						array('HIDE_ICONS' => 'Y')
					);?>
                </div>
			<?endif?>
        </form>
    </div>

	<?if(isset($arResult["REQUEST"]["ORIGINAL_QUERY"])):
		?>
        <div class="search-language-guess">
			<?echo GetMessage("CT_BSP_KEYBOARD_WARNING", array("#query#"=>'<a href="'.$arResult["ORIGINAL_QUERY_URL"].'">'.$arResult["REQUEST"]["ORIGINAL_QUERY"].'</a>'))?>
        </div><br /><?
	endif;?>