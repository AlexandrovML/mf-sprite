<? foreach ($arResult["QUESTIONS"] as $arQuestion) { ?>
	<div class="opros-img">
		<img src="<?= $arQuestion['IMAGE']['SRC'] ?>" alt="<?= $arQuestion['IMAGE']['QUESTION'] ?>">
		<span><?= $arQuestion['QUESTION'] ?></span>
	</div>
<? } ?>

<? foreach ($arResult["QUESTIONS"] as $arQuestion) { ?>
	<?foreach ($arQuestion["ANSWERS"] as $arAnswer) { ?>
		<label class="opros-answer <?= !empty($arUserCheckedAnswers[$arQuestion['ID']][$arAnswer['ID']]) ? 'choice' : ''?>">
			<input type="checkbox" name="opros-answer">
			<div class="opros-answer__bg"></div>
			<span><?= $arQuestion['QUESTION'] ?></span>
			<span class="opros-num">
                        <? if (!empty($arUserCheckedAnswers[$arQuestion['ID']][$arAnswer['ID']])) { ?>
	                        <svg width="13" height="10" viewBox="0 0 13 10" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 4.76471L4.80769 9L12 1" stroke="#ad1380"></path></svg>
                        <? } ?>
				<?= $arAnswer["PERCENT"] ?>%
                    </span>
		</label>

		<? break ?>
	<? } ?>
<? } ?>