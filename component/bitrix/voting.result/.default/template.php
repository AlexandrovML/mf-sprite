<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
global $DB;
require_once ($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/vote/vote_tools.php");
if (!empty($arResult["ERROR_MESSAGE"])):
?>
<div class="vote-note-box vote-note-error">
	<div class="vote-note-box-text"><?=ShowError($arResult["ERROR_MESSAGE"])?></div>
</div>
<?
endif;

/*if (!empty($arResult["OK_MESSAGE"])):
?>
<div class="vote-note-box vote-note-note">
	<div class="vote-note-box-text"><?=ShowNote($arResult["OK_MESSAGE"])?></div>
</div>
<?
endif;*/

if (empty($arResult["VOTE"]) || empty($arResult["QUESTIONS"]) ):
	return true;
endif;

use Bitrix\Main\Grid\Declension;
$voteDeclension = new Declension('человек', 'человека', 'человек');

$arUserCheckedAnswers = [];
$bUserIdVoted = false;
//pr($arResult);
//pr($arResult['QUESTIONS']);

// если пользователь авторизован, то ищем его ответы на данный опрос, если найдутся....
if ($USER->IsAuthorized())
{
    $sql = "SELECT `ID` FROM `b_vote_user` WHERE `AUTH_USER_ID` = " . $USER->getId();
	$arVoteUserIds = [];
	$db_res = $DB->Query($sql);
	while ($res = $db_res->Fetch())
    {
	    $arVoteUserIds[$res['ID']] = $res['ID'];
    }

	$arFilter = ['VOTE_ID' => $arResult['VOTE']['ID']];

	$by = 'ID';
	$order = 'desc';
	$is_filtered = null;
	$rsData = CVoteEvent::GetList($by, $order, $arFilter, $is_filtered);
	while ($arRes = $rsData->fetch())
	{
		if (isset($arVoteUserIds[$arRes['VOTE_USER_ID']]))
        {
	        $bUserIdVoted = true;

	        $VOTE_ID = $arRes['VOTE_ID'];
	        $EVENT_ID = $arRes['ID'];
	        GetVoteDataByID($VOTE_ID, $arChannel, $arVote, $arQuestions, $arAnswers, $arDropDown, $arMultiSelect, $arGroupAnswers, "N");

			foreach ($arQuestions as $key=>$arQuestion)
			{

//	    pr($arQuestion);
				$QUESTION_ID = $arQuestion["ID"];

				if (!array_key_exists($QUESTION_ID, $arAnswers))
					continue;


				reset($arAnswers[$QUESTION_ID]);

				$show_multiselect = "N";
				$show_dropdown = "N";

				foreach ($arAnswers[$QUESTION_ID] as $a_key => $arAnswer)
				{
//            pr($arAnswer);
					switch ($arAnswer["FIELD_TYPE"]) :
						case 0:
							$field_name = "vote_radio_".$QUESTION_ID;
							$checked = (CVoteEvent::GetAnswer($EVENT_ID,$arAnswer["ID"])) ? "checked" : "";
							$arUserCheckedAnswers[$arQuestion["ID"]][$arAnswer["ID"]] = $checked;
//			        var_dump($checked);
							/*?><input type="radio" name="<?=$field_name?>" value="<?=$arAnswer["ID"]?>" <?=$checked?>><font class="text">&nbsp;<?=$arAnswer["MESSAGE"]?></font><?*/
							break;
						case 1:
							$field_name = "vote_checkbox_".$QUESTION_ID;
							$checked = (CVoteEvent::GetAnswer($EVENT_ID,$arAnswer["ID"])) ? "checked" : "";
							$arUserCheckedAnswers[$arQuestion["ID"]][$arAnswer["ID"]] = $checked;

//			        var_dump($checked);
							/*?><input type="checkbox" name="<?=$field_name?>[]" value="<?=$arAnswer["ID"]?>" <?=$checked?>><font class="text">&nbsp;<?=$arAnswer["MESSAGE"]?></font><?*/
							break;

						case 2:
							if ($show_dropdown!="Y")
							{
								$field_name = "vote_dropdown_".$QUESTION_ID;
								$arDropDown[$QUESTION_ID]["reference"] = $arDropDown[$QUESTION_ID]["~reference"];
								foreach ($arDropDown[$QUESTION_ID]["reference_id"] as $q)
								{
									$selected = CVoteEvent::GetAnswer($EVENT_ID,$q);
									if (intval($selected)>0) break;
								}
//				        echo SelectBoxFromArray($field_name, $arDropDown[$QUESTION_ID], $selected, "", $arAnswer["FIELD_PARAM"]);
								$show_dropdown = "Y";
							}
							break;

						case 3:
							if ($show_multiselect!="Y")
							{
								$field_name = "vote_multiselect_".$QUESTION_ID;
								$arr = array();
								$arMultiSelect[$QUESTION_ID]["reference"] = $arMultiSelect[$QUESTION_ID]["~reference"];
								foreach ($arMultiSelect[$QUESTION_ID]["reference_id"] as $q)
								{
									$selected = CVoteEvent::GetAnswer($EVENT_ID,$q);
									if (intval($selected)>0) $arr[] = intval($selected);
								}
								echo SelectBoxMFromArray($field_name."[]", $arMultiSelect[$QUESTION_ID], $arr, "", false, $arAnswer["FIELD_HEIGHT"], $arAnswer["FIELD_PARAM"]);
								$show_multiselect = "Y";
							}
							break;

						case 4:
							$field_name = "vote_field_".$arAnswer["ID"];
							$value = CVoteEvent::GetAnswer($EVENT_ID,$arAnswer["ID"]);
							/*?><?if (strlen(trim($arAnswer["MESSAGE"]))>0):?><font class="text"><?=$arAnswer["MESSAGE"]?></font><br><?endif?><input type="text" name="<?=$field_name?>" value="<?=htmlspecialcharsbx($value)?>" size="<?=$arAnswer["FIELD_WIDTH"]?>" <?=$arAnswer["FIELD_PARAM"]?>><?*/
							break;

						case 5:
							$field_name = "vote_memo_".$arAnswer["ID"];
							$text = CVoteEvent::GetAnswer($EVENT_ID,$arAnswer["ID"]);
							/*?><font class="text"><?if (strlen(trim($arAnswer["MESSAGE"]))>0) echo $arAnswer["MESSAGE"]."<br>"?></font><textarea name="<?=$field_name?>" <?=$arAnswer["FIELD_PARAM"]?> cols="<?=$arAnswer["FIELD_WIDTH"]?>" rows="<?=$arAnswer["FIELD_HEIGHT"]?>"><?=htmlspecialcharsbx($text)?></textarea><?*/
							break;

					endswitch;
				}
			}
        }
	}
}
?>
<div class="opros-tagline"><?= $arResult['VOTE']['DESCRIPTION'] ?></div>
<div class="opros-block">
    <div class="opros-question"><?= $arResult['VOTE']['TITLE'] ?></div>
    <div class="opros-body voted">
	    <?
	    reset($arResult['QUESTIONS']);
	    $arQuestion = current($arResult['QUESTIONS']);
	    if (!empty($arQuestion["IMAGE"])) include '_images.php';
	        else include '_text.php'
	    ?>

        <div class="opros-total">
            <? if ($bUserIdVoted) { ?>
                <span>Ваш голос учтён! Благодарим Вас за участие!</span><br>
            <? } ?>
            Проголосовало <b><?= $arResult['VOTE']['COUNTER'] ?></b> <?= $voteDeclension->get($arResult['VOTE']['COUNTER']); ?>
        </div>

	    <?if ($arParams["CAN_VOTE"] == "Y"):?>
            <div class="opros-vote">
                <a class="opros-vote__btn" href="<?=$APPLICATION->GetCurPageParam("", array("VOTE_ID","VOTING_OK","VOTE_SUCCESSFULL", "view_result"))?>"><?=GetMessage("VOTE_BACK")?></a>
            </div>
	    <?endif;?>
    </div>
</div>

<?/*
<ol class="vote-items-list vote-question-list voting-result-box">

<?
$iCount = 0;
foreach ($arResult["QUESTIONS"] as $arQuestion):
	$iCount++;
//pr($arQuestion);

?>
	<li class="vote-item-vote <?=($iCount == 1 ? "vote-item-vote-first " : "")?><?
				?><?=($iCount == count($arResult["QUESTIONS"]) ? "vote-item-vote-last " : "")?><?
				?><?=($iCount%2 == 1 ? "vote-item-vote-odd " : "vote-item-vote-even ")?><?
				?>">
		<div class="vote-item-header">

<?
	if ($arQuestion["IMAGE"] !== false):
?>
			<div class="vote-item-image"><img src="<?=$arQuestion["IMAGE"]["SRC"]?>" width="30" height="30" /></div>
<?
	endif;

?>
			<div class="vote-item-title vote-item-question"><?=$arQuestion["QUESTION"]?></div>
			<div class="vote-clear-float"></div>
		</div>
<?
	if ($arQuestion["DIAGRAM_TYPE"] == "circle"):
?>
			<table class="vote-answer-table">
				<tr>
					<td width="160"><img width="150" height="150" src="<?=$componentPath?>/draw_chart.php?qid=<?=$arQuestion["ID"]?>&dm=150" /></td>
					<td>
						<table class="vote-bar-table">
							<?foreach ($arQuestion["ANSWERS"] as $arAnswer):?>
								<tr>
									<td><div class="vote-bar-square" style="background-color:#<?=htmlspecialcharsbx($arAnswer["COLOR"])?>"></div></td>
									<td><nobr><?=$arAnswer["COUNTER"]?> (<?=$arAnswer["PERCENT"]?>%)</nobr></td>
									<td><?=htmlspecialcharsbx($arAnswer["~MESSAGE"])?></td>
								</tr>
							<?endforeach?>
						</table>
					</td>
				</tr>
			</table>

<?
	else://histogram
?>

			<table width="100%" class="vote-answer-table">
			<?foreach ($arQuestion["ANSWERS"] as $arAnswer):?>
				<? if (isset($arResult['GROUP_ANSWERS'][$arAnswer['ID']])):?>
					<tr><td></td><td style='vertical-align:middle;'><div style='width:80%; height:1px; background-color:#<?=htmlspecialcharsbx($arAnswer["COLOR"])?>;'></div></td></tr>
				<? endif; ?>
				<tr>
					<? $percent = round($arAnswer["BAR_PERCENT"] * 0.8); // (100% bar * 0.8) + (20% span counter) = 100% td ?>
						<td width="24%" style=''>
						<?=htmlspecialcharsbx($arAnswer["~MESSAGE"])?>
						<? if (isset($arResult['GROUP_ANSWERS'][$arAnswer['ID']])) 
						{
							if (trim($arAnswer["MESSAGE"]) != '') 
								echo '&nbsp';
							echo '('.GetMessage('VOTE_GROUP_TOTAL') .')';
						}
						?>
					&nbsp;</td>
					<td><div class="vote-answer-bar" style="width:<?=$percent?>%;background-color:#<?=htmlspecialcharsbx($arAnswer["COLOR"])?>"></div>
					<span class="vote-answer-counter"><nobr><?=($arAnswer["COUNTER"] > 0?'&nbsp;':'')?><?=$arAnswer["COUNTER"]?> (<?=$arAnswer["PERCENT"]?>%)</nobr></span></td>
					<? if (isset($arResult['GROUP_ANSWERS'][$arAnswer['ID']])): ?>
						<? $arGroupAnswers = $arResult['GROUP_ANSWERS'][$arAnswer['ID']]; ?> 
						</tr>
						<?foreach ($arGroupAnswers as $arGroupAnswer):?>
							<? $percent = round($arGroupAnswer["PERCENT"] * 0.8); // (100% bar * 0.8) + (20% span counter) = 100% td ?>
							<tr>
								<td width="24%">
									<? if (trim($arAnswer["MESSAGE"]) != '') { ?>
										<span class='vote-answer-lolight'><?=htmlspecialcharsbx($arAnswer["~MESSAGE"])?>:&nbsp;</span>
									<? } ?>
									<?=htmlspecialcharsbx($arGroupAnswer["~MESSAGE"])?>
								</td>
								<td><div class="vote-answer-bar" style="width:<?=$percent?>%;background-color:#<?=htmlspecialcharsbx($arAnswer["COLOR"])?>"></div>
								<span class="vote-answer-counter"><nobr><?=($arGroupAnswer["COUNTER"] > 0?'&nbsp;':'')?><?=$arGroupAnswer["COUNTER"]?> (<?=$arGroupAnswer["PERCENT"]?>%)</nobr></span></td>
							</tr>
						<?endforeach?>
						<tr><td></td><td style='vertical-align:middle;'><div style='width:80%; height:1px; background-color:#<?=htmlspecialcharsbx($arAnswer["COLOR"])?>;'></div></td></tr>
					<? else: ?>
				</tr>
					<? endif; // USER_ANSWERS ?>
			<?endforeach?>
			</table>
<?
	endif;
?>
	</li>
<?
endforeach;

?>
</ol>
<?/**/?>