<?php
$this->__component->arResult["PRODUCT_IDS_FOR_FILTER_LOOKBOOK"] = $arResult['PROPERTIES']['PRODUCT_LOOKBOOK']['VALUE'];
$this->__component->SetResultCacheKeys(["PRODUCT_IDS_FOR_FILTER_LOOKBOOK"]);

$this->__component->arResult["VIEW_LIST_ALL_PRODUCT"] = $arResult['PROPERTIES']['OTHER_LIST_PRODUCT']['VALUE_XML_ID'];
$this->__component->SetResultCacheKeys(["VIEW_LIST_ALL_PRODUCT"]);

$this->__component->arResult["IMG_PRODUCT_LIST"] = $arResult['PROPERTIES']['PHOTO_CATALOG']['VALUE'];
$this->__component->SetResultCacheKeys(["IMG_PRODUCT_LIST"]);