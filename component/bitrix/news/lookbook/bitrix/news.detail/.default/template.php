<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<? //pr($arResult['PREVIEW_TEXT']);?>
<?if(!empty($arResult["PREVIEW_TEXT"])):?>
<p><?= $arResult["PREVIEW_TEXT"] ?></p>
<?endif;?>
<section class="lookbook-present">
    <div class="present">
        <div class="lookbook-img num-1" data-animate="opacity-moveRight"
             style="background-image: url(<?= CFile::GetPath($arResult["PROPERTIES"]['PHOTO_LEFT_1']['VALUE']) ?>);"></div>
        <p class="present-desc">
            <?= $arResult["PROPERTIES"]['TEXT_2']['VALUE'] ?>
        </p>
        <div class="lookbook-img num-2" data-animate="opacity-moveUp"
             style="background-image: url(<?= CFile::GetPath($arResult["PROPERTIES"]['PHOTO_LEFT_2']['VALUE']) ?>);"></div>
        <p class="present-desc with-line">
						<span class="line-container">
							<span class="line" data-animate="line-moveRight"></span>
						</span>
            <?= $arResult["PROPERTIES"]['TEXT_3']['VALUE'] ?>
        </p>
        <? if ($arResult["PROPERTIES"]['PHOTO_LEFT_3']['VALUE']) { ?>
            <div class="lookbook-img num-3" data-animate="opacity-moveRight"
                 style="background-image: url(<?= CFile::GetPath($arResult["PROPERTIES"]['PHOTO_LEFT_3']['VALUE']) ?>);"></div>
        <? } ?>
    </div>
    <div class="present">
        <p class="present-desc with-line">
						<span class="line-container">
							<span class="line" data-animate="line-moveLeft"></span>
						</span>
            <?= $arResult["PROPERTIES"]['TEXT_1']['VALUE'] ?>
        </p>
        <div class="lookbook-img num-4" data-animate="opacity-moveLeft"
             style="background-image: url(<?= CFile::GetPath($arResult["PROPERTIES"]['PHOTO_RIGHT_1']['VALUE']) ?>);"></div>

        <? if ($arResult['PROPERTIES']['IMG_VIDEO']['VALUE_XML_ID'] == "VIDEO") {
            if ($arResult['PROPERTIES']['VIDEO_RIGHT_2']['VALUE']) { ?>
                <div class="lookbook-img num-5 video-c">
                    <video controls="controls" class="">
                        <source src="<?= CFile::GetPath($arResult['PROPERTIES']['VIDEO_RIGHT_2']['VALUE']) ?>"
                                type='video/ogg; codecs="theora, vorbis"'>
                        <source src="<?= CFile::GetPath($arResult['PROPERTIES']['VIDEO_RIGHT_2']['VALUE']) ?>"
                                type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                        <source src="<?= CFile::GetPath($arResult['PROPERTIES']['VIDEO_RIGHT_2']['VALUE']) ?>"
                                type='video/webm; codecs="vp8, vorbis"'>
                    </video>
                </div>
            <? } else { ?>
                <div class='slide'
                     style="background-image: url(<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>) !important;"
                     id="<?= $this->GetEditAreaId($arResult['ID']); ?>">
                </div>
            <? } ?>
        <? } else { ?>
            <div class="lookbook-img num-5 lookbook_5_img" data-animate="opacity"
                 style="background-image: url(<?= CFile::GetPath($arResult["PROPERTIES"]['PHOTO_RIGHT_2']['VALUE']) ?>)  !important;"></div>

        <? } ?>

        <? if ($arResult["PROPERTIES"]['PHOTO_RIGHT_3']['VALUE']) { ?>
            <div class="lookbook-img num-6" data-animate="opacity"
                 style="background-image: url(<?= CFile::GetPath($arResult["PROPERTIES"]['PHOTO_RIGHT_3']['VALUE']) ?>)"></div>
        <? } ?>
    </div>
</section>

<section class="lookbook-zoom carousel-init">
    <ul class="owl-carousel">
        <? $photo_block = 1;
        foreach ($arResult["PROPERTIES"]['MORE_PHOTO']['VALUE'] as $item) {
            if ($photo_block == 1) {
                ?>
                <div class="item">
            <? } ?>
            <li>
                <div style="background-image: url(<?= CFile::GetPath($item) ?>);" class="lookbook-img"></div>
            </li>
            <? if ($photo_block == 3) {
                ?>
                </div>
                <?
                $photo_block = 0;
            }
            $photo_block++;
        } ?>
    </ul>
</section>

<section class="lookbook-zoom carousel-init for_mob">
    <ul class="owl-carousel">
        <? foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $item) { ?>
            <li class="item">
                <div style="background-image: url(<?= CFile::GetPath($item) ?>);" class="lookbook-img"></div>
            </li>
        <? } ?>

    </ul>
</section>
