<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$pos_block = 1;
$first_item = 0;
?>
<div class="fix-block">
   <!-- <?/*if ($arResult['SECTION']){
        $str = '';
        foreach ($arResult['SECTION']['PATH'] as $section){
            $str .= $section['NAME'].', ';
        }
        $str = substr($str, 0, -2);
        */?>
        <h1><?/*=$str*/?></h1>
    --><?/*}*/?>
    <section class="list-lookbooks">
        <? foreach ($arResult["ITEMS"] as $arItem): ?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            if ($pos_block == 1) { // слева картинка большая
                ?>
                <div class="lookbook-element" id="<?=$this->GetEditAreaId($arItem['ID'])?>" <? if ($first_item == 1) { echo 'data-preload-block';}?>>
                    <div class="lookbook-part-1 preview-img">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" data-img>
                            <img class="lookbook-prev-img" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                 alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                 title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"/>
                        </a>
                    </div>
                    <div class="lookbook-part-2 preview-text">
                        <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                            <div class='h3'><? echo $arItem["NAME"] ?></div>
                        </a>
                        <div class="line"></div>
                        <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                            <p><? echo $arItem["PREVIEW_TEXT"]; ?></p>
                        <? endif; ?>
                        <div class="lookbook-img-list">
                            <div class="lookbook-img-container">
                                <div class="lookbook-img-column">
                                    <a href="javascript:void(0);">
                                        <div data-img
                                             style="background-image:url(<?=CFile::GetPath($arItem["PROPERTIES"]['LEFT_VERTICAL_PHOTO']['VALUE'])?>);"></div>
                                    </a>
                                </div>
                                <div class="lookbook-img-column">
                                    <a href="javascript:void(0);">
                                        <div data-img
                                             style="background-image:url(<?=CFile::GetPath($arItem["PROPERTIES"]['UP_GORIZONTAL_PHOTO']['VALUE'])?>);"></div>
                                    </a>
                                    <a href="javascript:void(0);">
                                        <div data-img
                                             style="background-image:url(<?=CFile::GetPath($arItem["PROPERTIES"]['DOWN_GORIZONTAL_PHOTO']['VALUE'])?>);"></div>
                                    </a>
                                </div>
                                <div class="lookbook-img-column">
                                    <a href="javascript:void(0);">
                                        <div data-img
                                             style="background-image:url(<?=CFile::GetPath($arItem["PROPERTIES"]['RIGHT_VERTICAL_PHOTO']['VALUE'])?>);"></div>
                                    </a>
                                </div>
                            </div>
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="button-link black-tr-b"><?=GetMessage('VIEW_LOOKBOOK');?></a>
                        </div>
                    </div>
                </div>
            <?  $first_item = 1;
                $pos_block = 2;
            } else {
                if ($pos_block == 2) { // справа картинка большая
                    ?>
                    <div class="lookbook-element" id="<?= $this->GetEditAreaId($arItem['ID']) ?>" <? if ($first_item == 1) { echo 'data-preload-block';}?>>
                        <div class="lookbook-part-1 preview-text">
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                                <div class='h3'><? echo $arItem["NAME"] ?></div>
                            </a>
                            <div class="line"></div>
                            <? if ($arParams["DISPLAY_PREVIEW_TEXT"] != "N" && $arItem["PREVIEW_TEXT"]): ?>
                                <p><? echo $arItem["PREVIEW_TEXT"]; ?></p>
                            <? endif; ?>
                            <div class="lookbook-img-list">
                                <div class="lookbook-img-container">
                                    <div class="lookbook-img-column">
                                        <a href="javascript:void(0);">
                                            <div data-img
                                                 style="background-image:url(<?= CFile::GetPath($arItem["PROPERTIES"]['LEFT_VERTICAL_PHOTO']['VALUE']) ?>);"></div>
                                        </a>
                                    </div>
                                    <div class="lookbook-img-column">
                                        <a href="javascript:void(0);">
                                            <div data-img
                                                 style="background-image:url(<?= CFile::GetPath($arItem["PROPERTIES"]['UP_GORIZONTAL_PHOTO']['VALUE']) ?>);"></div>
                                        </a>
                                        <a href="javascript:void(0);">
                                            <div data-img
                                                 style="background-image:url(<?= CFile::GetPath($arItem["PROPERTIES"]['DOWN_GORIZONTAL_PHOTO']['VALUE']) ?>);"></div>
                                        </a>
                                    </div>
                                    <div class="lookbook-img-column">
                                        <a href="javascript:void(0);">
                                            <div data-img
                                                 style="background-image:url(<?= CFile::GetPath($arItem["PROPERTIES"]['RIGHT_VERTICAL_PHOTO']['VALUE']) ?>);"></div>
                                        </a>
                                    </div>
                                </div>
                                <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" class="button-link black-tr-b"><?=GetMessage('VIEW_LOOKBOOK');?></a>
                            </div>
                        </div>
                        <div class="lookbook-part-2 preview-img">
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>" data-img>
                                <img class="lookbook-prev-img" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                     alt="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"
                                     title="<?= $arItem["PREVIEW_PICTURE"]["TITLE"] ?>"/>
                            </a>
                        </div>
                    </div>
                <? }
                $pos_block = 1;
            }?>


        <? endforeach; ?>


    </section>
    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>