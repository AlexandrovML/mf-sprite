<?php

// находим следующий и предыдущий элементы на детальной странице новости
$arResult['PREV_ELEMENT'] = [];
$arResult['NEXT_ELEMENT'] = [];

$arRelated = [];

$query = CIBlockElement::GetList(array('ID' => 'ASC'), array(
	'IBLOCK_ID' => $arParams['IBLOCK_ID'],
	'ACTIVE' => 'Y'),
	false, array('nPageSize' => 1, 'nElementID' => $arResult['ID']),
	array('ID', 'DETAIL_PAGE_URL')
);
while($elem = $query->GetNextElement()){
	$arFields = $elem->GetFields();
	$arRelated[] = $arFields;
}

if(count($arRelated) == 3)
{
	// находимся где-то не с краю списка
	$arResult['PREV_ELEMENT'] = $arRelated[2];
	$arResult['NEXT_ELEMENT'] = $arRelated[0];
}
elseif($arRelated[1]['ID'] == $arResult['ID'])
{
	// находимся на первом элементе
	$arResult['NEXT_ELEMENT'] = $arRelated[0];
}
elseif($arRelated[0]['ID'] == $arResult['ID'])
{
	// находимся на последнем элементе
	$arResult['PREV_ELEMENT'] = $arRelated[1];
}

