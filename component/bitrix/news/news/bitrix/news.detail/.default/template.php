<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="fix-block">
    <section class="content-section">
        <?//pr($arResult['PROPERTIES']);
        if ($arResult['PROPERTIES']['VIEW_PREVIEW'] && $arResult['PROPERTIES']['VIEW_PREVIEW']['VALUE_XML_ID'] == 'Y'){?>
            <?if($arParams["PREVIEW_PICTURE"]!="N" && is_array($arResult["PREVIEW_PICTURE"])):?>
                <img
                        class="align-left"
                        border="0"
                        data-animate="opacity"
                        src="<?=$arResult["PREVIEW_PICTURE"]["SRC"]?>"
                        width="<?=$arResult["PREVIEW_PICTURE"]["WIDTH"]?>"
                        height="<?=$arResult["PREVIEW_PICTURE"]["HEIGHT"]?>"
                        alt="<?=$arResult["PREVIEW_PICTURE"]["ALT"]?>"
                        title="<?=$arResult["PREVIEW_PICTURE"]["TITLE"]?>"
                />
            <?endif?>
            <?if(strlen($arResult["DETAIL_TEXT"])>0) { ?>
                <?=$arResult["DETAIL_TEXT"];
            }?>
        <?}else{?>
            <div class="content_img">
                <?=$arResult["DETAIL_TEXT"];?>
            </div>
        <?}?>
    </section>
    <section class="pagination pagination-news">
        <?if(is_array($arResult["TOLEFT"])):?>
            <a href="<?=$arResult["TOLEFT"]["URL"]?>" class="news-moving">
                <svg class="prev-page-arrow" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="0 0 30 15" width="30" height="15"><defs><path d="M5.35 10.2L1.67 7.2" id="b18Z6TSLm1"></path><path d="M29 7.5L2.48 7.5" id="j3TXHzvyCG"></path><path d="M5.35 4.7L1.67 7.7" id="c10U1gTYy5"></path></defs><g><g><g><use xlink:href="#b18Z6TSLm1" opacity="1" fill="#000000" fill-opacity="1"></use><g><use xlink:href="#b18Z6TSLm1" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#j3TXHzvyCG" opacity="1" fill="#000000" fill-opacity="1"></use><g><use xlink:href="#j3TXHzvyCG" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#c10U1gTYy5" opacity="1" fill="#000000" fill-opacity="1"></use><g><use xlink:href="#c10U1gTYy5" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g></g></g></svg>
                <div class="news-mov-text">
                    <p><?=GetMessage('TOLEFT');?></p>
                    <p class="news-moving-title"><?=$arResult["TOLEFT"]["NAME"]?></p>
                </div>
            </a>
        <?endif?>
        <?if(is_array($arResult["TORIGHT"])):?>
            <a href="<?=$arResult["TORIGHT"]["URL"]?>" class="news-moving">
                <div class="news-mov-text">
                    <p><?=GetMessage('TORIGHT');?></p>
                    <p class="news-moving-title"><?=$arResult["TORIGHT"]["NAME"]?></p>
                </div>
                <svg class="next-page-arrow" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="0 0 30 15" width="30" height="15"><defs><path d="M25.32 10.2L29 7.2" id="a8311Vz9N"></path><path d="M25.32 4.7L29 7.7" id="bDevWo6z4"></path><path d="M1.67 7.5L28.2 7.5" id="cukLGz9lf"></path></defs><g><g><g><use xlink:href="#a8311Vz9N" opacity="1" fill="#000000" fill-opacity="1"></use><g><use xlink:href="#a8311Vz9N" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#bDevWo6z4" opacity="1" fill="#000000" fill-opacity="1"></use><g><use xlink:href="#bDevWo6z4" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g><g><use xlink:href="#cukLGz9lf" opacity="1" fill="#000000" fill-opacity="1"></use><g><use xlink:href="#cukLGz9lf" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="1" stroke-opacity="1"></use></g></g></g></g></svg>
            </a>
        <?endif?>
    </section>
</div>