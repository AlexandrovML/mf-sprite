<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="news-list news-page-container">
    <? if ($arParams["DISPLAY_TOP_PAGER"]): ?>
        <?= $arResult["NAV_STRING"] ?><br/>
    <? endif; ?>
    <div class="fix-block">
        <section class="news-section">
            <ul class="news-list">


                <? foreach ($arResult["ITEMS"] as $arItem) { ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    ?>
                    <li data-animate="opacity-moveUp" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                            <a href="<?=$arItem['DETAIL_PAGE_URL']?>" data-img class="news-list-image"
                                 style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>);"></a>
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                            <h3><? echo $arItem["NAME"] ?></h3>
                        </a>
                        <p><? echo mb_strimwidth($arItem["PREVIEW_TEXT"], 0, 140, "..."); ?></p>
                    </li>
                <? } ?>
            </ul>
        </section>
    </div>


    <? if ($arParams["DISPLAY_BOTTOM_PAGER"]): ?>
        <br/><?= $arResult["NAV_STRING"] ?>
    <? endif; ?>
</div>
