<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>

<script>
    $(document).ready(function () {
        $.magnificPopup.open({
            items: {src: '#changepasswd'},
            type: 'inline',
            preloader: false,
            modal: true
        });
    });
</script>


<div class="popup_shop popup_input popup_auth_form" id="changepasswd">
    <div class="name">
        <div class="red"><?=GetMessage('AUTH_CHANGE_PASSWORD');?></div>
    </div>
    <?
    ShowMessage($arParams["~AUTH_RESULT"]);
    ?>
    <div class="ss">
        <form class="forgot_form_err" name="bform" method="post" target="_top" action="<?=$arResult["AUTH_FORM"] ?>">
            <? if (strlen($arResult["BACKURL"]) > 0):  ?>
                <input type="hidden" name="backurl" value="<?=$arResult["BACKURL"] ?>" />
            <?  endif  ?>
            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="CHANGE_PWD">

            <div class="block">
                <label for=""><?= GetMessage("AUTH_LOGIN") ?><span class="starrequired">*</span></label>
                <input type="text" name="USER_LOGIN" maxlength="50" value="<?=$arResult["LAST_LOGIN"] ?>">
            </div>
            <div class="block">
                <label for=""><?= GetMessage("AUTH_CHECKWORD") ?><span class="starrequired">*</span></label>
                <input type="text" name="USER_CHECKWORD" maxlength="50" value="<?=$arResult["USER_CHECKWORD"] ?>">
            </div>
            <div class="block">
                <label for=""><?= GetMessage("AUTH_NEW_PASSWORD_REQ") ?><span class="starrequired">*</span></label>
                <input type="password" name="USER_PASSWORD" maxlength="50" value="<?=$arResult["USER_PASSWORD"] ?>" autocomplete="off">
            </div>
            <? if($arResult["SECURE_AUTH"]): ?>
                <span class="bx-auth-secure" id="bx_auth_secure" title="<? echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
                <noscript>
				<span class="bx-auth-secure" title="<? echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
                </noscript>
                <script type="text/javascript">
                    document.getElementById('bx_auth_secure').style.display = 'inline-block';
                </script>
            <? endif ?>
            <div class="block">
                <label for=""><?= GetMessage("AUTH_NEW_PASSWORD_CONFIRM") ?><span class="starrequired">*</span></label>
                <input type="password" name="USER_CONFIRM_PASSWORD" maxlength="50" value="<?=$arResult["USER_CONFIRM_PASSWORD"] ?>" autocomplete="off">
            </div>

            <? if ($arResult["USE_CAPTCHA"]): ?>
                <div class="block">
                    <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180"
                         height="40" alt="CAPTCHA"/>
                    <? echo GetMessage("system_auth_captcha") ?>
                    <input type="text" name="captcha_word" maxlength="50" value=""/>
                </div>
            <? endif ?>

            <div class="block login_btn_sign">
                <input type="submit" name="send_account_info" placeholder="Сменить" value="Сменить">
            </div>
        </form>
    </div>
</div>