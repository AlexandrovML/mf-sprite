<?
$MESS["CT_GIFTS_SECTION_LIST_BLOCK_TITLE_DEFAULT"] = "Подарки к товарам этого раздела";
$MESS["CT_SORT_DEFAULT"] = "Сортировать по";
$MESS["CT_SORT_PROPERTY_SALELEADER"] = "Хиты продаж";
$MESS["CT_SORT_PROPERTY_NEWPRODUCT"] = "Новинки";
$MESS["CT_SORT_PROPERTY_MINIMUM_PRICE_ASC"] = "Цена по возрастанию";
$MESS["CT_SORT_PROPERTY_MAXIMUM_PRICE_DESC"] = "Цена по убыванию";
$MESS["CT_SORT_DEFAULT"] = "По умолчанию";
