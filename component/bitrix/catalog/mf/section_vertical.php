<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var CBitrixComponent $component
 * @var array $arParams
 * @var array $arResult
 * @var array $arCurSection
 */

//if ($_SERVER['REMOTE_ADDR'] == '178.121.58.24')
//{
//	pr($APPLICATION->sDirPath);
//}

//pr($arResult);


if (isset($arParams['USE_COMMON_SETTINGS_BASKET_POPUP']) && $arParams['USE_COMMON_SETTINGS_BASKET_POPUP'] == 'Y')
{
	$basketAction = isset($arParams['COMMON_ADD_TO_BASKET_ACTION']) ? $arParams['COMMON_ADD_TO_BASKET_ACTION'] : '';
}
else
{
	$basketAction = isset($arParams['SECTION_ADD_TO_BASKET_ACTION']) ? $arParams['SECTION_ADD_TO_BASKET_ACTION'] : '';
}

$isAjax = false;
if ($_SERVER['REQUEST_METHOD'] == 'GET')
{
	$isAjax = (
		(isset($_GET['ajax_get_filter']) && $_GET['ajax_get_filter'] == 'Y')
		|| (isset($_GET['ajax_get']) && $_GET['ajax_get'] == 'Y')
	);
}


$sortField = !empty($_REQUEST['sort-field']) ? htmlspecialcharsbx($_REQUEST['sort-field']) : '';
$sortDir = !empty($_REQUEST['sort-dir']) ? htmlspecialcharsbx($_REQUEST['sort-dir']) : '';
$viewType = !empty($_REQUEST['view']) ? htmlspecialcharsbx($_REQUEST['view']) : '';





//pr($sortField);
//pr($sortDir);

if(!empty($sortField))
{
	switch ($sortField)
	{
		case 'sort':
			$sortField = 'sort';
			break;

		case 'hits':
			$sortField = 'PROPERTY_SALELEADER';
			break;

		case 'new':
			$sortField = 'PROPERTY_NEWPRODUCT';
			break;

		case 'price':
			switch ($sortDir)
			{
				case 'asc':
					$sortField = 'PROPERTY_MINIMUM_PRICE';
					break;

				case 'desc':
					$sortField = 'PROPERTY_MAXIMUM_PRICE';
					break;
			}

			break;

		default:
			$sortField = '';

	}

	$_SESSION['catalog_sort_field'] = $sortField;
}

if(!empty($sortDir))
{
	$_SESSION['catalog_sort_order'] = $sortDir;
}

if(empty($sortField))
{
	$sortField = !empty($_SESSION['catalog_sort_field']) ? $_SESSION['catalog_sort_field'] : '';
}

if(empty($sortDir))
{
	$sortDir = !empty($_SESSION['catalog_sort_order']) ? $_SESSION['catalog_sort_order'] : '';
}


//pr($sortField . ' - ' . $sortDir);
//pr($sortDir);

if(!empty($sortField))
{
	$arParams["ELEMENT_SORT_FIELD"] = $sortField;
}

if(!empty($sortDir))
{
	$arParams["ELEMENT_SORT_ORDER"] = $sortDir;
}

//pr(($arParams["ELEMENT_SORT_FIELD"] . ' -' . $arParams["ELEMENT_SORT_ORDER"]));


$activeSortMessage = 'CT_SORT_DEFAULT';
if(!empty($sortField))
{
	switch ($sortField)
	{
		case 'PROPERTY_SALELEADER':
			$activeSortMessage = 'CT_SORT_PROPERTY_SALELEADER';
			break;

		case 'PROPERTY_NEWPRODUCT':
			$activeSortMessage = 'CT_SORT_PROPERTY_NEWPRODUCT';
			break;

		case 'PROPERTY_MINIMUM_PRICE':
			$activeSortMessage = 'CT_SORT_PROPERTY_MINIMUM_PRICE_ASC';
			break;

		case 'PROPERTY_MAXIMUM_PRICE':
			$activeSortMessage = 'CT_SORT_PROPERTY_MAXIMUM_PRICE_DESC';
			break;

		default:
			$activeSortMessage = 'CT_SORT_DEFAULT';

	}

	$_SESSION['catalog_sort_field'] = $sortField;
}

$activeSort = Loc::getMessage($activeSortMessage);


if(!empty($viewType))
{
	$_SESSION['catalog_view_type'] = $viewType;
}

if(empty($viewType))
{
	$viewType = !empty($_SESSION['catalog_view_type']) ? $_SESSION['catalog_view_type'] : 'tile-small';
}

//$GLOBALS['arrFilter'] = array("!PROPERTY_SHOW_IN_CATALOG" => "N");

$checkBoxNovinli = 'arrFilter_6_2212294583';
$checkAge1_2 = 'arrFilter_161_1680248052';
$checkAge10_11 = 'arrFilter_161_2649248939';
$checkAge11 = 'arrFilter_161_446165065';
$checkAge11_12 = 'arrFilter_161_59556104';
$checkAge12 = 'arrFilter_161_1838203103';
$checkAge13_18 = 'arrFilter_161_3984760868';
$checkAge2_3 = 'arrFilter_161_1794966726';
$checkAge3_4 = 'arrFilter_161_2101198261';
$checkAge4_5 = 'arrFilter_161_171609379';
$checkAge5_6 = 'arrFilter_161_502936656';
$checkAge6_7 = 'arrFilter_161_2230543850';
$checkAge7_8 = 'arrFilter_161_3941557309';
$checkAge8 = 'arrFilter_161_4092876156';
$checkAge9_10 = 'arrFilter_161_2592444594';
$checkAge0_2 = 'arrFilter_161_2208211443';

$checkSHOW_IN_CATALOG = 'arrFilter_211_3233089245';
$checkNOT_SHOW_IN_RU = 'arrFilter_232_1130791706';

$sectionIdDevochkam = 955;
$sectionIdMalchikam = 980;

$sectionCodeDevochkam = 'devochkam';
$sectionCodeMalchikam = 'malchikam';

//var_dump($arParams["ELEMENT_SORT_FIELD"]);
//var_dump($arParams["ELEMENT_SORT_ORDER"]);
$hideFilterBlock = false;

//pr($arResult);
//var_dump($arParams["ELEMENT_SORT_FIELD"]);

if($arResult['VARIABLES']['SECTION_CODE_PATH'] == 'detyam/malchikam/po-vozrastu')
{
	LocalRedirect('/catalog/detyam/malchikam/');
}
elseif($arResult['VARIABLES']['SECTION_CODE_PATH'] == 'detyam/devochkam/po-vozrastu')
{
	LocalRedirect('/catalog/detyam/devochkam/');
}

$IPROPERTY = [];


$arSectionIdsForDisabledFilter = [1134];
$arSectionIdsForDisabledSorting = [1132, 1134];

if (in_array($arResult['VARIABLES']['SECTION_ID'], $arSectionIdsForDisabledSorting)) $isSorting = false;
else $isSorting = true;


if (in_array($arResult['VARIABLES']['SECTION_ID'], $arSectionIdsForDisabledFilter))
	$isFilter = false;
?>
<h1 class="js-h1-section"><?$APPLICATION->ShowTitle(false);?></h1>
<div class="fix-block">
	<? //include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/include/_bc.php'; ?>
	<?
	switch(SITE_ID){
		case "s2":
			$addBasket = '_ru';
			$addUrl = "/ru";
			break;
		case "s3":
			$addBasket = '_kz';
			$addUrl = "/kz";
			break;
	}
	if(!empty($arResult['SECTION_TAGS'])) { ?>
        <ul class="cat-tags-list">
			<? foreach ($arResult['SECTION_TAGS'] as $arTag) { ?>
                <li><a href="<?//=$addUrl?><?= $arTag['DETAIL_PAGE_URL'] ?>"><?= $arTag['NAME'] ?></a></li>
			<? } ?>
        </ul>
	<? } ?>

    <div class="catalog-options">
		<?
		$APPLICATION->IncludeComponent(
			"bitrix:catalog.section.list",
			"mf",
			array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
				"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"COUNT_ELEMENTS" => $arParams["SECTION_COUNT_ELEMENTS"],
				"TOP_DEPTH" => $arParams["SECTION_TOP_DEPTH"],
				"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
				"VIEW_MODE" => $arParams["SECTIONS_VIEW_MODE"],
				"SHOW_PARENT_NAME" => $arParams["SECTIONS_SHOW_PARENT_NAME"],
				"HIDE_SECTION_NAME" => (isset($arParams["SECTIONS_HIDE_SECTION_NAME"]) ? $arParams["SECTIONS_HIDE_SECTION_NAME"] : "N"),
				"ADD_SECTIONS_CHAIN" => (isset($arParams["ADD_SECTIONS_CHAIN"]) ? $arParams["ADD_SECTIONS_CHAIN"] : ''),
				"SECTION_USER_FIELDS" => array("UF_HIDE_FROM_INDEX"),
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);


		if(!empty($arCurSection['UF_FILTER_PROP']))
		{
			// если имеется подмена id раздела - получаем мета-инфу по актуальному разделу, чтобы ниже ее добавить

			$cacheKey = ['IPROPERTY_SECTION_ID'=>$arResult["VARIABLES"]["SECTION_ID"]];
			$obCache = new CPHPCache();
			if ($obCache->InitCache(36000, serialize($cacheKey), "/iblock/catalog_iprop_info"))
			{
				$IPROPERTY = $obCache->GetVars();
			}
            elseif ($obCache->StartDataCache())
			{
				$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams['IBLOCK_ID'], $arResult["VARIABLES"]["SECTION_ID"]);
				$IPROPERTY  = $ipropValues->getValues();

				if(defined("BX_COMP_MANAGED_CACHE"))
				{
					global $CACHE_MANAGER;
					$CACHE_MANAGER->StartTagCache("/iblock/catalog");

					if (!empty($IPROPERTY))
						$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);

					$CACHE_MANAGER->EndTagCache();
				}

				$obCache->EndDataCache($IPROPERTY);
			}


			$arCurSection['ID'] = $arCurSection['UF_FILTER_SECTION'];

			$arResult["VARIABLES"]["SECTION_ID"] = $arCurSection['UF_FILTER_SECTION'];
			$arResult["VARIABLES"]["SECTION_CODE"] = $arCurSection['CODE'];
			if(empty($arCurSection['UF_FILTER_SECTION'])){
//	            $APPLICATION->AddChainItem($arCurSection['NAME'], $arCurSection['SECTION_PAGE_URL']);
				$arCurSection['ID'] = false;
//	            $isFilter = false;
				$hideFilterBlock = true;
				unset($arResult["VARIABLES"]["SECTION_CODE"]);
				unset($arResult["VARIABLES"]["SECTION_CODE_PATH"]);
			}
            $needHit = false;
		}else{
            $needHit = true;
			
		}

		if ($isFilter):
			if(strpos($arResult['VARIABLES']['SECTION_CODE_PATH'], 'novinki') !== false)
			{
				$_GET[$checkBoxNovinli] = 'Y';
				$_GET['set_filter'] = 'Y';
			}

			if($arCurSection['CODE'] == 'ot-3-do-6-let'
				|| $arCurSection['CODE'] == 'ot-6-do-12-let'
				|| $arCurSection['CODE'] == 'ot-12-do-18-let'
			)
			{
				$ipropValues = new \Bitrix\Iblock\InheritedProperty\SectionValues($arParams['IBLOCK_ID'],$arCurSection['ID']);
				$IPROPERTY  = $ipropValues->getValues();
				$IPROPERTY['SECTION_NAME'] = $arCurSection['NAME'];

				if(strpos($arResult['VARIABLES']['SECTION_CODE_PATH'], 'detyam/devochkam') !== false)
				{
					$arResult["VARIABLES"]["SECTION_ID"] = $sectionIdDevochkam;
					$arResult["VARIABLES"]["SECTION_CODE"] = $sectionCodeDevochkam;
					$arCurSection['ID'] = $sectionIdDevochkam;
				}
                elseif(strpos($arResult['VARIABLES']['SECTION_CODE_PATH'], 'detyam/malchikam') !== false)
				{
					$arResult["VARIABLES"]["SECTION_ID"] = $sectionIdMalchikam;
					$arResult["VARIABLES"]["SECTION_CODE"] = $sectionCodeMalchikam;
					$arCurSection['ID'] = $sectionIdMalchikam;
				}
			}

			if($arCurSection['CODE'] == 'ot-3-do-6-let')
			{
				$_GET[$checkAge3_4] = 'Y';
				$_GET[$checkAge4_5] = 'Y';
				$_GET[$checkAge5_6] = 'Y';

				$_GET['set_filter'] = 'Y';
			}
            elseif($arCurSection['CODE'] == 'ot-6-do-12-let')
			{
				$_GET[$checkAge5_6] = 'Y';
				$_GET[$checkAge6_7] = 'Y';
				$_GET[$checkAge7_8] = 'Y';
				$_GET[$checkAge9_10] = 'Y';
				$_GET[$checkAge10_11] = 'Y';
				$_GET[$checkAge11_12] = 'Y';

				$_GET['set_filter'] = 'Y';
			}
            elseif($arCurSection['CODE'] == 'ot-12-do-18-let')
			{
				$_GET[$checkAge11_12] = 'Y';
				$_GET[$checkAge12] = 'Y';
				$_GET[$checkAge13_18] = 'Y';

				$_GET['set_filter'] = 'Y';
			}

			if (
				strpos($APPLICATION->sDirPath,'show_in_catalog-is-n-or-y') === false &&
				strpos($APPLICATION->sDirPath,'show_in_catalog-is-y') === false &&
				strpos($arResult['VARIABLES']['SECTION_CODE_PATH'], 'noski-podpiska') === false
			)
			{
				$_GET[$checkSHOW_IN_CATALOG] = 'Y';
				$_GET['set_filter'] = 'Y';
			}

			if(SITE_ID == 's2') $_GET[$checkNOT_SHOW_IN_RU] = 'Y';

			if($hideFilterBlock) echo '<div class="hidden-block">';

			$frame = new \Bitrix\Main\Page\FrameBuffered("smart_filter_frame");
			$frame->begin();
			$APPLICATION->IncludeComponent(
				"bitrix:catalog.smart.filter",
				"mf",
				array(
					"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
					"IBLOCK_ID" => $arParams["IBLOCK_ID"],
					"SECTION_ID" => $arCurSection['ID'],
					"FILTER_NAME" => $arParams["FILTER_NAME"],
					"PRICE_CODE" => $arParams["~PRICE_CODE"],
					"CACHE_TYPE" => $arParams["CACHE_TYPE"],
					"CACHE_TIME" => $arParams["CACHE_TIME"],
					"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
					"SAVE_IN_SESSION" => "N",
					"FILTER_VIEW_MODE" => $arParams["FILTER_VIEW_MODE"],
					"XML_EXPORT" => "N",
					"SECTION_TITLE" => "NAME",
					"SECTION_DESCRIPTION" => "DESCRIPTION",
					'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
					"TEMPLATE_THEME" => $arParams["TEMPLATE_THEME"],
					'CONVERT_CURRENCY' => SITE_ID == 's1' ? $arParams['CONVERT_CURRENCY'] : 'N',
					'CURRENCY_ID' => $arParams['CURRENCY_ID'],
					"SEF_MODE" => $arParams["SEF_MODE"],
					"SEF_RULE" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["smart_filter"],
					"SMART_FILTER_PATH" => $arResult["VARIABLES"]["SMART_FILTER_PATH"],
					"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
					"INSTANT_RELOAD" => $arParams["INSTANT_RELOAD"],
					"DISPLAY_ELEMENT_COUNT" => "N",
					"MAX_SECTIONS_SHOW" => "4",
				),
				$component,
				array('HIDE_ICONS' => 'Y')
			);
			$frame->end();

			if($hideFilterBlock) echo '</div>';
		endif;

		if(!empty($arCurSection['UF_FILTER_PROP']))
		{
			$GLOBALS[$arParams["FILTER_NAME"]]['PROPERTY_'.$arCurSection['UF_FILTER_PROP']]=$arCurSection['UF_FILTER_PROP_VALUE'];
		}
		?>

		<? if($isSorting) { ?>
            <div class="catalog-viewer">
                Просмотреть как:
                <ul>
                    <li<?= $viewType=='tile-small'?' class="selected-cat-viewer"':'' ?>>
                        <a href="?view=tile-small">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="0 0 514 512" ><defs><path d="M149.38 148.2L13 148.2L13 11L149.38 11L149.38 148.2Z" id="a1s9BHzP1l"></path><clipPath id="clipb4ea3cR4FX"><use xlink:href="#a1s9BHzP1l" opacity="1"></use></clipPath><path d="M149.38 148.2L13 148.2L13 11L149.38 11L149.38 148.2Z" id="aXO3G7Rf0"></path><clipPath id="clipbuNt59C9C"><use xlink:href="#aXO3G7Rf0" opacity="1"></use></clipPath><path d="M503 148.2L366.62 148.2L366.62 11L503 11L503 148.2Z" id="aQkjf1jOt"></path><clipPath id="clipbVctwg8YG"><use xlink:href="#aQkjf1jOt" opacity="1"></use></clipPath><path d="M503 148.2L366.62 148.2L366.62 11L503 11L503 148.2Z" id="czjAOI5F1"></path><clipPath id="clipa1JTPL2ue1"><use xlink:href="#czjAOI5F1" opacity="1"></use></clipPath><path d="M327.65 148.2L191.27 148.2L191.27 11L327.65 11L327.65 148.2Z" id="f631KYSFC"></path><clipPath id="clipc4tQS3ddyB"><use xlink:href="#f631KYSFC" opacity="1"></use></clipPath><path d="M327.65 148.2L191.27 148.2L191.27 11L327.65 11L327.65 148.2Z" id="a4kwVbr1dU"></path><clipPath id="clipd9tjeXap5F"><use xlink:href="#a4kwVbr1dU" opacity="1"></use></clipPath><path d="M149.38 324.6L13 324.6L13 187.4L149.38 187.4L149.38 324.6Z" id="avJIg76uj"></path><clipPath id="clipb4iX9e8ez"><use xlink:href="#avJIg76uj" opacity="1"></use></clipPath><path d="M149.38 324.6L13 324.6L13 187.4L149.38 187.4L149.38 324.6Z" id="bBNcoB67K"></path><clipPath id="clipa2scqyzW6O"><use xlink:href="#bBNcoB67K" opacity="1"></use></clipPath><path d="M327.65 324.6L191.27 324.6L191.27 187.4L327.65 187.4L327.65 324.6Z" id="cdhxXgIzN"></path><clipPath id="clipasga8LHz8"><use xlink:href="#cdhxXgIzN" opacity="1"></use></clipPath><path d="M327.65 324.6L191.27 324.6L191.27 187.4L327.65 187.4L327.65 324.6Z" id="a9PXnfojEQ"></path><clipPath id="clipb3urwgpa5"><use xlink:href="#a9PXnfojEQ" opacity="1"></use></clipPath><path d="M503 324.6L366.62 324.6L366.62 187.4L503 187.4L503 324.6Z" id="a1QgouM5Ax"></path><clipPath id="clipc8YKuw6iMJ"><use xlink:href="#a1QgouM5Ax" opacity="1"></use></clipPath><path d="M503 324.6L366.62 324.6L366.62 187.4L503 187.4L503 324.6Z" id="aapjAr9jY"></path><clipPath id="clipa2sMVoy0gV"><use xlink:href="#aapjAr9jY" opacity="1"></use></clipPath><path d="M503 501L366.62 501L366.62 363.8L503 363.8L503 501Z" id="a4K71AiU2j"></path><clipPath id="clipbgpjTJkIu"><use xlink:href="#a4K71AiU2j" opacity="1"></use></clipPath><path d="M503 501L366.62 501L366.62 363.8L503 363.8L503 501Z" id="c18NSCspIw"></path><clipPath id="clipaHLOh34s9"><use xlink:href="#c18NSCspIw" opacity="1"></use></clipPath><path d="M149.38 501L13 501L13 363.8L149.38 363.8L149.38 501Z" id="aVe7XorjJ"></path><clipPath id="clipcY7MK9FbC"><use xlink:href="#aVe7XorjJ" opacity="1"></use></clipPath><path d="M149.38 501L13 501L13 363.8L149.38 363.8L149.38 501Z" id="l5idw6D59"></path><clipPath id="clipb2c3YQm13V"><use xlink:href="#l5idw6D59" opacity="1"></use></clipPath><path d="M327.65 501L191.27 501L191.27 363.8L327.65 363.8L327.65 501Z" id="a449IoaA9X"></path><clipPath id="clipaOel1u93j"><use xlink:href="#a449IoaA9X" opacity="1"></use></clipPath><path d="M327.65 501L191.27 501L191.27 363.8L327.65 363.8L327.65 501Z" id="bgV05bWvM"></path><clipPath id="clipbQnwdtJIi"><use xlink:href="#bgV05bWvM" opacity="1"></use></clipPath></defs><g><g><g><use xlink:href="#a1s9BHzP1l" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipb4ea3cR4FX)"><use xlink:href="#a1s9BHzP1l" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="0"></use></g></g><g><use xlink:href="#aXO3G7Rf0" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipbuNt59C9C)"><use xlink:href="#aXO3G7Rf0" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#aQkjf1jOt" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipbVctwg8YG)"><use xlink:href="#aQkjf1jOt" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="0"></use></g></g><g><use xlink:href="#czjAOI5F1" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipa1JTPL2ue1)"><use xlink:href="#czjAOI5F1" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#f631KYSFC" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipc4tQS3ddyB)"><use xlink:href="#f631KYSFC" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="0"></use></g></g><g><use xlink:href="#a4kwVbr1dU" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipd9tjeXap5F)"><use xlink:href="#a4kwVbr1dU" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#avJIg76uj" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipb4iX9e8ez)"><use xlink:href="#avJIg76uj" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="0"></use></g></g><g><use xlink:href="#bBNcoB67K" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipa2scqyzW6O)"><use xlink:href="#bBNcoB67K" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#cdhxXgIzN" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipasga8LHz8)"><use xlink:href="#cdhxXgIzN" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="0"></use></g></g><g><use xlink:href="#a9PXnfojEQ" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipb3urwgpa5)"><use xlink:href="#a9PXnfojEQ" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#a1QgouM5Ax" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipc8YKuw6iMJ)"><use xlink:href="#a1QgouM5Ax" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="0"></use></g></g><g><use xlink:href="#aapjAr9jY" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipa2sMVoy0gV)"><use xlink:href="#aapjAr9jY" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#a4K71AiU2j" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipbgpjTJkIu)"><use xlink:href="#a4K71AiU2j" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="0"></use></g></g><g><use xlink:href="#c18NSCspIw" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipaHLOh34s9)"><use xlink:href="#c18NSCspIw" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#aVe7XorjJ" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipcY7MK9FbC)"><use xlink:href="#aVe7XorjJ" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="0"></use></g></g><g><use xlink:href="#l5idw6D59" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipb2c3YQm13V)"><use xlink:href="#l5idw6D59" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#a449IoaA9X" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipaOel1u93j)"><use xlink:href="#a449IoaA9X" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="0"></use></g></g><g><use xlink:href="#bgV05bWvM" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipbQnwdtJIi)"><use xlink:href="#bgV05bWvM" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g></g></g></svg>
                        </a>
                    </li>
                    <li<?= $viewType=='tile-big'?' class="selected-cat-viewer"':'' ?>>
                        <a href="?view=tile-big">
                            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet" viewBox="0 0 514 512" ><defs><path d="M233 281.45L233 481.45L33 481.45L33 281.45L233 281.45Z" id="aj952qbuw"></path><clipPath id="clipb4WlWc38HK"><use xlink:href="#aj952qbuw" opacity="1"></use></clipPath><path d="M479.45 281.45L479.45 481.45L279.45 481.45L279.45 281.45L479.45 281.45Z" id="ezx0k0AHL"></path><clipPath id="clipb1mgodjAuT"><use xlink:href="#ezx0k0AHL" opacity="1"></use></clipPath><path d="M479.45 35L479.45 235L279.45 235L279.45 35L479.45 35Z" id="c6GonEhMaG"></path><clipPath id="clipc1xro1joXF"><use xlink:href="#c6GonEhMaG" opacity="1"></use></clipPath><path d="M233 35L233 235L33 235L33 35L233 35Z" id="aaf7JX7sw"></path><clipPath id="clipam1Vhncau"><use xlink:href="#aaf7JX7sw" opacity="1"></use></clipPath></defs><g><g><g><use xlink:href="#aj952qbuw" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipb4WlWc38HK)"><use xlink:href="#aj952qbuw" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#ezx0k0AHL" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipb1mgodjAuT)"><use xlink:href="#ezx0k0AHL" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#c6GonEhMaG" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipc1xro1joXF)"><use xlink:href="#c6GonEhMaG" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g><g><use xlink:href="#aaf7JX7sw" opacity="1" fill="#000000" fill-opacity="0"></use><g clip-path="url(#clipam1Vhncau)"><use xlink:href="#aaf7JX7sw" opacity="1" fill-opacity="0" stroke="#000000" stroke-width="30" stroke-opacity="1"></use></g></g></g></g></svg>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="catalog-sort-container">
                <ul class="catalog-sort">
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"/></g></svg>
                    <li class="sort-default"><?= Loc::getMessage($activeSortMessage) ?></li>
					<? if($sortField != 'PROPERTY_SALELEADER') { ?>
                        <li class="cat-sort-el">
                            <a href="?sort-field=hits&sort-dir=desc"><?= Loc::getMessage("CT_SORT_PROPERTY_SALELEADER") ?></a>
                        </li>
					<? } ?>

					<? if($sortField != 'PROPERTY_NEWPRODUCT') { ?>
                        <li class="cat-sort-el">
                            <a href="?sort-field=new&sort-dir=desc"><?= Loc::getMessage("CT_SORT_PROPERTY_NEWPRODUCT") ?></a>
                        </li>
					<? } ?>

					<? if($sortField != 'PROPERTY_MINIMUM_PRICE') { ?>
                        <li class="cat-sort-el">
                            <a href="?sort-field=price&sort-dir=asc"><?= Loc::getMessage("CT_SORT_PROPERTY_MINIMUM_PRICE_ASC") ?></a>
                        </li>
					<? } ?>

					<? if($sortField != 'PROPERTY_MAXIMUM_PRICE') { ?>
                        <li class="cat-sort-el">
                            <a href="?sort-field=price&sort-dir=desc"><?= Loc::getMessage("CT_SORT_PROPERTY_MAXIMUM_PRICE_DESC") ?></a>
                        </li>
					<? } ?>

					<? if($sortField != 'sort' && $sortField != '') { ?>
                        <li class="cat-sort-el">
                            <a href="?sort-field=sort&sort-dir=asc"><?= Loc::getMessage("CT_SORT_DEFAULT") ?></a>
                        </li>
					<? } ?>
                </ul>
                <div style="display: none">
					<?= $arParams["ELEMENT_SORT_FIELD"] . ' -' . $arParams["ELEMENT_SORT_ORDER"] ?>
                </div>
            </div>
		<? } ?>
    </div>


	<?/*
	if (ModuleManager::isModuleInstalled("sale"))
	{
		$arRecomData = array();
		$recomCacheID = array('IBLOCK_ID' => $arParams['IBLOCK_ID']);
		$obCache = new CPHPCache();
		if ($obCache->InitCache(36000, serialize($recomCacheID), "/sale/bestsellers"))
		{
			$arRecomData = $obCache->GetVars();
		}
        elseif ($obCache->StartDataCache())
		{
			if (Loader::includeModule("catalog"))
			{
				$arSKU = CCatalogSku::GetInfoByProductIBlock($arParams['IBLOCK_ID']);
				$arRecomData['OFFER_IBLOCK_ID'] = (!empty($arSKU) ? $arSKU['IBLOCK_ID'] : 0);
			}
			$obCache->EndDataCache($arRecomData);
		}

		if (!empty($arRecomData) && $arParams['USE_GIFTS_SECTION'] === 'Y')
		{
			?>
            <div data-entity="parent-container">
				<?
				if (!isset($arParams['GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE']) || $arParams['GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE'] !== 'Y')
				{
					?>
                    <div class="catalog-block-header" data-entity="header" data-showed="false" style="display: none; opacity: 0;">
						<?=($arParams['GIFTS_SECTION_LIST_BLOCK_TITLE'] ?: \Bitrix\Main\Localization\Loc::getMessage('CT_GIFTS_SECTION_LIST_BLOCK_TITLE_DEFAULT'))?>
                    </div>
					<?
				}

				CBitrixComponent::includeComponentClass('bitrix:sale.products.gift.section');
				$APPLICATION->IncludeComponent(
					'bitrix:sale.products.gift.section',
					'.default',
					array(
						'IBLOCK_TYPE' => $arParams['IBLOCK_TYPE'],
						'IBLOCK_ID' => $arParams['IBLOCK_ID'],

						'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
						'SECTION_CODE' => $arResult['VARIABLES']['SECTION_CODE'],
						'SECTION_ID_VARIABLE' => $arParams['SECTION_ID_VARIABLE'],

						'PRODUCT_ID_VARIABLE' => $arParams['PRODUCT_ID_VARIABLE'],
						'ACTION_VARIABLE' => (!empty($arParams['ACTION_VARIABLE']) ? $arParams['ACTION_VARIABLE'] : 'action').'_spgs',

						'PRODUCT_ROW_VARIANTS' => \Bitrix\Main\Web\Json::encode(
							SaleProductsGiftSectionComponent::predictRowVariants(
								$arParams['GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT'],
								$arParams['GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT']
							)
						),
						'PAGE_ELEMENT_COUNT' => $arParams['GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT'],
						'DEFERRED_PRODUCT_ROW_VARIANTS' => '',
						'DEFERRED_PAGE_ELEMENT_COUNT' => 0,

						'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
						'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
						'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
						'PRODUCT_DISPLAY_MODE' => 'Y',
						'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
						'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
						'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
						'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

						'TEXT_LABEL_GIFT' => $arParams['GIFTS_DETAIL_TEXT_LABEL_GIFT'],

						'LABEL_PROP_'.$arParams['IBLOCK_ID'] => array(),
						'LABEL_PROP_MOBILE_'.$arParams['IBLOCK_ID'] => array(),
						'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],

						'ADD_TO_BASKET_ACTION' => $basketAction,
						'MESS_BTN_BUY' => $arParams['~GIFTS_MESS_BTN_BUY'],
						'MESS_BTN_ADD_TO_BASKET' => $arParams['~GIFTS_MESS_BTN_BUY'],
						'MESS_BTN_DETAIL' => $arParams['~MESS_BTN_DETAIL'],
						'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],

						'PROPERTY_CODE' => (isset($arParams['LIST_PROPERTY_CODE']) ? $arParams['LIST_PROPERTY_CODE'] : []),
						'PROPERTY_CODE_MOBILE' => $arParams['LIST_PROPERTY_CODE_MOBILE'],
						'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],

						'OFFERS_FIELD_CODE' => $arParams['LIST_OFFERS_FIELD_CODE'],
						'OFFERS_PROPERTY_CODE' => (isset($arParams['LIST_OFFERS_PROPERTY_CODE']) ? $arParams['LIST_OFFERS_PROPERTY_CODE'] : []),
						'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
						'OFFERS_CART_PROPERTIES' => (isset($arParams['OFFERS_CART_PROPERTIES']) ? $arParams['OFFERS_CART_PROPERTIES'] : []),
						'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],

						'HIDE_NOT_AVAILABLE' => 'Y',
						'HIDE_NOT_AVAILABLE_OFFERS' => 'Y',
						'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
						'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
						'PRICE_CODE' => $arParams['~PRICE_CODE'],
						'SHOW_PRICE_COUNT' => $arParams['SHOW_PRICE_COUNT'],
						'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_INCLUDE'],
						'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
						'BASKET_URL' => $arParams['BASKET_URL'],
						'ADD_PROPERTIES_TO_BASKET' => $arParams['ADD_PROPERTIES_TO_BASKET'],
						'PRODUCT_PROPS_VARIABLE' => $arParams['PRODUCT_PROPS_VARIABLE'],
						'PARTIAL_PRODUCT_PROPERTIES' => $arParams['PARTIAL_PRODUCT_PROPERTIES'],
						'USE_PRODUCT_QUANTITY' => 'N',
						'PRODUCT_QUANTITY_VARIABLE' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
						'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],

						'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
						'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
						'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),
					),
					$component,
					array("HIDE_ICONS" => "Y")
				);
				?>
            </div>
			<?
		}
	}*/
	?>


	<?

	if ($arParams["USE_COMPARE"]=="Y")
	{
		$APPLICATION->IncludeComponent(
			"bitrix:catalog.compare.list",
			"",
			array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"NAME" => $arParams["COMPARE_NAME"],
				"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
				"COMPARE_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["compare"],
				"ACTION_VARIABLE" => (!empty($arParams["ACTION_VARIABLE"]) ? $arParams["ACTION_VARIABLE"] : "action"),
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				'POSITION_FIXED' => isset($arParams['COMPARE_POSITION_FIXED']) ? $arParams['COMPARE_POSITION_FIXED'] : '',
				'POSITION' => isset($arParams['COMPARE_POSITION']) ? $arParams['COMPARE_POSITION'] : ''
			),
			$component,
			array("HIDE_ICONS" => "Y")
		);
	}
	//    pr($GLOBALS['arrFilter']);
	?><div id="catalog-product-cnt"><?
		//if ($isAjax) $GLOBALS['APPLICATION']->RestartBuffer();
		if(isset($_GET['PAGEN_1']))  $HIDE_SECTION_DESCRIPTION = 'Y';
		else $HIDE_SECTION_DESCRIPTION = 'N';

		$intSectionID = $APPLICATION->IncludeComponent(
			"bitrix:catalog.section",
			"mf",
			array(
				"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
				"IBLOCK_ID" => $arParams["IBLOCK_ID"],
				"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
				"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
				"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
				"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
				"PROPERTY_CODE" => (isset($arParams["LIST_PROPERTY_CODE"]) ? $arParams["LIST_PROPERTY_CODE"] : []),
				"PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
				"META_KEYWORDS" => $arParams["LIST_META_KEYWORDS"],
				"META_DESCRIPTION" => $arParams["LIST_META_DESCRIPTION"],
				"BROWSER_TITLE" => $arParams["LIST_BROWSER_TITLE"],
				"SET_LAST_MODIFIED" => $arParams["SET_LAST_MODIFIED"],
				"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
				"BASKET_URL" => $arParams["BASKET_URL"],
				"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
				"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
				"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
				"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
				"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
				"FILTER_NAME" => $arParams["FILTER_NAME"],
				"CACHE_TYPE" => $arParams["CACHE_TYPE"],
				"CACHE_TIME" => $arParams["CACHE_TIME"],
				"CACHE_FILTER" => $arParams["CACHE_FILTER"],
				"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
				"SET_TITLE" => $arParams["SET_TITLE"],
				"MESSAGE_404" => $arParams["~MESSAGE_404"],
				"SET_STATUS_404" => $arParams["SET_STATUS_404"],
				"SHOW_404" => $arParams["SHOW_404"],
				"FILE_404" => $arParams["FILE_404"],
				"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
				"PAGE_ELEMENT_COUNT" => $arParams["PAGE_ELEMENT_COUNT"],
				"LINE_ELEMENT_COUNT" => $arParams["LINE_ELEMENT_COUNT"],
				"PRICE_CODE" => $arParams["~PRICE_CODE"],
				"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
				"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

				"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
				"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
				"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
				"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
				"PRODUCT_PROPERTIES" => (isset($arParams["PRODUCT_PROPERTIES"]) ? $arParams["PRODUCT_PROPERTIES"] : []),

				"DISPLAY_TOP_PAGER" => $arParams["DISPLAY_TOP_PAGER"],
				"DISPLAY_BOTTOM_PAGER" => $arParams["DISPLAY_BOTTOM_PAGER"],
				"PAGER_TITLE" => $arParams["PAGER_TITLE"],
				"PAGER_SHOW_ALWAYS" => $arParams["PAGER_SHOW_ALWAYS"],
				"PAGER_TEMPLATE" => $arParams["PAGER_TEMPLATE"],
				"PAGER_DESC_NUMBERING" => $arParams["PAGER_DESC_NUMBERING"],
				"PAGER_DESC_NUMBERING_CACHE_TIME" => $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"],
				"PAGER_SHOW_ALL" => $arParams["PAGER_SHOW_ALL"],
				"PAGER_BASE_LINK_ENABLE" => $arParams["PAGER_BASE_LINK_ENABLE"],
				"PAGER_BASE_LINK" => $arParams["PAGER_BASE_LINK"],
				"PAGER_PARAMS_NAME" => $arParams["PAGER_PARAMS_NAME"],
				"LAZY_LOAD" => $arParams["LAZY_LOAD"],
				"MESS_BTN_LAZY_LOAD" => $arParams["~MESS_BTN_LAZY_LOAD"],
				"LOAD_ON_SCROLL" => $arParams["LOAD_ON_SCROLL"],

				"OFFERS_CART_PROPERTIES" => (isset($arParams["OFFERS_CART_PROPERTIES"]) ? $arParams["OFFERS_CART_PROPERTIES"] : []),
				"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
				"OFFERS_PROPERTY_CODE" => (isset($arParams["LIST_OFFERS_PROPERTY_CODE"]) ? $arParams["LIST_OFFERS_PROPERTY_CODE"] : []),
				"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
				"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
				"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
				"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
				"OFFERS_LIMIT" => (isset($arParams["LIST_OFFERS_LIMIT"]) ? $arParams["LIST_OFFERS_LIMIT"] : 0),

				"SECTION_ID" => $arResult["VARIABLES"]["SECTION_ID"],
				"SECTION_CODE" => $arResult["VARIABLES"]["SECTION_CODE"],
				"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
				"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
				"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
				'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
				'CURRENCY_ID' => $arParams['CURRENCY_ID'],
				'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
				'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

				'LABEL_PROP' => $arParams['LABEL_PROP'],
				'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
				'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
				'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
				'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
				'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
				'PRODUCT_ROW_VARIANTS' => $arParams['LIST_PRODUCT_ROW_VARIANTS'],
				'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
				'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
				'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
				'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
				'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

				'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
				'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
				'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
				'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
				'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
				'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
				'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
				'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
				'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
				'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
				'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
				'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
				'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
				'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
				'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
				'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
				'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

				'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
				'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
				'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

				'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
				"ADD_SECTIONS_CHAIN" => "N",
				'ADD_TO_BASKET_ACTION' => $basketAction,
				'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
				'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
				'COMPARE_NAME' => $arParams['COMPARE_NAME'],
				'USE_COMPARE_LIST' => 'Y',
				'BACKGROUND_IMAGE' => (isset($arParams['SECTION_BACKGROUND_IMAGE']) ? $arParams['SECTION_BACKGROUND_IMAGE'] : ''),
				'COMPATIBLE_MODE' => (isset($arParams['COMPATIBLE_MODE']) ? $arParams['COMPATIBLE_MODE'] : ''),
				'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : ''),
				"ELEMENTS_VIEW_TYPE" => $viewType,
				'SHOW_ALL_WO_SECTION' => 'Y',
				'HIDE_SECTION_DESCRIPTION' => $HIDE_SECTION_DESCRIPTION,
			),
			$component
		);
		//if ($isAjax) die();
		?></div><?


	$GLOBALS['CATALOG_CURRENT_SECTION_ID'] = $intSectionID;

	if (ModuleManager::isModuleInstalled("sale"))
	{
		if (!empty($arRecomData))
		{
			if (!isset($arParams['USE_BIG_DATA']) || $arParams['USE_BIG_DATA'] != 'N')
			{
				?>
                <div class="col-xs-12" data-entity="parent-container">
                    <div class="catalog-block-header" data-entity="header" data-showed="false" style="display: none; opacity: 0;">
						<?=GetMessage('CATALOG_PERSONAL_RECOM')?>
                    </div>
					<?
					$APPLICATION->IncludeComponent(
						"bitrix:catalog.section",
						"",
						array(
							"IBLOCK_TYPE" => $arParams["IBLOCK_TYPE"],
							"IBLOCK_ID" => $arParams["IBLOCK_ID"],
							"ELEMENT_SORT_FIELD" => $arParams["ELEMENT_SORT_FIELD"],
							"ELEMENT_SORT_ORDER" => $arParams["ELEMENT_SORT_ORDER"],
							"ELEMENT_SORT_FIELD2" => $arParams["ELEMENT_SORT_FIELD2"],
							"ELEMENT_SORT_ORDER2" => $arParams["ELEMENT_SORT_ORDER2"],
							"PROPERTY_CODE" => (isset($arParams["LIST_PROPERTY_CODE"]) ? $arParams["LIST_PROPERTY_CODE"] : []),
							"PROPERTY_CODE_MOBILE" => $arParams["LIST_PROPERTY_CODE_MOBILE"],
							"INCLUDE_SUBSECTIONS" => $arParams["INCLUDE_SUBSECTIONS"],
							"BASKET_URL" => $arParams["BASKET_URL"],
							"ACTION_VARIABLE" => $arParams["ACTION_VARIABLE"],
							"PRODUCT_ID_VARIABLE" => $arParams["PRODUCT_ID_VARIABLE"],
							"SECTION_ID_VARIABLE" => $arParams["SECTION_ID_VARIABLE"],
							"PRODUCT_QUANTITY_VARIABLE" => $arParams["PRODUCT_QUANTITY_VARIABLE"],
							"PRODUCT_PROPS_VARIABLE" => $arParams["PRODUCT_PROPS_VARIABLE"],
							"CACHE_TYPE" => $arParams["CACHE_TYPE"],
							"CACHE_TIME" => $arParams["CACHE_TIME"],
							"CACHE_FILTER" => $arParams["CACHE_FILTER"],
							"CACHE_GROUPS" => $arParams["CACHE_GROUPS"],
							"DISPLAY_COMPARE" => $arParams["USE_COMPARE"],
							"PAGE_ELEMENT_COUNT" => 0,
							"PRICE_CODE" => $arParams["~PRICE_CODE"],
							"USE_PRICE_COUNT" => $arParams["USE_PRICE_COUNT"],
							"SHOW_PRICE_COUNT" => $arParams["SHOW_PRICE_COUNT"],

							"SET_BROWSER_TITLE" => "N",
							"SET_META_KEYWORDS" => "N",
							"SET_META_DESCRIPTION" => "N",
							"SET_LAST_MODIFIED" => "N",
							"ADD_SECTIONS_CHAIN" => "N",

							"PRICE_VAT_INCLUDE" => $arParams["PRICE_VAT_INCLUDE"],
							"USE_PRODUCT_QUANTITY" => $arParams['USE_PRODUCT_QUANTITY'],
							"ADD_PROPERTIES_TO_BASKET" => (isset($arParams["ADD_PROPERTIES_TO_BASKET"]) ? $arParams["ADD_PROPERTIES_TO_BASKET"] : ''),
							"PARTIAL_PRODUCT_PROPERTIES" => (isset($arParams["PARTIAL_PRODUCT_PROPERTIES"]) ? $arParams["PARTIAL_PRODUCT_PROPERTIES"] : ''),
							"PRODUCT_PROPERTIES" => (isset($arParams["PRODUCT_PROPERTIES"]) ? $arParams["PRODUCT_PROPERTIES"] : []),

							"OFFERS_CART_PROPERTIES" => (isset($arParams["OFFERS_CART_PROPERTIES"]) ? $arParams["OFFERS_CART_PROPERTIES"] : []),
							"OFFERS_FIELD_CODE" => $arParams["LIST_OFFERS_FIELD_CODE"],
							"OFFERS_PROPERTY_CODE" => (isset($arParams["LIST_OFFERS_PROPERTY_CODE"]) ? $arParams["LIST_OFFERS_PROPERTY_CODE"] : []),
							"OFFERS_SORT_FIELD" => $arParams["OFFERS_SORT_FIELD"],
							"OFFERS_SORT_ORDER" => $arParams["OFFERS_SORT_ORDER"],
							"OFFERS_SORT_FIELD2" => $arParams["OFFERS_SORT_FIELD2"],
							"OFFERS_SORT_ORDER2" => $arParams["OFFERS_SORT_ORDER2"],
							"OFFERS_LIMIT" => (isset($arParams["LIST_OFFERS_LIMIT"]) ? $arParams["LIST_OFFERS_LIMIT"] : 0),

							"SECTION_ID" => $intSectionID,
							"SECTION_CODE" => "",
							"SECTION_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["section"],
							"DETAIL_URL" => $arResult["FOLDER"].$arResult["URL_TEMPLATES"]["element"],
							"USE_MAIN_ELEMENT_SECTION" => $arParams["USE_MAIN_ELEMENT_SECTION"],
							'CONVERT_CURRENCY' => $arParams['CONVERT_CURRENCY'],
							'CURRENCY_ID' => $arParams['CURRENCY_ID'],
							'HIDE_NOT_AVAILABLE' => $arParams["HIDE_NOT_AVAILABLE"],
							'HIDE_NOT_AVAILABLE_OFFERS' => $arParams["HIDE_NOT_AVAILABLE_OFFERS"],

							'LABEL_PROP' => $arParams['LABEL_PROP'],
							'LABEL_PROP_MOBILE' => $arParams['LABEL_PROP_MOBILE'],
							'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],
							'ADD_PICT_PROP' => $arParams['ADD_PICT_PROP'],
							'PRODUCT_DISPLAY_MODE' => $arParams['PRODUCT_DISPLAY_MODE'],
							'PRODUCT_BLOCKS_ORDER' => $arParams['LIST_PRODUCT_BLOCKS_ORDER'],
							'PRODUCT_ROW_VARIANTS' => "[{'VARIANT':'3','BIG_DATA':true}]",
							'ENLARGE_PRODUCT' => $arParams['LIST_ENLARGE_PRODUCT'],
							'ENLARGE_PROP' => isset($arParams['LIST_ENLARGE_PROP']) ? $arParams['LIST_ENLARGE_PROP'] : '',
							'SHOW_SLIDER' => $arParams['LIST_SHOW_SLIDER'],
							'SLIDER_INTERVAL' => isset($arParams['LIST_SLIDER_INTERVAL']) ? $arParams['LIST_SLIDER_INTERVAL'] : '',
							'SLIDER_PROGRESS' => isset($arParams['LIST_SLIDER_PROGRESS']) ? $arParams['LIST_SLIDER_PROGRESS'] : '',

							"DISPLAY_TOP_PAGER" => 'N',
							"DISPLAY_BOTTOM_PAGER" => 'N',
							"HIDE_SECTION_DESCRIPTION" => "Y",

							"RCM_TYPE" => isset($arParams['BIG_DATA_RCM_TYPE']) ? $arParams['BIG_DATA_RCM_TYPE'] : '',
							"SHOW_FROM_SECTION" => 'Y',

							'OFFER_ADD_PICT_PROP' => $arParams['OFFER_ADD_PICT_PROP'],
							'OFFER_TREE_PROPS' => (isset($arParams['OFFER_TREE_PROPS']) ? $arParams['OFFER_TREE_PROPS'] : []),
							'PRODUCT_SUBSCRIPTION' => $arParams['PRODUCT_SUBSCRIPTION'],
							'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'],
							'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
							'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'],
							'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
							'MESS_SHOW_MAX_QUANTITY' => (isset($arParams['~MESS_SHOW_MAX_QUANTITY']) ? $arParams['~MESS_SHOW_MAX_QUANTITY'] : ''),
							'RELATIVE_QUANTITY_FACTOR' => (isset($arParams['RELATIVE_QUANTITY_FACTOR']) ? $arParams['RELATIVE_QUANTITY_FACTOR'] : ''),
							'MESS_RELATIVE_QUANTITY_MANY' => (isset($arParams['~MESS_RELATIVE_QUANTITY_MANY']) ? $arParams['~MESS_RELATIVE_QUANTITY_MANY'] : ''),
							'MESS_RELATIVE_QUANTITY_FEW' => (isset($arParams['~MESS_RELATIVE_QUANTITY_FEW']) ? $arParams['~MESS_RELATIVE_QUANTITY_FEW'] : ''),
							'MESS_BTN_BUY' => (isset($arParams['~MESS_BTN_BUY']) ? $arParams['~MESS_BTN_BUY'] : ''),
							'MESS_BTN_ADD_TO_BASKET' => (isset($arParams['~MESS_BTN_ADD_TO_BASKET']) ? $arParams['~MESS_BTN_ADD_TO_BASKET'] : ''),
							'MESS_BTN_SUBSCRIBE' => (isset($arParams['~MESS_BTN_SUBSCRIBE']) ? $arParams['~MESS_BTN_SUBSCRIBE'] : ''),
							'MESS_BTN_DETAIL' => (isset($arParams['~MESS_BTN_DETAIL']) ? $arParams['~MESS_BTN_DETAIL'] : ''),
							'MESS_NOT_AVAILABLE' => (isset($arParams['~MESS_NOT_AVAILABLE']) ? $arParams['~MESS_NOT_AVAILABLE'] : ''),
							'MESS_BTN_COMPARE' => (isset($arParams['~MESS_BTN_COMPARE']) ? $arParams['~MESS_BTN_COMPARE'] : ''),

							'USE_ENHANCED_ECOMMERCE' => (isset($arParams['USE_ENHANCED_ECOMMERCE']) ? $arParams['USE_ENHANCED_ECOMMERCE'] : ''),
							'DATA_LAYER_NAME' => (isset($arParams['DATA_LAYER_NAME']) ? $arParams['DATA_LAYER_NAME'] : ''),
							'BRAND_PROPERTY' => (isset($arParams['BRAND_PROPERTY']) ? $arParams['BRAND_PROPERTY'] : ''),

							'TEMPLATE_THEME' => (isset($arParams['TEMPLATE_THEME']) ? $arParams['TEMPLATE_THEME'] : ''),
							'ADD_TO_BASKET_ACTION' => $basketAction,
							'SHOW_CLOSE_POPUP' => isset($arParams['COMMON_SHOW_CLOSE_POPUP']) ? $arParams['COMMON_SHOW_CLOSE_POPUP'] : '',
							'COMPARE_PATH' => $arResult['FOLDER'].$arResult['URL_TEMPLATES']['compare'],
							'COMPARE_NAME' => $arParams['COMPARE_NAME'],
							'USE_COMPARE_LIST' => 'Y',
							'BACKGROUND_IMAGE' => '',
							'DISABLE_INIT_JS_IN_COMPONENT' => (isset($arParams['DISABLE_INIT_JS_IN_COMPONENT']) ? $arParams['DISABLE_INIT_JS_IN_COMPONENT'] : '')
						),
						$component
					);
					?>
                </div>
				<?
			}
		}
	}
	?>
</div>
<?
//pr($IPROPERTY);

if(!empty($IPROPERTY))
{
	if(!empty($IPROPERTY['SECTION_PAGE_TITLE']))
	{
		$APPLICATION->SetTitle($IPROPERTY['SECTION_PAGE_TITLE']);
	}
	else
	{
		$APPLICATION->SetTitle($IPROPERTY['SECTION_NAME']);
	}

	if(!empty($IPROPERTY['SECTION_META_TITLE']))
	{
		$APPLICATION->SetPageProperty('title', $IPROPERTY['SECTION_META_TITLE']);
	}

	if(!empty($IPROPERTY['SECTION_META_KEYWORDS']))
	{
		$APPLICATION->SetPageProperty('keywords',$IPROPERTY['SECTION_META_KEYWORDS']);
	}

	if(!empty($IPROPERTY['SECTION_META_DESCRIPTION']))
	{
		$APPLICATION->SetPageProperty('description', $IPROPERTY['SECTION_META_DESCRIPTION']);
	}
}

?>    <button id="ajax_basket" class="ajax_basket" data-mfp-src="/ajax/basket-popup.php" style="display: none;">CART</button>
<?if($needHit):?>
    <script type="text/javascript">
    (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() {
        try { rrApi.categoryView(<?=$arResult['VARIABLES']['SECTION_ID'];?>); } catch(e) {}
    })
    </script>
<?Bitrix\Main\Page\Frame::getInstance()->startDynamicWithID("header_hit");?>
    <div data-retailrocket-markup-block="5d1dba4b97a5280cd4e57050" data-category-id="<?=$arResult['VARIABLES']['SECTION_ID'];?>" data-stock-id="<?=$_SESSION['city'];?>"></div>
    <script>retailrocket.markup.render();</script>
<?Bitrix\Main\Page\Frame::getInstance()->finishDynamicWithID("header_hit", ""); ?>
<?endif;?>