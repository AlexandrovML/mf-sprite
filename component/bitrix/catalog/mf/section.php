<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
use Bitrix\Main\Loader;
use Bitrix\Main\ModuleManager;

$this->setFrameMode(true);
if (!isset($arParams['FILTER_VIEW_MODE']) || (string)$arParams['FILTER_VIEW_MODE'] == '')
	$arParams['FILTER_VIEW_MODE'] = 'VERTICAL';
$arParams['USE_FILTER'] = (isset($arParams['USE_FILTER']) && $arParams['USE_FILTER'] == 'Y' ? 'Y' : 'N');

$isVerticalFilter = ('Y' == $arParams['USE_FILTER'] && $arParams["FILTER_VIEW_MODE"] == "VERTICAL");
$isSidebar = ($arParams["SIDEBAR_SECTION_SHOW"] == "Y" && isset($arParams["SIDEBAR_PATH"]) && !empty($arParams["SIDEBAR_PATH"]));
$isFilter = ($arParams['USE_FILTER'] == 'Y');

if ($isFilter)
{
	$arFilter = array(
		"IBLOCK_ID" => $arParams["IBLOCK_ID"],
		"ACTIVE" => "Y",
		"GLOBAL_ACTIVE" => "Y",
	);
	if (0 < intval($arResult["VARIABLES"]["SECTION_ID"]))
		$arFilter["ID"] = $arResult["VARIABLES"]["SECTION_ID"];
	elseif ('' != $arResult["VARIABLES"]["SECTION_CODE"])
		$arFilter["=CODE"] = $arResult["VARIABLES"]["SECTION_CODE"];

	$obCache = new CPHPCache();
	if ($obCache->InitCache(36000, serialize($arFilter), "/iblock/catalog"))
	{
		$arCurSection = $obCache->GetVars();
	}
	elseif ($obCache->StartDataCache())
	{
		$arCurSection = array();
		if (Loader::includeModule("iblock"))
		{
			$dbRes = CIBlockSection::GetList(array(), $arFilter, false, array("ID", "NAME", "CODE", "SECTION_PAGE_URL", "UF_*"));

			if(defined("BX_COMP_MANAGED_CACHE"))
			{
				global $CACHE_MANAGER;
				$CACHE_MANAGER->StartTagCache("/iblock/catalog");

				if ($arCurSection = $dbRes->Fetch())
					$CACHE_MANAGER->RegisterTag("iblock_id_".$arParams["IBLOCK_ID"]);

				$CACHE_MANAGER->EndTagCache();
			}
			else
			{
				if(!$arCurSection = $dbRes->Fetch())
					$arCurSection = array();
			}
		}
		$obCache->EndDataCache($arCurSection);
	}
	if (!isset($arCurSection))
		$arCurSection = array();
}

// получаем все имеющиеся теги у товаров данного раздела и его подразделов
$arCacheKey = [
	'SECTION_ID' => $arResult['VARIABLES']['SECTION_ID'],
	'TAG_KEY' => 'TAG_KEY',
];

$cache = new CPHPCache();
$cache_time = 36000;
$cache_id = serialize($arCacheKey);
$cache_path = '/iblock/catalog-tags';

$arResult['SECTION_TAGS'] = [];

if ($cache_time > 0 && $cache->InitCache($cache_time, $cache_id, $cache_path))
{
	$arResult['SECTION_TAGS'] = $cache->GetVars();
}

if (empty($arResult['SECTION_TAGS']))
{
	$arIds = [];
	$arTagIds = [0];
	$tagsIBlockId = false;


	// собираем ID всех подразделов и текущего раздела
	$rsParentSection = CIBlockSection::GetByID($arResult['VARIABLES']['SECTION_ID']);
	if ($arParentSection = $rsParentSection->GetNext())
	{
		$arIds[] = $arParentSection['ID'];
		$arFilter = array('IBLOCK_ID' => $arParentSection['IBLOCK_ID'],'>LEFT_MARGIN' => $arParentSection['LEFT_MARGIN'],'<RIGHT_MARGIN' => $arParentSection['RIGHT_MARGIN'],'>DEPTH_LEVEL' => $arParentSection['DEPTH_LEVEL']); // выберет потомков без учета активности
		$rsSect = CIBlockSection::GetList(array('left_margin' => 'asc'),$arFilter, false, ['ID', 'NAME']);
		while ($arSect = $rsSect->GetNext())
		{
			// получаем подразделы
			$arIds[] = $arSect['ID'];
		}
	}

	// получаем имеющиеся теги (id) у каждого товара

	$arFilter = [
		"IBLOCK_ID" => $arParams['IBLOCK_ID'],
		"SECTION_ID" => $arIds,
//        "PROPERTY_SHOW_IN_CATALOG"=>'Y'
	];
//	pr($arFilter);
	$arSelect = ["ID", "IBLOCK_ID", "NAME","PROPERTY_TAGS"];
	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
	while($arEl = $res->GetNext())
	{
		if(!empty($arEl['PROPERTY_TAGS_VALUE']))
		{
				if(empty($arTagIds[$arEl['PROPERTY_TAGS_VALUE']])) $arTagIds[$arEl['PROPERTY_TAGS_VALUE']] = $arEl['PROPERTY_TAGS_VALUE'];
		}

		if($tagsIBlockId === false) $tagsIBlockId = 6;
	}

	// получаем сами теги
	$arResult['SECTION_TAGS'] = [];
	$arFilter = [
		"IBLOCK_ID" => $tagsIBlockId,
		"ID" => $arTagIds,
        'ACTIVE' => 'Y'
	];
	$arSelect = ["ID", "IBLOCK_ID", "SECTION_ID", "NAME", "DETAIL_PAGE_URL"];
	$res = CIBlockElement::GetList(['sert'=>'asc'], $arFilter, false, false, $arSelect);
	while($ob = $res->GetNextElement())
	{
		$arFields = $ob->GetFields();
		$arResult['SECTION_TAGS'][] = [
			'ID' => $arFields['ID'],
			'NAME' => $arFields['NAME'],
			'DETAIL_PAGE_URL' => $arFields['DETAIL_PAGE_URL'],
		];
	}

//	pr(count($arResult['SECTION_TAGS']));
	if ($cache_time > 0)
	{
		$cache->StartDataCache($cache_time, $cache_id, $cache_path);
		$cache->EndDataCache($arResult['SECTION_TAGS']);
	}
}




include($_SERVER["DOCUMENT_ROOT"]."/".$this->GetFolder()."/section_vertical.php");
?>
