<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");
?>

<section class="pagination other_pagination tac">
    <? /*if ($arResult["NavPageCount"] > 1):
        if ($arResult["NavPageNomer"] + 1 <= $arResult["nEndPage"]) {
            $plus = $arResult["NavPageNomer"] + 1;
            $url = $arResult["sUrlPath"] . "?PAGEN_" . $arResult["NavNum"] . "=" . $plus; ?>
            <a href="<?= $url ?>" class="button-link black-tr-b next_page_pagination"><?=GetMessage('next_page');?></a>
            <?
        }
     endif;*/ ?>
    <div class="pagination-list">
        <ul>
            <? if ($arResult["bDescPageNumbering"] === true) { ?>
                <? if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
                    $bFirst = false;
                    if ($arResult["bSavePage"]):?>
                        <li>
                            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>">1</a>
                        </li>
                    <? else: ?>
                        <li>
                            <a class="modern-page-first"
                               href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
                        </li>
                    <?endif;
                    if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)):?>
                        <li class="mf-pagi-dots">
                            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= intVal($arResult["nStartPage"] + ($arResult["NavPageCount"] - $arResult["nStartPage"]) / 2) ?>">
                                ...
                            </a>
                        </li>
                    <?endif;
                endif; ?>
                <? do {
                    $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;
                    if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                        <li class="selected-page"><?= $NavRecordGroupPrint ?></li>
                    <?elseif ($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):?>
                        <li>
                            <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
                                <?= $NavRecordGroupPrint ?>
                            </a>
                        </li>
                    <?else:?>
                        <li>
                            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>">
                                <?= $NavRecordGroupPrint ?>
                            </a>
                        </li>
                    <?endif;
                    $arResult["nStartPage"]--;
                    $bFirst = false;
                } while ($arResult["nStartPage"] >= $arResult["nEndPage"]);?>
            <? } else {
                $bFirst = true;
                if ($arResult["NavPageNomer"] > 1):
                    if ($arResult["bSavePage"]):?>
                        <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"
                           title="<?= GetMessage("nav_prev") ?>">
                            <svg class="prev-page-arrow" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet"
                                 viewBox="0 0 30 15" width="30" height="15">
                                <defs>
                                    <path d="M9.35 14.2L1.67 7.2" id="b18Z6TSLm1"></path>
                                    <path d="M29 7.5L2.48 7.5" id="j3TXHzvyCG"></path>
                                    <path d="M9.35 0.7L1.67 7.7" id="c10U1gTYy5"></path>
                                </defs>
                                <g>
                                    <g>
                                        <g>
                                            <use xlink:href="#b18Z6TSLm1" opacity="1" fill="#000000"
                                                 fill-opacity="1"></use>
                                            <g>
                                                <use xlink:href="#b18Z6TSLm1" opacity="1" fill-opacity="0"
                                                     stroke="#000000"
                                                     stroke-width="1" stroke-opacity="1"></use>
                                            </g>
                                        </g>
                                        <g>
                                            <use xlink:href="#j3TXHzvyCG" opacity="1" fill="#000000"
                                                 fill-opacity="1"></use>
                                            <g>
                                                <use xlink:href="#j3TXHzvyCG" opacity="1" fill-opacity="0"
                                                     stroke="#000000"
                                                     stroke-width="1" stroke-opacity="1"></use>
                                            </g>
                                        </g>
                                        <g>
                                            <use xlink:href="#c10U1gTYy5" opacity="1" fill="#000000"
                                                 fill-opacity="1"></use>
                                            <g>
                                                <use xlink:href="#c10U1gTYy5" opacity="1" fill-opacity="0"
                                                     stroke="#000000"
                                                     stroke-width="1" stroke-opacity="1"></use>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </svg>
                        </a>
                    <?else:
                        if ($arResult["NavPageNomer"] > 2):?>
                            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>"
                               title="<?= GetMessage("nav_prev") ?>">
                                <svg class="prev-page-arrow" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet"
                                     viewBox="0 0 30 15" width="30" height="15">
                                    <defs>
                                        <path d="M9.35 14.2L1.67 7.2" id="b18Z6TSLm1"></path>
                                        <path d="M29 7.5L2.48 7.5" id="j3TXHzvyCG"></path>
                                        <path d="M9.35 0.7L1.67 7.7" id="c10U1gTYy5"></path>
                                    </defs>
                                    <g>
                                        <g>
                                            <g>
                                                <use xlink:href="#b18Z6TSLm1" opacity="1" fill="#000000"
                                                     fill-opacity="1"></use>
                                                <g>
                                                    <use xlink:href="#b18Z6TSLm1" opacity="1" fill-opacity="0"
                                                         stroke="#000000"
                                                         stroke-width="1" stroke-opacity="1"></use>
                                                </g>
                                            </g>
                                            <g>
                                                <use xlink:href="#j3TXHzvyCG" opacity="1" fill="#000000"
                                                     fill-opacity="1"></use>
                                                <g>
                                                    <use xlink:href="#j3TXHzvyCG" opacity="1" fill-opacity="0"
                                                         stroke="#000000"
                                                         stroke-width="1" stroke-opacity="1"></use>
                                                </g>
                                            </g>
                                            <g>
                                                <use xlink:href="#c10U1gTYy5" opacity="1" fill="#000000"
                                                     fill-opacity="1"></use>
                                                <g>
                                                    <use xlink:href="#c10U1gTYy5" opacity="1" fill-opacity="0"
                                                         stroke="#000000"
                                                         stroke-width="1" stroke-opacity="1"></use>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        <?else:?>
                            <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"
                               title="<?= GetMessage("nav_prev") ?>">
                                <svg class="prev-page-arrow" version="1.1" xmlns="http://www.w3.org/2000/svg"
                                     xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet"
                                     viewBox="0 0 30 15" width="30" height="15">
                                    <defs>
                                        <path d="M9.35 14.2L1.67 7.2" id="b18Z6TSLm1"></path>
                                        <path d="M29 7.5L2.48 7.5" id="j3TXHzvyCG"></path>
                                        <path d="M9.35 0.7L1.67 7.7" id="c10U1gTYy5"></path>
                                    </defs>
                                    <g>
                                        <g>
                                            <g>
                                                <use xlink:href="#b18Z6TSLm1" opacity="1" fill="#000000"
                                                     fill-opacity="1"></use>
                                                <g>
                                                    <use xlink:href="#b18Z6TSLm1" opacity="1" fill-opacity="0"
                                                         stroke="#000000"
                                                         stroke-width="1" stroke-opacity="1"></use>
                                                </g>
                                            </g>
                                            <g>
                                                <use xlink:href="#j3TXHzvyCG" opacity="1" fill="#000000"
                                                     fill-opacity="1"></use>
                                                <g>
                                                    <use xlink:href="#j3TXHzvyCG" opacity="1" fill-opacity="0"
                                                         stroke="#000000"
                                                         stroke-width="1" stroke-opacity="1"></use>
                                                </g>
                                            </g>
                                            <g>
                                                <use xlink:href="#c10U1gTYy5" opacity="1" fill="#000000"
                                                     fill-opacity="1"></use>
                                                <g>
                                                    <use xlink:href="#c10U1gTYy5" opacity="1" fill-opacity="0"
                                                         stroke="#000000"
                                                         stroke-width="1" stroke-opacity="1"></use>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </a>
                        <?endif;
                    endif;

                    if ($arResult["nStartPage"] > 1):
                        $bFirst = false;
                        if ($arResult["bSavePage"]):?>
                            <li>
                                <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1">1</a>
                            </li>
                        <?else:?>
                            <li>
                                <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">1</a>
                            </li>
                        <?endif;
                        if ($arResult["nStartPage"] > 2):?>
                            <li class="mf-pagi-dots">
                                <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nStartPage"] / 2) ?>">...</a>
                            </li>
                        <?endif;
                    endif;
                endif;

                do {
                    if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):?>
                        <li class="selected-page"><?= $arResult["nStartPage"] ?></li>
                    <?elseif ($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):?>
                        <li>
                            <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><?= $arResult["nStartPage"] ?></a>
                        </li>
                    <?else:?>
                        <li>
                            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"<?
                            ?> class="<?= ($bFirst ? "modern-page-first" : "") ?>"><?= $arResult["nStartPage"] ?></a>
                        </li>
                    <?
                    endif;
                    $arResult["nStartPage"]++;
                    $bFirst = false;
                } while ($arResult["nStartPage"] <= $arResult["nEndPage"]);

                if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
                    if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
                        if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):?>
                            <li class="mf-pagi-dots">
                                <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= round($arResult["nEndPage"] + ($arResult["NavPageCount"] - $arResult["nEndPage"]) / 2) ?>">...</a>
                            </li>
                        <?endif;?>
                        <li>
                            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><?= $arResult["NavPageCount"] ?></a>
                        </li>
                    <?endif;?>
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"
                       title="<?= GetMessage("nav_next") ?>">
                        <svg class="next-page-arrow" version="1.1" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" preserveAspectRatio="xMidYMid meet"
                             viewBox="0 0 30 15"
                             width="30" height="15">
                            <defs>
                                <path d="M21.32 14.2L29 7.2" id="a8311Vz9N"></path>
                                <path d="M21.32 0.7L29 7.7" id="bDevWo6z4"></path>
                                <path d="M1.67 7.5L28.2 7.5" id="cukLGz9lf"></path>
                            </defs>
                            <g>
                                <g>
                                    <g>
                                        <use xlink:href="#a8311Vz9N" opacity="1" fill="#000000" fill-opacity="1"></use>
                                        <g>
                                            <use xlink:href="#a8311Vz9N" opacity="1" fill-opacity="0" stroke="#000000"
                                                 stroke-width="1" stroke-opacity="1"></use>
                                        </g>
                                    </g>
                                    <g>
                                        <use xlink:href="#bDevWo6z4" opacity="1" fill="#000000" fill-opacity="1"></use>
                                        <g>
                                            <use xlink:href="#bDevWo6z4" opacity="1" fill-opacity="0" stroke="#000000"
                                                 stroke-width="1" stroke-opacity="1"></use>
                                        </g>
                                    </g>
                                    <g>
                                        <use xlink:href="#cukLGz9lf" opacity="1" fill="#000000" fill-opacity="1"></use>
                                        <g>
                                            <use xlink:href="#cukLGz9lf" opacity="1" fill-opacity="0" stroke="#000000"
                                                 stroke-width="1" stroke-opacity="1"></use>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </svg>
                    </a>
                <?endif;
            } ?>
        </ul>
    </div>
</section>

