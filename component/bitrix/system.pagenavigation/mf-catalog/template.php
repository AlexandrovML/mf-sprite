<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);

/*if(!$arResult["NavShowAlways"])
{
	if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
		return;
}
*/
//if($_SERVER['REMOTE_ADDR'] == '81.44.174.122')
//{
//    log_array($arResult["NavQueryString"]);

    if(!empty($arResult["NavQueryString"]))
    {
	    $arNavQueryString = [];

        $arQuery = explode('&', $arResult["NavQueryString"]);
        foreach ($arQuery as $pair)
        {
            $arPair = explode('=', $pair);
//            log_array($arPair);
            if($arPair[0] != 'bxajaxid')
            {
	            $arNavQueryString[] = $pair;
            }
        }

	    $arResult["NavQueryString"] = implode('&', $arNavQueryString);
    }

//	log_array($arResult["NavQueryString"]);
//}
$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"]."&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?".$arResult["NavQueryString"] : "");
echo '<ul>';
if($arResult["bDescPageNumbering"] === true):
	$bFirst = true;
	if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
		if($arResult["bSavePage"]):
?>
			<li>
			<a class="mf-catalog-previous" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>" title="<?=GetMessage("nav_prev")?>">
                <? include '_prev-page-arrow.php'?>
            </a>
            </li>
<?
		else:
			if ($arResult["NavPageCount"] == ($arResult["NavPageNomer"]+1) ):
?>
                <li><a class="mf-catalog-previous" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" title="<?=GetMessage("nav_prev")?>">
				<? include '_prev-page-arrow.php'?>
                </a></li>
<?
			else:
?>
                <li><a class="mf-catalog-previous" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>" title="<?=GetMessage("nav_prev")?>">
				<? include '_prev-page-arrow.php'?>
                    </a></li>
<?
			endif;
		endif;
		
		if ($arResult["nStartPage"] < $arResult["NavPageCount"]):
			$bFirst = false;
			if($arResult["bSavePage"]):
?>
                <li><a class="mf-catalog-first" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>">1</a></li>
<?
			else:
?>
                <li><a class="mf-catalog-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a></li>
<?
			endif;
			if ($arResult["nStartPage"] < ($arResult["NavPageCount"] - 1)):
?>
			<li class="mf-pagi-dots mf-catalog-dots">...</li>
<?
			endif;
		endif;
	endif;
	do
	{
		$NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1;
		
		if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
?>
		<li class="<?=($bFirst ? "mf-catalog-first " : "")?>selected-page mf-catalog-current"><?=$NavRecordGroupPrint?></li>
<?
		elseif($arResult["nStartPage"] == $arResult["NavPageCount"] && $arResult["bSavePage"] == false):
?>
            <li><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="<?=($bFirst ? "mf-catalog-first" : "")?>"><?=$NavRecordGroupPrint?></a></li>
<?
		else:
?>
            <li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"<?
                ?> class="<?=($bFirst ? "mf-catalog-first" : "")?>"><?=$NavRecordGroupPrint?></a></li>
<?
		endif;
		
		$arResult["nStartPage"]--;
		$bFirst = false;
	} while($arResult["nStartPage"] >= $arResult["nEndPage"]);
	
	if ($arResult["NavPageNomer"] > 1):
		if ($arResult["nEndPage"] > 1):
			if ($arResult["nEndPage"] > 2):
?>
		<li class="mf-pagi-dots mf-catalog-dots">...</li>
<?
			endif;
?>
            <li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1"><?=$arResult["NavPageCount"]?></a></li>
<?
		endif;
	
?>
        <li><a class="mf-catalog-next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" title="<?=GetMessage("nav_next")?>">
            <? include '_next-page-arrow.php'?>
            </a></li>
<?
	endif; 

else:
	$bFirst = true;

	if ($arResult["NavPageNomer"] > 1):
		if($arResult["bSavePage"]):
?>
            <li><a class="mf-catalog-previous" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" title="<?=GetMessage("nav_prev")?>">
				<? include '_prev-page-arrow.php'?>
                </a></li>
<?
		else:
			if ($arResult["NavPageNomer"] > 2):
?>
                <li><a class="mf-catalog-previous" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]-1)?>" title="<?=GetMessage("nav_prev")?>">
				<? include '_prev-page-arrow.php'?>
                    </a></li>
<?
			else:
?>
                <li><a class="mf-catalog-previous" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" title="<?=GetMessage("nav_prev")?>">
				<? include '_prev-page-arrow.php'?>
                    </a></li>
<?
			endif;
		
		endif;
		
		if ($arResult["nStartPage"] > 1):
			$bFirst = false;
			if($arResult["bSavePage"]):
?>
                <li><a class="mf-catalog-first" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=1">1</a></li>
<?
			else:
?>
                <li><a class="mf-catalog-first" href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>">1</a></li>
<?
			endif;
			if ($arResult["nStartPage"] > 2):
?>
			<li class="mf-pagi-dots mf-catalog-dots">...</li>
<?
			endif;
		endif;
	endif;

	do
	{
		if ($arResult["nStartPage"] == $arResult["NavPageNomer"]):
?>
		<li class="<?=($bFirst ? "mf-catalog-first " : "")?>selected-page mf-catalog-current"><?=$arResult["nStartPage"]?></li>
<?
		elseif($arResult["nStartPage"] == 1 && $arResult["bSavePage"] == false):
?>
            <li><a href="<?=$arResult["sUrlPath"]?><?=$strNavQueryStringFull?>" class="<?=($bFirst ? "mf-catalog-first" : "")?>"><?=$arResult["nStartPage"]?></a></li>
<?
		else:
?>
            <li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["nStartPage"]?>"<?
                ?> class="<?=($bFirst ? "mf-catalog-first" : "")?>"><?=$arResult["nStartPage"]?></a></li>
<?
		endif;
		$arResult["nStartPage"]++;
		$bFirst = false;
	} while($arResult["nStartPage"] <= $arResult["nEndPage"]);
	
	if($arResult["NavPageNomer"] < $arResult["NavPageCount"]):
		if ($arResult["nEndPage"] < $arResult["NavPageCount"]):
			if ($arResult["nEndPage"] < ($arResult["NavPageCount"] - 1)):
?>
		<li class="mf-pagi-dots mf-catalog-dots">...</li>
<?
			endif;
?>
            <li><a href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=$arResult["NavPageCount"]?>"><?=$arResult["NavPageCount"]?></a></li>
<?
		endif;
?>
        <li><a class="mf-catalog-next" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>PAGEN_<?=$arResult["NavNum"]?>=<?=($arResult["NavPageNomer"]+1)?>" title="<?=GetMessage("nav_next")?>">
			<? include '_next-page-arrow.php'?>
        </a></li>
<?
	endif;
endif;

if ($arResult["bShowAll"]):
	if ($arResult["NavShowAll"]):
?>
        <li><a class="mf-catalog-pagen" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=0"><?=GetMessage("nav_paged")?></a></li>
<?
	else:
?>
        <li><a class="mf-catalog-all" href="<?=$arResult["sUrlPath"]?>?<?=$strNavQueryString?>SHOWALL_<?=$arResult["NavNum"]?>=1"><?=GetMessage("nav_all")?></a></li>
<?
	endif;
endif;
echo '</ul>';
?>

