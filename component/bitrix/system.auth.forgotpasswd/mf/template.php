<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * Bitrix vars
 *
 * @var CBitrixComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $arParams
 * @var array $arResult
 * @var array $arLangMessages
 * @var array $templateData
 *
 * @var string $templateFile
 * @var string $templateFolder
 * @var string $parentTemplateFolder
 * @var string $templateName
 * @var string $componentPath
 *
 * @var CDatabase $DB
 * @var CUser $USER
 * @var CMain $APPLICATION
 */
//if(method_exists($this, 'setFrameMode')) $this->setFrameMode(TRUE);
?>
<div class="popup_shop popup_input popup_auth_form">
    <div class="name">
        <div class="red">Восстановить пароль</div>
    </div>
    <div class="social">
        <p>
            Пожалуйста, введите свой адрес электронной почты ниже, чтобы получить ссылку на сброс пароля.
        </p>
    </div>
    <?
    ShowMessage($arParams["~AUTH_RESULT"]);
    ?>
    <div class="ss">
        <form class="forgot_form_err" name="bform" method="post" target="_top" action="<?= $arResult["AUTH_URL"] ?>">
            <? if (strlen($arResult["BACKURL"]) > 0): ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
            <? endif; ?>
            <input type="hidden" name="AUTH_FORM" value="Y">
            <input type="hidden" name="TYPE" value="SEND_PWD">
            <input type="hidden" name="TYPE_F" value="SEND_PWD_FORGOT">

            <div class="block">
                <label for=""><?= GetMessage("AUTH_EMAIL") ?></label>
                <input type="text" name="USER_EMAIL" maxlength="255">
            </div>
            <? if ($arResult["USE_CAPTCHA"]): ?>
                <div class="block">
                    <input type="hidden" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180"
                         height="40" alt="CAPTCHA"/>
                    <? echo GetMessage("system_auth_captcha") ?>
                    <input type="text" name="captcha_word" maxlength="50" value=""/>
                </div>
            <? endif ?>

            <div class="block login_btn_sign">
                <input type="submit" name="send_account_info" placeholder="Отправить" value="Отправить">
            </div>
        </form>
    </div>
    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
</div>