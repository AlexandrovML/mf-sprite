<?php
use \Bitrix\Main\Page\Asset;

if (!empty($arResult['SECTION']['UF_HIDE_FROM_INDEX']) && $arResult['SECTION']['UF_HIDE_FROM_INDEX'] == '1')
	Asset::getInstance()->addString('<meta name="robots" content="noindex" />');
