$(document).ready(function () {
    if($('select[name=search_area]').val() =='4'){
        $('.search_page-input').addClass('camera--active');
    }

    $('select[name=search_area]').change(function(){
        if($(this).val()!='4'){
            $('.api-search-input').prop( "disabled", false);
            $('.input_sbmt').prop( "disabled", false);
            $('.search_page-input').removeClass('camera--active');
        }else{
            $('.api-search-input').prop( "disabled", true );
            $('.input_sbmt').prop( "disabled", true);
            $('.search_page-input').addClass('camera--active');
        }
    });
});