<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $mobileColumns
 * @var array $arParams
 * @var string $templateFolder
 */

$usePriceInAdditionalColumn = in_array('PRICE', $arParams['COLUMNS_LIST']) && $arParams['PRICE_DISPLAY_MODE'] === 'Y';
$useSumColumn = in_array('SUM', $arParams['COLUMNS_LIST']);
$useActionColumn = in_array('DELETE', $arParams['COLUMNS_LIST']);

$restoreColSpan = 2 + $usePriceInAdditionalColumn + $useSumColumn + $useActionColumn;

$positionClassMap = array(
	'left' => 'basket-item-label-left',
	'center' => 'basket-item-label-center',
	'right' => 'basket-item-label-right',
	'bottom' => 'basket-item-label-bottom',
	'middle' => 'basket-item-label-middle',
	'top' => 'basket-item-label-top'
);

$discountPositionClass = '';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = '';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}
?>
<script id="basket-item-template" type="text/html">
	<div class="mf-cart-item basket-items-list-item-container{{#SHOW_RESTORE}} basket-items-list-item-container-expend{{/SHOW_RESTORE}}{{#DELAYED}} basket-item-hide{{/DELAYED}}" id="basket-item-{{ID}}" data-entity="basket-item" data-id="{{ID}}">
		{{#SHOW_RESTORE}}
			<div class="basket-items-list-item-notification">
				<div class="basket-items-list-item-notification-inner basket-items-list-item-notification-removed" id="basket-item-height-aligner-{{ID}}">
					{{#SHOW_LOADING}}
						<div class="basket-items-list-item-overlay"></div>
					{{/SHOW_LOADING}}
					<div class="basket-items-list-item-removed-container">
						<div>
							<?=Loc::getMessage('SBB_GOOD_CAP')?> <strong>{{NAME}}</strong> <?=Loc::getMessage('SBB_BASKET_ITEM_DELETED')?>.
						</div>
						<div class="basket-items-list-item-removed-block">
							<a href="javascript:void(0)" data-entity="basket-item-restore-button">
								<?=Loc::getMessage('SBB_BASKET_ITEM_RESTORE')?>
							</a>
							<span class="basket-items-list-item-clear-btn" data-entity="basket-item-close-restore-button"></span>
						</div>
					</div>
				</div>
			</div>
		{{/SHOW_RESTORE}}
		{{^SHOW_RESTORE}}
            <div class="mf-cart-i-img-cnt">
                <a href="{{DETAIL_PAGE_URL}}">
                    <div class="mf-cart-i-img" data-img style="background-image: url('{{{IMAGE_URL}}}{{^IMAGE_URL}}<?=$templateFolder?>/images/no_photo.png{{/IMAGE_URL}}');"></div>
                </a>
            </div>

            <div class="mf-cart-i-inf">
                <a class="mf-cart-link-title" href="{{DETAIL_PAGE_URL}}">{{NAME}}</a>
                <ul>
		            <?
		            if (!empty($arParams['PRODUCT_BLOCKS_ORDER']))
		            {
			            foreach ($arParams['PRODUCT_BLOCKS_ORDER'] as $blockName)
			            {
				            switch (trim((string)$blockName))
				            {
					            case 'props':
						            if (in_array('PROPS', $arParams['COLUMNS_LIST']))
						            {
							            ?>
                                        {{#PROPS}}
                                        <li>
                                            <span>{{{NAME}}}</span>
                                            <span data-entity="basket-item-property-value" data-property-code="{{CODE}}">{{{VALUE}}}</span>
                                        </li>
                                        {{/PROPS}}
							            <?
						            }
						            break;

					            case 'sku':
						            ?>
                                    {{#SKU_BLOCK_LIST}}
                                    {{#IS_IMAGE}}
                                    <li class="basket-item-property basket-item-property-scu-image"
                                         data-entity="basket-item-sku-block">
                                        <div class="basket-item-property-name">{{NAME}}</div>
                                        <div class="basket-item-property-value">
                                            <ul class="basket-item-scu-list">
                                                {{#SKU_VALUES_LIST}}
                                                <li class="basket-item-scu-item{{#SELECTED}} selected{{/SELECTED}}
																		{{#NOT_AVAILABLE_OFFER}} not-available{{/NOT_AVAILABLE_OFFER}}"
                                                    title="{{NAME}}"
                                                    data-entity="basket-item-sku-field"
                                                    data-initial="{{#SELECTED}}true{{/SELECTED}}{{^SELECTED}}false{{/SELECTED}}"
                                                    data-value-id="{{VALUE_ID}}"
                                                    data-sku-name="{{NAME}}"
                                                    data-property="{{PROP_CODE}}">
                                                    <span class="basket-item-scu-item-inner"
                                                          style="background-image: url({{PICT}});"></span>
                                                </li>
                                                {{/SKU_VALUES_LIST}}
                                            </ul>
                                        </div>
                                    </li>
                                    {{/IS_IMAGE}}

                                    {{^IS_IMAGE}}
                                    <li class="basket-item-property basket-item-property-scu-text"
                                         data-entity="basket-item-sku-block">
                                        <span class="basket-item-property-name">{{NAME}}</span>
                                        <span class="basket-item-property-value">
                                            <ul class="basket-item-scu-list">
                                                {{#SKU_VALUES_LIST}}
                                                <li class="basket-item-scu-item{{#SELECTED}} selected{{/SELECTED}}
																		{{#NOT_AVAILABLE_OFFER}} not-available{{/NOT_AVAILABLE_OFFER}}"
                                                    title="{{NAME}}"
                                                    data-entity="basket-item-sku-field"
                                                    data-initial="{{#SELECTED}}true{{/SELECTED}}{{^SELECTED}}false{{/SELECTED}}"
                                                    data-value-id="{{VALUE_ID}}"
                                                    data-sku-name="{{NAME}}"
                                                    data-property="{{PROP_CODE}}">
                                                    <span class="basket-item-scu-item-inner">{{NAME}}</span>
                                                </li>
                                                {{/SKU_VALUES_LIST}}
                                            </ul>
                                        </span>
                                    </li>
                                    {{/IS_IMAGE}}
                                    {{/SKU_BLOCK_LIST}}

                                    {{#HAS_SIMILAR_ITEMS}}
                                    <li class="basket-items-list-item-double" data-entity="basket-item-sku-notification">
                                        <div class="alert alert-info alert-dismissable text-center">
                                            {{#USE_FILTER}}
                                            <a href="javascript:void(0)"
                                               class="basket-items-list-item-double-anchor"
                                               data-entity="basket-item-show-similar-link">
                                                {{/USE_FILTER}}
									            <?=Loc::getMessage('SBB_BASKET_ITEM_SIMILAR_P1')?>{{#USE_FILTER}}</a>{{/USE_FILTER}}
								            <?=Loc::getMessage('SBB_BASKET_ITEM_SIMILAR_P2')?>
                                            {{SIMILAR_ITEMS_QUANTITY}} {{MEASURE_TEXT}}
                                            <br>
                                            <a href="javascript:void(0)" class="basket-items-list-item-double-anchor"
                                               data-entity="basket-item-merge-sku-link">
									            <?=Loc::getMessage('SBB_BASKET_ITEM_SIMILAR_P3')?>
                                                {{TOTAL_SIMILAR_ITEMS_QUANTITY}} {{MEASURE_TEXT}}?
                                            </a>
                                        </div>
                                    </li>
                                    {{/HAS_SIMILAR_ITEMS}}
						            <?
						            break;
					            case 'columns':
						            ?>
                                    {{#COLUMN_LIST}}
                                    {{#IS_IMAGE}}
                                    <li data-entity="basket-item-property">
                                        <div class="basket-item-property-custom-name">{{NAME}}</div>
                                        <div class="basket-item-property-custom-value">
                                            {{#VALUE}}
                                            <span>
                                                <img class="basket-item-custom-block-photo-item"
                                                     src="{{{IMAGE_SRC}}}" data-image-index="{{INDEX}}"
                                                     data-column-property-code="{{CODE}}">
                                            </span>
                                            {{/VALUE}}
                                        </div>
                                    </li>
                                    {{/IS_IMAGE}}

                                    {{#IS_TEXT}}
                                    <li data-entity="basket-item-property">
                                        <span>{{NAME}}</span>&nbsp;<span data-column-property-code="{{CODE}}" data-entity="basket-item-property-column-value">{{{VALUE}}}</span>
                                    </li>
                                    {{/IS_TEXT}}

                                    {{#IS_HTML}}
                                    <li data-entity="basket-item-property">
                                        <span class="basket-item-property-custom-name">{{NAME}}</span>
                                        <span class="basket-item-property-custom-value"
                                             data-column-property-code="{{CODE}}"
                                             data-entity="basket-item-property-column-value">
                                            {{{VALUE}}}
                                        </span>
                                    </li>
                                    {{/IS_HTML}}

                                    {{#IS_LINK}}
                                    <li data-entity="basket-item-property">
                                        <div class="basket-item-property-custom-name">{{NAME}}</div>
                                        <div class="basket-item-property-custom-value"
                                             data-column-property-code="{{CODE}}"
                                             data-entity="basket-item-property-column-value">
                                            {{#VALUE}}
                                            {{{LINK}}}{{^IS_LAST}}<br>{{/IS_LAST}}
                                            {{/VALUE}}
                                        </div>
                                    </li>
                                    {{/IS_LINK}}
                                    {{/COLUMN_LIST}}
						            <?
						            break;
				            }
			            }
		            }
		            ?>
                </ul>
            </div>
            <div class="mf-cart-i-calc">
                <span class="mf-cart-i-title">Количество</span>
                <span class="mf-cart-i-counter{{#NOT_AVAILABLE}} disabled{{/NOT_AVAILABLE}}" data-entity="basket-item-quantity-block">
                    <span class="basket-item-amount-btn-minus" data-entity="basket-item-quantity-minus"></span>
                    <input type="text" class="basket-item-amount-filed1 mf-cart-counter" value="{{QUANTITY}}"
                           {{#NOT_AVAILABLE}} disabled="disabled"{{/NOT_AVAILABLE}}
                    data-value="{{QUANTITY}}" data-entity="basket-item-quantity-field"
                    id="basket-item-quantity-{{ID}}">
                    <span class="basket-item-amount-btn-plus" data-entity="basket-item-quantity-plus"></span>

                    <div class="basket-item-amount-field-description" style="display: none;">
                        <?
                        if ($arParams['PRICE_DISPLAY_MODE'] === 'Y')
                        {
                            ?>
                            {{MEASURE_TEXT}}
                            <?
                        }
                        else
                        {
                            ?>
                            {{#SHOW_PRICE_FOR}}
                            {{MEASURE_RATIO}} {{MEASURE_TEXT}} =
                            <span id="basket-item-price-{{ID}}">{{{PRICE_FORMATED}}}</span>
                            {{/SHOW_PRICE_FOR}}
                            {{^SHOW_PRICE_FOR}}
                            {{MEASURE_TEXT}}
                            {{/SHOW_PRICE_FOR}}
                            <?
                        }
                        ?>
                    </div>
                    {{#SHOW_LOADING}}
                    <div class="basket-items-list-item-overlay"></div>
                    {{/SHOW_LOADING}}
                </span>
            </div>

            <? if ($usePriceInAdditionalColumn) { ?>
                <div class="mf-cart-i-price">
                    <span class="mf-cart-i-title"><?=Loc::getMessage('SBB_BASKET_ITEM_PRICE_FOR')?> {{MEASURE_RATIO}} {{MEASURE_TEXT}}</span>
                    <span class="mf-cart-i-price" id="basket-item-price-{{ID}}">
                        {{{PRICE_FORMATED}}}
                    </span>

                    {{#SHOW_DISCOUNT_PRICE}}
                        <span class="mf-cart-i-price-disc">
                            {{{FULL_PRICE_FORMATED}}}
                        </span>
                    {{/SHOW_DISCOUNT_PRICE}}

                    {{#SHOW_LOADING}}
                    <div class="basket-items-list-item-overlay"></div>
                    {{/SHOW_LOADING}}
                </div>
            <? } ?>

            <? if ($useSumColumn) { ?>
                <div class="mf-cart-i-summ-price">
                    <span class="mf-cart-i-title"><?=Loc::getMessage('SBB_BASKET_ITEM_PRICE_SUMM')?></span>

                    <span class="mf-cart-i-price" id="basket-item-sum-price-{{ID}}">
                        {{{SUM_PRICE_FORMATED}}}
                    </span>

                    <?/*
                    {{#SHOW_DISCOUNT_PRICE}}
                    <span class="mf-cart-i-price-disc" id="basket-item-sum-price-old-{{ID}}">
                        {{{SUM_FULL_PRICE_FORMATED}}}
                    </span>
                    {{/SHOW_DISCOUNT_PRICE}}

                    {{#SHOW_DISCOUNT_PRICE}}
                    <div class="basket-item-price-difference">
                        <?=Loc::getMessage('SBB_BASKET_ITEM_ECONOMY')?>
                        <span id="basket-item-sum-price-difference-{{ID}}" style="white-space: nowrap;">
                            {{{SUM_DISCOUNT_PRICE_FORMATED}}}
                        </span>
                    </div>
                    {{/SHOW_DISCOUNT_PRICE}}

                    */?>
                    {{#SHOW_LOADING}}
                        <div class="basket-items-list-item-overlay"></div>
                    {{/SHOW_LOADING}}
                </div>
            <? } ?>



            <?/*
            <div class="remove-cart-item">
                <svg class="remove-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64">
                    <g>
                        <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                    </g>
                </svg>
            </div>*/?>

            <? if ($useActionColumn) { ?>
                <div class="remove-cart-item">
                    <span class="basket-item-actions-remove" data-entity="basket-item-delete"></span>
                    {{#SHOW_LOADING}}
                    <div class="basket-items-list-item-overlay"></div>
                    {{/SHOW_LOADING}}
                </div>
            <? } ?>
		{{/SHOW_RESTORE}}
	</div>
</script>