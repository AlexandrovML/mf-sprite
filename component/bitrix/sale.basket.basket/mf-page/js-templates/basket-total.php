<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-template" type="text/html">
    <div class="mf-form-header">
        <div class="mf-cartform-total-inf">
            <div class="mf-cartform-t-pr">
	            <?=Loc::getMessage('SBB_TOTAL')?>: <span class="mf-t-pr-num" data-entity="basket-total-count">{{{PRODUCTS_COUNT}}}</span>
                <?/*
                <div class="basket-checkout-block-total-description">
                    {{#WEIGHT_FORMATED}}
		            <?=Loc::getMessage('SBB_WEIGHT')?>: {{{WEIGHT_FORMATED}}}
                    {{#SHOW_VAT}}<br>{{/SHOW_VAT}}
                    {{/WEIGHT_FORMATED}}
                    {{#SHOW_VAT}}
		            <?=Loc::getMessage('SBB_VAT')?>: {{{VAT_SUM_FORMATED}}}
                    {{/SHOW_VAT}}
                </div>
                */?>
            </div>
            <div class="mf-cartform-t-price" data-entity="basket-checkout-aligner">
                <ul>
                    {{#DISCOUNT_PRICE_FORMATED}}
                    <li class="t-pr-disc">
                        <?/*410 000,00<span class="curr"> руб.</span>*/?>
                        <span>
                            {{{PRICE_WITHOUT_DISCOUNT_FORMATED}}}
                        </span>
                    </li>
                    {{/DISCOUNT_PRICE_FORMATED}}

                    <li class="t-pr">
                        <?/*390 800,00<span class="curr"> руб.</span>*/?>
                        <span data-entity="basket-total-price">
                            {{{PRICE_FORMATED}}}
                        </span>
                    </li>

                    <?/*<li class="t-pr-disc-percent">скидка -5%</li>*/?>

                    <?/*
                    {{#DISCOUNT_PRICE_FORMATED}}
                    <div class="basket-coupon-block-total-price-difference">
		                <?=Loc::getMessage('SBB_BASKET_ITEM_ECONOMY')?>
                        <span style="white-space: nowrap;">{{{DISCOUNT_PRICE_FORMATED}}}</span>
                    </div>
                    {{/DISCOUNT_PRICE_FORMATED}}
                    */?>
                </ul>
            </div>
        </div>
    </div>
        <div class="mf-form-body">
            <div class="mf-form-cart-body">
	            <? if ($arParams['HIDE_COUPON'] !== 'Y') { ?>
                    <label class="mf-form-label mf-form-label__promo noPromo mf-form-label--coupon" style="font-size: 10.5pt; color: rgb(160, 160, 160);">
	                    <?=Loc::getMessage('SBB_COUPON_ENTER')?>
                        <input type="text" class="form-control" id="" placeholder="" data-entity="basket-coupon-input"  style="padding-top: 2.5px; padding-bottom: 7.5px;">
                        <span class="basket-coupon-block-coupon-btn coupon-btn-txt">Применить</span>
                        <span class="coupon-btn-note">(после ввода промокода нажмите "Применить")</span>
                    </label>
	            <? } ?>


                <? if ($arParams['HIDE_COUPON'] !== 'Y') { ?>
                    <div class="basket-coupon-alert-section checkout-partbody">
                        <div class="basket-coupon-alert-inner">
                            {{#COUPON_LIST}}
                            {{#DISCOUNT_NAME}}<p>{{{DISCOUNT_NAME}}}</p>{{/DISCOUNT_NAME}}
                            <div class="basket-coupon-alert text-{{CLASS}}">
                                <span class="basket-coupon-text">
                                    <strong>{{COUPON}}</strong>
                                    <span class="bx-soa-tooltip bx-soa-tooltip-coupon bx-soa-tooltip-success tooltip top"><span class="tooltip-arrow"></span><span class="tooltip-inner">{{JS_CHECK_CODE}}</span></span>
                                </span>

                                <span class="close-link" data-entity="basket-coupon-delete" data-coupon="{{COUPON}}">
                                    <? //=Loc::getMessage('SBB_DELETE')?>
                                </span>
                            </div>
                            {{/COUPON_LIST}}
                        </div>
                    </div>

                    <?/*
                    <div class="basket-coupon-alert-section">
                        <div class="basket-coupon-alert-inner">
                            {{#COUPON_LIST}}
                            <div class="basket-coupon-alert text-{{CLASS}}">
                                <span class="basket-coupon-text">
                                    <strong>{{COUPON}}</strong> - <?=Loc::getMessage('SBB_COUPON')?> {{JS_CHECK_CODE}}
                                    {{#DISCOUNT_NAME}}({{{DISCOUNT_NAME}}}){{/DISCOUNT_NAME}}
                                </span>
                                <span class="close-link" data-entity="basket-coupon-delete" data-coupon="{{COUPON}}">
                                    <? //=Loc::getMessage('SBB_DELETE')?>
                                </span>
                            </div>
                            {{/COUPON_LIST}}
                        </div>
                    </div>
                    */?>
	            <? } ?>

                <?/*
                <label class="mf-form-label mf-form-label__promo noPromo mf-form-label--coupon" style="font-size: 10.5pt; color: rgb(160, 160, 160);">
                    Введите промокод
                    <input type="text" name="cart_promocode" value="347yut9182" style="padding-top: 2.5px; padding-bottom: 7.5px;">
                    <a href="" class="coupon-btn-txt">Применить</a>
                    <div class="mf-cartform-promotext">
                        Ваша скидка <span class="pr-disc">-5%</span>
                    </div>
                    <span class="coupon-btn-note">(после ввода промокода нажмите "Применить")</span>
                    <p></p>
                </label>
                */?>
            </div>
        </div>

        <div id="mf-cart-form-footer" class="mf-form-footer">
            <span id="cart-submit-btn" class="mf-submit-el">
                <button class="button-link black-tr-b{{#DISABLE_CHECKOUT}} disabled{{/DISABLE_CHECKOUT}}" data-entity="basket-checkout-button">
                    <?=Loc::getMessage('SBB_ORDER')?>
                </button>
            </span>

            {{^SHOW_CHECKOUT_BTN}}
            <div class="b-ghost">
                <div id="cart-section-error-msg" class="cart-section-error-msg"><?=Loc::getMessage('SBB_CART_SUBSCRIPTION_ERROR')?></div>
            </div>
            {{/SHOW_CHECKOUT_BTN}}

            {{#BASKET_ERROR_MESSAGE}}
                <div class="cart-section-error-msg" style="background: none; padding: 0">{{{BASKET_ERROR_MESSAGE}}}</div>
            {{/BASKET_ERROR_MESSAGE}}
        </div>

</script>