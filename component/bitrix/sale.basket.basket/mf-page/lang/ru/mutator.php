<?
$MESS["SBB_DETAIL_PICTURE_NAME"] = "Детальное изображение";
$MESS["SBB_PREVIEW_TEXT_NAME"] = "Краткое описание";
$MESS["SBB_PRICE_TYPE_NAME"] = "Тип цены";
$MESS["SBB_DISCOUNT_NAME"] = "Скидка";
$MESS["SBB_WEIGHT_NAME"] = "Вес";
$MESS["SBB_COUPON_INFO"] = "Скидка <b>#VALUE#</b> на весь заказ по промокоду";
$MESS["BASKET_ERROR_MESSAGE"] = "Максимальная сумма заказа составляет <b>#VALUE#</b>";

?>