<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Localization\Loc;

/**
 * @var array $arParams
 */
?>
<script id="basket-total-template" type="text/html">
        <div class="mf-cartform-total-inf">
            <div class="mf-cartform-t-pr">
	            <?=Loc::getMessage('SBB_TOTAL')?>: <span class="mf-t-pr-num" data-entity="basket-total-count">{{{PRODUCTS_COUNT}}}</span>
                <?/*
                <div class="basket-checkout-block-total-description">
                    {{#WEIGHT_FORMATED}}
		            <?=Loc::getMessage('SBB_WEIGHT')?>: {{{WEIGHT_FORMATED}}}
                    {{#SHOW_VAT}}<br>{{/SHOW_VAT}}
                    {{/WEIGHT_FORMATED}}
                    {{#SHOW_VAT}}
		            <?=Loc::getMessage('SBB_VAT')?>: {{{VAT_SUM_FORMATED}}}
                    {{/SHOW_VAT}}
                </div>
                */?>
            </div>
            <div class="mf-cartform-t-price" data-entity="basket-checkout-aligner">
		        <? if ($arParams['HIDE_COUPON'] !== 'Y') { ?>
                    <div class="basket-coupon-section">
                        <div class="basket-coupon-block-field">
                            <div class="basket-coupon-block-field-description">
						        <?=Loc::getMessage('SBB_COUPON_ENTER')?>:
                            </div>
                            <div class="form">
                                <div class="form-group" style="position: relative;">
                                    <input type="text" class="form-control" id="" placeholder="" data-entity="basket-coupon-input">
                                    <span class=" basket-coupon-block-coupon-btn"></span>
                                </div>
                            </div>

                        </div>
                    </div>
                <? } ?>

                <ul>
                    {{#DISCOUNT_PRICE_FORMATED}}
                    <li class="t-pr-disc">
                        <?/*410 000,00<span class="curr"> руб.</span>*/?>
                        <span>
                            {{{PRICE_WITHOUT_DISCOUNT_FORMATED}}}
                        </span>
                    </li>
                    {{/DISCOUNT_PRICE_FORMATED}}

                    <li class="t-pr">
                        <?/*390 800,00<span class="curr"> руб.</span>*/?>
                        <span data-entity="basket-total-price">
                            {{{PRICE_FORMATED}}}
                        </span>
                    </li>

                    <?/*<li class="t-pr-disc-percent">скидка -5%</li>*/?>

                    <?/*
                    {{#DISCOUNT_PRICE_FORMATED}}
                    <div class="basket-coupon-block-total-price-difference">
		                <?=Loc::getMessage('SBB_BASKET_ITEM_ECONOMY')?>
                        <span style="white-space: nowrap;">{{{DISCOUNT_PRICE_FORMATED}}}</span>
                    </div>
                    {{/DISCOUNT_PRICE_FORMATED}}
                    */?>
                </ul>


                <div class="basket-checkout-section">
                    <div class="basket-checkout-section-inner<?=(($arParams['HIDE_COUPON'] == 'Y') ? ' justify-content-between' : '')?>">
                        <div class="basket-checkout-block basket-checkout-block-btn">
                            <button class="btn btn-lg btn-primary basket-btn-checkout{{#DISABLE_CHECKOUT}} disabled{{/DISABLE_CHECKOUT}}"
                                    data-entity="basket-checkout-button">
						        <?=Loc::getMessage('SBB_ORDER')?>
                            </button>
                        </div>
                    </div>
                </div>

		        <? if ($arParams['HIDE_COUPON'] !== 'Y') { ?>
                    <div class="basket-coupon-alert-section">
                        <div class="basket-coupon-alert-inner">
                            {{#COUPON_LIST}}
                            <div class="basket-coupon-alert text-{{CLASS}}">
                                <span class="basket-coupon-text">
                                    <strong>{{COUPON}}</strong> - <?=Loc::getMessage('SBB_COUPON')?> {{JS_CHECK_CODE}}
                                    {{#DISCOUNT_NAME}}({{{DISCOUNT_NAME}}}){{/DISCOUNT_NAME}}
                                </span>
                                <span class="close-link" data-entity="basket-coupon-delete" data-coupon="{{COUPON}}">
                                    <?=Loc::getMessage('SBB_DELETE')?>
                                </span>
                            </div>
                            {{/COUPON_LIST}}
                        </div>
                    </div>
                <? } ?>
            </div>
        </div>

</script>