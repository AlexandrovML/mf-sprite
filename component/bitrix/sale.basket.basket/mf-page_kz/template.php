<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main;
use Bitrix\Main\Localization\Loc;
\Bitrix\Main\UI\Extension::load("ui.fonts.ruble");

/**
 * @var array $arParams
 * @var array $arResult
 * @var string $templateFolder
 * @var string $templateName
 * @var CMain $APPLICATION
 * @var CBitrixBasketComponent $component
 * @var CBitrixComponentTemplate $this
 * @var array $giftParameters
 */

$documentRoot = Main\Application::getDocumentRoot();

if (empty($arParams['TEMPLATE_THEME']))
{
	$arParams['TEMPLATE_THEME'] = Main\ModuleManager::isModuleInstalled('bitrix.eshop') ? 'site' : 'blue';
}

if ($arParams['TEMPLATE_THEME'] === 'site')
{
	$templateId = Main\Config\Option::get('main', 'wizard_template_id', 'eshop_bootstrap', $component->getSiteId());
	$templateId = preg_match('/^eshop_adapt/', $templateId) ? 'eshop_adapt' : $templateId;
	$arParams['TEMPLATE_THEME'] = Main\Config\Option::get('main', 'wizard_'.$templateId.'_theme_id', 'blue', $component->getSiteId());
}

//if (!empty($arParams['TEMPLATE_THEME']))
//{
//	if (!is_file($documentRoot.'/bitrix/css/main/themes/'.$arParams['TEMPLATE_THEME'].'/style.css'))
//	{
//		$arParams['TEMPLATE_THEME'] = 'blue';
//	}
//}

if (!isset($arParams['DISPLAY_MODE']) || !in_array($arParams['DISPLAY_MODE'], array('extended', 'compact')))
{
	$arParams['DISPLAY_MODE'] = 'extended';
}

$arParams['USE_DYNAMIC_SCROLL'] = isset($arParams['USE_DYNAMIC_SCROLL']) && $arParams['USE_DYNAMIC_SCROLL'] === 'N' ? 'N' : 'Y';
$arParams['SHOW_FILTER'] = isset($arParams['SHOW_FILTER']) && $arParams['SHOW_FILTER'] === 'N' ? 'N' : 'Y';

$arParams['PRICE_DISPLAY_MODE'] = isset($arParams['PRICE_DISPLAY_MODE']) && $arParams['PRICE_DISPLAY_MODE'] === 'N' ? 'N' : 'Y';

if (!isset($arParams['TOTAL_BLOCK_DISPLAY']) || !is_array($arParams['TOTAL_BLOCK_DISPLAY']))
{
	$arParams['TOTAL_BLOCK_DISPLAY'] = array('top');
}

if (empty($arParams['PRODUCT_BLOCKS_ORDER']))
{
	$arParams['PRODUCT_BLOCKS_ORDER'] = 'props,sku,columns';
}

if (is_string($arParams['PRODUCT_BLOCKS_ORDER']))
{
	$arParams['PRODUCT_BLOCKS_ORDER'] = explode(',', $arParams['PRODUCT_BLOCKS_ORDER']);
}

$arParams['USE_PRICE_ANIMATION'] = isset($arParams['USE_PRICE_ANIMATION']) && $arParams['USE_PRICE_ANIMATION'] === 'N' ? 'N' : 'Y';
$arParams['EMPTY_BASKET_HINT_PATH'] = isset($arParams['EMPTY_BASKET_HINT_PATH']) ? (string)$arParams['EMPTY_BASKET_HINT_PATH'] : '/';
$arParams['USE_ENHANCED_ECOMMERCE'] = isset($arParams['USE_ENHANCED_ECOMMERCE']) && $arParams['USE_ENHANCED_ECOMMERCE'] === 'Y' ? 'Y' : 'N';
$arParams['DATA_LAYER_NAME'] = isset($arParams['DATA_LAYER_NAME']) ? trim($arParams['DATA_LAYER_NAME']) : 'dataLayer';
$arParams['BRAND_PROPERTY'] = isset($arParams['BRAND_PROPERTY']) ? trim($arParams['BRAND_PROPERTY']) : '';

if ($arParams['USE_GIFTS'] === 'Y')
{
	CBitrixComponent::includeComponentClass('bitrix:sale.products.gift.basket');

	$giftParameters = array(
		'SHOW_PRICE_COUNT' => 1,
		'PRODUCT_SUBSCRIPTION' => 'N',
		'PRODUCT_ID_VARIABLE' => 'id',
		'USE_PRODUCT_QUANTITY' => 'N',
		'ACTION_VARIABLE' => 'actionGift',
		'ADD_PROPERTIES_TO_BASKET' => 'Y',
		'PARTIAL_PRODUCT_PROPERTIES' => 'Y',

		'BASKET_URL' => $APPLICATION->GetCurPage(),
		'APPLIED_DISCOUNT_LIST' => $arResult['APPLIED_DISCOUNT_LIST'],
		'FULL_DISCOUNT_LIST' => $arResult['FULL_DISCOUNT_LIST'],

		'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
		'PRICE_VAT_INCLUDE' => $arParams['PRICE_VAT_SHOW_VALUE'],
		'CACHE_GROUPS' => $arParams['CACHE_GROUPS'],

		'BLOCK_TITLE' => $arParams['GIFTS_BLOCK_TITLE'],
		'HIDE_BLOCK_TITLE' => $arParams['GIFTS_HIDE_BLOCK_TITLE'],
		'TEXT_LABEL_GIFT' => $arParams['GIFTS_TEXT_LABEL_GIFT'],

		'DETAIL_URL' => isset($arParams['GIFTS_DETAIL_URL']) ? $arParams['GIFTS_DETAIL_URL'] : null,
		'PRODUCT_QUANTITY_VARIABLE' => $arParams['GIFTS_PRODUCT_QUANTITY_VARIABLE'],
		'PRODUCT_PROPS_VARIABLE' => $arParams['GIFTS_PRODUCT_PROPS_VARIABLE'],
		'SHOW_OLD_PRICE' => $arParams['GIFTS_SHOW_OLD_PRICE'],
		'SHOW_DISCOUNT_PERCENT' => $arParams['GIFTS_SHOW_DISCOUNT_PERCENT'],
		'DISCOUNT_PERCENT_POSITION' => $arParams['DISCOUNT_PERCENT_POSITION'],
		'MESS_BTN_BUY' => $arParams['GIFTS_MESS_BTN_BUY'],
		'MESS_BTN_DETAIL' => $arParams['GIFTS_MESS_BTN_DETAIL'],
		'CONVERT_CURRENCY' => $arParams['GIFTS_CONVERT_CURRENCY'],
		'HIDE_NOT_AVAILABLE' => $arParams['GIFTS_HIDE_NOT_AVAILABLE'],

		'PRODUCT_ROW_VARIANTS' => '',
		'PAGE_ELEMENT_COUNT' => 0,
		'DEFERRED_PRODUCT_ROW_VARIANTS' => \Bitrix\Main\Web\Json::encode(
			SaleProductsGiftBasketComponent::predictRowVariants(
				$arParams['GIFTS_PAGE_ELEMENT_COUNT'],
				$arParams['GIFTS_PAGE_ELEMENT_COUNT']
			)
		),
		'DEFERRED_PAGE_ELEMENT_COUNT' => $arParams['GIFTS_PAGE_ELEMENT_COUNT'],

		'ADD_TO_BASKET_ACTION' => 'BUY',
		'PRODUCT_DISPLAY_MODE' => 'Y',
		'PRODUCT_BLOCKS_ORDER' => isset($arParams['GIFTS_PRODUCT_BLOCKS_ORDER']) ? $arParams['GIFTS_PRODUCT_BLOCKS_ORDER'] : '',
		'SHOW_SLIDER' => isset($arParams['GIFTS_SHOW_SLIDER']) ? $arParams['GIFTS_SHOW_SLIDER'] : '',
		'SLIDER_INTERVAL' => isset($arParams['GIFTS_SLIDER_INTERVAL']) ? $arParams['GIFTS_SLIDER_INTERVAL'] : '',
		'SLIDER_PROGRESS' => isset($arParams['GIFTS_SLIDER_PROGRESS']) ? $arParams['GIFTS_SLIDER_PROGRESS'] : '',
		'LABEL_PROP_POSITION' => $arParams['LABEL_PROP_POSITION'],

		'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
		'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
		'BRAND_PROPERTY' => $arParams['BRAND_PROPERTY']
	);
}

\CJSCore::Init(array('fx', 'popup', 'ajax'));

//$this->addExternalCss('/bitrix/css/main/bootstrap.css');
//$this->addExternalCss($templateFolder.'/themes/'.$arParams['TEMPLATE_THEME'].'/style.css');

$this->addExternalJs($templateFolder.'/js/mustache.js');
$this->addExternalJs($templateFolder.'/js/action-pool.js');
$this->addExternalJs($templateFolder.'/js/filter.js');
$this->addExternalJs($templateFolder.'/js/component.js');

$mobileColumns = isset($arParams['COLUMNS_LIST_MOBILE'])
	? $arParams['COLUMNS_LIST_MOBILE']
	: $arParams['COLUMNS_LIST'];
$mobileColumns = array_fill_keys($mobileColumns, true);
/*
$this->SetViewTarget("main_class_page");
?>cart-section<?
$this->EndViewTarget("main_class_page");
*/
//$_SESSION['BASKET_USER_DATA'] = [];
if(empty($_SESSION['BASKET_USER_DATA']) && $USER->IsAuthorized())
{
	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
//	var_dump($arUser);
	$_SESSION['BASKET_USER_DATA'] = [
		'username' => $USER->GetFirstName(),
		'email' => $USER->GetEmail(),
		'phone' => $arUser['PERSONAL_PHONE'],
	];
}
?>
<?/*<div class="fix-block"><? include_once $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/include/_bc.php'; ?></div>*/?>
<section class="cart-section-background">
    <div id="basket-root" class="fix-block" style="opacity: 0;">

        <div class="cart-content">
            <div class="mf-cart-list">
				<?

				$jsTemplates = new Main\IO\Directory($documentRoot.$templateFolder.'/js-templates');
				/** @var Main\IO\File $jsTemplate */
				foreach ($jsTemplates->getChildren() as $jsTemplate)
				{
					include($jsTemplate->getPath());
				}

				$displayModeClass = $arParams['DISPLAY_MODE'] === 'compact' ? ' basket-items-list-wrapper-compact' : '';

				if (empty($arResult['ERROR_MESSAGE']))
				{
					if ($arParams['USE_GIFTS'] === 'Y' && $arParams['GIFTS_PLACE'] === 'TOP')
					{
						$APPLICATION->IncludeComponent(
							'bitrix:sale.products.gift.basket',
							'.default',
							$giftParameters,
							$component
						);
					}

				if ($arResult['BASKET_ITEM_MAX_COUNT_EXCEEDED'])
				{
					?>
                    <div id="basket-item-message">
						<?=Loc::getMessage('SBB_BASKET_ITEM_MAX_COUNT_EXCEEDED', array('#PATH#' => $arParams['PATH_TO_BASKET']))?>
                    </div>
				<?
				}
				?>
                    <div class="bx-basket bx-step-opacity">
						<?
						/*if (
							$arParams['BASKET_WITH_ORDER_INTEGRATION'] !== 'Y'
							&& in_array('top', $arParams['TOTAL_BLOCK_DISPLAY'])
						)
						{
							?><div data-entity="basket-total-block"></div><?
						}*/
						?>
                        <div class="alert alert-warning alert-dismissable checkout-user-info checkout-user-info--basket-warning" id="basket-warning" style="display: none;">
							<?/*<span class="close" data-entity="basket-items-warning-notification-close">&times;</span>*/?>
                            <svg viewBox="0 0 27 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M26.7609 21.7747L15.0312 1.45493C14.3513 0.277537 12.6487 0.277537 11.9688 1.45493L0.239091 21.7747C-0.440814 22.9521 0.410449 24.428 1.77026 24.428H25.2297C26.5895 24.428 27.4408 22.9521 26.7609 21.7747ZM13.5166 7.71227C14.2683 7.71227 14.8764 8.33689 14.8543 9.08866L14.6332 16.8053C14.6166 17.4133 14.1191 17.8942 13.511 17.8942C12.903 17.8942 12.4055 17.4078 12.3889 16.8053L12.1733 9.08866C12.1568 8.33689 12.7593 7.71227 13.5166 7.71227ZM13.5 21.6697C12.7316 21.6697 12.107 21.045 12.107 20.2767C12.107 19.5083 12.7316 18.8837 13.5 18.8837C14.2683 18.8837 14.893 19.5083 14.893 20.2767C14.893 21.045 14.2683 21.6697 13.5 21.6697Z"></path>
                            </svg>

                            <p data-entity="basket-general-warnings"></p>
                            <p data-entity="basket-item-warnings"><?=Loc::getMessage('SBB_BASKET_ITEM_WARNING')?></p>
                        </div>

                        <div class="basket-items-list-wrapper basket-items-list-wrapper-height-fixed basket-items-list-wrapper-light<?=$displayModeClass?>" id="basket-items-list-wrapper">
                            <div class="basket-items-list-header" data-entity="basket-items-list-header">
                                <div class="basket-items-search-field" data-entity="basket-filter">
                                    <div class="form has-feedback">
                                        <input type="text" class="form-control"
                                               placeholder="<?=Loc::getMessage('SBB_BASKET_FILTER')?>"
                                               data-entity="basket-filter-input">
                                        <span class="form-control-feedback basket-clear" data-entity="basket-filter-clear-btn"></span>
                                    </div>
                                </div>
                                <div class="basket-items-list-header-filter">
                                    <a href="javascript:void(0)" class="basket-items-list-header-filter-item active"
                                       data-entity="basket-items-count" data-filter="all" style="display: none;"></a>
                                    <a href="javascript:void(0)" class="basket-items-list-header-filter-item"
                                       data-entity="basket-items-count" data-filter="similar" style="display: none;"></a>
                                    <a href="javascript:void(0)" class="basket-items-list-header-filter-item"
                                       data-entity="basket-items-count" data-filter="warning" style="display: none;"></a>
                                    <a href="javascript:void(0)" class="basket-items-list-header-filter-item"
                                       data-entity="basket-items-count" data-filter="delayed" style="display: none;"></a>
                                    <a href="javascript:void(0)" class="basket-items-list-header-filter-item"
                                       data-entity="basket-items-count" data-filter="not-available" style="display: none;"></a>
                                </div>
                            </div>
                            <div class="basket-items-list-container" id="basket-items-list-container">
                                <div class="basket-items-list-overlay" id="basket-items-list-overlay" style="display: none;"></div>
                                <div class="basket-items-list" id="basket-item-list">
                                    <div class="basket-search-not-found" id="basket-item-list-empty-result" style="display: none;">
                                        <div class="basket-search-not-found-icon"></div>
                                        <div class="basket-search-not-found-text">
											<?=Loc::getMessage('SBB_FILTER_EMPTY_RESULT')?>
                                        </div>
                                    </div>
                                    <div class="mf-cart-body" id="basket-item-table"></div>
                                </div>
                            </div>
                        </div>
						<?/*
			            if (
				            $arParams['BASKET_WITH_ORDER_INTEGRATION'] !== 'Y'
				            && in_array('bottom', $arParams['TOTAL_BLOCK_DISPLAY'])
			            )
			            {
				            ?><div data-entity="basket-total-block"></div><?
			            }/**/
						?>
                    </div>
				<?
				if (!empty($arResult['CURRENCIES']) && Main\Loader::includeModule('currency'))
				{
				CJSCore::Init('currency');

				?>
                    <script>
                        BX.Currency.setCurrencies(<?=CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true)?>);
                    </script>
				<?
				}

				$signer = new \Bitrix\Main\Security\Sign\Signer;
				$signedTemplate = $signer->sign($templateName, 'sale.basket.basket');
				$signedParams = $signer->sign(base64_encode(serialize($arParams)), 'sale.basket.basket');
				$messages = Loc::loadLanguageFile(__FILE__);
				?>
                    <script>
                        BX.message(<?=CUtil::PhpToJSObject($messages)?>);
                        BX.Sale.BasketComponent.init({
                            result: <?=CUtil::PhpToJSObject($arResult, false, false, true)?>,
                            params: <?=CUtil::PhpToJSObject($arParams)?>,
                            template: '<?=CUtil::JSEscape($signedTemplate)?>',
                            signedParamsString: '<?=CUtil::JSEscape($signedParams)?>',
                            siteId: '<?=CUtil::JSEscape($component->getSiteId())?>',
                            templateFolder: '<?=CUtil::JSEscape($templateFolder)?>'
                        });
                    </script>
					<?
					if ($arParams['USE_GIFTS'] === 'Y' && $arParams['GIFTS_PLACE'] === 'BOTTOM')
					{
						$APPLICATION->IncludeComponent(
							'bitrix:sale.products.gift.basket',
							'.default',
							$giftParameters,
							$component
						);
					}
				}
                elseif ($arResult['EMPTY_BASKET'])
				{
					include(Main\Application::getDocumentRoot().$templateFolder.'/empty.php');
				}
				else
				{
					ShowError($arResult['ERROR_MESSAGE']);
				}
				?>
            </div>

            <div class="mf-cart-advs">
                <div class="mf-cart-advantage">
                    <h3 class="mf-cart-adv-title">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_RECURSIVE" => "N",
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "inc-adv1-ttl",
								"EDIT_TEMPLATE" => ""
							)
						);?>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
										<g>
                                            <g>
                                                <path d="M491.797,348.594L266.656,229.656V214.64c0.094-0.023,0.188-0.055,0.297-0.078c35.469-8.781,59.281-30.875,59.281-59.281
													c0-32.742-26.547-59.281-59.281-59.281c-30.234,0-56.516,22.125-59.109,51.773c-0.078,0.453-0.125,0.922-0.125,1.391
													c0,0.055-0.016,0.109-0.016,0.172c0,5.891,4.781,10.664,10.672,10.664s10.672-4.773,10.672-10.664h0.062
													c0.859-9.227,5.344-15.844,9.281-19.969c7.281-7.648,17.703-12.031,28.562-12.031c20.922,0,37.938,17.023,37.938,37.945
													c0,7.984-3.328,15.305-9.922,21.758c-7.703,7.539-19.156,13.352-33.141,16.812c0,0-16.203,5-16.484,5.102v3.711h-0.016v26.992
													L20.219,348.594C8.141,354.812,0,366.5,0,379.906C0,399.844,18.016,416,40.234,416c0.406,0,1.203,0,1.203,0h429.141
													c0,0,0.797,0,1.203,0C494,416,512,399.844,512,379.906C512,366.5,503.875,354.812,491.797,348.594z M471.781,394.656h-1.203
													H41.438h-1.203c-10.234,0-18.891-6.75-18.891-14.75c0-4.938,3.234-9.562,8.656-12.375l0.094-0.031l0.094-0.062l225.797-119.289
													l225.844,119.289l0.094,0.062l0.094,0.031c5.422,2.812,8.656,7.438,8.656,12.375C490.672,387.906,482.016,394.656,471.781,394.656
													z"/>
                                            </g>
                                        </g>
									</svg>
                    </h3>
                    <p class="mf-cart-adv-text">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_RECURSIVE" => "N",
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "inc-adv1-body",
								"EDIT_TEMPLATE" => ""
							)
						);?>
                    </p>
                </div>
                <div class="mf-cart-advantage">
                    <h3 class="mf-cart-adv-title">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_RECURSIVE" => "N",
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "inc-adv2-ttl",
								"EDIT_TEMPLATE" => ""
							)
						);?>
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
										<g>
                                            <g>
                                                <path d="M119.467,337.067c-28.237,0-51.2,22.963-51.2,51.2c0,28.237,22.963,51.2,51.2,51.2s51.2-22.963,51.2-51.2
													C170.667,360.03,147.703,337.067,119.467,337.067z M119.467,422.4c-18.825,0-34.133-15.309-34.133-34.133
													c0-18.825,15.309-34.133,34.133-34.133s34.133,15.309,34.133,34.133C153.6,407.091,138.291,422.4,119.467,422.4z"/>
                                            </g>
                                        </g>
                            <g>
                                <g>
                                    <path d="M409.6,337.067c-28.237,0-51.2,22.963-51.2,51.2c0,28.237,22.963,51.2,51.2,51.2c28.237,0,51.2-22.963,51.2-51.2
													C460.8,360.03,437.837,337.067,409.6,337.067z M409.6,422.4c-18.825,0-34.133-15.309-34.133-34.133
													c0-18.825,15.309-34.133,34.133-34.133c18.825,0,34.133,15.309,34.133,34.133C443.733,407.091,428.425,422.4,409.6,422.4z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path d="M510.643,289.784l-76.8-119.467c-1.57-2.441-4.275-3.917-7.177-3.917H332.8c-4.719,0-8.533,3.823-8.533,8.533v213.333
													c0,4.719,3.814,8.533,8.533,8.533h34.133v-17.067h-25.6V183.467h80.674l72.926,113.442v82.825h-42.667V396.8h51.2
													c4.719,0,8.533-3.814,8.533-8.533V294.4C512,292.77,511.531,291.157,510.643,289.784z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path d="M375.467,277.333V217.6h68.267v-17.067h-76.8c-4.719,0-8.533,3.823-8.533,8.533v76.8c0,4.719,3.814,8.533,8.533,8.533h128
													v-17.067H375.467z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <path d="M332.8,106.667H8.533C3.823,106.667,0,110.49,0,115.2v273.067c0,4.719,3.823,8.533,8.533,8.533H76.8v-17.067H17.067v-256
													h307.2v256H162.133V396.8H332.8c4.719,0,8.533-3.814,8.533-8.533V115.2C341.333,110.49,337.519,106.667,332.8,106.667z"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <rect x="8.533" y="345.6" width="51.2" height="17.067"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <rect x="179.2" y="345.6" width="145.067" height="17.067"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <rect x="469.333" y="345.6" width="34.133" height="17.067"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <rect x="34.133" y="140.8" width="298.667" height="17.067"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <rect x="110.933" y="379.733" width="17.067" height="17.067"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <rect x="401.067" y="379.733" width="17.067" height="17.067"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <rect x="34.133" y="72.533" width="119.467" height="17.067"/>
                                </g>
                            </g>
                            <g>
                                <g>
                                    <rect y="72.533" width="17.067" height="17.067"/>
                                </g>
                            </g>
									</svg>
                    </h3>
                    <p class="mf-cart-adv-text">
						<?$APPLICATION->IncludeComponent(
							"bitrix:main.include",
							"",
							Array(
								"AREA_FILE_RECURSIVE" => "N",
								"AREA_FILE_SHOW" => "sect",
								"AREA_FILE_SUFFIX" => "inc-adv2-body",
								"EDIT_TEMPLATE" => ""
							)
						);?>
                    </p>
                </div>
            </div>
        </div>

        <div class="cart-form-container">
            <div class="mf-form-container mf-form-container--basket">
                <div class="mf-form-bck"></div>
                <div class="mf-form">
                    <div id="basket-total-block" class="mf-form-header11" data-entity="basket-total-block"></div>
                </div>
            </div>
        </div>
    </div>
</section>