<div class="opros-body opros-text">
	<? foreach ($arResult["QUESTIONS"] as $arQuestion) { ?>
		<? foreach ($arQuestion["ANSWERS"] as $arAnswer) { ?>
			<?
			switch ($arAnswer["FIELD_TYPE"]):
				case 0://radio
					$value=(isset($_REQUEST['vote_radio_'.$arAnswer["QUESTION_ID"]]) &&
						$_REQUEST['vote_radio_'.$arAnswer["QUESTION_ID"]] == $arAnswer["ID"]) ? 'checked="checked"' : '';
					break;
				case 1://checkbox
					$value=(isset($_REQUEST['vote_checkbox_'.$arAnswer["QUESTION_ID"]]) &&
						array_search($arAnswer["ID"],$_REQUEST['vote_checkbox_'.$arAnswer["QUESTION_ID"]])!==false) ? 'checked="checked"' : '';
					break;
				case 2://select
					$value=(isset($_REQUEST['vote_dropdown_'.$arAnswer["QUESTION_ID"]])) ? $_REQUEST['vote_dropdown_'.$arAnswer["QUESTION_ID"]] : false;
					break;
				case 3://multiselect
					$value=(isset($_REQUEST['vote_multiselect_'.$arAnswer["QUESTION_ID"]])) ? $_REQUEST['vote_multiselect_'.$arAnswer["QUESTION_ID"]] : array();
					break;
				case 4://text field
					$value = isset($_REQUEST['vote_field_'.$arAnswer["ID"]]) ? htmlspecialcharsbx($_REQUEST['vote_field_'.$arAnswer["ID"]]) : '';
					break;
				case 5://memo
					$value = isset($_REQUEST['vote_memo_'.$arAnswer["ID"]]) ?  htmlspecialcharsbx($_REQUEST['vote_memo_'.$arAnswer["ID"]]) : '';
					break;
			endswitch;
			?>
			<?
			switch ($arAnswer["FIELD_TYPE"]):
				case 0://radio
					?>
					<label for="vote_radio_<?=$arAnswer["QUESTION_ID"]?>_<?=$arAnswer["ID"]?>" class="opros-answer">
						<input type="radio" <?=$value?> name="vote_radio_<?=$arAnswer["QUESTION_ID"]?>" <?
						?>id="vote_radio_<?=$arAnswer["QUESTION_ID"]?>_<?=$arAnswer["ID"]?>" <?
						       ?>value="<?=$arAnswer["ID"]?>" <?=$arAnswer["~FIELD_PARAM"]?> />
						<div class="opros-answer__bg"></div>
						<span><?=$arAnswer["MESSAGE"]?></span>
					</label>
					<?
					break;
				case 1://checkbox?>
					<label for="vote_checkbox_<?=$arAnswer["QUESTION_ID"]?>_<?=$arAnswer["ID"]?>" class="opros-answer">
						<input <?=$value?> type="checkbox" name="vote_checkbox_<?=$arAnswer["QUESTION_ID"]?>[]" value="<?=$arAnswer["ID"]?>" <?
						?> id="vote_checkbox_<?=$arAnswer["QUESTION_ID"]?>_<?=$arAnswer["ID"]?>" <?=$arAnswer["~FIELD_PARAM"]?> />
						<div class="opros-answer__bg"></div>
						<span><?=$arAnswer["MESSAGE"]?></span>
					</label>
					<?break?>

				<?case 2://dropdown?>
				<span class="vote-answer-item vote-answer-item-dropdown">
							<select name="vote_dropdown_<?=$arAnswer["QUESTION_ID"]?>" <?=$arAnswer["~FIELD_PARAM"]?>>
							<?foreach ($arAnswer["DROPDOWN"] as $arDropDown):?>
								<option value="<?=$arDropDown["ID"]?>" <?=($arDropDown["ID"] === $value)?'selected="selected"':''?>><?=$arDropDown["MESSAGE"]?></option>
							<?endforeach?>
							</select>
						</span>
				<?break?>

			<?case 3://multiselect?>
				<span class="vote-answer-item vote-answer-item-multiselect">
							<select name="vote_multiselect_<?=$arAnswer["QUESTION_ID"]?>[]" <?=$arAnswer["~FIELD_PARAM"]?> multiple="multiple">
							<?foreach ($arAnswer["MULTISELECT"] as $arMultiSelect):?>
								<option value="<?=$arMultiSelect["ID"]?>" <?=(array_search($arMultiSelect["ID"], $value)!==false)?'selected="selected"':''?>><?=$arMultiSelect["MESSAGE"]?></option>
							<?endforeach?>
							</select>
						</span>
				<?break?>

			<?case 4://text field?>
				<span class="vote-answer-item vote-answer-item-textfield">
							<label for="vote_field_<?=$arAnswer["ID"]?>"><?=$arAnswer["MESSAGE"]?></label>
							<input type="text" name="vote_field_<?=$arAnswer["ID"]?>" id="vote_field_<?=$arAnswer["ID"]?>" <?
							?>value="<?=$value?>" size="<?=$arAnswer["FIELD_WIDTH"]?>" <?=$arAnswer["~FIELD_PARAM"]?> /></span>
				<?break?>

			<?case 5://memo?>
				<span class="vote-answer-item vote-answer-item-memo">
							<label for="vote_memo_<?=$arAnswer["ID"]?>"><?=$arAnswer["MESSAGE"]?></label><br />
							<textarea name="vote_memo_<?=$arAnswer["ID"]?>" id="vote_memo_<?=$arAnswer["ID"]?>" <?
							?><?=$arAnswer["~FIELD_PARAM"]?> cols="<?=$arAnswer["FIELD_WIDTH"]?>" <?
							          ?>rows="<?=$arAnswer["FIELD_HEIGHT"]?>"><?=$value?></textarea>
						</span>
				<?break;
			endswitch;
			?>
		<? } ?>
	<? } ?>

	<div class="opros-vote">
		<button class="opros-vote__btn" type="submit" name="vote" value="<?=GetMessage("VOTE_SUBMIT_BUTTON")?>"><?=GetMessage("VOTE_SUBMIT_BUTTON")?></button>
		<a class="opros-vote__btn" href="<?=$arResult["URL"]["RESULT"]?>"><?=GetMessage("VOTE_RESULTS")?></a>
	</div>

	<div class="opros-total">
		Проголосовало <b><?= $arResult['VOTE']['COUNTER'] ?></b> <?= $voteDeclension->get($arResult['VOTE']['COUNTER']); ?>
	</div>
</div>
