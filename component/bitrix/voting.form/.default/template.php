<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Grid\Declension;
$voteDeclension = new Declension('человек', 'человека', 'человек');
//pr($arResult);
?>
<div class="opros-tagline"><?= $arResult['VOTE']['DESCRIPTION'] ?></div>
<div class="opros-block">
    <div class="opros-question"><?= $arResult['VOTE']['TITLE'] ?></div>
    <?
    if (!empty($arResult["ERROR_MESSAGE"])):
        ?>
        <div class="vote-note-box vote-note-error">
            <div class="vote-note-box-text"><?=ShowError($arResult["ERROR_MESSAGE"])?></div>
        </div>
    <?
    endif;

    if (!empty($arResult["OK_MESSAGE"])):
        ?>
        <div class="vote-note-box vote-note-note">
            <div class="vote-note-box-text"><?=ShowNote($arResult["OK_MESSAGE"])?></div>
        </div>
    <?
    endif;

    if (empty($arResult["VOTE"])):
        return false;
    elseif (empty($arResult["QUESTIONS"])):
        return true;
    endif;
    ?>
    <form action="<?=POST_FORM_ACTION_URI?>" method="post" class="vote-form">
        <input type="hidden" name="vote" value="Y">
        <input type="hidden" name="PUBLIC_VOTE_ID" value="<?=$arResult["VOTE"]["ID"]?>">
        <input type="hidden" name="VOTE_ID" value="<?=$arResult["VOTE"]["ID"]?>">
        <?=bitrix_sessid_post()?>
        <?
        reset($arResult['QUESTIONS']);
        $arQuestion = current($arResult['QUESTIONS']);
        if (!empty($arQuestion["IMAGE"])) include '_images.php';
            else include '_text.php'
        ?>

        <? if (isset($arResult["CAPTCHA_CODE"])):  ?>
            <div class="vote-item-header">
                <div class="vote-item-title vote-item-question"><?=GetMessage("F_CAPTCHA_TITLE")?></div>
                <div class="vote-clear-float"></div>
            </div>
            <div class="vote-form-captcha">
                <input type="hidden" name="captcha_code" value="<?=$arResult["CAPTCHA_CODE"]?>"/>
                <div class="vote-reply-field-captcha-image">
                    <img src="/bitrix/tools/captcha.php?captcha_code=<?=$arResult["CAPTCHA_CODE"]?>" alt="<?=GetMessage("F_CAPTCHA_TITLE")?>" />
                </div>
                <div class="vote-reply-field-captcha-label">
                    <label for="captcha_word"><?=GetMessage("F_CAPTCHA_PROMT")?><span class='starrequired'>*</span></label><br />
                    <input type="text" size="20" name="captcha_word" />
                </div>
            </div>
        <? endif // CAPTCHA_CODE ?>
    </form>
</div>