<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CJSCore::Init();
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR'] && $_GET['current_fieldset'] == 'SOCSERV' &&
    $_GET['auth_service_error'] == 1) {
    $links = '/?';
    foreach ($_REQUEST as $key => $item) {
        $links .= $key . '=' . $item . '&';
    }
    header('Location: '.$links);
} else {
    ?>

    <div class="popup_shop popup_input popup_auth_form">
    <? if ($arResult["FORM_TYPE"] == "login" || $arResult["FORM_TYPE"] == "otp") { ?>
        <div class="name">
            <div class="red auth_auth"><?= GetMessage('SIGN_AUTH'); ?></div>
            <span>/</span>
            <? /* <a target="_blank" href="<?= SITE_DIR ?>reg/" onclick="yaCounter17899708.reachGoal('openRegistrarionForm');" class="auth_reg"><?= GetMessage('REGISTRATION_AUTH'); ?></a> */ ?>
            <a target="_blank" href="<?= SITE_DIR ?>" onclick="yaCounter17899708.reachGoal('openRegistrarionForm');" class="auth_reg"><?= GetMessage('REGISTRATION_AUTH'); ?></a>
        </div>
    <? } ?>
    <?
if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) {
    ShowMessage($arResult['ERROR_MESSAGE']);
}
  if ($arResult["FORM_TYPE"] == "login"): ?>
    <? if ($arResult["AUTH_SERVICES"]): ?>
        <?
        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "mf",
            array(
                "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
                "AUTH_URL" => $arResult["AUTH_URL"],
                "POST" => $arResult["POST"],
                "SUFFIX" => "form",
                "POPUP" => "N",
            ),
            $component,
            array("HIDE_ICONS" => "Y")
        );
        ?>
    <?endif

    /*<div class="social" style="display:block;">
        <div class="text"><?=GetMessage('SIGN_SOC_AUTH');?><!--</div>
        <ul class="social-icon-list">
            <li class="facebook"><a href=""></a></li>
            <li class="instagram"><a href="https://instagram.com/markformelle/"></a></li>
            <li class="telegram"><a href="#"></a></li>
            <li class="youtube"><a href="https://www.youtube.com/channel/UCXcHttoKh0nsvCPTTt9G9Iw"></a></li>
            <li class="twitter"><a href="https://twitter.com/MarkFormelle"></a></li>
            <li class="vk"><a href="http://vk.com/clubmarkformelle"></a></li>
            <li class="ok"><a href="http://ok.ru/group/54581598486528"></a></li>
        </ul>
    </div>-->
    */
    ?>

    <div class="form_block">

        <form name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top"
              action="<?= $arResult["AUTH_URL"] ?>">
            <?
            if ($arResult["BACKURL"] <> ''): ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
            <? endif ?>
            <?
            foreach ($arResult["POST"] as $key => $value): ?>
                <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
            <? endforeach ?>
            <input type="hidden" name="AUTH_FORM" value="Y"/>
            <input type="hidden" name="TYPE" value="AUTH"/>
            <table width="95%">
                <tr>
                    <td colspan="2">
                        <?= GetMessage("AUTH_LOGIN") ?><br/>
                        <input type="text" name="USER_LOGIN" maxlength="50" value="" size="17"/>
                        <script>
                            BX.ready(function () {
                                var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                                if (loginCookie) {
                                    var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                                    var loginInput = form.elements["USER_LOGIN"];
                                    loginInput.value = loginCookie;
                                }
                            });
                        </script>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?= GetMessage("AUTH_PASSWORD") ?><br/>
                        <input type="password" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off"/>
                        <?
                        if ($arResult["SECURE_AUTH"]): ?>
                            <span class="bx-auth-secure" id="bx_auth_secure<?= $arResult["RND"] ?>" title="<?
                            echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
                            <noscript>
				<span class="bx-auth-secure" title="<?
                echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
                            </noscript>
                            <script type="text/javascript">
                                document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
                            </script>
                        <? endif ?>
                    </td>
                </tr>
                <?
                if ($arResult["CAPTCHA_CODE"]): ?>
                    <tr>
                        <td colspan="2">
                            <?
                            echo GetMessage("AUTH_CAPTCHA_PROMT") ?>:<br/>
                            <input type="hidden" name="captcha_sid" value="<?
                            echo $arResult["CAPTCHA_CODE"] ?>"/>
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?
                            echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA"/><br/><br/>
                            <input type="text" name="captcha_word" maxlength="50" value=""/></td>
                    </tr>
                <? endif ?>
                <tr>
                    <td class="btn_block login_btn_sign">
                        <input type="submit" name="Login" value="<?= GetMessage("AUTH_LOGIN_BUTTON") ?>"/></td>

                    <td class="btn_block">
                        <noindex>
                            <div class="block" style="float: right;">
                                <a class="popup-modal-input forgot" href="javascript:void(0);"
                                   class=""><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></a>
                            </div>
                    </td>
                </tr>
            </table>
        </form>

        <? elseif ($arResult["FORM_TYPE"] == "otp"): ?>
        <div class="form_block">
            <form name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top"
                  action="<?= $arResult["AUTH_URL"] ?>">
                <?
                if ($arResult["BACKURL"] <> ''):?>
                    <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
                <? endif ?>
                <input type="hidden" name="AUTH_FORM" value="Y"/>
                <input type="hidden" name="TYPE" value="OTP"/>
                <table width="95%">
                    <tr>
                        <td colspan="2">
                            <?
                            echo GetMessage("auth_form_comp_otp") ?><br/>
                            <input type="text" name="USER_OTP" maxlength="50" value="" size="17" autocomplete="off"/>
                        </td>
                    </tr>
                    <?
                    if ($arResult["CAPTCHA_CODE"]):?>
                        <tr>
                            <td colspan="2">
                                <?
                                echo GetMessage("AUTH_CAPTCHA_PROMT") ?>:<br/>
                                <input type="hidden" name="captcha_sid" value="<?
                                echo $arResult["CAPTCHA_CODE"] ?>"/>
                                <img src="/bitrix/tools/captcha.php?captcha_sid=<?
                                echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA"/><br/><br/>
                                <input type="text" name="captcha_word" maxlength="50" value=""/></td>
                        </tr>
                    <? endif ?>
                    <?
                    if ($arResult["REMEMBER_OTP"] == "Y"):?>
                        <tr>
                            <td valign="top"><input type="checkbox" id="OTP_REMEMBER_frm" name="OTP_REMEMBER"
                                                    value="Y"/></td>
                            <td width="100%"><label for="OTP_REMEMBER_frm" title="<?
                                echo GetMessage("auth_form_comp_otp_remember_title") ?>"><?
                                    echo GetMessage("auth_form_comp_otp_remember") ?></label></td>
                        </tr>
                    <? endif ?>
                    <tr>
                        <td colspan="2"><input type="submit" name="Login"
                                               value="<?= GetMessage("AUTH_LOGIN_BUTTON") ?>"/></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <noindex><a href="<?= $arResult["AUTH_LOGIN_URL"] ?>" rel="nofollow"><?
                                    echo GetMessage("auth_form_comp_auth") ?></a></noindex>
                            <br/></td>
                    </tr>
                </table>
            </form>
        </div>
        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
    </div>
<? else: ?>
    <div class="name">
        <div class="red">
            <a href="<?= $arResult["PROFILE_URL"] ?>"
               title="<?= GetMessage("AUTH_PROFILE") ?>"><?= GetMessage("AUTH_PROFILE") ?></a>
        </div>
    </div>
    <div class="form_block form_auth_suc">
    <form action="<?= $arResult["AUTH_URL"] ?>">
        <table width="95%">
            <tr>
                <td align="center" class="login_profile">
                    <?= $arResult["USER_NAME"] ?><br/>
                    [<?= $arResult["USER_LOGIN"] ?>]<br/>
                    <br/>
                </td>
            </tr>
            <tr>
                <td align="center" class="login_btn_sign">
                    <? foreach ($arResult["GET"] as $key => $value): ?>
                        <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
                    <? endforeach ?>
                    <input type="hidden" name="logout" value="yes"/>
                    <input type="submit" name="logout_butt"
                           value="<?= GetMessage("AUTH_LOGOUT_BUTTON") ?>"/>
                </td>
            </tr>
        </table>
    </form>
<? endif ?>
<? } ?>