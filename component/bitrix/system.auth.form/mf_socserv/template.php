<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CJSCore::Init(); ?>

<div class="popup_shop popup_input popup_auth_form terew" id="auth_service_error">
    <? if ($arResult['SHOW_ERRORS'] == 'Y' && $arResult['ERROR']) { ?>
        <div class="name">
            <div class="red auth_auth"><?= GetMessage('ERROR'); ?></div>
        </div>
        <div class="error_soc"><?=GetMessage('ERROR_TEXT');?></div>
        <button title="Close (Esc)" type="button" class="mfp-close">×</button>
        <style>
            #auth_service_error .error_soc {
                margin: 0 25px 25px 25px;
            }
        </style>
        <script>
            var oldURL = window.location.href;
            var index = 0;
            var newURL = oldURL;
            index = oldURL.indexOf('?');
            if (index == -1) {
                index = oldURL.indexOf('#');
            }
            if (index != -1) {
                newURL = oldURL.substring(0, index);
            }
            history.pushState('', '', newURL);
            $(document).ready(function () {
                $('body').on('click', '.terew button', function () {
                    $.magnificPopup.open({
                        items: {src: '/ajax/auth.php'},
                        type: 'ajax',
                        closeOnBgClick: true,
                        mainClass: 'popup_size-card popup_shop-body popup_input',
                    });
                    return false;
                });
            });
        </script>
    <? }else{ ?>
    <div class="name">
        <div class="red auth_auth"><?= GetMessage('SIGN_AUTH'); ?></div>
        <span>/</span>
        <div class="auth_reg"><?= GetMessage('REGISTRATION_AUTH'); ?></div>
    </div>
    <? if ($arResult["FORM_TYPE"] == "login"): ?>
    <? //if ($arResult['SHOW_ERRORS'] !== 'Y' && !$arResult['ERROR']) {
    if ($arResult["AUTH_SERVICES"]): ?>
        <?
        $APPLICATION->IncludeComponent("bitrix:socserv.auth.form", "mf",
            array(
                "AUTH_SERVICES" => $arResult["AUTH_SERVICES"],
            ),
            array("HIDE_ICONS" => "Y")
        );
        ?>
    <? endif ?>
    <? //}

    /*<div class="social" style="display:block;">
        <div class="text"><?=GetMessage('SIGN_SOC_AUTH');?><!--</div>
        <ul class="social-icon-list">
            <li class="facebook"><a href=""></a></li>
            <li class="instagram"><a href="https://instagram.com/markformelle/"></a></li>
            <li class="telegram"><a href="#"></a></li>
            <li class="youtube"><a href="https://www.youtube.com/channel/UCXcHttoKh0nsvCPTTt9G9Iw"></a></li>
            <li class="twitter"><a href="https://twitter.com/MarkFormelle"></a></li>
            <li class="vk"><a href="http://vk.com/clubmarkformelle"></a></li>
            <li class="ok"><a href="http://ok.ru/group/54581598486528"></a></li>
        </ul>
    </div>-->
    */
    ?>

    <div class="form_block">

        <form name="system_auth_form<?= $arResult["RND"] ?>" method="post" target="_top"
              action="<?= $arResult["AUTH_URL"] ?>">
            <?
            if ($arResult["BACKURL"] <> ''): ?>
                <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
            <? endif ?>
            <?
            foreach ($arResult["POST"] as $key => $value): ?>
                <input type="hidden" name="<?= $key ?>" value="<?= $value ?>"/>
            <? endforeach ?>
            <input type="hidden" name="AUTH_FORM" value="Y"/>
            <input type="hidden" name="TYPE" value="AUTH"/>
            <table width="95%">
                <tr>
                    <td colspan="2">
                        <?= GetMessage("AUTH_LOGIN") ?><br/>
                        <input type="text" name="USER_LOGIN" maxlength="50" value="" size="17"/>
                        <script>
                            BX.ready(function () {
                                var loginCookie = BX.getCookie("<?=CUtil::JSEscape($arResult["~LOGIN_COOKIE_NAME"])?>");
                                if (loginCookie) {
                                    var form = document.forms["system_auth_form<?=$arResult["RND"]?>"];
                                    var loginInput = form.elements["USER_LOGIN"];
                                    loginInput.value = loginCookie;
                                }
                            });
                        </script>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?= GetMessage("AUTH_PASSWORD") ?><br/>
                        <input type="password" name="USER_PASSWORD" maxlength="50" size="17" autocomplete="off"/>
                        <?
                        if ($arResult["SECURE_AUTH"]): ?>
                            <span class="bx-auth-secure" id="bx_auth_secure<?= $arResult["RND"] ?>" title="<?
                            echo GetMessage("AUTH_SECURE_NOTE") ?>" style="display:none">
					<div class="bx-auth-secure-icon"></div>
				</span>
                            <noscript>
				<span class="bx-auth-secure" title="<?
                echo GetMessage("AUTH_NONSECURE_NOTE") ?>">
					<div class="bx-auth-secure-icon bx-auth-secure-unlock"></div>
				</span>
                            </noscript>
                            <script type="text/javascript">
                                document.getElementById('bx_auth_secure<?=$arResult["RND"]?>').style.display = 'inline-block';
                            </script>
                        <? endif ?>
                    </td>
                </tr>
                <?
                if ($arResult["CAPTCHA_CODE"]): ?>
                    <tr>
                        <td colspan="2">
                            <?
                            echo GetMessage("AUTH_CAPTCHA_PROMT") ?>:<br/>
                            <input type="hidden" name="captcha_sid" value="<?
                            echo $arResult["CAPTCHA_CODE"] ?>"/>
                            <img src="/bitrix/tools/captcha.php?captcha_sid=<?
                            echo $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA"/><br/><br/>
                            <input type="text" name="captcha_word" maxlength="50" value=""/></td>
                    </tr>
                <? endif ?>
                <tr>
                    <td class="btn_block login_btn_sign">
                        <input type="submit" name="Login" value="<?= GetMessage("AUTH_LOGIN_BUTTON") ?>"/></td>

                    <td class="btn_block">
                        <noindex>
                            <div class="block" style="float: right;">
                                <a class="popup-modal-input forgot" href="javascript:void(0);"
                                   class=""><?= GetMessage("AUTH_FORGOT_PASSWORD_2") ?></a>
                            </div>
                    </td>
                </tr>
            </table>
        </form>
        <? endif ?>
        <? } ?>
    </div>
