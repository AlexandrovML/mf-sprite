<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

//print_r($arParams["AUTH_SERVICES"])
?>
<div class="social" style="display:block;">
    <div class="text"><?=GetMessage('SIGN_SOC_AUTH');?></div>
    <ul class="social-icon-list">
        <li class="facebook">
            <a href="javascript:void(0)" onclick="<?=$arParams["AUTH_SERVICES"]['Facebook']['ONCLICK']?>"></a>
        </li>
        <li class="vk">
            <a href="javascript:void(0)" onclick="<?=$arParams["AUTH_SERVICES"]['VKontakte']['ONCLICK']?>"></a>
        </li>
        <li class="ok">
            <a href="javascript:void(0)" onclick="<?=$arParams["AUTH_SERVICES"]['Odnoklassniki']['ONCLICK']?>"></a>
        </li>
        <li class="google">
            <a href="javascript:void(0)" onclick="<?=$arParams["AUTH_SERVICES"]['GoogleOAuth']['ONCLICK']?>"></a>
        </li>
    </ul>
</div>