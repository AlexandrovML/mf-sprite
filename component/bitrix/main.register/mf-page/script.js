function checkSubReg(){
    if($('#soglSubs').prop('checked')){
        email = $('.email_registration').val();
        (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() { rrApi.setEmail(email,{stockId: "<?=$_SESSION['city'];?>"});    });
    }
}
$('#soglSubs').change(function(){
    if($(this).prop('checked')){
        $('input[name=UF_SUBSCRIBE_CHECK]').val('Y');
    }else{
        $('input[name=UF_SUBSCRIBE_CHECK]').val('N');
    }
});

$(document).ready(function() {
    $('.custom_sel').styler();
    $('.custom_check').styler();

    $('.js-phone-mask').each(function () {
        $(this).inputmask($(this).data('mask'));
    });


    $('#reg-form-page').find('.registr-form__social').remove();
    $('#hidden-auth-cnt').find('.social').addClass('registr-form__social').removeClass('social').attr('style',false).detach().prependTo('#reg-form-page');
    // $('#hidden-auth-cnt').find('.social').show();
});

if (window.frameCacheVars !== undefined)
{
    BX.addCustomEvent("onFrameDataReceived" , function(json) {
        console.log('onFrameDataReceived');
        // $('.custom_sel').styler();
        // $('.custom_check').styler();
        //
        // $('.phone_registr').inputmask("+375 (99) 9999999");
        //
        // $('#reg-form-page').find('.social').remove();
        // $('#hidden-auth-cnt').find('.social').detach().prependTo('#reg-form-page');
        // $('#hidden-auth-cnt').find('.social').show();
    });
}
