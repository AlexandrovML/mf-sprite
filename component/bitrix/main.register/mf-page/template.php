<?
/**
 * Bitrix Framework
 * @package bitrix
 * @subpackage main
 * @copyright 2001-2014 Bitrix
 */

/**
 * Bitrix vars
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @param array $arParams
 * @param array $arResult
 * @param CBitrixComponentTemplate $this
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

//echo '<pre>';print_r($arResult);echo '</pre>';

switch (SITE_ID)
{
    case 's2':
    case 's3':
    case 's5':
    case 's6':
    case 'or':
        $phoneMask = "+7 (999) 9999999";
        break;

    case 's4':
	    $phoneMask = "+375 (99) 9999999";
	    break;

    default:
	    $phoneMask = "+375 (99) 9999999";
	    break;
}
?>

<div class="registr-form__bg popup_auth_form popup_shop">
    <div class="fix-block">
        <div id="reg-form-page" class="registr-form">
			<? if ($USER->IsAuthorized()): ?>
                <div class="registr-form__form">
                    <div class="registr-form__wrap">
                        <p><? echo GetMessage("MAIN_REGISTER_AUTH") ?></p>
                    </div>
                </div>
			<? else: ?>
                <div class="registr-form__social">
                    <div class="text"><?= GetMessage('SIGN_SOC_AUTH'); ?></div>
                    <ul class="social-icon-list">
                        <li class="facebook"><a href="https://www.facebook.com/markformelle"></a></li>
                        <li class="instagram"><a href="https://instagram.com/markformelle/"></a></li>
                        <li class="telegram"><a href="#"></a></li>
                        <li class="youtube"><a href="https://www.youtube.com/channel/UCXcHttoKh0nsvCPTTt9G9Iw"></a></li>
                        <li class="twitter"><a href="https://twitter.com/MarkFormelle"></a></li>
                        <li class="vk"><a href="http://vk.com/clubmarkformelle"></a></li>
                        <li class="ok"><a href="http://ok.ru/group/54581598486528"></a></li>
                    </ul>
                </div>

                <form class="registr-form__form" method="post" action="<?= POST_FORM_ACTION_URI ?>" name="regform" enctype="multipart/form-data">
                    <input type="hidden" name="TYPE" value="REGISTRATION"/>
                    <input type="hidden" name="register_submit_button" value="Y"/>
					<?if ($arResult["BACKURL"] <> ''):?>
                        <input type="hidden" name="backurl" value="<?= $arResult["BACKURL"] ?>"/>
					<?endif; ?>

                    <div class="registr-form__wrap">
                        <div class="block wid3">
							<?/*<label for=""><?= GetMessage('REGISTER_FIELD_PERSONAL_GENDER'); ?></label>*/?>
                            <select class="custom_sel" name="REGISTER[PERSONAL_GENDER]">
                                <option <?= ($arResult["VALUES"][$arResult["SHOW_FIELDS"]['PERSONAL_GENDER']] != "M"&&$arResult["VALUES"][$arResult["SHOW_FIELDS"]['PERSONAL_GENDER']] != "F") ? " selected=\"selected\"" : "" ?> disabled>
									<?= GetMessage('REGISTER_FIELD_PERSONAL_GENDER'); ?>
                                </option>
                                <option value="M"<?= $arResult["VALUES"][$arResult["SHOW_FIELDS"]['PERSONAL_GENDER']] == "M" ? " selected=\"selected\"" : "" ?>>
									<?= GetMessage("USER_MALE") ?>
                                </option>
                                <option value="F"<?= $arResult["VALUES"][$arResult["SHOW_FIELDS"]['PERSONAL_GENDER']] == "F" ? " selected=\"selected\"" : "" ?>>
									<?= GetMessage("USER_FEMALE") ?>
                                </option>
                            </select>
                        </div>

                        <div class="block wid3">
                            <input type="text" name="REGISTER[NAME]" value="" placeholder="<?= GetMessage('REGISTER_FIELD_NAME'); ?>">
                        </div>

                        <div class="block wid3">
                            <input type="text" name="REGISTER[LAST_NAME]" value="" placeholder="<?= GetMessage('REGISTER_FIELD_LAST_NAME'); ?>">
                        </div>

                        <div class="block wid3">
                            <input type="email" name="REGISTER[EMAIL]" placeholder="<?= GetMessage('EMAIL_YOU'); ?>" class="email_registration" />
                            <input type="text" name="REGISTER[LOGIN]" style="display: none" class="login_registration" />
                        </div>

                        <div class="block wid3">

                            <input type="text" name="REGISTER[PERSONAL_PHONE]" class="js-phone-mask" data-mask="<?= $phoneMask ?>" data-lid="<?= SITE_ID ?>" value="" placeholder="<?= GetMessage('PHONE_YOU'); ?>">
                        </div>

                        <div class="block wid3 birth">
                            <label for="" style="width: 100%;"><?= GetMessage('REGISTER_FIELD_PERSONAL_BIRTHDAY'); ?></label>
                            <select class="custom_sel" id="day_registration">
                                <option value="" data-value="01">1</option>
                                <option value="" data-value="02">2</option>
                                <option value="" data-value="03">3</option>
                                <option value="" data-value="04">4</option>
                                <option value="" data-value="05">5</option>
                                <option value="" data-value="06">6</option>
                                <option value="" data-value="07">7</option>
                                <option value="" data-value="08">8</option>
                                <option value="" data-value="09">9</option>
                                <option value="" data-value="10">10</option>
                                <option value="" data-value="11">11</option>
                                <option value="" data-value="12">12</option>
                                <option value="" data-value="13">13</option>
                                <option value="" data-value="14">14</option>
                                <option value="" data-value="15">15</option>
                                <option value="" data-value="16">16</option>
                                <option value="" data-value="17">17</option>
                                <option value="" data-value="18">18</option>
                                <option value="" data-value="19">19</option>
                                <option value="" data-value="20">20</option>
                                <option value="" data-value="21">21</option>
                                <option value="" data-value="22">22</option>
                                <option value="" data-value="23">23</option>
                                <option value="" data-value="24">24</option>
                                <option value="" data-value="25">25</option>
                                <option value="" data-value="26">26</option>
                                <option value="" data-value="27">27</option>
                                <option value="" data-value="28">28</option>
                                <option value="" data-value="29">29</option>
                                <option value="" data-value="30">30</option>
                                <option value="" data-value="31">31</option>
                            </select>
                            <select class="custom_sel" id="month_registration">
                                <option value="" data-value="01"><?= GetMessage('MONTH_JANUARY'); ?></option>
                                <option value="" data-value="02"><?= GetMessage('MONTH_FEBRUARY'); ?></option>
                                <option value="" data-value="03"><?= GetMessage('MONTH_MARCH'); ?></option>
                                <option value="" data-value="04"><?= GetMessage('MONTH_APRIL'); ?></option>
                                <option value="" data-value="05"><?= GetMessage('MONTH_MAY'); ?></option>
                                <option value="" data-value="06"><?= GetMessage('MONTH_JUNE'); ?></option>
                                <option value="" data-value="07"><?= GetMessage('MONTH_JULY'); ?></option>
                                <option value="" data-value="08"><?= GetMessage('MONTH_AUGUST'); ?></option>
                                <option value="" data-value="09"><?= GetMessage('MONTH_SEPTEMBER'); ?></option>
                                <option value="" data-value="10"><?= GetMessage('MONTH_OCTOBER'); ?></option>
                                <option value="" data-value="11"><?= GetMessage('MONTH_NOVEMBER'); ?></option>
                                <option value="" data-value="12"><?= GetMessage('MONTH_DECEMBER'); ?></option>
                            </select>
                            <select class="custom_sel" id="year_registration">
								<?
								$now = date('Y');
								$now = IntVal($now) - 10;
								for ($i = $now; $i != 1949; $i--) { ?>
                                    <option value="" data-value="<?= $i; ?>">
										<?= $i; ?>
                                    </option>
								<? } ?>
                            </select>
                            <input type="text" name="REGISTER[PERSONAL_BIRTHDAY]" style="display: none" class="personal_birthday" >
                        </div>

                        <div class="block wid3">
                            <input type="password" name="REGISTER[PASSWORD]" class="regist_password" value="<?= $arResult["VALUES"][$arResult["SHOW_FIELDS"]['PASSWORD']] ?>" autocomplete="off" placeholder="<?= GetMessage('REGISTER_FIELD_PASSWORD'); ?>"/>
                        </div>

                        <div class="block wid3">
                            <input type="password" name="REGISTER[CONFIRM_PASSWORD]" class="regist_confirm_password" value="<?= $arResult["VALUES"][$arResult["SHOW_FIELDS"]['CONFIRM_PASSWORD']] ?>" autocomplete="off" placeholder="<?= GetMessage('REGISTER_FIELD_CONFIRM_PASSWORD'); ?>"/>
                        </div>
                    </div>



                    <div class="registr-form__wrap captcha">
						<?
						/* CAPTCHA */
						if ($arResult["USE_CAPTCHA"] == "Y") {
							?>

                            <div class="block wid3">
                                <label for=""><?= GetMessage('REGISTER_CAPTCHA_TITLE'); ?></label>
                                <div id="captchaBlock">
                                    <input type="hidden" name="captcha_sid" value="<?=$arResult["CAPTCHA_CODE"]?>" />
                                    <img src="/bitrix/tools/captcha.php?captcha_sid=<?=$arResult["CAPTCHA_CODE"]?>" width="180" height="40" alt="CAPTCHA" />
                                </div>
                                <?/*
                                <div id="captchaBlock">
                                    <input type="hidden" id="captchaSid" name="captcha_sid" value="<?= $arResult["CAPTCHA_CODE"] ?>"/>
                                    <div id="whiteBlock"></div>
                                    <img id="captchaImg" src="/bitrix/tools/captcha.php?captcha_sid=<?= $arResult["CAPTCHA_CODE"] ?>" width="180" height="40" alt="CAPTCHA" class="captchaImg" />
                                </div>
                                <button id="reloadCaptcha" class="refresh-capcha"><?= GetMessage('REGISTER_CAPTCHA_RELOAD'); ?></button>
                                */?>
                            </div>

                            <div class="block wid3">
                                <input type="text" name="captcha_word" maxlength="50" value="" placeholder="<?= GetMessage("REGISTER_CAPTCHA_PROMT") ?>"/>
                            </div>


                            <style>
                                #captchaBlock{position:relative;}
                                #captchaBlock #whiteBlock{
                                    display:none;
                                    text-align:center;
                                    position:absolute;
                                    width:180px;
                                    height:40px;
                                    background: url("<?= SITE_TEMPLATE_PATH ?>/images/ajax-loader.gif") center center no-repeat #fff;
                                    border: 1px solid #000;
                                }
                                #captchaBlock #whiteBlock #loaderImg{margin-top:3px;}
                            </style>

                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $('#reloadCaptcha').on('click', function (e) {
                                        e.preventDefault();
                                        $('#whiteBlock').show();
                                        $.getJSON('<?=$this->__folder?>/reload_captcha.php', function(data) {
                                            $('#captchaImg').attr('src','/bitrix/tools/captcha.php?captcha_sid='+data);
                                            $('#captchaSid').val(data);
                                            $('#whiteBlock').hide();
                                        });
                                        return false;
                                    });
                                });
                            </script>
							<?
						}
						/* !CAPTCHA */
						?>
						<? // ********************* User properties ***************************************************?>
						<? if ($arResult["USER_PROPERTIES"]["SHOW"] == "Y"): ?>
							<? foreach ($arResult["USER_PROPERTIES"]["DATA"] as $FIELD_NAME => $arUserField): ?>
                                <div class="block wid3 check">
									<?$APPLICATION->IncludeComponent(
										"bitrix:system.field.edit",
										"string_registration",
										array("bVarsFromForm" => $arResult["bVarsFromForm"], "arUserField" => $arUserField, "form_name" => "regform"),
										null,
										array("HIDE_ICONS" => "Y")); ?>

									<?if($FIELD_NAME == 'UF_SOGLAS'):?>
                                        <p>
                                            <label for="sogl" id="form_sogl">
                                                <input type="checkbox" id="sogl" class="custom_check" checked><span><?=GetMessage('CONFIDENCE', Array ("#SITE_DIR#" => SITE_DIR));?></span>
                                            </label>
                                        </p>
									<?endif;?>

									<?//if($FIELD_NAME == 'UF_SUBSCRIBE_CHECK'):?>
                                        <p style="display: none;">
                                            <label for="soglSubs" id="form_soglSubs">
                                                <input type="checkbox" id="soglSubs" class="custom_check"><?=GetMessage('SUB_CHECK');?>
                                            </label>
                                        </p>
									<?//endif;?>
                                </div>
							<? endforeach; ?>
						<? endif; ?>
						<? // ******************** /User properties ***************************************************?>
                    </div>
                    <div class="registr-form__wrap">
                        <div class="block check login_btn_sign">
                            <input type="submit" onclick="checkSubReg();yaCounter17899708.reachGoal('clickRegistrarionButton');" name="register_submit_button" class="btn_registration" value="<?= GetMessage("AUTH_REGISTER") ?>"/>
                        </div>
                    </div>


					<?/*
                    <p><? echo $arResult["GROUP_POLICY"]["PASSWORD_REQUIREMENTS"]; ?></p>
                    <p><span class="starrequired">*</span> <?= GetMessage("AUTH_REQ") ?></p>
                    */?>

                </form>
			<? endif ?>
        </div>
    </div>
</div>
<script>
    function checkSubReg(){
        email = $('.email_registration').val();
        if($('#sogl').prop('checked')&&email!=''){
            (window["rrApiOnReady"] = window["rrApiOnReady"] || []).push(function() { rrApi.setEmail(email,{stockId: "<?=$_SESSION['city'];?>"});    });
        }
    }
    $('#soglSubs').change(function(){
        if($(this).prop('checked')){
            $('input[name=UF_SUBSCRIBE_CHECK]').val('Y');
        }else{
            $('input[name=UF_SUBSCRIBE_CHECK]').val('N');
        }
    });
</script>