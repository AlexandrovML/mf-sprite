<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$this->addExternalJs('/local/templates/markformelle/js/JOB.js');
$this->addExternalJs('/local/templates/markformelle/js/job-barcode.js');
?>
<form id="header-search-form" action="<?=$arResult["FORM_ACTION"]?>" class="search">
    <div class="fix-block">
        <div class="svg"></div>
        <button type="button" class="btn-search-barcode js-mfp-open-barcode" title="Поиск по штрих-коду" data-mfp-src="#barcode-search-form">Поиск по штрих-коду</button>
        <svg class="search-close-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64"
             xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64">
            <g>
                <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
            </g>
        </svg>
        <svg class="search-icon" xmlns="http://www.w3.org/2000/svg" viewBox="-1 0 20 18">
            <path fill="rgb(94, 93, 93)" d="M12.453 13.18c-1.318 1.056-3.005 1.69-4.844 1.69-4.213 0-7.627-3.328-7.627-7.435S3.398 0 7.61 0c4.21 0 7.624 3.33 7.624 7.436 0 1.643-.547 3.16-1.473 4.392l5.036 4.708c.25.236.25.62 0 .857l-.462.432c-.252.235-.664.235-.916 0l-4.964-4.646zm-4.844-.36c3.047 0 5.52-2.41 5.52-5.385S10.66 2.05 7.61 2.05 2.086 4.463 2.086 7.437 4.56 12.82 7.61 12.82z"/>
        </svg>
        <input type="search" name="q" value="" size="15" maxlength="50" placeholder="Поиск" autocomplete="off" />
        <input name="s" type="submit" value="<?=GetMessage("BSF_T_SEARCH_BUTTON");?>" style="opacity: 0; visibility: hidden;" />

    </div>
</form>

<?/*<a href="StreamDemo.html" style="position:absolute;left:350px;">GetUserMedia Demo</a>*/?>

<div id="barcode-search-form" class="barcode-search-form popup_shop popup_input white-popup-block mfp-hide">
    <div class="name">Поиск по штрихкоду</div>
    <div id="container" class="barcode-search-cnt">
        <button type="button" id="barcode-upload-photo" class="btn-barcode-upload-photo">загрузить фото</button>
        <input id="Take-Picture" class="file-styler" type="file" accept="image/*;capture=camera" />
        <canvas width="320" height="240" id="picture"></canvas>
        <p id="textbit"></p>
    </div>
    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
</div>


<?/*<script type="text/javascript" src="/local/templates/markformelle/js/JOB.js"></script>*/?>
<script type="text/javascript">
</script>
