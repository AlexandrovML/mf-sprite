<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$ID = intval($_GET['id']);

$arProduct = false;
$res = CIBlockElement::GetByID($ID);
if($arProduct = $res->GetNext())
{
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => &$arQuestion)
	{
		if (empty($arQuestion['VALUE']))
		{
			switch ($FIELD_SID)
			{
				case 'USER_ID':
					$arQuestion["HTML_CODE"] = str_replace('value=""', 'value="'.$USER->getId().'"', $arQuestion["HTML_CODE"]);
					$arQuestion["VALUE"] = $USER->getId();
					break;

				case 'PRODUCT_ID':
					$arQuestion["HTML_CODE"] = str_replace('value=""', 'value="'.$ID.'"', $arQuestion["HTML_CODE"]);
					$arQuestion["VALUE"] = $ID;
					break;

				case 'PRODUCT_NAME':
					$arQuestion["HTML_CODE"] = str_replace('value=""', 'value="'.$arProduct['NAME'].'"', $arQuestion["HTML_CODE"]);
					$arQuestion["VALUE"] = $arProduct['NAME'];
					break;

				case 'PRODUCT_URL':
					$arQuestion["HTML_CODE"] = str_replace('value=""', 'value="'.$arProduct['DETAIL_PAGE_URL'].'"', $arQuestion["HTML_CODE"]);
					$arQuestion["VALUE"] = $arProduct['DETAIL_PAGE_URL'];
					break;

			}
		}
	}
	unset($arQuestion);
}
?>
<script type="text/javascript" src="<?= SITE_TEMPLATE_PATH ?>/js/jquery.raty.min.js"></script>
<script>
    function ratyInit()
    {
        if($body.find('.js-raty').length) {
            $body.find('.js-raty').raty({
                starOff : '/local/templates/markformelle/images/star-off.svg',
                starOn  : '/local/templates/markformelle/images/star-on.svg',
                target  : '#raty-val',
                targetType : 'score',
                targetKeep: true,
                click: function(score, evt) {
                    $body.find('.js-rating-input').val(score);
                },
                score: function(score, evt) {
                    $body.find('.js-rating-input').val(5);
                    return 5;
                }
            })
        }
    }

    jQuery(document).ready(function () {
        ratyInit();


    });

    jQuery(document).on('click', '.js-close-review', function () {
        $.magnificPopup.close();
    });

</script>

<?if ($arResult["isFormNote"] != "Y") { ?>
    <div class="review-modal__header">
        <span>Оставить отзыв</span>
        <button class="review-modal__close mfp-close"><svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" viewBox="0 0 17 17"><path d="M16.071,17.485L9,10.414,1.929,17.485,0.515,16.071,7.586,9,0.515,1.929,1.929,0.515,9,7.586l7.071-7.071,1.414,1.414L10.414,9l7.071,7.071Z"/></svg></button>
    </div>
	<? /*if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;*/ ?>
	<?=$arResult["FORM_NOTE"]?>
<?=$arResult["FORM_HEADER"]?>
    <div class="review-modal__item">
        <span>Оцените товар</span>
        <div class="review-item__rate">
            <div id="raty-val" class="raty-val" style="display: none"></div>
            <div class="js-raty raty-stars"></div>
			<?/*
        <svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
        <svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
        <svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
        <svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
        <svg class="review-item__rate-icon" width="15" height="15" viewBox="0 0 15 15" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M7.9458 0.57619L9.82946 5.03448L14.6519 5.44876C14.9863 5.47765 15.1224 5.89497 14.8685 6.11465L11.2107 9.28372L12.3068 13.9982C12.3828 14.3258 12.0278 14.5835 11.7405 14.4094L7.59613 11.91L3.45179 14.4094C3.16369 14.5827 2.80946 14.325 2.88548 13.9982L3.98161 9.28372L0.323005 6.11389C0.0691139 5.89421 0.204421 5.47689 0.539648 5.448L5.36205 5.03372L7.2457 0.57619C7.37645 0.266049 7.81506 0.266049 7.9458 0.57619Z"/></svg>
        */?>
        </div>
    </div>

	<?
	foreach ($arResult["QUESTIONS"] as $FIELD_SID => $arQuestion)
	{
		if ($arQuestion['STRUCTURE'][0]['FIELD_TYPE'] == 'hidden')
		{
			echo $arQuestion["HTML_CODE"];
		}
		else
		{
			?>
            <div class="review-modal__item<?= is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])?' has-error':'' ?>">
                <span><?=$arQuestion["CAPTION"]?></span>
	            <?=$arQuestion["HTML_CODE"]?>
            </div>
            <?/*
            <tr>
                <td>
					<?if (is_array($arResult["FORM_ERRORS"]) && array_key_exists($FIELD_SID, $arResult['FORM_ERRORS'])):?>
                        <span class="error-fld" title="<?=htmlspecialcharsbx($arResult["FORM_ERRORS"][$FIELD_SID])?>"></span>
					<?endif;?>
					<?=$arQuestion["CAPTION"]?><?if ($arQuestion["REQUIRED"] == "Y"):?><?=$arResult["REQUIRED_SIGN"];?><?endif;?>
					<?=$arQuestion["IS_INPUT_CAPTION_IMAGE"] == "Y" ? "<br />".$arQuestion["IMAGE"]["HTML_CODE"] : ""?>
                </td>
                <td></td>
            </tr>
            */?>
			<?
		}
	} //endwhile
	?>
	<?
	if($arResult["isUseCaptcha"] == "Y")
	{
		?>
        <div class="review-modal__item">
            <input type="hidden" name="captcha_sid" value="<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" /><img src="/bitrix/tools/captcha.php?captcha_sid=<?=htmlspecialcharsbx($arResult["CAPTCHACode"]);?>" width="180" height="40" />
            <br>
	        <?=GetMessage("FORM_CAPTCHA_FIELD_TITLE")?><?=$arResult["REQUIRED_SIGN"];?>
            <br>
            <input type="text" name="captcha_word" size="30" maxlength="50" value="" class="inputtext" />
        </div>
		<?
	} // isUseCaptcha
	?>
    <input class="review-modal__submit" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />
    <?=$arResult["FORM_FOOTER"]?>
<? } else { ?>
    <div class="review-modal review-modal-msg">
        <div class="review-modal__header">Спасибо за отзыв</div>
        <div class="review-modal__item">Ваш отзыв принят и будет опубликован после модерации</div>
        <button class="write-review js-close-review">Ок</button>
    </div>

<? } ?>