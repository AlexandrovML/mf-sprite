<?php
/** @var $key */
/** @var $arQuestion */
?>
<div class="block">
	<label for=""><?=GetMessage("FORM_VACANCY_$key");?> <?= $arQuestion['REQUIRED']=='Y'?'<span class="red">*</span>':'' ?></label>
	<?= $arQuestion['HTML_CODE'] ?>
</div>
