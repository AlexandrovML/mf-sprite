<?
$MESS ['FORM_REQUIRED_FIELDS'] = "Поля, обязательные для заполнения";
$MESS ['FORM_APPLY'] = "Применить";
$MESS ['FORM_ADD'] = "Добавить";
$MESS ['FORM_ACCESS_DENIED'] = "Не хватает прав доступа к веб-форме.";
$MESS ['FORM_DATA_SAVED1'] = "Спасибо!<br><br>Ваша заявка №";
$MESS ['FORM_DATA_SAVED2'] = " принята к рассмотрению.";
$MESS ['FORM_MODULE_NOT_INSTALLED'] = "Модуль веб-форм не установлен.";
$MESS ['FORM_NOT_FOUND'] = "Веб-форма не найдена.";
$MESS ['ADD_FILE'] = "Прикрепить файл";
$MESS ['MEN'] = "Мужской";
$MESS ['WOMEN'] = "Женский";
$MESS ['DOWNLOAD'] = "Скачать";
$MESS ['ANKETA'] = "Анкету";
$MESS ['FORM_VACANCY_ZAG'] = 'Оставьте свой отклик на вакансию или <a href="#LINK#" class="red">скачать анкету</a>';
$MESS ['FORM_VACANCY_phone'] = "Номер телефона";
$MESS ['FORM_VACANCY_email'] = "Email";
$MESS ['FORM_VACANCY_age'] = "Возраст";
$MESS ['FORM_VACANCY_gender'] = "Пол";
$MESS ['FORM_VACANCY_education'] = "Образование";
$MESS ['FORM_VACANCY_about_me'] = "Расскажите о себе";
$MESS ['FORM_VACANCY_file_vacancy'] = "Прикрепить файл";
?>
