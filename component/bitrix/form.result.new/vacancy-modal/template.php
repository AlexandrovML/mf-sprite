<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
CModule::IncludeModule("iblock");
$res12 = CIBlockElement::GetByID(VACANCY_SERVICE_BLOCK_ID);
$res23 = $res12->GetNextElement();
$arProps = $res23->GetProperties();
?>
<div class="form_vacancy">
    <div class="bzag"><?=GetMessage('FORM_VACANCY_ZAG', ['#LINK#'=>!empty($arProps['ANKETA']['VALUE'])?CFile::GetPath($arProps['ANKETA']['VALUE']):'#']);?></a></div>
    <?
//    pr($arResult["QUESTIONS"]['gender']);
    ?>
<? if ($arResult["isFormErrors"] == "Y"): ?><?= $arResult["FORM_ERRORS_TEXT"]; ?><? endif; ?>

<?= $arResult["FORM_NOTE"] ?>

<? if ($arResult["isFormNote"] != "Y") {
    ?>
    <?= $arResult["FORM_HEADER"] ?>

    <div class="block">
        <?
        $key = 'phone';
        $arQuestion = $arResult["QUESTIONS"][$key];
        include '_text-input.php';

        $key = 'email';
        $arQuestion = $arResult["QUESTIONS"][$key];
        include '_text-input.php';
        ?>
    </div>

    <div class="block_container">
        <div class="block wid2">
	        <?
	        $key = 'age';
	        $key1 = 'gender';
	        $arQuestion = $arResult["QUESTIONS"][$key];
	        $arQuestion1 = $arResult["QUESTIONS"][$key1];
	        ?>
            <label for=""><?=GetMessage("FORM_VACANCY_$key");?><?= $arQuestion['REQUIRED']=='Y'?' <span class="red">*</span>':'' ?></label>
            <label for=""><?=GetMessage("FORM_VACANCY_$key1");?><?= $arQuestion1['REQUIRED']=='Y'?' <span class="red">*</span>':'' ?></label>
	        <?= $arQuestion['HTML_CODE'] ?>
            <select  class="custom_sel">
                <option value="" ><?= $arQuestion1["CAPTION"] ?>
			        <?if ($arQuestion1["REQUIRED"] == "Y"): ?>
                        <sup><?= $arResult["REQUIRED_SIGN"]; ?></sup>
			        <? endif; ?>
                </option>

                <option value="" data-name="form_hidden_<?=$arQuestion1['STRUCTURE'][0]['FIELD_ID']?>"><?=GetMessage('MEN');?></option>
                <option value="" data-name="form_hidden_<?=$arQuestion1['STRUCTURE'][0]['FIELD_ID']?>"><?=GetMessage('WOMEN');?></option>
            </select>
	        <?= $arQuestion1['HTML_CODE'] ?>
        </div>

        <?
        $key = 'education';
        $arQuestion = $arResult["QUESTIONS"][$key];
        include '_text-input.php';
        ?>
    </div>

    <div class="block">
	    <?
	    $key = 'about_me';
	    $arQuestion = $arResult["QUESTIONS"][$key];
	    include '_text-input.php';
	    ?>
        <div class="block">
	        <?
	        $key = 'file_vacancy';
	        $arQuestion = $arResult["QUESTIONS"][$key];
	        $arQuestion["HTML_CODE"] = str_replace('<input', '<input accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,text/plain, application/pdf, image/*"', $arQuestion["HTML_CODE"]);
	        ?>
            <label for="vac_ref" class="mf-field-file">
                <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 197.696 197.696" style="enable-background:new 0 0 197.696 197.696;" xml:space="preserve">
                        <path d="M179.546,73.358L73.111,179.783c-13.095,13.095-34.4,13.095-47.481,0.007
                            c-13.095-13.095-13.095-34.396,0-47.495l13.725-13.739l92.696-92.689l11.166-11.159c8.829-8.833,23.195-8.833,32.038,0
                            c8.829,8.836,8.829,23.209,0,32.041L145.79,76.221l-74.383,74.383l-1.714,1.714c-4.42,4.413-11.606,4.42-16.026,0
                            c-4.42-4.413-4.42-11.599,0-16.019l76.101-76.097c1.582-1.578,1.582-4.141,0-5.723c-1.585-1.582-4.134-1.582-5.723,0
                            l-76.097,76.101c-7.58,7.573-7.58,19.895,0,27.464c7.566,7.573,19.884,7.566,27.464,0l1.714-1.714l74.383-74.383l29.465-29.472
                            c11.989-11.989,12-31.494,0-43.487c-11.986-11.986-31.49-11.986-43.487,0l-11.152,11.159L33.64,112.84l-13.725,13.732
                            c-16.252,16.244-16.252,42.685,0,58.937c16.241,16.252,42.678,16.248,58.929,0L185.265,79.081c1.585-1.578,1.585-4.137,0-5.719
                            C183.68,71.777,181.131,71.777,179.546,73.358z"></path>
                </svg>
                <span>Прикрепить файл</span>
            </label>
            <div style="display: none"><?= $arQuestion["HTML_CODE"] ?></div>
        </div>
    </div>

	<? if ($arResult["isUseCaptcha"] == "Y") { ?>
        <div class="block">
            <label for=""><?= GetMessage("FORM_CAPTCHA_TABLE_TITLE") ?></label>

            <input type="hidden" name="captcha_sid"
                   value="<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"/>
            <img
                    src="/bitrix/tools/captcha.php?captcha_sid=<?= htmlspecialcharsbx($arResult["CAPTCHACode"]); ?>"
                    width="180" height="40"/>

            <?= GetMessage("FORM_CAPTCHA_FIELD_TITLE") ?><?= $arResult["REQUIRED_SIGN"]; ?>
            <input type="text" name="captcha_word" size="30" maxlength="50" value=""/>
        </div>
    <? } ?>

    <div class="block w1">
        <input <?= (intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : ""); ?>
                type="submit"
                name="web_form_submit"
                value="<?= htmlspecialcharsbx(strlen(trim($arResult["arForm"]["BUTTON"])) <= 0 ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]); ?>"
    </div>
    <?= $arResult["FORM_FOOTER"] ?>
    <?
} //endif (isFormNote)
?>
</div>
<script>
    BX.addCustomEvent('onAjaxSuccess', function() {
        $('body').find('.custom_sel').styler();
    });
</script>