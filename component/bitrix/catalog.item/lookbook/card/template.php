<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>

<li>
    <a href="<?= $item['DETAIL_PAGE_URL'] ?>" id="<?= $this->GetEditAreaId($item['ID']) ?>">
        <?if ($item["DETAIL_PICTURE"]["SRC"]){?>
            <div data-img class="lookbook-product-img" style="background-image: url(<?= $item["DETAIL_PICTURE"]["SRC"] ?>)"></div>
        <?}else{?>
            <div data-img class="lookbook-product-img" style="background-image: url(/local/templates/markformelle/components/bitrix/catalog.section/lookbook/images/no_photo.png)"></div>
        <?}?>
        <h4 class="lookbook-product-title"><? echo $item["NAME"] ?></h4>
        <p class="lookbook-product-price">
            <?
            if ($arParams['SHOW_OLD_PRICE'] === 'Y')
            {
                ?>
                <span class="discount" id="<?=$itemIds['PRICE_OLD']?>"
                    <?=($price['RATIO_PRICE'] >= $price['RATIO_BASE_PRICE'] ? 'style="display: none;"' : '')?>>
								<?=$price['PRINT_RATIO_BASE_PRICE']?>
							</span>&nbsp;
                <?
            }
            if (!empty($price)) {
                if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                    echo Loc::getMessage(
                        'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                        array(
                            '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                            '#VALUE#' => $measureRatio,
                            '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                        )
                    );
                } else {
                    echo $price['PRINT_RATIO_PRICE'];
                }
            }
            ?>
        </p>
    </a>
</li>