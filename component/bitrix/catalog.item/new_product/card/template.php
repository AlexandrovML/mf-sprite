<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $item
 * @var array $actualItem
 * @var array $minOffer
 * @var array $itemIds
 * @var array $price
 * @var array $measureRatio
 * @var bool $haveOffers
 * @var bool $showSubscribe
 * @var array $morePhoto
 * @var bool $showSlider
 * @var bool $itemHasDetailUrl
 * @var string $imgTitle
 * @var string $productTitle
 * @var string $buttonSizeClass
 * @var CatalogSectionComponent $component
 */
?>
<a href="<?= $item['DETAIL_PAGE_URL'] ?>" id="<?= $this->GetEditAreaId($item['ID']) ?>">
    <?if ($item["DETAIL_PICTURE"]["SRC"]){?>
        <span class="img-container">
            <img class="main-product-img" src="<?= $item["DETAIL_PICTURE"]["SRC"] ?>" alt="<?=$item["NAME"] ?>"/>
        </span>
        <?}else{?>
            <span class="img-container">
                <img class="main-product-img" src="/local/templates/markformelle/components/bitrix/catalog.section/lookbook/images/no_photo.png" alt="<?=$item["NAME"] ?>"/>
            </span>
        <?}?>
    <div class="h4" class="main-product-title">
        <? echo $item["NAME"] ?>
    </div>
    <p class="main-product-price">
        <?
        if (!empty($price)) {
            if ($arParams['PRODUCT_DISPLAY_MODE'] === 'N' && $haveOffers) {
                echo Loc::getMessage(
                    'CT_BCI_TPL_MESS_PRICE_SIMPLE_MODE',
                    array(
                        '#PRICE#' => $price['PRINT_RATIO_PRICE'],
                        '#VALUE#' => $measureRatio,
                        '#UNIT#' => $minOffer['ITEM_MEASURE']['TITLE']
                    )
                );
            } else {
                echo $price['PRINT_RATIO_PRICE'];
            }
        }
        ?>
    </p>
</a>