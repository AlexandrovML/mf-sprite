<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? foreach ($arResult["ITEMS"] as $arItem): ?>
    <?
    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>

    <? if ($arItem['PROPERTIES']['BTN_ON']['VALUE_XML_ID']
        && $arItem['PROPERTIES']['BTN_ON']['VALUE_XML_ID'] == "Y") { ?>
        <div class="narrow_banner" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <a href="<? if (strlen($arItem['PROPERTIES']['LINK']['VALUE']) > 0) { echo $arItem['PROPERTIES']['LINK']['VALUE'];}else {echo '#';} ?>" <?= !empty($arItem['PROPERTIES']['A_CLASS']['VALUE'])?'class="'.$arItem['PROPERTIES']['A_CLASS']['VALUE'].'"':'' ?>>
                <img class='lazy' data-src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<? echo $arItem["NAME"] ?>" style="width: 100%">
            </a>
        </div>
    <? } else { ?>
        <section class="upper-slide" id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
                 style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>) !important;">
            <div class="content_mini_banner">
                <div>
                    <h3><? echo $arItem["NAME"] ?></h3>
                </div>
                <? if (strlen($arItem["PREVIEW_TEXT"]) > 0) { ?>
                    <div>
                        <p><? echo $arItem["PREVIEW_TEXT"]; ?></p>
                    </div>
                <? } ?>
                <? if (strlen($arItem['PROPERTIES']['LINK']['VALUE']) > 0) { ?>
                    <div>
                        <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"
                           class="button-link black-tr-b"><?= GetMessage('BUY'); ?></a>
                    </div>
                <? } ?>
            </div>
        </section>
    <? } ?>

<? endforeach; ?>

