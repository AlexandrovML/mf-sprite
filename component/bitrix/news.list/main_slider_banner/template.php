<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//pr($arParams);
?>

<? if ($arParams['DISPLAY_SLIDER'] == 'Y') {
	$arItem = $arResult["ITEMS"][0];
    $this->AddEditAction($arResult["ITEMS"][0]['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arResult["ITEMS"][0]["IBLOCK_ID"], "ELEMENT_EDIT"));
    $this->AddDeleteAction($arResult["ITEMS"][0]['ID'], $arResult["ITEMS"][0]['DELETE_LINK'], CIBlock::GetArrayByID($arResult["ITEMS"][0]["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
    ?>
    <div class="main_banner" id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
        <a href="<? if (strlen($arItem['PROPERTIES']['LINK']['VALUE']) > 0) { echo $arItem['PROPERTIES']['LINK']['VALUE'];}else {echo '#';} ?>">
            <img srcset=" 600px" src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" alt="<? echo $arItem["NAME"] ?>" style="width: 100%">
        </a>
    </div>
<? } else { ?>
    <section class="lower-slide">
        <div class='slider_banner'>
            <? foreach ($arResult["ITEMS"] as $arItem):
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                if ($arItem['PROPERTIES']['IMG_VIDEO']['VALUE_XML_ID'] == "VIDEO") {
                    if ($arItem['PROPERTIES']['VIDEO']['VALUE']) { ?>
                        <video controls="controls" class="video_banner">
                            <source src="<?= CFile::GetPath($arItem['PROPERTIES']['VIDEO']['VALUE']) ?>"
                                    type='video/ogg; codecs="theora, vorbis"'>
                            <source src="<?= CFile::GetPath($arItem['PROPERTIES']['VIDEO']['VALUE']) ?>"
                                    type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
                            <source src="<?= CFile::GetPath($arItem['PROPERTIES']['VIDEO']['VALUE']) ?>"
                                    type='video/webm; codecs="vp8, vorbis"'>
                        </video>
                    <? } else { ?>
                        <div class='slide'
                             style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>) !important;"
                             id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        </div>
                    <? } ?>
                <? } else { ?>
                    <div class='slide'
                         style="background-image: url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>) !important;"
                         id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
                        <div class="content_slider_main">
                            <div>
                                <? if ($arItem['PROPERTIES']['ACTIVE_TITLE']['VALUE_XML_ID']
                                    && $arItem['PROPERTIES']['ACTIVE_TITLE']['VALUE_XML_ID'] == "Y") { ?>
                                    <div>
                                        <h3><? echo $arItem["NAME"] ?></h3>
                                    </div>
                                <? } ?>
                                <? if ($arItem['PROPERTIES']['ACTIVE_TEXT']['VALUE_XML_ID']
                                    && $arItem['PROPERTIES']['ACTIVE_TEXT']['VALUE_XML_ID'] == "Y") { ?>
                                    <div>
                                        <p><? echo $arItem["PREVIEW_TEXT"]; ?></p>
                                    </div>
                                <? } ?>
                                <? if ($arItem['PROPERTIES']['BTN_ON']['VALUE_XML_ID']
                                    && $arItem['PROPERTIES']['BTN_ON']['VALUE_XML_ID'] == "Y") { ?>
                                    <div>
                                        <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>"
                                           class="button-link black-tr-b  <? if ($arItem['PROPERTIES']['BTN_COLOR']['VALUE_XML_ID']) echo $arItem['PROPERTIES']['BTN_COLOR']['VALUE_XML_ID']; ?>">
                                            <?= $arItem['PROPERTIES']['BTN_TEXT']['VALUE'] ?>
                                        </a>
                                    </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                <? } ?>
            <? endforeach; ?>
        </div>
    </section>
<? } ?>
