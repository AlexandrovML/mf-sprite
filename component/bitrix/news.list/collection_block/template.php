<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$nom_block = $arParams['NUMBER_BLOCK'];
$i = 0;
?>
<section class="new-collections-products" data-preload-block>
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <? $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        if (/*$arItem['PROPERTIES']['MAIN_PAGE_ON']['VALUE_XML_ID']
            && $arItem['PROPERTIES']['MAIN_PAGE_ON']['VALUE_XML_ID'] == 'Y'
            && */$arItem['PROPERTIES']['BLOCK']['VALUE_XML_ID'] == $nom_block) {
            $i++; ?>
            <a href="<?= $arItem['PROPERTIES']['LINK']['VALUE'] ?>" class="collections-block lazy" data-img id="<?= $this->GetEditAreaId($arItem['ID']); ?>"
	 data-bg="url(<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>)">
<!--                style="background-image:url(--><?//= $arItem["PREVIEW_PICTURE"]["SRC"] ?><!--) !important;"-->
<div>
                <?if ($arItem['PROPERTIES']['ACTIVE_TITLE']['VALUE_XML_ID']
                    && $arItem['PROPERTIES']['ACTIVE_TITLE']['VALUE_XML_ID'] == "Y") {?>
                    <h3><? echo htmlspecialchars_decode($arItem['PROPERTIES']['TITLE']['VALUE']) ?></h3>
                <?}
                if ($arItem['PROPERTIES']['BTN_ON']['VALUE_XML_ID']
                    && $arItem['PROPERTIES']['BTN_ON']['VALUE_XML_ID'] == "Y") {?>
                    <div
                       class="button-link black-tr-b <?if ($arItem['PROPERTIES']['BTN_COLOR']['VALUE_XML_ID']) echo $arItem['PROPERTIES']['BTN_COLOR']['VALUE_XML_ID'];?>">
                        <?= $arItem['PROPERTIES']['BTN_TEXT']['VALUE'] ?>
                    </div>
                <?}?>
</div>
            </a>
        <?}
        if ($i == 2) {
            break;
        } ?>
    <? endforeach; ?>
</section>