<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

?>
<ul class="main-carousel-products-list owl-carousel">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <li class="item" id="<?= $this->GetEditAreaId($arItem['ID']) ?>">
            <a href="<?= $arItem['DETAIL_PAGE_URL'] ?>">
                <span class="img-container">
                    <img class="main-product-img" src="<?= $arItem["DETAIL_PICTURE"]["SRC"] ?>"/></span>
                <h4 class="main-product-title">
                    <? echo $arItem["NAME"] ?>
                </h4>
                <p class="main-product-price">
                    <?
                    $intIBlockID = CATALOG_ID;
                    $mxResult = CCatalogSKU::GetInfoByProductIBlock(
                        $intIBlockID
                    );
                    $min_price = 0;
                    $first = 0;
                    if (is_array($mxResult)) {
                        $rsOffers = CIBlockElement::GetList(array("PRICE" => "ASC"), array('IBLOCK_ID' => $mxResult['IBLOCK_ID'], 'PROPERTY_' . $mxResult['SKU_PROPERTY_ID'] => $arItem['ID']));
                        while ($arOffer = $rsOffers->GetNext()) {
                            $ar_price = GetCatalogProductPriceList($arOffer["ID"], 'PRICE', 'DESC');
                            if ($first != 1) {
                                $first = 1;
                                $min_price = $ar_price[0]["PRICE"];
                            }
                            if ($min_price > $ar_price[0]["PRICE"]) {
                                $min_price = $ar_price[0]["PRICE"];
                            }
                        }
                    }
                    echo "<p>" . $min_price . " руб. </p>";
                    ?>
                </p>
            </a>
        </li>
    <? endforeach; ?>
</ul>