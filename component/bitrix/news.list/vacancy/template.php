<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="vacancy-list">
    <div class='h3'>
        <span class="dotted"><?= GetMessage('LOOK_VACANCY'); ?></span> <?= GetMessage('LOOK_VACANCY_IN_COMPANY'); ?>
        <span class="mf-main-color"><?= GetMessage('COMPANY_NAME'); ?></span>
    </div>

    <div class="vacancy-list__filter">
        <div class="item">
            <select name="department" id="department" class="custom_sel">
                <option value="">Выберите отдел</option>
			    <? foreach ($arResult['DEPARTMENT'] as $value) { ?>
                    <option value="<?= $value ?>"><?= $value ?></option>
			    <? } ?>
            </select>
        </div>
        <div class="item">
            <select name="region" id="region" class="custom_sel">
                <option value="">Выберите регион</option>
			    <? foreach ($arResult['REGION'] as $value) { ?>
                    <option value="<?= $value ?>"><?= $value ?></option>
			    <? } ?>
            </select>
        </div>
        <div class="item">
            <select name="city" id="city" class="custom_sel">
                <option value="">Выберите город</option>
			    <? foreach ($arResult['CITY'] as $value) { ?>
                    <option value="<?= $value ?>" data-region="<?= $arResult['CITY_REGION'][$value] ?>"><?= $value ?></option>
			    <? } ?>
            </select>
        </div>
        <div class="item">
            <select name="type_employment" id="type_employment" class="custom_sel">
                <option value="">Выберите тип занятости</option>
			    <? foreach ($arResult['TYPE_EMPLOYMENT'] as $value) { ?>
                    <option value="<?= $value ?>"><?= $value ?></option>
			    <? } ?>
            </select>
        </div>
        <div class="item">
            <button type="button" id="show-vacancy-btn" class="vacancy-list__btn button-link black-tr-b">Найти вакансии</button>
        </div>
    </div>
    <ul>
        <? foreach ($arResult["ITEMS"] as $arItem):
//            if ($arItem['PROPERTIES']['VACANCY_ON']['VALUE_XML_ID'] && $arItem['PROPERTIES']['VACANCY_ON']['VALUE_XML_ID'] == 'Y') {
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
//                pr($arItem['PROPERTIES']['DESCRIPTIONS']);
                ?>
                <li id="<?= $this->GetEditAreaId($arItem['ID']); ?>" class="js-vacancy-item vacancy-item" data-department="<?= $arItem['DISPLAY_PROPERTIES']['DEPARTMENT']['DISPLAY_VALUE'] ?>" data-type_employment="<?= $arItem['DISPLAY_PROPERTIES']['TYPE_EMPLOYMENT']['DISPLAY_VALUE'] ?>" data-region="<?= $arItem['DISPLAY_PROPERTIES']['REGION']['DISPLAY_VALUE'] ?>" data-city="<?= $arItem['DISPLAY_PROPERTIES']['CITY']['DISPLAY_VALUE'] ?>">
                    <div class="mf-vacancy-upper-el">
                        <div class='h4'><? echo $arItem["NAME"] ?></div>
                        <p class="mf-salary mf-main-color">
                            <?if ($arItem['PROPERTIES']['MONEY']['VALUE']){?>
                                <?= $arItem['PROPERTIES']['MONEY']['VALUE']?>
                            <?}else{
                                echo GetMessage('NO_MONEY');
                            }?>
                        </p>
                    </div>
                    <p class="mf-vacancy-text">
                        <? if (strlen($arItem['PREVIEW_TEXT']) > 0) { ?>
                            <?= $arItem['PREVIEW_TEXT'] ?>
                        <? } else {?>
                            <?= $arItem['PROPERTIES']['DESCRIPTIONS']['~VALUE']['TEXT'] ?>
                        <? } ?>
                    </p>
                    <div class="popup_vacancy">
                        <a href="/ajax/vacancy.php?vac_id=<?= $arItem['ID'] ?>"
                           class="popup_vacancy_a"><?= GetMessage('READ'); ?></a>
                    </div>
                </li>
            <? //} ?>

        <? endforeach; ?>
    </ul>
</div>
<script>
    $(document).ready(function () {
        $('.popup_vacancy_a').magnificPopup({
            closeOnBgClick: false,
            type: 'ajax',
            mainClass: 'vacancy_content-popup popup_shop-body popup_input',
            callbacks: {
                open: function() {
                    setTimeout(function () {
                        $('.custom_sel').styler();
                        $('.custom_check').styler();
                        $('.custom_js').styler();
                    },300)
                },
                ajaxContentAdded: function () {
                    setTimeout(function () {
                        $('.custom_sel').styler();
                        $('.custom_check').styler();
                        $('.custom_js').styler();
                    },200)
                }
            }
        });
    })
</script>