$(document).ready(function() {
    var type_employment = '',
        region = '',
        city = '',
        department = '';

    function filterItems()
    {

        $('.js-vacancy-item').hide();

        $('.js-vacancy-item').each(function () {
            if(
                (department == '' || (department != '' && $(this).data('department') == department)) &&
                (type_employment == '' || (type_employment != '' && $(this).data('type_employment') == type_employment)) &&
                (region == '' || (region != '' && $(this).data('region') == region)) &&
                (city == '' || (city != '' && $(this).data('city') == city)))
            {
                $(this).show();
            }
        });
    }


    $(document).on('change', 'select#type_employment', function () {
        type_employment = $(this).val();
        // filterItems();
    });

    $(document).on('change', 'select#department', function () {
        department = $(this).val();
        // filterItems();
    });

    $(document).on('change', 'select#region', function () {
        // console.log($(this).val());
        region = $(this).val();

        if (region !== '')
        {
            $('select#city').find('option').attr('disabled', true);
            $('select#city').find('option').each(function () {
                if ($(this).attr('value') == '')
                {
                    $(this).attr('disabled', false);
                }
                else if ($(this).data('region') == region)
                {
                    $(this).attr('disabled', false);
                }
            });
        }
        else
        {
            $('select#city').find('option').attr('disabled', false);
        }

        $('select#city').val('').trigger('refresh');
        city = '';

        // filterItems();
    });

    $(document).on('change', 'select#city', function () {
        // console.log($(this).val());
        city = $(this).val();
        // filterItems();
    });

    $(document).on('click', '#show-vacancy-btn', function () {
        filterItems();
    });
});