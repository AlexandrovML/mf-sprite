<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//pr($arResult['PROPERTIES']);

if ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE']) {
    $temp = '<div class="slider_text"><div class="img_main">';
    $temp1 = '';

    foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $photo) {
        $temp1 .= '<div class="img" style="background-image: url(' . CFile::GetPath($photo) . ')">
        </div>';
    }

    $temp .= $temp1.'</div><div class="img_nav">'.$temp1.'</div></div>';
}?>

<h1><?=$arResult["NAME"]?></h1>

<div class="news-detail">
    <?= str_replace("#GALLERY#", $temp, $arResult["DETAIL_TEXT"]); ?>
    <? $temp = ''; ?>
</div>
