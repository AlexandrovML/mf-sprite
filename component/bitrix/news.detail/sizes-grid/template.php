<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

//pr($arResult['DISPLAY_PROPERTIES']['LEGEND']);
?>
<div class="popup_shop">
    <div class="name">РАЗМЕРНАЯ ТАБЛИЦА</div>
    <div class="help_flex">
        <div class="help">
            <div class="help_size">
                <ul>
                    <? foreach ($arResult['DISPLAY_PROPERTIES']['LEGEND']['DISPLAY_VALUE'] as $k=>$v) { ?>
                        <li><span><?= $k+1 ?></span><?= $v ?></li>
                    <? } ?>
                </ul>
            </div>
        </div>

        <div class="help_table_size">
            <div class="zag"><?= $arResult['DISPLAY_PROPERTIES']['TABLE_HEAD']['DISPLAY_VALUE'] ?></div>
	        <?echo $arResult["DETAIL_TEXT"];?>
        </div>

        <? if(!empty($arResult['DISPLAY_PROPERTIES']['TABLE_HEAD2']['DISPLAY_VALUE'])) { ?>
            <div class="help_table_size">
                <div class="zag"><?= $arResult['DISPLAY_PROPERTIES']['TABLE_HEAD2']['DISPLAY_VALUE'] ?></div>
		        <?echo $arResult['DISPLAY_PROPERTIES']['ADDITIONAL_GRID']['DISPLAY_VALUE'];?>
            </div>
        <? } ?>

        <div class="help_img">
            <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arResult["DETAIL_PICTURE"]["ALT"]?>">
        </div>
    </div>

    <button title="Close (Esc)" type="button" class="mfp-close">×</button>
</div>