<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
//pr($arResult['PROPERTIES']['DUTIES']['VALUE']);

$zp = '';
if (!empty($arResult['PROPERTIES']['MONEY']['VALUE']))
{
    $_ar = explode(' ', $arResult['PROPERTIES']['MONEY']['VALUE']);
    $curr = $_ar[count($_ar)-1];
    unset($_ar[count($_ar)-1]);
    $val = implode(' ', $_ar);
    $zp = '<span>'.$val.'</span>' . $curr;
}

$bShowListInfo = false;
if (!empty($arResult['PROPERTIES']['DEPARTMENT']['VALUE'])) $bShowListInfo = true;
if (!empty($arResult['PROPERTIES']['CITY']['VALUE'])) $bShowListInfo = true;
if (!empty($arResult['PROPERTIES']['SECTION']['VALUE'])) $bShowListInfo = true;
if (!empty($arResult['PROPERTIES']['TYPE_EMPLOYMENT']['VALUE'])) $bShowListInfo = true;
//if (!empty($arResult['PROPERTIES']['MONEY']['VALUE'])) $bShowListInfo = true;

?>
<?/*<div id="vacancy1" class="vacancy_text" data-vac="<?=$arResult["ID"]?>">*/?>
    <div class="name">
        <div class="zag"><?= $arResult['NAME'] ?></div>
        <? if(!empty($zp)) { ?>
            <div class="price red"><?= $zp ?></div>
        <? } ?>
    </div>

    <? if ($bShowListInfo) { ?>
        <div class="list_ifo">
            <? if (!empty($arResult['PROPERTIES']['DEPARTMENT']['VALUE'])) { ?>
            <ul>
                <li>Отдел:</li>
                <li class="red"><?= $arResult['PROPERTIES']['DEPARTMENT']['VALUE'] ?></li>
            </ul>
            <? } ?>

            <? if (!empty($arResult['PROPERTIES']['CITY']['VALUE'])) { ?>
            <ul>
                <li>Город:</li>
                <li class="red"><?= $arResult['PROPERTIES']['CITY']['VALUE'] ?></li>
            </ul>
            <? } ?>

	        <? if (!empty($arResult['PROPERTIES']['SECTION']['VALUE'])) { ?>
                <ul>
                    <li>Категория:</li>
                    <li class="red"><?= $arResult['PROPERTIES']['SECTION']['VALUE'] ?></li>
                </ul>
	        <? } ?>

	        <? if (!empty($arResult['PROPERTIES']['TYPE_EMPLOYMENT']['VALUE'])) { ?>
                <ul>
                    <li>Тип занятости:</li>
                    <li class="red"><?= $arResult['PROPERTIES']['TYPE_EMPLOYMENT']['VALUE'] ?></li>
                </ul>
	        <? } ?>

	        <? if (!empty($arResult['PROPERTIES']['MONEY']['VALUE'])) { ?>
                <ul>
                    <li>Заработная плата:</li>
                    <li class="red"><?= $arResult['PROPERTIES']['MONEY']['VALUE'] ?></li>
                </ul>
	        <? } ?>
        </div>
    <? } ?>

    <div class="text_info">
        <? if (!empty($arResult['PROPERTIES']['DUTIES']['VALUE']) || !empty($arResult['PROPERTIES']['REQUIREMENTS']['VALUE'])) { ?>
            <div class="width2">
                <div class="zag">Требования к кандидату</div>
		        <? if (!empty($arResult['PROPERTIES']['DUTIES']['VALUE'])) { ?>
                    <div class="block">
                        <div class="name">Основные обязанности:</div>
                        <ul>
					        <? foreach ($arResult['PROPERTIES']['DUTIES']['VALUE'] as $v) { ?>
                                <li><?= $v ?></li>
					        <? } ?>
                        </ul>
                    </div>
		        <? } ?>

		        <? if (!empty($arResult['PROPERTIES']['REQUIREMENTS']['VALUE'])) { ?>
                    <div class="block">
                        <div class="name">Требования:</div>
                        <ul>
					        <? foreach ($arResult['PROPERTIES']['REQUIREMENTS']['VALUE'] as $v) { ?>
                                <li><?= $v ?></li>
					        <? } ?>
                        </ul>
                    </div>
		        <? } ?>
            </div>
        <? } ?>

	    <? if (!empty($arResult['PROPERTIES']['CONDITIONS']['VALUE'])) { ?>
            <div class="width2">
                <div class="zag">Компания предлагает</div>
                <div class="block">
                    <div class="name">Условия:</div>
                    <ul>
	                    <? foreach ($arResult['PROPERTIES']['CONDITIONS']['VALUE'] as $v) { ?>
                            <li><?= $v ?></li>
	                    <? } ?>
                    </ul>
                </div>
            </div>
	    <? } ?>

	    <? if (empty($arResult['PROPERTIES']['DUTIES']['VALUE']) && empty($arResult['PROPERTIES']['REQUIREMENTS']['VALUE']) && empty($arResult['PROPERTIES']['CONDITIONS']['VALUE'])) { ?>
            <?= $arResult['DETAIL_TEXT'] ?>
	    <? } ?>
    </div>

    <?/*
    <div class="form_vacancy">
        <div class="bzag">Оставьте свой отклик на вакансию или <a href="#" class="red">скачать анкету</a></div>
        <form action="">

            <div class="block">
                <div class="block">
                    <label for="">Номер телефона <span class="red">*</span></label>
                    <input type="text">
                </div>
                <div class="block">
                    <label for="">Email<span class="red">*</span></label>
                    <input type="text">
                </div>
            </div>
            <div class="block_container">
                <div class="block wid2">
                    <label for="">Возраст</label>
                    <label for="">Пол</label>
                    <input type="text">
                    <select name="" id="" class="custom_sel">
                        <option value="">День</option>
                        <option value="">1</option>
                        <option value="">1</option>
                    </select>
                </div>
                <div class="block">
                    <label for="">Образование</label>
                    <input type="text">
                </div>
            </div>
            <div class="block">
                <div class="block">
                    <label for="">Расскажите о себе</label>
                    <input type="text">
                </div>
                <div class="block">
                    <label for="vac_ref" class="mf-field-file">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 197.696 197.696" style="enable-background:new 0 0 197.696 197.696;" xml:space="preserve">
                                                        <path d="M179.546,73.358L73.111,179.783c-13.095,13.095-34.4,13.095-47.481,0.007
															c-13.095-13.095-13.095-34.396,0-47.495l13.725-13.739l92.696-92.689l11.166-11.159c8.829-8.833,23.195-8.833,32.038,0
															c8.829,8.836,8.829,23.209,0,32.041L145.79,76.221l-74.383,74.383l-1.714,1.714c-4.42,4.413-11.606,4.42-16.026,0
															c-4.42-4.413-4.42-11.599,0-16.019l76.101-76.097c1.582-1.578,1.582-4.141,0-5.723c-1.585-1.582-4.134-1.582-5.723,0
															l-76.097,76.101c-7.58,7.573-7.58,19.895,0,27.464c7.566,7.573,19.884,7.566,27.464,0l1.714-1.714l74.383-74.383l29.465-29.472
															c11.989-11.989,12-31.494,0-43.487c-11.986-11.986-31.49-11.986-43.487,0l-11.152,11.159L33.64,112.84l-13.725,13.732
															c-16.252,16.244-16.252,42.685,0,58.937c16.241,16.252,42.678,16.248,58.929,0L185.265,79.081c1.585-1.578,1.585-4.137,0-5.719
															C183.68,71.777,181.131,71.777,179.546,73.358z"></path>
												</svg>
                        <span>Прикрепить файл</span>
                    </label>

                </div>
            </div>

            <div class="block w1">
                <input type="submit" placeholder="Войти" value="Отправить">
            </div>
        </form>
    </div>
    */?>

    <?/*
    <div class="zag">
        <?=$arResult["NAME"]?>
        <div class="bold"><?=$arResult['PROPERTIES']['MONEY']['VALUE']?></div>
    </div>
    <?= $arResult['PROPERTIES']['DESCRIPTIONS']['VALUE']['TEXT'] ?>
    <?if ($arResult['PROPERTIES']['DUTIES']['VALUE']){?>
        <div class="bold"><?=GetMessage('DUTIES');?></div>
        <ul>
            <?foreach ($arResult['PROPERTIES']['DUTIES']['VALUE'] as $item){?>
                <li><?=$item?></li>
            <?}?>
        </ul>
    <?}?>

    <a href="#" class="link_vacancy-done"><?=GetMessage('SEND_RESUME');?></a>
    */?>
<?/*</div>*/?>