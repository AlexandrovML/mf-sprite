<?
use Bitrix\Main,
	Bitrix\Main\Localization\Loc,
	Bitrix\Main\Page\Asset;
?>
<div class="us-ac-hb-order clearfix">
    <div class="us-ac-header">
        <div class="us-ac-order-num">
			<?=Loc::getMessage('SPOL_TPL_ORDER')?>
			<?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?>
        </div>

        <div class="us-ac-order-date">
			<?=Loc::getMessage('SPOL_TPL_FROM_DATE')?>
			<?=$order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT'])?>
        </div>
        <div class="us-ac-order-status us-ac-order-status--<?= $order['ORDER']['STATUS_ID'] ?>">
			<?=Loc::getMessage('SPOL_ORDER_STATUS')?>:
            <?if($order['ORDER']['CANCELED']=='Y'):?>
                <span>Отменен</span>
            <?else:?>
			    <span><?=$arResult['LIST_STATUS_NAMES'][$order['ORDER']['STATUS_ID']]?></span>
            <?endif;?>
        </div>

        <div class="us-ac-order-summ">
			<?=Loc::getMessage('SPOL_TPL_SUMOF')?>
			<?=$order['ORDER']['FORMATED_PRICE']?>
        </div>
        <div class="us-ac-order-icon">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129">
                <g>
                    <path d="m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z"></path>
                </g>
            </svg>
        </div>
    </div>
    <div class="us-ac-body">
        <div class="us-ac-list-items">
            <div class="mf-cart-list">
                <div class="mf-cart-body">
					<? foreach ($order['BASKET_ITEMS'] as $basketItem) { ?>
                        <div class="mf-cart-item">
                            <div class="mf-cart-i-img-cnt">
                                <a href="<?= $basketItem['DETAIL_PAGE_URL'] ?>"><div class="mf-cart-i-img" data-img="" style="background-image: url(<?= $arResult['IMAGES'][$basketItem['PRODUCT_ID']] ?>);"></div></a>
                            </div>
                            <div class="mf-cart-i-inf">
                                <a class="mf-cart-link-title" href="<?= $basketItem['DETAIL_PAGE_URL'] ?>"><?= $basketItem['NAME'] ?></a>
                                <ul>
									<? if(!empty($arResult['COLOR_OFFER_MAPS'][$basketItem['PRODUCT_ID']])) { ?>
                                        <li>
                                            Цвет
											<?
											if(!empty($arResult['COLOR_REFS'][$arResult['COLOR_OFFER_MAPS'][$basketItem['PRODUCT_ID']]]['UF_FILE']))
											{
												echo '<div style="background-image:url('.$arResult['COLOR_REFS'][$arResult['COLOR_OFFER_MAPS'][$basketItem['PRODUCT_ID']]]['UF_FILE'].')" title="'.$arResult['COLOR_REFS'][$arResult['COLOR_OFFER_MAPS'][$basketItem['PRODUCT_ID']]]['UF_NAME'].'" data-colormaterial></div>';
											}
                                            elseif(!empty($arResult['COLOR_REFS'][$arResult['COLOR_OFFER_MAPS'][$basketItem['PRODUCT_ID']]]['UF_COLOR_CODE']))
											{
												echo ' <div style="background-color:'.$arResult['COLOR_REFS'][$arResult['COLOR_OFFER_MAPS'][$basketItem['PRODUCT_ID']]]['UF_COLOR_CODE'].'" title="'.$arResult['COLOR_REFS'][$arResult['COLOR_OFFER_MAPS'][$basketItem['PRODUCT_ID']]]['UF_NAME'].'" data-colormaterial></div>';
											}
											?>
                                        </li>
									<? } ?>

									<? foreach ($basketItem['PROPS'] as $arProp) { ?>
                                        <li><? echo $arProp['NAME'] . ' ' . $arProp['VALUE'] ?></li>
									<? } ?>
                                </ul>
                            </div>
                            <div class="mf-cart-i-calc">
                                <span class="mf-cart-i-title">Количество</span>
                                <span class="mf-cart-i-counter">
                                    <input maxlength="2" name="mf-cart-counter" value="<?= $basketItem['QUANTITY'] ?>" type="text" readonly>
                                </span>
                            </div>
                            <div class="mf-cart-i-price">
                                <span class="mf-cart-i-title">Цена за шт.</span>
                                <span class="mf-cart-i-price">
                                    <?= $basketItem['FORMATED_PRICE'] ?>
                                </span>
								<? if($basketItem['PRICE'] != $basketItem['BASE_PRICE']) { ?>
                                    <span class="mf-cart-i-price-disc">
										<?= CCurrencyLang::CurrencyFormat($basketItem['BASE_PRICE'], $basketItem['CURRENCY']) ?>
	                                </span>
								<? } ?>
                            </div>
                            <div class="mf-cart-i-summ-price">
                                <span class="mf-cart-i-title">Сумма</span>
                                <span class="mf-cart-i-price">
                                                <?= $basketItem['FORMATED_SUM'] ?>
                                            </span>
                            </div>
							<?/*
                                        <div class="remove-cart-item">
                                            <svg class="remove-icon" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 64 64">
                                                <g>
                                                    <path d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                                                </g>
                                            </svg>
                                        </div>
                                        */?>
                        </div>
					<? } ?>
                </div>
            </div>
        </div>
        <div class="us-ac-body-footer">
            <a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>" class="button-link black-tr-b"><?=Loc::getMessage('SPOL_TPL_REPEAT_ORDER')?></a>
			<?/*
            <a href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"])?>" class="us-ac-remove-product"><?=Loc::getMessage('SPOL_TPL_CANCEL_ORDER')?>
				<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 59 59" style="enable-background:new 0 0 59 59;" xml:space="preserve">
												<path d="M29.5,51c0.552,0,1-0.447,1-1V17c0-0.553-0.448-1-1-1s-1,0.447-1,1v33C28.5,50.553,28.948,51,29.5,51z"/>
					<path d="M19.5,51c0.552,0,1-0.447,1-1V17c0-0.553-0.448-1-1-1s-1,0.447-1,1v33C18.5,50.553,18.948,51,19.5,51z"/>
					<path d="M39.5,51c0.552,0,1-0.447,1-1V17c0-0.553-0.448-1-1-1s-1,0.447-1,1v33C38.5,50.553,38.948,51,39.5,51z"/>
					<path d="M52.5,6H38.456c-0.11-1.25-0.495-3.358-1.813-4.711C35.809,0.434,34.751,0,33.499,0H23.5c-1.252,0-2.31,0.434-3.144,1.289
													C19.038,2.642,18.653,4.75,18.543,6H6.5c-0.552,0-1,0.447-1,1s0.448,1,1,1h2.041l1.915,46.021C10.493,55.743,11.565,59,15.364,59
													h28.272c3.799,0,4.871-3.257,4.907-4.958L50.459,8H52.5c0.552,0,1-0.447,1-1S53.052,6,52.5,6z M21.792,2.681
													C22.24,2.223,22.799,2,23.5,2h9.999c0.701,0,1.26,0.223,1.708,0.681c0.805,0.823,1.128,2.271,1.24,3.319H20.553
													C20.665,4.952,20.988,3.504,21.792,2.681z M46.544,53.979C46.538,54.288,46.4,57,43.636,57H15.364
													c-2.734,0-2.898-2.717-2.909-3.042L10.542,8h37.915L46.544,53.979z"/>
											</svg>
			</a>*/?>

			<?
			foreach ($order['PAYMENT'] as $payment)
			{
				if ($order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y')
				{
					$paymentChangeData[$payment['ACCOUNT_NUMBER']] = array(
						"order" => htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']),
						"payment" => htmlspecialcharsbx($payment['ACCOUNT_NUMBER']),
						"allow_inner" => $arParams['ALLOW_INNER'],
						"refresh_prices" => $arParams['REFRESH_PRICES'],
						"path_to_payment" => $arParams['PATH_TO_PAYMENT'],
						"only_inner_full" => $arParams['ONLY_INNER_FULL']
					);
				}
				?>
                <div class="sale-order-list-inner-row">
                    <div class="sale-order-list-inner-row-body">
                        <div class="sale-order-list-payment">
                            <div class="sale-order-list-payment-title">
								<?
								$paymentSubTitle = Loc::getMessage('SPOL_TPL_BILL')." ".Loc::getMessage('SPOL_TPL_NUMBER_SIGN').htmlspecialcharsbx($payment['ACCOUNT_NUMBER']);
								if(isset($payment['DATE_BILL']))
								{
									$paymentSubTitle .= " ".Loc::getMessage('SPOL_TPL_FROM_DATE')." ".$payment['DATE_BILL']->format($arParams['ACTIVE_DATE_FORMAT']);
								}
								$paymentSubTitle .=",";
								//echo $paymentSubTitle;
								?>
                                <span class="sale-order-list-payment-title-element"><?=$payment['PAY_SYSTEM_NAME']?></span>
								<?
								if ($payment['PAID'] === 'Y')
								{
									?>
                                    <span class="sale-order-list-status-success"><?=Loc::getMessage('SPOL_TPL_PAID')?></span>
									<?
								}
                                elseif ($order['ORDER']['IS_ALLOW_PAY'] == 'N')
								{
									?>
                                    <span class="sale-order-list-status-restricted"><?=Loc::getMessage('SPOL_TPL_RESTRICTED_PAID')?></span>
									<?
								}
								else
								{
									?>
                                    <span class="sale-order-list-status-alert"><?=Loc::getMessage('SPOL_TPL_NOTPAID')?></span>
									<?
								}
								?>
                            </div>
							<?/*
                            <div class="sale-order-list-payment-price">
                                <span class="sale-order-list-payment-element"><?=Loc::getMessage('SPOL_TPL_SUM_TO_PAID')?>:</span>

                                <span class="sale-order-list-payment-number"><?=$payment['FORMATED_SUM']?></span>
                            </div>
                            */?>
							<?
							if (!empty($payment['CHECK_DATA']))
							{
								$listCheckLinks = "";
								foreach ($payment['CHECK_DATA'] as $checkInfo)
								{
									$title = Loc::getMessage('SPOL_CHECK_NUM', array('#CHECK_NUMBER#' => $checkInfo['ID']))." - ". htmlspecialcharsbx($checkInfo['TYPE_NAME']);
									if (strlen($checkInfo['LINK']))
									{
										$link = $checkInfo['LINK'];
										$listCheckLinks .= "<div><a href='$link' target='_blank'>$title</a></div>";
									}
								}
								if (strlen($listCheckLinks) > 0)
								{
									?>
                                    <div class="sale-order-list-payment-check">
                                        <div class="sale-order-list-payment-check-left"><?= Loc::getMessage('SPOL_CHECK_TITLE')?>:</div>
                                        <div class="sale-order-list-payment-check-left">
											<?=$listCheckLinks?>
                                        </div>
                                    </div>
									<?
								}
							}

							/*if ($payment['PAID'] !== 'Y' && $order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y')
							{
								?>
								<a href="#" class="sale-order-list-change-payment" id="<?= htmlspecialcharsbx($payment['ACCOUNT_NUMBER']) ?>">
									<?= Loc::getMessage('SPOL_TPL_CHANGE_PAY_TYPE') ?>
								</a>
								<?
							}*/
							if ($order['ORDER']['IS_ALLOW_PAY'] == 'N' && $payment['PAID'] !== 'Y')
							{
								?>
                                <div class="sale-order-list-status-restricted-message-block">
                                    <span class="sale-order-list-status-restricted-message"><?=Loc::getMessage('SOPL_TPL_RESTRICTED_PAID_MESSAGE')?></span>
                                </div>
								<?
							}
							?>

                        </div>
						<?
						// метод оплаты не = Оплата через ЕРИП
						if ($payment['PAID'] === 'N' && $payment['IS_CASH'] !== 'Y' && $payment['PAY_SYSTEM_ID'] != 14)
						{
							if ($order['ORDER']['IS_ALLOW_PAY'] == 'N')
							{
								?>
                                <div class="sale-order-list-button-container">
                                    <a class="sale-order-list-button inactive-button button-link black-tr-b">
										<?=Loc::getMessage('SPOL_TPL_PAY')?>
                                    </a>
                                </div>
								<?
							}
                            elseif ($payment['NEW_WINDOW'] === 'Y')
							{
								?>
                                <div class="sale-order-list-button-container">
                                    <a class="button-link black-tr-b" target="_blank" href="<?=htmlspecialcharsbx($payment['PSA_ACTION_FILE'])?>">
										<?=Loc::getMessage('SPOL_TPL_PAY')?>
                                    </a>
                                </div>
								<?
							}
							else
							{
								?>
                                <div class="sale-order-list-button-container">
                                    <a class="sale-order-list-button ajax_reload button-link black-tr-b" href="<?=htmlspecialcharsbx($payment['PSA_ACTION_FILE'])?>">
										<?=Loc::getMessage('SPOL_TPL_PAY')?>
                                    </a>
                                </div>
								<?
							}
						}
						?>
                    </div>
                </div>
				<?
			}
			?>
        </div>
    </div>
</div>