<?php
use Bitrix\Sale;

$arResult['LIST_STATUS_NAMES'] = Sale\OrderStatus::getAllStatusesNames(LANGUAGE_ID);
//pr($listStatusNames);

$arProductIds = $arOfferIds = $arColorNames = [];
foreach ($arResult['ORDERS'] as $key => &$arOrder)
{
//	pr($order);
	$order = Sale\Order::load($arOrder['ORDER']['ID']);
	$basketN = $order->getBasket();

	$basketItemsList = $basketN->getBasketItems();

//	pr($arOrder['BASKET_ITEMS']);
	foreach ($basketItemsList as $basketItem)
	{
		$basketValues = $basketItem->getFieldValues();
//		pr($basketItem->getField('IBLOCK_ID'));
//		pr($basketValues);

		$ProductInfo = CCatalogSku::GetProductInfo($basketValues['PRODUCT_ID']);
		if(!empty($ProductInfo['ID']))
		{
			$arProductIds[$basketValues['PRODUCT_ID']] = $ProductInfo['ID'];
		}

		$arOfferIds[$basketValues['PRODUCT_ID']] = $basketValues['PRODUCT_ID'];

		$basketPropertyCollection = $basketItem->getPropertyCollection();

		$arOrder['BASKET_ITEMS'][$basketValues['ID']]['PROPS'] = array();

		/**  @var Sale\BasketPropertyItem $basketProperty*/
		foreach ($basketPropertyCollection as $basketProperty)
		{
			$basketPropertyList = $basketProperty->getFieldValues();
			if ($basketPropertyList['CODE'] !== "CATALOG.XML_ID"&&
				$basketPropertyList['CODE'] !== "PRODUCT.XML_ID"&&
				$basketPropertyList['CODE'] !== "SUM_OF_CHARGE"
			)
			{
				$arOrder['BASKET_ITEMS'][$basketValues['ID']]['PROPS'][] = $basketPropertyList;
			}
		}

		$arOrder['BASKET_ITEMS'][$basketValues['ID']]['FORMATED_PRICE'] = SaleFormatCurrency($basketValues["PRICE"], $basketValues["CURRENCY"]);
		$arOrder['BASKET_ITEMS'][$basketValues['ID']]['FORMATED_SUM'] = SaleFormatCurrency($basketValues["PRICE"] * $basketValues['QUANTITY'], $basketValues["CURRENCY"]);
//		pr($basketValues);
//		pr($arOrder['BASKET_ITEMS']);

//		$basket[$basketValues['ID']] = $basketValues;
	}

}
unset($arOrder);
//CModule::IncludeModule("iblock");
//pr($arProductIds);

$TABLE_NAME_COLOR_REF = '';
if(!empty($arProductIds)){
    $arResult['IMAGES'] = $arColorCodes = $arResult['COLOR_OFFER_MAPS'] = [];
    $arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE");
    $arFilter = Array("ID"=>$arProductIds);
    $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $arProps = $ob->GetProperties();
    //    pr($arProps);
        foreach ($arProductIds as $offerId=>$productId)
        {
            if($productId == $arFields['ID'])
            {
                $arResult['IMAGES'][$offerId] = CFile::getPath($arFields['PREVIEW_PICTURE']);
                $arResult['COLOR_OFFER_MAPS'][$offerId] = $arProps['COLOR_REF']['VALUE'];
            }
        }

        $arColorCodes[$arProps['COLOR_REF']['VALUE']] = $arProps['COLOR_REF']['VALUE'];
        if(empty($TABLE_NAME_DECOR_REF))
        {
            $TABLE_NAME_COLOR_REF = $arProps['COLOR_REF']['USER_TYPE_SETTINGS']['TABLE_NAME'];
        }
    }
}


if(!empty($TABLE_NAME_COLOR_REF))
{
	$arFilter = ['UF_XML_ID'=>$arColorCodes];
	$arResult['COLOR_REFS'] = getDataFromReference($TABLE_NAME_COLOR_REF, ['*'], 'UF_XML_ID', $arFilter);
}
else
{
	$arResult['COLOR_REFS'] = [];
}
//pr($arResult['COLOR_REFS']);
//pr($arResult['COLOR_OFFER_MAPS']);

//$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_PICTURE");
//$arFilter = Array("ID"=>$arOfferIds);
//$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
//while($ob = $res->GetNextElement())
//{
//	$arFields = $ob->GetFields();
//	$arProps = $ob->GetProperties();
//
//}

//pr($arResult['IMAGES']);