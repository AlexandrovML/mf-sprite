<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/**
 * Bitrix vars
 *
 * @var CBitrixComponent         $component
 * @var CBitrixComponentTemplate $this
 * @var array                    $arParams
 * @var array                    $arResult
 * @var array                    $arLangMessages
 * @var array                    $templateData
 *
 * @var string                   $templateFile
 * @var string                   $templateFolder
 * @var string                   $parentTemplateFolder
 * @var string                   $templateName
 * @var string                   $componentPath
 *
 * @var CDatabase                $DB
 * @var CUser                    $USER
 * @var CMain                    $APPLICATION
 */
?>
<? if($arParams['DISPLAY_TOP_PAGER'] && strlen($arResult['NAV_STRING'])): ?>
	<div class="api-pagination"><?=$arResult['NAV_STRING']?></div>
<? endif; ?>
<? /*if($arResult['ITEMS']): ?>
	<div class="api-count-result"><?=$arResult['COUNT_RESULT']?></div>
<? endif;*/ ?>
<? if($arResult['ITEMS']): ?><div class="search_page-catalog"><? endif; ?>
<div class="container api-list">
	<? if($arResult['ITEMS']): ?>
		<? foreach($arResult['ITEMS'] as $i => $arItem): ?>
			<div class="api-item search_page-card">
                <? //pr($arItem) ?>
				<? if($arParams['PICTURE']): ?>
                    <div class="img">
                        <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                            <div class="mf-cart-i-img" data-img="" style="background-image: url('<?= $arItem['PICTURE']?$arItem['PICTURE']['SRC']:$arResult['DEFAULT_PICTURE']['SRC'] ?>');"></div>
                        </a>
                    </div>
				<? endif ?>

                <div class="text">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>" class="zag"><?=$arItem['FAKE_NAME']?></a>
	                <? if($arItem['DESCRIPTION']) { ?>
                        <div class="text_block"><?=$arItem['DESCRIPTION']?></div>
	                <? } elseif($arItem['DETAIL_TEXT']) { ?>
                        <div class="text_block"><?=$arItem['DETAIL_TEXT']?></div>
	                <? } ?>
                </div>

				<? if($arItem['PROPERTY']): ?>
                    <div class="prop">
                        <ul>
						<? foreach($arItem['PROPERTY'] as $arProp): ?>
							<? if($arProp['FAKE_VALUE']): ?>
                                <li>

                                    <? if($arProp['NAME']): ?>
                                        <span><?= $arProp['NAME']!='Артикул' ? $arProp['NAME'] : 'Модель' ?></span>&nbsp;
                                    <? endif ?>
									<? if($arProp['FAKE_VALUE']): ?>
                                        <span><?=$arProp['FAKE_VALUE']?></span>
									<? endif ?>

                                </li>
							<? endif ?>
						<? endforeach; ?>
                        </ul>
                    </div>
				<? endif ?>

                <div class="price">
                <? if($minPrice = $arItem['MIN_PRICE']): ?>

                        <div class="block">
	                        <?
	                        echo GetMessage(
		                        ($arParams['PRICE_EXT'] ? 'API_SEARCH_PAGE_PRICE_EXT_MODE' : 'API_SEARCH_PAGE_PRICE_SIMPLE_MODE'),
		                        array(
			                        '#PRICE#'   => $minPrice['PRINT_DISCOUNT_VALUE'] . $arParams['CURRENCY_SYMBOL'],
			                        '#MEASURE#' => GetMessage(
				                        ($arParams['PRICE_EXT'] ? 'API_SEARCH_PAGE_MEASURE_EXT_MODE' : 'API_SEARCH_PAGE_MEASURE_SIMPLE_MODE'),
				                        array(
					                        '#VALUE#' => $minPrice['CATALOG_MEASURE_RATIO'],
					                        '#UNIT#'  => $minPrice['CATALOG_MEASURE_NAME'],
				                        )
			                        ),
		                        )
	                        );
	                        ?>
                        </div>



                    <? elseif($arItem['PRICES']): ?>
                        <? foreach($arItem['PRICES'] as $code => $arPrice): ?>
                            <? if($arPrice['CAN_ACCESS']): ?>
                                <div class="block"><? //=$arResult['PRICES'][$code]['TITLE'];?>
                                    <? if($arPrice['DISCOUNT_VALUE'] < $arPrice['VALUE']): ?>
                                        <span class="api-item-price"><?=$arPrice['PRINT_DISCOUNT_VALUE'] . $arParams['CURRENCY_SYMBOL']?></span>
                                        <span class="api-item-discount"><?=$arPrice['PRINT_VALUE'] . $arParams['CURRENCY_SYMBOL']?></span>
                                    <? else: ?>
                                        <span class="api-item-price"><?=$arPrice['PRINT_VALUE'] . $arParams['CURRENCY_SYMBOL']?></span>
                                    <? endif; ?>
                                </div>
                            <? endif; ?>
                        <? endforeach; ?>
                    <? endif; ?>
                </div>
                <?/*
                <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                    <? if($arItem['BRAND'] || $arItem['MIN_PRICE'] || $arItem['PRICES'] || $arParams['MORE_BUTTON_TEXT']): ?>
                        <div class="api-item-block-right">
                            <? if($arBrand = $arItem['BRAND']): ?>
                                <? if($arBrand['PICTURE']): ?>
                                    <div class="api-item-brand">
                                        <span class="api-item-brand-img">
                                            <? if($arBrand['DETAIL_PAGE_URL']): ?>
                                                <a href="<?=$arBrand['DETAIL_PAGE_URL']?>"><img src="<?=$arBrand['PICTURE']['SRC']?>" alt="<?=$arBrand['NAME']?>" title="<?=$arBrand['NAME']?>"></a>
                                            <? else: ?>
                                                <img src="<?=$arBrand['PICTURE']['SRC']?>" alt="<?=$arBrand['NAME']?>" title="<?=$arBrand['NAME']?>">
                                            <? endif ?>
                                        </span>
                                    </div>
                                <? endif ?>
                            <? endif ?>

                            <? if($arParams['MORE_BUTTON_TEXT']): ?>
                                <div class="api-item-more-button">
                                    <!--noindex-->
                                    <a rel="nofollow" href="<?=$arItem['DETAIL_PAGE_URL']?>" class="<?=$arParams['MORE_BUTTON_CLASS']?>"><?=$arParams['MORE_BUTTON_TEXT']?></a>
                                    <!--/noindex-->
                                </div>
                            <? endif; ?>
                        </div>
                    <? endif ?>
                    <div class="api-item-info" style="<? if($arResult['MARGIN_LEFT']): ?>margin-left:<?=($arResult['MARGIN_LEFT'])?>px;<? endif ?>">
                        <div class="api-item-name">
                            <?=$arItem['FAKE_NAME']?>
                        </div>
                        <? if($arSection = $arItem['SECTION']): ?>
                            <div class="api-item-section">
                                <a href="<?=$arSection['SECTION_PAGE_URL']?>"><?=$arSection['NAME']?></a>
                            </div>
                        <? endif ?>
                    </div>
                </a>
                <?/**/?>
			</div>
		<? endforeach; ?>
	<? elseif(is_set($_REQUEST['q']) || $arResult['isAjax']): ?>
		<li class="api-not-found">
            <div class="search_page-empty">
                <div class="img"></div>
                <div class="text"><span class="red">Сожалеем</span>, но по вашему запросу ничего не найдено</div>
            </div>
        </li>
	<? endif; ?>
</div>
	<? if($arResult['ITEMS']): ?></div><? endif; ?>
<? if($arParams['DISPLAY_BOTTOM_PAGER'] && strlen($arResult['NAV_STRING'])): ?>
	<div class="api-pagination"><?=$arResult['NAV_STRING']?></div>
<? endif; ?>
