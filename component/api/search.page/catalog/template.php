<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
/**
 * Bitrix vars
 *
 * @var CBitrixComponent         $component
 * @var CBitrixComponentTemplate $this
 * @var array                    $arParams
 * @var array                    $arResult
 * @var array                    $arLangMessages
 * @var array                    $templateData
 *
 * @var string                   $templateFile
 * @var string                   $templateFolder
 * @var string                   $parentTemplateFolder
 * @var string                   $templateName
 * @var string                   $componentPath
 *
 * @var CDatabase                $DB
 * @var CUser                    $USER
 * @var CMain                    $APPLICATION
 */

if(method_exists($this, 'setFrameMode'))
	$this->setFrameMode(true);
?>
<div class="container_search api-search-page tpl-default" id="<?=$arResult['COMPONENT_ID']?>">
	<div class="theme1-<?=$arParams['THEME'];?>">
        <div class="search_page-input">
		<form action="<?=POST_FORM_ACTION_URI?>" autocomplete="off">
			<div class="api-search-fields">
				<div class="api-query">
					<input class="input_text api-search-input"
					       placeholder="<?=$arParams['INPUT_PLACEHOLDER']?>"
					       name="q"
					       maxlength="300"
					       value="<?=htmlspecialcharsEx($arResult['q'])?>"
					       type="text">
					<span class="api-ajax-icon"></span>
					<span class="api-clear-icon"></span>
                    <button type="submit" class="input_sbmt"> </button>
                    <div class="text_search">
                        <span>Искать по:</span>
                        <select name="search_area" id="" class="custom_sel search-area-control">
                            <option value="1"<?= $arResult['SEARCH_AREA']=='1'?' selected':'' ?>>Сайту</option>
                            <option value="2"<?= $arResult['SEARCH_AREA']=='2'?' selected':'' ?>>Каталогу</option>
                            <option value="3"<?= $arResult['SEARCH_AREA']=='3'?' selected':'' ?>>Новостям</option>
                        </select>
                    </div>
				</div>
			</div>
		</form>
        </div>
		<div class="api-search-result"><?include 'ajax.php'; ?></div>
		<div class="api-preload"></div>
	</div>
</div>
<script>
	jQuery(function ($) {
		$.fn.apiSearchPage({
			container_id: '#<?=$arResult['COMPONENT_ID']?>',
			input_id: '.api-search-input',
			result_id: '.api-search-result',
			ajax_icon_id: '', //.api-ajax-icon
			clear_icon_id: '.api-clear-icon',
			ajax_preload_id: '.api-preload',
			pagination_id: '.api-pagination',
			search_area_id: 'select.search-area-control',
			wait_time: <?=intval($arParams['JQUERY_WAIT_TIME'])?>,
			mess: {}
		});
	});
</script>