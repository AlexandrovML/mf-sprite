<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
/** @var CBitrixComponentTemplate $this */
/** @var array $arParams */
/** @var array $arResult */
$arResult['MARGIN_LEFT'] = ($arParams['PICTURE'] && $arParams['RESIZE_PICTURE'] && $arParams['PICTURE_WIDTH'] ? $arParams['PICTURE_WIDTH'] + 15 : 0);
$arResult['DEFAULT_PICTURE'] = array();

if($arParams['PICTURE'])
{
	$arEmptyPreview  = array();
	$strEmptyPreviewSRC = $this->GetFolder() . '/images/no_photo.png';
	$strEmptyPreview = $_SERVER['DOCUMENT_ROOT'] . $strEmptyPreviewSRC;

	if(file_exists($strEmptyPreview))
	{
		if($arParams['RESIZE_PICTURE'])
		{
			$arImageSize = array(
				'width' => $arParams['PICTURE_WIDTH'],
				'height' => $arParams['PICTURE_HEIGHT']
			);

			$strDestinationFileSRC = '/upload/resize_cache/api_search/'.$arResult['COMPONENT_ID'].'_'.$arImageSize['width'].'_'.$arImageSize['height'].'_no_photo.png';
			$strDestinationFile = $_SERVER['DOCUMENT_ROOT'] . $strDestinationFileSRC;
			if(file_exists($strDestinationFile))
			{
				$arEmptyPreview = array(
					'SRC'    => $strDestinationFileSRC,
					'WIDTH'  => intval($arImageSize['width']),
					'HEIGHT' => intval($arImageSize['height']),
				);
			}
			else
			{
				if(CFile::ResizeImageFile($strEmptyPreview,$strDestinationFile,$arImageSize))
				{
					$arEmptyPreview = array(
						'SRC'    => $strDestinationFileSRC,
						'WIDTH'  => intval($arImageSize['width']),
						'HEIGHT' => intval($arImageSize['height']),
					);
				}
			}
		}

		if(empty($arEmptyPreview))
		{
			if($arImageSize = CFile::GetImageSize($strEmptyPreview))
			{
				$arEmptyPreview = array(
					'SRC'    => $strEmptyPreviewSRC,
					'WIDTH'  => intval($arImageSize[0]),
					'HEIGHT' => intval($arImageSize[1]),
					//'SIZE'   => $arImageSize[2],
				);
			}
		}
		
		if($arEmptyPreview['SRC'])
			$arEmptyPreview['SRC'] = CUtil::GetAdditionalFileURL($arEmptyPreview['SRC']);
	}

	$arResult['DEFAULT_PICTURE'] = $arEmptyPreview;
}

$arResult['SEARCH_AREA'] = !empty($_REQUEST['search_area'])?htmlspecialcharsbx($_REQUEST['search_area']):'2';

/*
$arItemIds = [];


if(!empty($arResult['CATEGORIES']))
{

	foreach($arResult['CATEGORIES'] as $category_id => &$arCategory)
	{
		if($arCategory['ITEMS']) {
			foreach($arCategory['ITEMS'] as $i => &$arItem)
			{
				$arItemIds[] = $arItem['ID'];

				if(!empty($arItem['PREVIEW_PICTURE']))
				{
					$arFile = CFile::ResizeImageGet($arItem["PREVIEW_PICTURE"], Array("width"=>$arParams['PICTURE_WIDTH'], "height"=>$arParams['PICTURE_HEIGHT']), BX_RESIZE_IMAGE_EXACT, true);
//					pr($arParams['PICTURE_WIDTH']);
//					pr($arParams['PICTURE_HEIGHT']);
//					pr($arFile);
					$arItem['PICTURE'] = [
						'SRC' => $arFile['src'],
						'WIDTH' => $arFile['width'],
						'HEIGHT' => $arFile['height']
					];
				}

//				pr($arItem['PICTURE']);
			}
			unset($arItem);
		}
	}
	unset($arCategory);

	if(!empty($arItemIds))
	{
		$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PREVIEW_TEXT",  "DATE_ACTIVE_FROM");
		$arFilter = Array("ID"=>$arItemIds);
		$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while($ob = $res->GetNextElement())
		{
			$arFields = $ob->GetFields();
			$arResult['ITEMS_DATE_ACTIVE'][$arFields['ID']] = CIBlockFormatProperties::DateFormat('j F Y', MakeTimeStamp($arFields['DATE_ACTIVE_FROM'], CSite::GetDateFormat()));
			$arResult['ITEMS_PREVIEW_TEXT'][$arFields['ID']] = $arFields['PREVIEW_TEXT'];
		}
	}
}

pr($arResult['ITEMS']);*/